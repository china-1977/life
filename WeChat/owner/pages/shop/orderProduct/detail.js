Page({
  mixins: [require('../../../mixin/common')],
  data: {
  },
  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderProducts/${options.id}`,
        header: { authorization },
      }).then((data) => {
        this.setData({
          ...options,
          orderProduct: data
        });
      });
    })
  },

  clipBoard(e) {
    wx.setClipboardData({
      data: this.data.orderProduct.outTradeNo
    })
  },

  makePhoneCall(e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone
    })
  },


  openLocation(e) {
    const x = e.currentTarget.dataset.x;
    const y = e.currentTarget.dataset.y;
    const name = e.currentTarget.dataset.name;
    wx.openLocation({
      latitude: parseFloat(y),
      longitude: parseFloat(x),
      name: name,
    })
  },
})
