/**
 * 商户列表
 */
import {merchantStatusEnum} from '@/constants';
import services from '@/services/api';
import {replacer} from '@/utils/format';
import {history, useModel, useRequest} from '@@/exports';
import {ProTable} from '@ant-design/pro-components';
import {Avatar, Button, Dropdown, Image, Typography} from 'antd';

const {getMerchants, bind} = services.MerchantController;

const MerchantList = () => {
    const { refresh } = useModel('@@initialState');
    let {loading: bindLoading, run: bindRun} = useRequest(bind, {
        manual: true,
    });

    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '主键', dataIndex: 'id', copyable: true},
        {
            title: 'LOGO',
            dataIndex: 'trademark',
            align: 'center',
            render: (text: string, record: any) => <Avatar shape={'square'} src={<Image src={record.trademark}/>}/>,
        },
        {title: '简称', dataIndex: 'shortname', copyable: true},
        {title: '描述', dataIndex: 'description', copyable: true},
        {
            title: '图片', align: 'center', dataIndex: 'pictures', copyable: true,
            render: (text: object, record: { pictures: [string] }) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={record?.pictures?.map((value, index) => (
                                <Image src={value}/>
                            ))}
                        />
                    }/>
            )
        },
        {title: '微信商户ID', dataIndex: 'subMchId', copyable: true},
        {title: '地址名称', dataIndex: 'addressName', copyable: true},
        {title: '地址详情', dataIndex: 'addressDetail', copyable: true},
        {
            title: '营业时间',
            copyable: true,
            align: 'center',
            render: (text: string, record: any) => <Typography.Text
                copyable={{text: [record.openTime, record.closeTime].toString()}}>{[record.openTime, '~', record.closeTime]}</Typography.Text>,
        },
        {
            title: '客服信息',
            copyable: true,
            align: 'center',
            render: (text: string, record: any) => <Typography.Text
                copyable={{text: [record.username, record.phone].toString()}}>{[record.username, ':', record.phone]}</Typography.Text>,
        },
        {
            title: '是否经营',
            dataIndex: 'status',
            align: 'center',
            fieldProps: {mode: 'multiple'},
            valueEnum: merchantStatusEnum,
        },
        {
            title: '操作',
            valueType: 'option',
            render: (text: any, record: any) => (
                <Dropdown.Button
                    key={record.id}
                    loading={bindLoading}
                    menu={{
                        items: [
                            {
                                key: 'AUTHORIZE',
                                label: <Button type="primary">授权</Button>,
                                onClick: () => {
                                    bindRun(record.id)
                                        .then((res: any) => {
                                            localStorage.setItem('name', res.info.name);
                                            localStorage.setItem('shortname', res.info.shortname);
                                            localStorage.setItem('lastTime', res.info.lastTime);
                                            localStorage.setItem('authorization', res.authorization);
                                            refresh();
                                        })
                                        .then(() => {
                                            history.push('/');
                                        });
                                },
                            },
                        ],
                    }}
                >
                    <Typography.Text
                        copyable={{
                            text: JSON.stringify(record, ['id', 'shortname', 'openTime', 'closeTime', 'username', 'phone', 'status']),
                        }}
                    >
                        操作
                    </Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                params={{categories:['STORE']}}
                pagination={false} rowKey="id" headerTitle="查询表格" columns={columns}
                request={(params: any) => getMerchants(params)} search={false}/>
        </div>
    );
};

export default MerchantList;
