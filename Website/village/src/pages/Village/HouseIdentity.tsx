/**
 * 入住申请列表
 */
import {houseCustomerTypeEnum, houseTypeEnum, HouseIdentityStatusEnum} from '@/constants';
import services from '@/services/api';
import {ActionType, ProTable} from '@ant-design/pro-components';
import {Avatar, Button, DatePicker, Image} from 'antd';
import {useRef} from 'react';

const {page} = services.HouseIdentityController;
const {exportData} = services.FileController;
const HouseIdentityList = () => {
    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {title: '房屋ID', align: 'center', dataIndex: 'houseId', copyable: true},
        {
            title: '楼号',
            align: 'center',
            dataIndex: 'floorNumber',
            copyable: true,
            sorter: {multiple: 1},
            onSorter: false,
        },
        {title: '单元', align: 'center', dataIndex: 'unit', copyable: true, sorter: {multiple: 1}, onSorter: false},
        {
            title: '室',
            align: 'center',
            dataIndex: 'roomNumber',
            copyable: true,
            sorter: {multiple: 1},
            onSorter: false,
        },
        {
            title: '类型',
            align: 'center',
            dataIndex: 'type',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: houseTypeEnum.columns,
        },
        {
            title: '人脸',
            align: 'center',
            dataIndex: 'face',
            copyable: true,
            search: false,
            render: (text: string, record: any) => <Avatar shape={'square'} src={<Image src={record.face}/>}/>,
        },
        {title: '姓名', align: 'center', dataIndex: 'name', copyable: true},
        {
            title: '身份证',
            align: 'center',
            dataIndex: 'idCard',
            copyable: true,
        },
        {
            title: '图片', align: 'center', search: false,
            render: (text: object, record: { idCardFace: string, idCardNational: string }) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={[
                                <Image src={record.idCardFace}/>,
                                <Image src={record.idCardNational}/>
                            ]}
                        />
                    }/>
            )
        },
        {title: '手机号', align: 'center', dataIndex: 'phone', copyable: true},
        {
            title: '关系',
            align: 'center',
            dataIndex: 'relation',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: houseCustomerTypeEnum.columns,
        },
        {
            title: '状态',
            align: 'center',
            dataIndex: 'status',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: HouseIdentityStatusEnum.columns,
        },
        {
            title: '申请日期',
            dataIndex: 'insertDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '更新日期',
            dataIndex: 'updateDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
    ];

    const ref = useRef<ActionType>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        houseId: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params: any, sorter, filter) => page(params, sorter, filter)}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/houseIdentitys/export', values, '入住申请.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
            />
        </div>
    );
};

export default HouseIdentityList;
