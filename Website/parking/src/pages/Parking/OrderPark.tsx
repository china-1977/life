/**
 * 停车费列表
 */
import {DeviceStatusEnum, ParkStatusEnum} from '@/constants';
import {exportData} from '@/services/FileController';
import services from '@/services/api';
import {ActionType, ModalForm, ProFormSelect, ProFormText, ProTable} from '@ant-design/pro-components';
import {Button, DatePicker, Dropdown, Form, Space, Typography} from 'antd';
import dayjs, {Dayjs} from 'dayjs';
import {useRef, useState} from 'react';

const {page} = services.OrderParkController;

const OrderParkList = () => {
    const [year, setYear] = useState<Dayjs | any>(dayjs());

    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '区域ID', align: 'center', dataIndex: 'parkRegionId', copyable: true},
        {
            title: '车牌号', align: 'center', dataIndex: 'carNumber', copyable: true,
            render: (text: string, record: any) =>
                <Typography.Link copyable={{text: record.carNumber}}>{record.carNumber}</Typography.Link>
        },
        {
            title: '停车状态', align: 'center', dataIndex: 'parkStatus', copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: ParkStatusEnum.columns,
        },
        {
            title: '设备编号', align: 'center', copyable: true, search: false,
            render: (text: string, record: any) =>
                <Space direction={"vertical"}>
                    <Typography.Link>{record.inDeviceNumber}</Typography.Link>
                    <Typography.Link>{record.outDeviceNumber}</Typography.Link>
                </Space>
        },

        {title: '应付金额', align: 'center', dataIndex: 'moneyMeet', copyable: true, search: false},
        {title: '减免金额', align: 'center', dataIndex: 'moneyReduction', copyable: true, search: false},
        {title: '实付金额', align: 'center', dataIndex: 'moneyPayment', copyable: true, search: false},
        {
            title: '驶入时间', align: 'center', dataIndex: 'inDatetime', copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '驶离时间', align: 'center', dataIndex: 'outDatetime', copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>
        },
        {title: '驶入编号', align: 'center', dataIndex: 'inDeviceNumber', copyable: true, hideInTable: true,},
        {title: '驶离编号', align: 'center', dataIndex: 'outDeviceNumber', copyable: true, hideInTable: true,},
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, record: any) => (
                <Dropdown.Button
                    key={record.id}
                    onClick={async (res) => {
                        setOrderPark(record);
                    }}
                    menu={{
                        items: [
                            {
                                key: 'refund',
                                label: <ModalForm
                                    title="停车费"
                                    grid={true}
                                    colProps={{span: 6}}
                                    onFinish={async (values) => {

                                    }}
                                    form={form}
                                    onOpenChange={(visible) => {
                                        form.resetFields();
                                        if (visible) {
                                            form.setFieldsValue(record);
                                        }
                                    }}
                                    trigger={<Button type="primary"> 退款 </Button>}
                                >
                                    <ProFormText disabled width="sm" name="carNumber" label="车牌号"/>
                                    <ProFormText disabled width="sm" name="inDatetime" label="驶入时间"/>
                                    <ProFormText disabled width="sm" name="outDatetime" label="驶离时间"/>
                                    <ProFormSelect name="parkStatus" label="停车状态"
                                                   valueEnum={ParkStatusEnum.ProFormSelect} disabled debounceTime={1000} width="sm"/>
                                    <ProFormText disabled width="sm" name="moneyMeet" label="应付金额"/>
                                    <ProFormText disabled width="sm" name="moneyReduction" label="减免金额"/>
                                    <ProFormText disabled width="sm" name="moneyPayment" label="实付金额"/>
                                    <ProFormText width="sm" name="moneyRefund" label="退款金额"/>
                                </ModalForm>
                            },
                            {
                                key: 'out',
                                label: <ModalForm
                                    title="停车费"
                                    grid={true}
                                    colProps={{span: 6}}
                                    onFinish={async (values) => {

                                    }}
                                    form={form}
                                    onOpenChange={(visible) => {
                                        form.resetFields();
                                        if (visible) {
                                            form.setFieldsValue(record);
                                        }
                                    }}
                                    trigger={<Button type="primary"> 驶离 </Button>}
                                >
                                    <ProFormText disabled width="sm" name="carNumber" label="车牌号"/>
                                    <ProFormText disabled width="sm" name="inDatetime" label="驶入时间"/>
                                    <ProFormText width="sm" name="outDatetime" label="驶离时间"/>
                                    <ProFormSelect name="parkStatus" label="停车状态"
                                                   valueEnum={ParkStatusEnum.ProFormSelect} debounceTime={1000} width="sm"/>
                                    <ProFormText width="sm" name="moneyMeet" label="应付金额"/>
                                    <ProFormText width="sm" name="moneyReduction" label="减免金额"/>
                                    <ProFormText width="sm" name="moneyPayment" label="实付金额"/>
                                </ModalForm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(orderPark)}}>操作</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const [form] = Form.useForm<any>();
    const ref = useRef<ActionType>();
    const [orderPark, setOrderPark] = useState<any>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        parkRegionId: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                params={{year: year?.year()}}
                columns={columns}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/orderParks/export', values, '停车费信息.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
                request={(params: any, sorter, filter) => page(params, sorter, filter)}
                toolbar={{
                    filter: <DatePicker picker={'year'} defaultValue={year} onChange={(date, value) => setYear(date)}/>,
                }}
            />
        </div>
    );
};

export default OrderParkList;
