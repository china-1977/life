package work.onss.community.domain.wechat;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PhoneEncryptedData implements Serializable {
    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;
    private WatermarkEntity watermark;

    @Data
    public static class WatermarkEntity {
        private String appid;
        private String timestamp;
    }
}
