import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: object, filter: any) => {
  return request<any>('/village/votes', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });
};

export const save = async (body?: any) =>
  request<any>('/village/votes', {
    method: 'POST',
    data: body,
  });

export const del = async (id: string[]) =>
  request<any>('/village/votes', {
    method: 'DELETE',
    data: id,
  });

export const syncResult = async (id: string[]) =>
  request<any>('/village/votes/syncResult', {
    method: 'POST',
    data: id,
  });

export const exportExcel = async (body?: any) =>
  request<any>('/village/votes/export', {
    method: 'POST',
    data: body,
  });

export const sedSubscribeMessage = async (params?: any) =>
  request<any>('/village/votes/sedSubscribeMessage', { params });