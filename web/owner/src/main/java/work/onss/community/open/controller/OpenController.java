package work.onss.community.open.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.pay.LePosBarcodePay;
import work.onss.community.domain.pay.LePosPayRequest;
import work.onss.community.domain.pay.LePosUnifiedPay;
import work.onss.community.domain.vo.Work;
import work.onss.community.domain.wechat.WXNotify;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.utils.Utils;
import work.onss.community.service.wechat.LePosPayService;
import work.onss.community.service.wechat.WechatMpProperties;

import java.security.GeneralSecurityException;
import java.util.Map;
import java.util.UUID;

@Log4j2
@RestController
public class OpenController {

    @Autowired
    private LePosPayService lePosPayService;
    @Autowired
    private WechatMpProperties wechatMpProperties;
    @Autowired
    private QuerydslService querydslService;

    @GetMapping(value = {"open/lePosBarcodePay"})
    public String lePosBarcodePay(@RequestBody LePosPayRequest lePosPayRequest) {

        // 订单号
        String thirdOrderId = UUID.randomUUID().toString().replace("-", "");
        // 随机字符串
        String nonceStr = UUID.randomUUID().toString().replace("-", "");
        // 接口名
        String service = lePosPayRequest.getService();

        Map<String, String> params = Map.of(
                "merchant_id", lePosPayRequest.getMerchantId(),
                "nonce_str", nonceStr,
                "server", service,
                "third_order_id", thirdOrderId
        );

        String apiKey = LePosPayRequest.merchantKey.get(lePosPayRequest.getMerchantId());
        String sign = Utils.generateSign(params, apiKey);

        if (service.equalsIgnoreCase("get_tdcode")) {
            // 统一下单

            LePosUnifiedPay lePosUnifiedPay = new LePosUnifiedPay();
            // 公众号APPID
            lePosUnifiedPay.setAppid(lePosPayRequest.getAppId());
            // OPENID
            lePosUnifiedPay.setSub_openid(lePosPayRequest.getSubOpenid());
            // 商户号
            lePosUnifiedPay.setMerchant_id(lePosPayRequest.getMerchantId());
            // 支付类型
            // 0-支付宝扫码支付；
            // 1-微信公众号、支付宝服务窗支付<原生支付>；
            // 2-微信公众号、支付宝服务窗支付<简易支付>；
            // 3-微信小程序支付、支付宝小程序支付
            lePosUnifiedPay.setJspay_flag(lePosPayRequest.getJspayFlag());
            // 支付方式
            lePosUnifiedPay.setPay_way(lePosPayRequest.getPayWay());
            // 金额
            lePosUnifiedPay.setAmount(lePosPayRequest.getAmount());

            // 订单号
            lePosUnifiedPay.setThird_order_id(thirdOrderId);
            // 随机字符串
            lePosUnifiedPay.setNonce_str(nonceStr);

            // 签名
            lePosUnifiedPay.setSign(sign);
            // 过期时间
            lePosUnifiedPay.setOrder_expiration("300");

            return lePosPayService.pay(lePosUnifiedPay);

        } else if (service.equalsIgnoreCase("upload_authcode")) {
            // 条形码支付

            LePosBarcodePay lePosBarcodePay = new LePosBarcodePay();
            // auth_code 条形码
            lePosBarcodePay.setAuth_code(lePosPayRequest.getAuthCode());
            // merchant_id 商户号
            lePosBarcodePay.setMerchant_id(lePosPayRequest.getMerchantId());
            // amount 金额
            lePosBarcodePay.setAmount(lePosPayRequest.getAmount());

            // third_order_id 订单号
            lePosBarcodePay.setThird_order_id(thirdOrderId);
            // nonce_str 随机字符串
            lePosBarcodePay.setNonce_str(nonceStr);
            // sign 签名
            lePosBarcodePay.setSign(sign);
            return lePosPayService.pay(lePosBarcodePay);

        } else {
            return null;
        }


    }

    @PostMapping(value = {"open/order/notify"})
    public Work<String> notify(@RequestBody WXNotify wxNotify) throws GeneralSecurityException {
        WXNotify.Resource resource = wxNotify.getResource();
        String decryptToString = Utils.decryptToString(resource.getAssociatedData(), resource.getNonce(), resource.getCiphertext(), wechatMpProperties.getApiV3Key());
        querydslService.updatePayStatus(decryptToString);
        return Work.success();
    }
}