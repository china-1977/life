package work.onss.community.controller;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.shop.Product;
import work.onss.community.domain.shop.ProductRepository;
import work.onss.community.domain.shop.QProduct;
import work.onss.community.domain.village.ProductPanel;
import work.onss.community.domain.village.ProductPanelRepository;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.exception.BusinessException;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Log4j2
@RestController
public class ProductController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductPanelRepository productPanelRepository;
    @Autowired
    private QuerydslService querydslService;

    @GetMapping(value = {"products/{id}"}, name = "商品详情")
    public Product product(@PathVariable String id, @RequestHeader(name = "mid") String mid) {
        return productRepository.findByIdAndMerchantId(id, mid).orElse(null);
    }

    /**
     * @param mid 商户ID
     * @return 商品列表
     */
    @GetMapping(value = {"products"}, name = "商品分页")
    public Page<Product> products(@RequestHeader(name = "mid") String mid,
                                  @QuerydslPredicate(bindings = ProductRepository.class) Predicate predicate,
                                  @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QProduct qProduct = QProduct.product;
        return productRepository.findAll(qProduct.merchantId.eq(mid).and(predicate), pageable);
    }

    @GetMapping(value = {"products/all"}, name = "商品列表")
    public Map<String, Object> list(@RequestHeader(name = "mid") String mid) {
        QProduct qProduct = QProduct.product;
        Iterable<Product> productIterable = productRepository.findAll(qProduct.merchantId.eq(mid), qProduct.orderLabel.asc());
        Map<String, List<Product>> map = StreamSupport.stream(productIterable.spliterator(), true).collect(Collectors.groupingBy(Product::getLabel));
        Map<String, String> labels = new HashMap<>(map.size());
        map.forEach((k, v) -> labels.put(v.getFirst().getOrderLabel(), v.getFirst().getLabel()));
        return Map.of("labels", labels, "products", map);
    }

    @PostMapping(value = {"products/create"}, name = "商品创建")
    public Product insert(@RequestHeader(name = "mid") String mid, @Validated @RequestBody Product product) {
        product.setMerchantId(mid);
        productRepository.save(product);
        return product;
    }

    @PostMapping(value = {"products/edit"}, name = "商品编辑")
    public Product update(@RequestHeader(name = "mid") String mid, @Validated @RequestBody Product product) {
        Long count = querydslService.setProduct(mid, product);
        if (count == 0) {
            throw new BusinessException("商品不存在");
        }
        return product;
    }

    @Transactional
    @PostMapping(value = {"products/updateStatus"}, name = "商品批量上下架")
    public void updateStatus(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids, @RequestParam(name = "status") Boolean status) {
        QProduct qProduct = QProduct.product;
        jpaQueryFactory.update(qProduct)
                .set(qProduct.status, status)
                .where(qProduct.id.in(ids), qProduct.merchantId.eq(mid))
                .execute();
    }

    @PostMapping(value = {"products/delete"}, name = "商品删除批量")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        productRepository.deleteByIdInAndMerchantId(ids, mid);
    }

    @PostMapping(value = {"products/panel"}, name = "商品加入橱窗")
    public void panel(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        List<Product> products = productRepository.findAllById(ids);
        if (!products.isEmpty()) {
            for (Product product : products) {
                ProductPanel productPanel = new ProductPanel(mid, product.getLabel(), product.getOrderLabel(), product.getId());
                try {
                    productPanelRepository.save(productPanel);
                } catch (Exception e) {
                    log.error(e);
                }
            }
        }
    }
}
