/**
 * 物业费列表
 */
import { OrderHouseStatusEnum, houseTypeEnum } from '@/constants';
import { OrderHouse } from '@/services/OrderHouseController';
import services from '@/services/api';
import { ActionType, ProTable } from '@ant-design/pro-components';
import { Button, DatePicker, Typography } from 'antd';
import dayjs, { Dayjs } from 'dayjs';
import { useRef, useState } from 'react';
import {exportData} from "@/services/FileController";

const { page } = services.OrderHouseController;

const OrderHouseList = () => {
  const [year, setYear] = useState<Dayjs | any>(dayjs());

  const columns: any = [
    {
      title: '序号',
      dataIndex: 'index',
      valueType: 'indexBorder',
      align: 'center',
    },
    { title: '编号', align: 'center', dataIndex: 'code', copyable: true },
    { title: '房屋ID', align: 'center', dataIndex: 'houseId', copyable: true },
    { title: '楼号', align: 'center', dataIndex: 'floorNumber', copyable: true },
    { title: '单元', align: 'center', dataIndex: 'unit', copyable: true },
    { title: '室', align: 'center', dataIndex: 'roomNumber', copyable: true },
    {
      title: '类型',
      align: 'center',
      dataIndex: 'type',
      copyable: true,
      fieldProps: { mode: 'multiple' },
      valueEnum: houseTypeEnum.columns,
      onFilter: false,
      filters: true,
      search: false,
    },
    {
      title: '状态',
      align: 'center',
      dataIndex: 'status',
      copyable: true,
      fieldProps: { mode: 'multiple' },
      valueEnum: OrderHouseStatusEnum.columns,
      onFilter: false,
      filters: true,
      search: false,
    },
    {
      title: '操作',
      align: 'center',
      valueType: 'option',
      render: (text: any, orderHouse: OrderHouse) => (
          <Button
              key={orderHouse.id}
              onClick={async (res) => {
                setOrderHouse(orderHouse);
                setModalVisit('DETAIL');
              }}>
            <Typography.Text copyable={{ text: JSON.stringify(orderHouse) }}>详情</Typography.Text>
          </Button>
      ),
    },
  ];

  const [modalVisit, setModalVisit] = useState<string | boolean>(false);
  const ref = useRef<ActionType>();
  const [orderHouse, setOrderHouse] = useState<OrderHouse>();

  return (
    <div>
      <ProTable
        actionRef={ref}
        rowKey="id"
        headerTitle="查询表格"
        params={{ year: year?.year() }}
        columns={columns}
        search={{
          optionRender: (searchConfig, formProps, dom) => [
            <Button
                key="out"
                onClick={() => {
                  const values = searchConfig?.form?.getFieldsValue();
                  exportData('/platform/orderHouses/export', values, '物业费.xlsx');
                }}
            >
              导出
            </Button>,
            ...dom.values(),
          ],
        }}
        request={(params: any, sorter, filter) => page(params, sorter, filter)}
        toolbar={{
          filter: <DatePicker picker={'year'} defaultValue={year} onChange={(date, value) => setYear(date)} />,
        }}
      />
    </div>
  );
};

export default OrderHouseList;
