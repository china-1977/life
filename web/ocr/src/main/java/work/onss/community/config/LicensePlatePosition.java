package work.onss.community.config;

import lombok.Data;

import java.io.Serializable;

/**
 * 标准坐标系下的车牌框坐标
 */
@Data
public class LicensePlatePosition implements Serializable {

    /**
     * 左上角坐标值
     */
    private Position leftTop;
    /**
     * 右上角坐标
     */
    private Position rightTop;
    /**
     * 右下角坐标
     */
    private Position rightBottom;
    /**
     * 左下角坐标
     */
    private Position leftBottom;

    /**
     * 私有的-构造一个标准车牌框的坐标
     *
     * @param leftTop     左上角坐标值
     * @param rightTop    右上角坐标
     * @param rightBottom 右下角坐标
     * @param leftBottom  左下角坐标
     */
    private LicensePlatePosition(Position leftTop, Position rightTop, Position rightBottom, Position leftBottom) {
        this.leftTop = leftTop;
        this.rightTop = rightTop;
        this.rightBottom = rightBottom;
        this.leftBottom = leftBottom;
    }

    /**
     * 构造函数
     *
     * @param x1 左上角坐标X的值
     * @param y1 左上角坐标Y的值
     * @param x2 右下角坐标X的值
     * @param y2 右下角坐标Y的值
     */
    private LicensePlatePosition(float x1, float y1, float x2, float y2) {
        this.leftTop = Position.build(x1, y1);
        this.rightTop = Position.build(x2, y1);
        this.rightBottom = Position.build(x2, y2);
        this.leftBottom = Position.build(x1, y2);
    }

    /**
     * 构造一个车牌框
     *
     * @param x1 左上角坐标X的值
     * @param y1 左上角坐标Y的值
     * @param x2 右下角坐标X的值
     * @param y2 右下角坐标Y的值
     */
    public static LicensePlatePosition build(float x1, float y1, float x2, float y2) {
        return new LicensePlatePosition((int) x1, (int) y1, (int) x2, (int) y2);
    }

    /**
     * 构造一个车牌框
     *
     * @param leftTop     左上角坐标值
     * @param rightTop    右上角坐标
     * @param rightBottom 右下角坐标
     * @param leftBottom  左下角坐标
     */
    public static LicensePlatePosition build(Position leftTop, Position rightTop, Position rightBottom, Position leftBottom) {
        return new LicensePlatePosition(leftTop, rightTop, rightBottom, leftBottom);
    }


    /**
     * 判断当前的车牌框是否是标准的车牌框，即非旋转后的车牌框。
     *
     * @return 否是标准的车牌框
     */
    public boolean normal() {
        if (Math.round(leftTop.getX()) == Math.round(leftBottom.getX()) && Math.round(leftTop.getY()) == Math.round(rightTop.getY())) {
            if (Math.round(rightBottom.getX()) == Math.round(rightTop.getX()) && Math.round(rightBottom.getY()) == Math.round(leftBottom.getY())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取宽度
     *
     * @return
     */
    public float width() {
        return (float) Math.sqrt(Math.pow((rightTop.getX() - leftTop.getX()), 2) + Math.pow((rightTop.getY() - leftTop.getY()), 2));
    }

    /**
     * 获取高度
     *
     * @return
     */
    public float height() {
        return (float) Math.sqrt(Math.pow((rightTop.getX() - rightBottom.getX()), 2) + Math.pow((rightTop.getY() - rightBottom.getY()), 2));
    }

    /**
     * 获取面积
     *
     * @return
     */
    public float area() {
        return this.width() * this.height();
    }

    /**
     * 中心点坐标
     *
     * @return
     */
    public Position center() {
        return Position.build((rightTop.getX() + leftBottom.getX()) / 2, (rightTop.getY() + leftBottom.getY()) / 2);
    }

    /**
     * 对车牌框进行旋转对应的角度
     *
     * @param angle 旋转角
     * @return
     */
    public LicensePlatePosition rotate(float angle) {
        Position center = this.center();
        Position rPoint1 = this.leftTop.rotation(center, angle);
        Position rPoint2 = this.rightTop.rotation(center, angle);
        Position rPoint3 = this.rightBottom.rotation(center, angle);
        Position rPoint4 = this.leftBottom.rotation(center, angle);
        return new LicensePlatePosition(rPoint1, rPoint2, rPoint3, rPoint4);
    }

    /**
     * 中心缩放
     *
     * @param scale
     * @return
     */
    public LicensePlatePosition scaling(float scale) {
        // p1-p3
        float lengthPosition1Position3 = leftTop.distance(rightBottom);
        float xDiffPosition1Position3 = leftTop.getX() - rightBottom.getX();
        float yDiffPosition1Position3 = leftTop.getY() - rightBottom.getY();
        float changePosition1Position3 = lengthPosition1Position3 * (1 - scale);
        float xCoordinateChangePosition1Position3 = changePosition1Position3 * xDiffPosition1Position3 / lengthPosition1Position3 / 2;
        float yCoordinateChangePosition1Position3 = changePosition1Position3 * yDiffPosition1Position3 / lengthPosition1Position3 / 2;

        // p2-p4
        float lengthPosition2Position4 = rightTop.distance(leftBottom);
        float xDiffPosition2Position4 = rightTop.getX() - leftBottom.getX();
        float yDiffPosition2Position4 = rightTop.getY() - leftBottom.getY();
        float changePosition2Position4 = lengthPosition2Position4 * (1 - scale);
        float xChangePosition2Position4 = changePosition2Position4 * xDiffPosition2Position4 / lengthPosition2Position4 / 2;
        float yChangePosition2Position4 = changePosition2Position4 * yDiffPosition2Position4 / lengthPosition2Position4 / 2;
        // 构造车牌框
        return new LicensePlatePosition(
                Position.build(leftTop.getX() - xCoordinateChangePosition1Position3, leftTop.getY() - yCoordinateChangePosition1Position3),
                Position.build(rightTop.getX() - xChangePosition2Position4, rightTop.getY() - yChangePosition2Position4),
                Position.build(rightBottom.getX() + xCoordinateChangePosition1Position3, rightBottom.getY() + yCoordinateChangePosition1Position3),
                Position.build(leftBottom.getX() + xChangePosition2Position4, leftBottom.getY() + yChangePosition2Position4)
        );
    }

    /**
     * 将框进行平移
     *
     * @param top    向上移动的像素点数
     * @param bottom 向下移动的像素点数
     * @param left   向左移动的像素点数
     * @param right  向右移动的像素点数
     * @return 平移后的框
     */
    public LicensePlatePosition move(int left, int right, int top, int bottom) {
        return new LicensePlatePosition(
                Position.build(leftTop.getX() - left + right, leftTop.getY() - top + bottom),
                Position.build(rightTop.getX() - left + right, rightTop.getY() - top + bottom),
                Position.build(rightBottom.getX() - left + right, rightBottom.getY() - top + bottom),
                Position.build(leftBottom.getX() - left + right, leftBottom.getY() - top + bottom)
        );
    }

    /**
     * 转换为数组
     *
     * @return
     */
    public Position[] toArray() {
        return new Position[]{leftTop, rightTop, rightBottom, leftBottom};
    }

    /**
     * x的最小坐标
     *
     * @return
     */
    public float x1() {
        return Math.min(Math.min(Math.min(leftTop.getX(), rightTop.getX()), rightBottom.getX()), leftBottom.getX());
    }

    /**
     * y的最小坐标
     *
     * @return
     */
    public float y1() {
        return Math.min(Math.min(Math.min(leftTop.getY(), rightTop.getY()), rightBottom.getY()), leftBottom.getY());
    }

    /**
     * x的最大坐标
     *
     * @return
     */
    public float x2() {
        return Math.max(Math.max(Math.max(leftTop.getX(), rightTop.getX()), rightBottom.getX()), leftBottom.getX());
    }

    /**
     * y的最大坐标
     *
     * @return
     */
    public float y2() {
        return Math.max(Math.max(Math.max(leftTop.getY(), rightTop.getY()), rightBottom.getY()), leftBottom.getY());
    }

}
