package work.onss.community.service.wechat;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import work.onss.community.domain.pay.LePosBarcodePay;
import work.onss.community.domain.pay.LePosUnifiedPay;


@HttpExchange(value = "https://paygate.leshuazf.com")
public interface LePosPayService {

    @PostExchange(value = "/cgi-bin/lepos_pay_gateway.cgi")
    String pay(@RequestBody LePosUnifiedPay lePosUnifiedPay);

    @PostExchange(value = "/cgi-bin/lepos_pay_gateway.cgi")
    String pay(@RequestBody LePosBarcodePay lePosBarcodePay);
}
