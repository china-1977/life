import { request } from '@@/exports';
import { message } from 'antd';

export const exportData = (url: any, params: any, fileName: any) =>
  request<any>(url, { method: 'POST', params, responseType: 'blob' })
    .then((res: any) => {
      const blob = new Blob([res]);
      const objectURL = URL.createObjectURL(blob);
      let btn: any = document.createElement('a');
      btn.download = fileName;
      btn.href = objectURL;
      btn.click();
      URL.revokeObjectURL(objectURL);
      btn = null;
    })
    .catch(() => {
      message.error('下载失败！');
    });
