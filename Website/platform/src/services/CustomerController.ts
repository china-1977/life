import { request } from '@@/exports';

export const getByIdCard = async (params: { keyWords: string; id: string }) =>
  request<any>('/platform/customers/getByIdCard', {
    method: 'POST',
    data: {
      idCard: params?.keyWords,
      id: params?.id,
    },
  });


export interface Customer {
  id: string;
  name: string;
  idCard: string;
  idCardFace: string;
  idCardNational: string;
  face: string;
  phone: string;
  username: string;
  password: string;
  insertDate: string;
  updateDate: string;
  sessionKey: string;
  appid: string;
  openid: string;
  status: boolean;
}
