import { wxLogin, domain, scoreStatus, scoreWay, size, wxRequest } from '../../utils/util.js';

Page({
  /**
   * 页面的初始数据
   */
  data: {
    domain,
    scoreStatus, scoreWay,
    scores: [],
    year: new Date().getFullYear(),
    keyword: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      options
    })
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/store/scores/search`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { ...options, size, year: this.data.year }
      }).then(({ content, number }) => {
        this.setData({
          scores: content, number,
        })
      })
    })
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      const { options, keyword, year } = this.data
      wxRequest({
        url: `${domain}/store/scores/search`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { ...options, size, year, keyword }
      }).then(({ content, number }) => {
        this.setData({
          scores: content, number
        });
        wx.stopPullDownRefresh()
      });
    });
  },

  onReachBottom: function () {
    wxLogin().then(({ authorization, info }) => {
      let { number, options, scores, year, keyword } = this.data
      wxRequest({
        url: `${domain}/store/scores/search`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { ...options, size, year, keyword, number: number + 2 }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          scores.push(...data.content)
          this.setData({
            scores, number: data.number
          });
        }
      })
    })
  },

  bindDateChange: function (e) {
    wxLogin().then(({ authorization, info }) => {
      const year = e.detail.value;
      const { options, keyword } = this.data
      wxRequest({
        url: `${domain}/store/scores/search`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { ...options, size, year, keyword }
      }).then((data) => {
        this.setData({
          scores: data.content, number: data.number, year
        });
      })
    })
  },

  inputChange: function ({ detail }) {
    this.setData({
      keyword: detail.value
    })
  },

  searchScore: function (e) {
    wxLogin().then(({ authorization, info }) => {
      const { options, year, keyword } = this.data
      wxRequest({
        url: `${domain}/store/scores/search`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { ...options, size, year, keyword }
      }).then((data) => {
        this.setData({
          scores: data.content, number: data.number
        });
      });
    });
  }

})