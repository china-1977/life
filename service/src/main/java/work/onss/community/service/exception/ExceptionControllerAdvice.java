package work.onss.community.service.exception;

import jakarta.validation.ConstraintViolationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import work.onss.community.domain.vo.Code;
import work.onss.community.domain.vo.Work;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author wangchanghao
 */
@Log4j2
@ControllerAdvice
@ResponseBody
public class ExceptionControllerAdvice {

    private static final Map<String, String> constraintCodeMap = new HashMap<>() {
        {
            put("house_customer_hid_cid_uindex", "请不要重复绑定");
            put("village_customer_vid_cid_uindex", "请不要重复绑定");
            put("uk_customer_id_card", "身份证已被注册");
            put("uk_customer_phone", "手机号已被注册");
            put("uk_store_license_number", "统一社会信用代码已被注册");
        }
    };


    @ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler({BusinessException.class, RuntimeException.class})
    public Work<Object> serviceException(Exception e) {
        log.error("Exception", e);
        Work.WorkBuilder<Object> builder = Work.builder();
        if (e instanceof BusinessException businessException) {
            builder.code(businessException.getCode()).message(e.getMessage()).data(businessException.getData());
        } else if (e instanceof RuntimeException runtimeException) {
            Code fail = Code.FAIL;
            builder.code(fail.name()).message(runtimeException.getMessage());
        }
        return builder.build();
    }


    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(HttpClientErrorException.class)
    public Work<Object> serviceException(HttpClientErrorException e) {
        log.error("Exception", e);
        return Work.fail(e.getMessage());
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MethodArgumentNotValidException.class, ConstraintViolationException.class})
    public Work<Object> methodArgumentNotValidException(Exception e) {
        log.error("Exception", e);
        StringJoiner message = new StringJoiner(",", "[", "]");
        if (e instanceof MethodArgumentNotValidException methodArgumentNotValidException) {
            methodArgumentNotValidException.getBindingResult().getAllErrors().forEach(item -> message.add(item.getDefaultMessage()));
        }
        if (e instanceof ConstraintViolationException constraintViolationException) {
            constraintViolationException.getConstraintViolations().forEach(constraintViolation -> message.add(constraintViolation.getMessage()));
        }
        return Work.fail(message.toString());
    }

    @ResponseStatus(code = HttpStatus.REQUEST_HEADER_FIELDS_TOO_LARGE)
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public Work<Object> maxUploadSizeExceededException(MaxUploadSizeExceededException e) {
        log.error("Exception", e);
        return Work.fail("文件大小上限为1M");
    }

    @ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(MissingRequestHeaderException.class)
    public Work<Object> missingServletRequestParameterException(MissingRequestHeaderException e) {
        log.error("Exception", e);
        String parameterName = e.getHeaderName();
        Work.WorkBuilder<Object> workBuilder = Work.builder();
        switch (parameterName) {
            case "mid" -> {
                Code merchantNotBind = Code.MERCHANT_NOT_BIND;
                workBuilder.code(merchantNotBind.name()).message(merchantNotBind.getValue());
            }
            case "cid" -> {
                Code sessionExpire = Code.SESSION_EXPIRE;
                workBuilder.code(sessionExpire.name()).message(sessionExpire.getValue());
            }
            default -> {
            }
        }
        return workBuilder.build();
    }

    @ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler({SQLException.class})
    public Work<Object> missingServletRequestParameterException(SQLException e) {
        log.error("Exception", e);
        AtomicReference<String> message = new AtomicReference<>(e.getMessage());
        constraintCodeMap.forEach((k, v) -> {
            if (message.get().contains(k)) {
                message.set(v);
            }
        });
        return Work.fail(message.get());
    }
}
