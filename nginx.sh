#!/bin/bash

# 下载Nginx源码包
wget http://nginx.org/download/nginx-1.26.2.tar.gz

# 解压源码包
tar xzf nginx-1.26.2.tar.gz
cd nginx-1.26.2

# 安装依赖包
sudo apt update
sudo apt install build-essential libpcre3-dev libssl-dev zlib1g-dev

# 配置并编译Nginx
./configure --prefix=/usr/local/nginx \
            --conf-path=/etc/nginx/nginx.conf \
            --error-log-path=/var/log/nginx/error.log \
            --http-log-path=/var/log/nginx/access.log \
            --pid-path=/run/nginx.pid \
            --with-http_ssl_module

# 编译并安装Nginx
make
sudo make install

# 创建系统服务文件
sudo tee /etc/systemd/system/nginx.service <<EOF
[Unit]
Description=The NGINX HTTP Server
After=network.target

[Service]
User=nginx
Group=nginx
PIDFile=/run/nginx.pid
ExecStartPre=-/bin/rm -f /run/nginx.pid
ExecStart=/usr/local/nginx/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF

# 重新加载systemd配置
sudo systemctl daemon-reload
sudo systemctl enable nginx

# 启动Nginx服务
sudo systemctl start nginx

# 检查状态
sudo systemctl status nginx

echo "Nginx installation complete."
