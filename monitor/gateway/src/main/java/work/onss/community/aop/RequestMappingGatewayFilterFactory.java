package work.onss.community.aop;


import lombok.Data;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import java.util.List;

@Component
public class RequestMappingGatewayFilterFactory extends AbstractGatewayFilterFactory<RequestMappingGatewayFilterFactory.Config> {

    public RequestMappingGatewayFilterFactory() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            ServerHttpRequest oldRequest = exchange.getRequest();
            HttpMethod method = oldRequest.getMethod();
            String url = oldRequest.getURI().getPath();
            AntPathMatcher antPathMatcher = new AntPathMatcher();
            List<String> excludePatterns = config.getExcludePatterns();
            for (String pattern : excludePatterns) {
                if (antPathMatcher.match(pattern, url)) {
                    return chain.filter(exchange.mutate().request(oldRequest).build());
                }
            }
            String path = String.join(":", method.name(), url);
            ServerHttpRequest request = oldRequest.mutate().headers((httpHeaders) -> httpHeaders.add("path", path)).build();
            return chain.filter(exchange.mutate().request(request).build());
        };
    }

    @Data
    public static class Config {
        private List<String> excludePatterns;
    }
}
