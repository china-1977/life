package work.onss.community.domain.wechat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class WXMerchantScore {

    private String spAppid;// 服务商应用ID
    private String spMchid;// 服务商户号
    private String subAppid;// 子商户/二级商户应用ID
    private String subMchid;// 子商户号/二级商户号
    private String description;// 商品描述
    private String outTradeNo;// 商户订单号
    private String timeExpire;// 交易结束时间 yyyy-MM-DDTHH:mm:ss+TIMEZONE
    private String attach;// 附加数据
    private String notifyUrl;// 通知地址|
    private String goodsTag;// 订单优惠标记
    private WXMerchantScore.SettleInfo settleInfo;// 结算信息
    private boolean supportFapiao;// 电子发票入口开放标识
    private WXMerchantScore.Amount amount;// 订单金额
    private WXMerchantScore.Payer payer;// 支付者
    private WXMerchantScore.Detail detail;// 优惠功能
    private WXMerchantScore.SceneInfo sceneInfo;// 场景信息

    @Data
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Amount {
        private String currency;// CNY：人民币，境内商户号仅支持人民币
        private Integer total;// 订单总金额，单位为分
    }

    @Data
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class Payer {
        private String spOpenid;// 用户在服务商AppID下的唯一标识
        private String subOpenid;//  用户在子商户AppID下的唯一标识
    }

    @NoArgsConstructor
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class SettleInfo {
        private boolean profitSharing;// 是否指定分账
    }

    @NoArgsConstructor
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class Detail {
        private long costPrice;
        private String invoiceId;
        private List<WXMerchantScore.GoodsDetail> goodsDetail;
    }

    @NoArgsConstructor
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class GoodsDetail {
        private String merchantGoodsId;
        private String wechatpayGoodsId;
        private String goodsName;
        private int quantity;
        private long unitPrice;
    }

    @NoArgsConstructor
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class SceneInfo {
        private String payerClientIp;
        private String deviceId;
        private WXMerchantScore.StoreInfo storeInfo;
    }

    @NoArgsConstructor
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class StoreInfo {
        private String id;
        private String name;
        private String areaCode;
        private String address;
    }

    @NoArgsConstructor
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class Result {
        private String prepayId;
    }
}
