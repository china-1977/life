package work.onss.community.domain.config;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.geometric.PGpath;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class PointPathType implements UserType<PGpath> {


    @Override
    public int getSqlType() {
        return Types.OTHER;
    }

    @Override
    public Class<PGpath> returnedClass() {
        return PGpath.class;
    }

    @Override
    public boolean equals(PGpath pGpath, PGpath j1) {
        if (pGpath == null && j1 == null)
            return true;
        else if (pGpath == null || j1 == null)
            return false;
        return pGpath.equals(j1);
    }

    @Override
    public int hashCode(PGpath pGpath) {
        return pGpath.hashCode();
    }

    @Override
    public PGpath nullSafeGet(ResultSet resultSet, int i, SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws SQLException {
        return resultSet.getObject(i, PGpath.class);
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, PGpath pGpath, int i, SharedSessionContractImplementor sharedSessionContractImplementor) throws SQLException {
        preparedStatement.setObject(i, pGpath);
    }

    @Override
    public PGpath deepCopy(PGpath pGpath) {
        return pGpath;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(PGpath pGpath) {
        return pGpath;
    }

    @Override
    public PGpath assemble(Serializable serializable, Object o) {
        return (PGpath) serializable;
    }

    @Override
    public PGpath replace(PGpath pGpath, PGpath j1, Object o) {
        return pGpath;
    }
}
