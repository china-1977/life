import { wxLogin, domain, wxRequest, OrderParkStatus, size } from '../../utils/util.js';

Page({

  data: {
    domain, OrderParkStatus,
    orderParks: [],
    year: new Date().getFullYear()
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderParks`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { year: this.data.year, size }
      }).then((data) => {
        this.setData({
          orderParks: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderParks`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { year: this.data.year, size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          orderParks: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderParks`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { page: this.data.number + 2, year: this.data.year, size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let orderParks = this.data.orderParks;
          orderParks.push(...data.content)
          this.setData({
            orderParks, number: data.number
          });
        }
      });
    });
  },

  yearChange: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderParks`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { year: e.detail.value, size }
      }).then((data) => {
        this.setData({
          orderParks: data.content, number: data.number, year: e.detail.value
        });
      });
    });
  }
})