// pages/engine/map.js

const points = [
  {
    id: 1,
    latitude: 36.43490,
    longitude: 115.98820,
    alpha: 0.75,
    iconPath: '/images/充电.png',
    width: 30,
    height: 30,
    shortname: '测试1',
    openTime: '07:00',
    closeTime: '22:00',
    trademark: '/images/logo2.png',
    status: '可用、可还'
  },
  {
    id: 2,
    latitude: 36.43440,
    longitude: 115.98820,
    alpha: 0.75,
    iconPath: '/images/充电.png',
    width: 25,
    height: 25,
    shortname: '测试2',
    openTime: '07:00',
    closeTime: '22:00',
    trademark: '/images/logo2.png',
    status: '可用、可还'
  },
  {
    id: 3,
    latitude: 36.43410,
    longitude: 115.98820,
    alpha: 0.75,
    iconPath: '/images/充电.png',
    width: 25,
    height: 25,
    shortname: '测试3',
    openTime: '07:00',
    closeTime: '22:00',
    trademark: '/images/logo2.png',
    status: '可用、可还'
  }
]

Page({

  /**
   * 页面的初始数据
   */
  data: {
    setting: {

    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.getLocation({
      success: (res) => {
        this.setData({
          latitude: res.latitude,
          longitude: res.longitude,
          markers: points
        })
      }
    })
  },
  toHelp() {
    wx.navigateTo({
      url: '/pages/engine/help',
    })
  },
  chooseLocation() {
    wx.chooseLocation({
      latitude: this.data.latitude,
      longitude: this.data.longitude,
      success: (res) => {
        this.setData({
          latitude: res.latitude,
          longitude: res.longitude,
          markers: points
        })
      }
    })
  },
  getLocation() {
    wx.getLocation({
      success: (res) => {
        this.setData({
          latitude: res.latitude,
          longitude: res.longitude,
          markers: points
        })
      }
    })
  },
  openLocation(e) {
    const merchant = e.currentTarget.dataset.merchant;
    wx.openLocation({
      latitude: merchant.latitude,
      longitude: merchant.longitude,
      name: merchant.shortname
    })

  },

  markerTap(e) {
    const markerId = e.detail.markerId;
    let markers = this.data.markers;
    markers.forEach(marker => {
      if (marker.id === markerId) {
        marker.width = 30;
        marker.height = 30;
      } else {
        marker.width = 25;
        marker.height = 25;
      }
    });
    this.setData({
      markers, intoView: markerId
    })
  },

  markerTap2(e) {
    const markerId = e.currentTarget.dataset.markerId;
    let markers = this.data.markers;
    markers.forEach(marker => {
      if (marker.id === markerId) {
        marker.width = 30;
        marker.height = 30;
      } else {
        marker.width = 25;
        marker.height = 25;
      }
    });
    this.setData({
      markers
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  scanCode() {
    wx.scanCode({
      success: (res) => {
        console.log(res)
      },
      fail: (res) => {
        console.log(res);
      }
    })
  },
  regionChange(res) {
    if (res.detail.causedBy === 'drag' && res.detail.type === 'end') {
      const centerLocation = res.detail.centerLocation
      this.setData({
        latitude: centerLocation.latitude,
        longitude: centerLocation.longitude,
        markers: points
      })
    }
  }

})