import { wxLogin, domain, wxRequest, AdviceStatus, size } from '../../utils/util.js';
Page({

  data: {
    AdviceStatus, status: 'WAIT'
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      const status = this.data.status;
      wxRequest({
        url: `${domain}/village/advices`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { status, size }
      }).then((data) => {
        this.setData({
          advices: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      const status = this.data.status;
      wxRequest({
        url: `${domain}/village/advices`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { status, size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          advices: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      const status = this.data.status;
      wxRequest({
        url: `${domain}/village/advices`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { page: this.data.number + 2, status, size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let advices = this.data.advices;
          advices.push(...data.content)
          this.setData({
            advices, number: data.number
          });
        }
      });
    });
  },

  itemTap: function (e) {
    const status = e.currentTarget.id;
    this.setData({
      status
    })
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/advices`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { status, size }
      }).then((data) => {
        this.setData({
          advices: data.content, number: data.number
        });
      });
    });
  }
})