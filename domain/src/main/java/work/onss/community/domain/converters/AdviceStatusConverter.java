package work.onss.community.domain.converters;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import work.onss.community.domain.village.Advice;

public class AdviceStatusConverter implements Converter<Advice.Status> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return Advice.Status.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Advice.Status convertToJavaData(ReadConverterContext<?> context) {
        for (Advice.Status status : Advice.Status.values()) {
            if (status.getValue().equals(context.getReadCellData().getStringValue())) {
                return status;
            }
        }
        return null;
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Advice.Status> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }

}
