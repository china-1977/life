package work.onss.community.domain.wechat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Jscode2SessionResult implements Serializable {

    @JsonProperty(value = "session_key")
    private String sessionKey;
    private String openid;
    private String unionid;
    private Integer errcode;
    private String errmsg;
}
