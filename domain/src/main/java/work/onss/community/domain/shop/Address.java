package work.onss.community.domain.shop;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.config.PointType;
import work.onss.community.domain.usertype.StringArrayType;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * 地址
 *
 * @author wangchanghao
 */
@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@EntityListeners(AuditingEntityListener.class)
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class Address implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @NotBlank(message = "请填写地址名称")
    private String name;
    @NotBlank(message = "请填写地址详情")
    @Size(min = 3, max = 50, message = "请尽可能填写详细地址")
    private String detail;
    @Type(value = PointType.class)
    @NotNull(message = "请重新定位收货地址")
    private Point2D.Double location;
    private String postcode;
    @Type(value = StringArrayType.class)
    @NotEmpty(message = "请选择地区")
    @Size(min = 3, max = 3, message = "请选择地区")
    private String[] code;
    @Type(value = StringArrayType.class)
    private String[] value;
    private String customerId;
    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate insertDate;
    @LastModifiedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate updateDate;
}
