
Page({
  mixins: [require('../../mixin/common')],
  data: {
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/repairs/${options.id}`,
        header: { authorization },
      }).then((repair) => {
        this.setData({
          repair
        });
      });
    });
  },
})