import { ParkSpace } from '@/services/ParkSpaceController';
import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/platform/houses', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const customers = (id: string) => request<any>(`/platform/houses/${id}/customers`);

export const getParkSpaces = (id: string) => request<any>(`/platform/houses/${id}/parkSpaces`);


export interface HouseCustomer {
  id: string;
  relation: string;
  houseId: string;
  customerId: string;
  merchantId: string;
  customer: Customer;
}

export interface House {
  id: string;
  merchantId: string;
  floorNumber: string;
  unit: string;
  roomNumber: string;
  area: number;
  areaPrice: number;
  propertyTotal: number;
  rubbishPrice: number;
  waterPrice: number;
  waterInitial: number;
  waterCurrent: number;
  waterCount: number;
  waterRatio: number;
  waterLoss: number;
  waterTotal: number;
  type: string;
  remarks: string;
  houseCustomers: [HouseCustomer];
  parkSpaces: [ParkSpace];
}

export interface Customer {
  id: number;
  name: string;
  idCard: string;
  idCardFace: string;
  idCardNational: string;
  face: string;
  phone: string;
  username: string;
  password: string;
  insertDate: string;
  updateDate: string;
  sessionKey: string;
  appid: string;
  openid: string;
  status: boolean;
}
