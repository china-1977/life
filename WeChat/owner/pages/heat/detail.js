Page({
  mixins: [require('../../mixin/common')],
  data: {
  },
  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderHeats/${options.id}`,
        header: { authorization },
      }).then((orderHeat) => {
        this.setData({
          orderHeat
        });
      });
    });
  },

  pay: function (e) {
    this.wxLogin().then(({ authorization }) => {
      wx.login({
        success: ({ code }) => {
          const orderHeat = this.data.orderHeat;
          this.wxRequest({
            url: `${this.data.domain}/owner/orderHeats/${orderHeat.id}/pay`,
            header: { authorization },
            method: 'POST',
            data: { appid, code }
          }).then((miniPayToken) => {
            wx.requestPayment({
              ...miniPayToken,
              success: (res) => {
                console.log(res);
              },
              fail: (res) => {
                console.log(res);
              }
            })
          })
        },
        fail: () => {
          wx.reLaunch({
            url: '/pages/login'
          })
        },
      })
    })
  },
})