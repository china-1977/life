export const DEFAULT_NAME = 'Umi Max';


export const StatusEnum = {
  columns:{
    true: { text: '是' },
    false: { text: '否' },
  }
};

export const merchantStatusEnum = {
  true: { text: '是' },
  false: { text: '否' },
};

export const ProductStatusEnum = {
  ProFormSelect: {
    true: '是',
    false: '否',
  },
  columns: {
    true: { text: '是' },
    false: { text: '否' },
  },
};

export const VisitorStatusEnum = {
  ProFormSelect: {
    PENDING: '待处理',
    YES: '同意',
    NO: '驳回',
    ING: '执行中',
    END: '结束',
  },
  columns: {
    PENDING: { text: '待处理' },
    YES: { text: '同意' },
    NO: { text: '驳回' },
    ING: { text: '执行中' },
    END: { text: '结束' },
  },
};

export const houseTypeEnum = {
  ProFormSelect: {
    住宅: '住宅',
    商铺: '商铺',
    公寓: '公寓',
    办公室: '办公室',
  },
  columns: {
    住宅: { text: '住宅' },
    商铺: { text: '商铺' },
    公寓: { text: '公寓' },
    办公室: { text: '办公室' },
  },
};

export const houseCustomerTypeEnum = {
  ProFormSelect: {
    房东: '房东',
    租户: '租户',
  },
  columns: {
    房东: { text: '房东' },
    租户: { text: '租户' },
  },
};

export const ParkSpaceStatusEnum = {
  ProFormSelect: {
    出售: '出售',
    出租: '出租',
    闲置: '闲置',
  },
  columns: {
    出售: { text: '出售' },
    出租: { text: '出租' },
    闲置: { text: '闲置' },
  },
};

export const ParkSpaceStateEnum = {
  ProFormSelect: {
    有车: '有车',
    无车: '无车',
  },
  columns: {
    有车: { text: '有车' },
    无车: { text: '无车' },
  },
};

export const DeviceStatusEnum = {
  ProFormSelect: {
    online: '上线',
    offline: '下线',
    abnormal: '异常',
  },
  columns: {
    online: { text: '上线' },
    offline: { text: '下线' },
    abnormal: { text: '异常' },
  },
};

export const AccessTypeEnum = {
  ProFormSelect: {
    enter: '入口',
    exit: '出口',
  },
  columns: {
    enter: { text: '入口' },
    exit: { text: '出口' },
  },
};

export const ParkStatusEnum = {
  ProFormSelect: {
    I: '在场',
    O: '离场',
  },
  columns: {
    I: { text: '在场' },
    O: { text: '离场' },
  },
};

export const OrderStatusEnum = {
  ProFormSelect: {
    WAIT_CONFIRM: '待确认',
    WAIT_PAY: '待支付',
    FINISH: '已支付',
  },
  columns: {
    WAIT_CONFIRM: { text: '待确认' },
    WAIT_PAY: { text: '待支付' },
    FINISH: { text: '已支付' },
  },
};

export const AdviceStatusEnum = {
  ProFormSelect: {
    WAIT: '待处理',
    CLOSED: '已关闭',
    ERROR: '未解决',
    SUCCESS: '已解决',
  },
  columns: {
    WAIT: { text: '待处理' },
    CLOSED: { text: '已关闭' },
    ERROR: { text: '未解决' },
    SUCCESS: { text: '已解决' },
  },
};

export const RepairStatusEnum = {
  ProFormSelect: {
    WAIT_ALLOT: '待分配',
    WAIT_REPAIR: '待维修',
    FINISHED: '已完成',
  },
  columns: {
    WAIT_ALLOT: { text: '待分配' },
    WAIT_REPAIR: { text: '待维修' },
    FINISHED: { text: '已完成' },
  },
};

export const PayStatusEnum = {
  ProFormSelect: {
    SUCCESS: '支付成功',
    REFUND: '转入退款',
    NOTPAY: '未支付',
    CLOSED: '已关闭',
    REVOKED: '已撤销',
    USERPAYING: '用户支付中',
    PAYERROR: '支付失败',
  },
  columns: {
    SUCCESS: { text: '支付成功' },
    REFUND: { text: '转入退款' },
    NOTPAY: { text: '未支付' },
    CLOSED: { text: '已关闭' },
    REVOKED: { text: '已撤销' },
    USERPAYING: { text: '用户支付中' },
    PAYERROR: { text: '支付失败' },
  },
};

export const HouseIdentityStatusEnum = {
  ProFormSelect: {
    WAIT: '待处理',
    YES: '已通过',
    NO: '已拒绝',
  },
  columns: {
    WAIT: { text: '待处理' },
    YES: { text: '已通过' },
    NO: { text: '已拒绝' },
  },
};

export const HouseVacantStatusEnum = {
  ProFormSelect: {
    TODO: '待处理',
    CONFIRM: '待确认',
    ING: '执行中',
    OK: '生效',
    NO: '失效',
  },
  columns: {
    TODO: { text: '待处理' },
    CONFIRM: { text: '待确认' },
    ING: { text: '执行中' },
    OK: { text: '生效' },
    NO: { text: '失效' },
  },
};
