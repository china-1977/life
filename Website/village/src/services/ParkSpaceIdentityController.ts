import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/village/parkSpaceIdentitys', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const setStatus = async (status: string, ids: any[]) =>
  request<any>(`/village/parkSpaceIdentitys/${status}/setStatus`, {
    method: 'POST',
    data: ids,
  });

export const save = async (body?: any) =>
  request<any>('/village/parkSpaceIdentitys', {
    method: 'POST',
    data: body,
  });

export const del = async (body?: any) =>
  request<any>('/village/parkSpaceIdentitys', {
    method: 'DELETE',
    data: body,
  });
