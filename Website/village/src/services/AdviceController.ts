import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/village/advices', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const setStatus = async (status: string, ids: any[]) =>
  request<any>(`/village/advices/${status}/setStatus`, {
    method: 'POST',
    data: ids,
  });

export const save = async (body?: any) =>
  request<any>('/village/advices', {
    method: 'POST',
    data: body,
  });

export const del = async (body?: any) =>
  request<any>('/village/advices', {
    method: 'DELETE',
    data: body,
  });

export interface Advice {
  id: string;
  merchantId: string;
  customerId: string;
  name: string;
  phone: string;
  title: string;
  description: string;
  remark: string;
  status: string;
  insertDatetime: string;
  updateDatetime: string;
}
