import { checkToken, domain, wxRequest, chooseImageToBase64, chooseImagesToBase64 } from '../../../utils/util.js';
Page({
  data: {

  },

  onLoad(options) {
    checkToken().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/products/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((product) => {
        console.log(product);
        const start = product.startDatetime.split('T');
        const end = product.endDatetime.split('T');
        this.setData({
          ...product,
          startDate: start[0],
          startTime: start[1],
          endDate: end[0],
          endTime: end[1],
          product
        })
      })
    })
  },

  dateTimeChange: function (e) {
    this.setData({
      [e.currentTarget.id]: e.detail.value
    })
  },

  chooseImages: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count
    const pictures = this.data[id] == null ? [] : this.data[id];
    count = count - pictures.length;
    chooseImagesToBase64('front', count).then((files) => {
      this.setData({
        [`${id}`]: [...pictures, ...files]
      })
    })
  },

  deletePictures: function (e) {
    const id = e.currentTarget.id;
    const index = e.currentTarget.dataset.index;
    const files = this.data[id];
    files.splice(index, 1);
    this.setData({
      [id]: files
    })
  },

  chooseImage: function (e) {
    const id = e.currentTarget.id;
    chooseImageToBase64('front').then((file) => {
      this.setData({
        [`${id}`]: file
      })
    })
  },

  deletePicture: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: null
    })
  },

  clearPictues: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: []
    })
  },

  update: function (e) {
    checkToken().then(({ authorization, info }) => {
      const { pictures, startDate, startTime, endDate, endTime, id } = this.data;
      wxRequest({
        url: `${domain}/village/products/edit`,
        header: { authorization, cid: info.cid, mid: info.mid },
        method: 'POST',
        data: { pictures, startDatetime: `${startDate}T${startTime}`, endDatetime: `${endDate}T${endTime}`, id, ...e.detail.value }
      }).then((content) => {
        wx.showModal({
          title: '提示',
          content: '编辑成功',
          showCancel: false,
          success: (res) => {
            wx.navigateBack({
              delta: 2
            });
          }
        })
      })
    })
  },


})