import ProductPanelAPI from '@/services/Store/ProductPanelAPI';
import {Avatar, Button, Dropdown, Form, Image, Popconfirm, Space, Tooltip, Typography} from 'antd';
import {ActionType, ModalForm, ProFormText, ProTable,} from '@ant-design/pro-components';
import {useRef} from 'react';
import {ProductStatusEnum} from "@/constants";

const ProductPanelList = () => {
    const columns: any = [
        {title: '序号', dataIndex: 'index', valueType: 'indexBorder', align: 'center',},
        {title: '主键', align: 'center', dataIndex: ['productPanel', 'id'], key: 'productPanel.id', copyable: true},
        {
            title: '图集', dataIndex: ['product', 'pictures'], copyable: true, search: false,
            render: (text: [string], record: any) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={record?.product.pictures?.map((value: string | undefined, index: any) => (
                                <Image src={value}/>
                            ))}
                        />
                    }/>
            )
        },
        {
            title: '名称', dataIndex: ['product', 'name'], copyable: true,
            render: (text: any, record: any) => (
                <Tooltip title={record?.description}>
                    <Button type="link">{text}</Button>
                </Tooltip>
            )
        },
        {
            title: '简称', dataIndex: ['merchant', 'shortname'], copyable: true, search: false,
            render: (text: string, record: any) =>
                <Button color="primary" variant="link" href={`#/shop/${record.merchant.id}/products`}>
                    {record.merchant.shortname}
                </Button>,
        },
        {title: '价格', dataIndex: ['product', 'price'], copyable: true, search: false},
        {title: '单位', dataIndex: ['product', 'priceUnit'], copyable: true, search: false},
        {title: '库存', dataIndex: ['product', 'stock'], copyable: true, search: false},
        {title: '标签', dataIndex: ['productPanel', 'label'], copyable: true},
        {title: '排序', dataIndex: ['productPanel', 'orderLabel'], copyable: true},
        {title: '状态', dataIndex: ['product', 'status'], valueEnum: ProductStatusEnum.columns,},
        {title: '视频', dataIndex: ['product', 'vid'], copyable: true},
        {
            title: '操作',
            align: 'center',
            dataIndex: 'option',
            valueType: 'option',
            render: (text: any, value: any) => (
                <Dropdown.Button
                    key={value.productPanel.id}
                    children={
                        productForm(<Typography.Text
                            onClick={() => {
                                form.setFieldsValue(value.productPanel)
                            }}
                            copyable={{text: JSON.stringify(value)}}>编辑</Typography.Text>, form, '编辑')
                    }
                    menu={{
                        items: [
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        ProductPanelAPI.del([value.productPanel.id]).then((res) => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                </Dropdown.Button>)
        }
    ];

    const ref = useRef<ActionType>();
    const [form] = Form.useForm();

    const productForm: any = (trigger: any, form: any, title: string) =>
        <ModalForm
            title={title}
            form={form}
            grid={true}
            colProps={{span: 8}}
            onFinish={async (values) => {
                await form.validateFields();
                await ProductPanelAPI.save(values);
                form.resetFields();
                ref.current?.reload();
                return true
            }}
            trigger={trigger}
        >
            <ProFormText name={['id']} label="主键" hidden={true} colProps={{span: 0}}/>
            <ProFormText name={['productId']} label="商品ID" hidden={true} colProps={{span: 0}}/>
            <ProFormText width="sm" name={['label']} label="标签名称" placeholder="请输入标签名称"
                         rules={[{required: true, message: '标签名称不能为空'}]}/>
            <ProFormText width="sm" name={['orderLabel']} label="标签排序" placeholder="请输入标签排序"
                         rules={[{required: true, message: '标签排序不能为空'}]}/>
        </ModalForm>


    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        'productPanel.id': {show: false},
                    }
                }}
                actionRef={ref}
                rowKey={(record) => record.productPanel.id}
                headerTitle="查询表格"
                columns={columns}
                request={(params, sorter, filter) => ProductPanelAPI.page(params, sorter, filter)}
                rowSelection={{}}
                tableAlertOptionRender={(data) => {
                    return (
                        <Space size={16}>
                            <Popconfirm
                                title="批量删除"
                                description={`是否批量删除？`}
                                okText="确认"
                                cancelText="取消"
                                onConfirm={() => {
                                    ProductPanelAPI.del(data?.selectedRowKeys).then(() => {
                                        // @ts-ignore
                                        ref.current.clearSelected();
                                        ref.current?.reload();
                                    });
                                }}
                            >
                                <Button type="primary" danger>删除</Button>
                            </Popconfirm>
                        </Space>
                    );
                }}
            />
        </div>
    );
};

export default ProductPanelList;
