package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;


@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@EntityListeners(AuditingEntityListener.class)
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class ProductPanel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键", index = 0)
    private String id;
    private String merchantId;
    @ExcelProperty(value = "标签", index = 1)
    @NotBlank(message = "商品标签不能为空")
    private String label;
    @ExcelProperty(value = "排序", index = 2)
    @NotBlank(message = "排序不能为空")
    private String orderLabel;
    @ExcelProperty(value = "商品ID", index = 3)
    @NotBlank(message = "商品ID不能为空")
    private String productId;

    public ProductPanel(String merchantId, String label, String orderLabel, String productId) {
        this.merchantId = merchantId;
        this.label = label;
        this.orderLabel = orderLabel;
        this.productId = productId;
    }
}