package work.onss.community.village.controller;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.dto.village.DataCount;
import work.onss.community.domain.village.*;
import work.onss.community.domain.village.relational.QHouseCustomer;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.exception.BusinessException;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

@Log4j2
@RestController
public class VoteController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private VoterRepository voterRepository;
    @Autowired
    private QuerydslService querydslService;

    @GetMapping(value = {"votes/{id}"})
    public Vote detail(@PathVariable String id) {
        return voteRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"votes"})
    public Page<Vote> page(@RequestHeader("cid") String cid, @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        List<String> mids = jpaQueryFactory.selectDistinct(qHouseCustomer.merchantId)
                .from(qHouseCustomer)
                .where(qHouseCustomer.customerId.eq(cid))
                .fetch();
        if (mids.isEmpty()) {
            return null;
        } else {
            QVote qVote = QVote.vote;
            return voteRepository.findAll(qVote.merchantId.in(mids), pageable);
        }
    }

    @PostMapping(value = {"votes"})
    public void save(@RequestHeader("cid") String cid, @Validated @RequestBody Voter voter) {
        Vote vote = voteRepository.findById(voter.getVoteId()).orElseThrow(() -> new BusinessException("投票对象已失效"));
        List<String> options = Arrays.asList(vote.getOptions());
        if (options.contains(voter.getOption())) {
            QVoter qVoter = QVoter.voter;
            Voter oldVoter = voterRepository.findOne(qVoter.voteId.eq(voter.getVoteId()).and(qVoter.houseId.eq(voter.getHouseId()))).orElse(null);
            if (oldVoter == null) {
                voter.setCustomerId(cid);
                voterRepository.save(voter);
            } else {
                oldVoter.setCustomerId(cid);
                oldVoter.setOption(voter.getOption());
                oldVoter.setRemark(voter.getRemark());
                voterRepository.save(oldVoter);
            }

            List<DataCount> dataCounts = jpaQueryFactory
                    .select(Projections.constructor(DataCount.class, qVoter.option, qVoter.option.count()))
                    .from(qVoter)
                    .where(qVoter.voteId.eq(voter.getVoteId()))
                    .groupBy(qVoter.option)
                    .fetch();
            StringJoiner result = new StringJoiner(",");
            if (dataCounts.isEmpty()) {
                querydslService.setResult(voter.getVoteId(), "");
            } else {
                for (DataCount dataCount : dataCounts) {
                    String data = String.join(":", dataCount.getKey().toString(), dataCount.getValue().toString());
                    result.add(data);
                    querydslService.setResult(voter.getVoteId(), result.toString());
                }
            }

        } else {
            throw new BusinessException("投票选项不存在");
        }
    }
}