import { orderProductWay, domain, checkToken, wxRequest, orderProductStatus } from '../../../utils/util.js';
Page({
  data: {
    orderProductWay,
    domain,
    orderProductStatus
  },
  onLoad(options) {
    checkToken().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderProducts/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((data) => {
        this.setData({
          ...options,
          orderProduct: data
        });
      });
    })
  },

  clipBoard(e) {
    wx.setClipboardData({
      data: this.data.orderProduct.outTradeNo
    })
  },

  makePhoneCall(e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone
    })
  },

  openLocation(e) {
    const x = e.currentTarget.dataset.x;
    const y = e.currentTarget.dataset.y;
    const name = e.currentTarget.dataset.name;
    wx.openLocation({
      latitude: parseFloat(y),
      longitude: parseFloat(x),
      name: name,
    })
  },

  refund: function (e) {
    checkToken().then(({ authorization, info }) => {
      wx.showModal({
        title: '提示',
        content: '是否退款?',
        complete: (res) => {
          if (res.confirm) {
            wxRequest({
              url: `${domain}/village/orderProducts/${options.id}/refund`,
              header: { authorization, cid: info.cid, mid: info.mid },
              method: 'POST',
              data: this.data.orderProduct
            }).then((data) => {
              console.log(data);
            });
          }
        }
      })
    })
  }
})
