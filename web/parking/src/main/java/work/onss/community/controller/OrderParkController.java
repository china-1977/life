package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.park.OrderPark;
import work.onss.community.domain.park.OrderParkRepository;
import work.onss.community.domain.park.QOrderPark;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class OrderParkController {
    @Autowired
    private OrderParkRepository orderParkRepository;

    @GetMapping(value = {"orderParks/{id}"}, name = "停车费详情")
    public OrderPark detail(@PathVariable String id) {
        return orderParkRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"orderParks"}, name = "停车费列表")
    public Page<OrderPark> page(
            @RequestHeader(name = "mid") String mid,
            @RequestParam Integer year,
            @QuerydslPredicate(bindings = OrderParkRepository.class) Predicate predicate,
            @PageableDefault(sort = {"inDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QOrderPark qOrderPark = QOrderPark.orderPark;
        return orderParkRepository.findAll(qOrderPark.merchantId.eq(mid).and(qOrderPark.inDatetime.year().eq(year)).and(predicate), pageable);
    }

    @PostMapping(value = {"orderParks/export"}, name = "停车费导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = OrderParkRepository.class, root = OrderPark.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QOrderPark qOrderPark = QOrderPark.orderPark;
        Collection<OrderPark> data = (Collection<OrderPark>) orderParkRepository.findAll(qOrderPark.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderPark.class).autoCloseStream(true).build();
        Map<String, List<OrderPark>> floorNumberData = data.stream().collect(Collectors.groupingBy(OrderPark::getParkRegionId));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }

    /**
     * 上次拍摄时间与本次拍摄时间 多长时间内为重复驶入
     * 上次驶入没有驶离：将上次驶入执行驶离，新增本次驶入（记录异常）
     * 本次驶离没有驶入：将本次驶离时间作为驶入时间（记录异常）
     *
     * @param orderPark
     */
    public void in(OrderPark orderPark) {
    }
}