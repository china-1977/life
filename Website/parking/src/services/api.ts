import * as ParkGateController from './Parking/ParkGateController';
import * as FileController from './FileController';
import * as LoginController from './LoginController';
import * as MemberController from './MemberController';
import * as ParkRegionController from './Parking/ParkRegionController';
import * as MerchantController from './MerchantController';
import * as OrderParkController from './Parking/OrderParkController';
import * as OpenController from './Parking/OpenController';
import * as PayOrderController from './Parking/PayOrderController';

export default {
  MerchantController,
  LoginController,
  MemberController,
  FileController,
  ParkGateController,
  ParkRegionController,
  OrderParkController,
  OpenController,
  PayOrderController,
};
