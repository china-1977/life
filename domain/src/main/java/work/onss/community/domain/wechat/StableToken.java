package work.onss.community.domain.wechat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class StableToken {
    private String grant_type;
    private String appid;
    private String secret;
    private Boolean force_refresh;

    public StableToken(String appid, String secret) {
        this.grant_type = "client_credential";
        this.appid = appid;
        this.secret = secret;
        this.force_refresh = false;
    }
}
