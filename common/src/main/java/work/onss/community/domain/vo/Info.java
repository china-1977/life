package work.onss.community.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 授权信息
 *
 * @author wangchanghao
 */
@Data
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Info implements Serializable {
    private String cid;
    private String name;
    private String mid;
    private String shortname;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime lastTime;
    private String openid;


    public Info(String openid, LocalDateTime lastTime) {
        this.openid = openid;
        this.lastTime = lastTime;
    }

    public Info(String cid, String name,String mid,String shortname, LocalDateTime lastTime) {
        this.cid = cid;
        this.name = name;
        this.mid = mid;
        this.shortname = shortname;
        this.lastTime = lastTime;
    }

    public Info(String cid, String name, LocalDateTime lastTime) {
        this.cid = cid;
        this.name = name;
        this.lastTime = lastTime;
    }

    public Info(LocalDateTime lastTime, String cid) {
        this.cid = cid;
        this.lastTime = lastTime;
    }

}
