import { wxLogin, domain, wxRequest, chooseImagesToBase64 } from '../../utils/util.js';

Page({
  data: { domain, },

  chooseImages: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count
    const pictures = this.data[id] == null ? [] : this.data[id];
    count = count - pictures.length;
    chooseImagesToBase64('front', count).then((files) => {
      this.setData({
        [`${id}`]: [...pictures, ...files]
      })
    })
  },

  deletePictures: function (e) {
    const id = e.currentTarget.id;
    const index = e.currentTarget.dataset.index;
    const files = this.data[id];
    files.splice(index, 1);
    this.setData({
      [id]: files
    })
  },
  clearPictues: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: []
    })
  },

  resetForm: function (e) {
    this.setData({
      ...this.data.product
    });
  },

  bindInput: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count;
    const value = e.detail.value;
    if (value.length == count) {
      this.setData({
        [id]: value
      })
    }
  },

  textareaInput: function (e) {
    const id = e.currentTarget.id;
    const value = e.detail.value;
    this.setData({
      [id]: value
    });
  },

  updateProduct: function (e) {
    const { index, description, pictures, id } = this.data;
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/store/products/${id}`,
        method: "PUT",
        data: { ...e.detail.value, description, pictures, id },
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((product) => {
        this.setData({
          ...product,
          product
        });
        let pages = getCurrentPages();//当前页面栈
        let detail = pages[pages.length - 2];//详情页面
        detail.setData({
          product
        });
        let list = pages[pages.length - 3];//列表页面
        const key = `products[${index}]`
        list.setData({
          [key]: product
        });
      }).then(() => {
        wx.navigateBack({
          delta: 1
        });
      });
    });
  },

  onLoad: function (options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/store/products/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((product) => {
        this.setData({
          index: options.index,
          ...product,
          product
        })
      });
    })
  },
})
