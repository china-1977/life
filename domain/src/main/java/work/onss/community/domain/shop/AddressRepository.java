package work.onss.community.domain.shop;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address, String>, QuerydslPredicateExecutor<Address>, QuerydslBinderCustomizer<QAddress> {

    default void customize(QuerydslBindings bindings, QAddress address) {
        bindings.bind(address.name).first((path, s) -> Objects.requireNonNull(path).contains(s));
        bindings.bind(address.insertDate).all(((path, value) -> {
            Iterator<? extends LocalDate> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
    }

    Optional<Address> findByIdAndCustomerId(String id, String customerId);

    @Modifying
    @Transactional
    void deleteByIdAndCustomerId(String id, String customerId);

    List<Address> findByCustomerIdOrderByUpdateDate(String customerId);
}
