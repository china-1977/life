import { IApi } from '@umijs/max';
import routes from './config/routes';
import * as fs from 'fs';

export default (api: IApi) => {
  api.onBeforeCompiler((opts) => {
    let routesString = JSON.stringify(routes);
    routesString = routesString.replaceAll('resources', 'routes');
    fs.writeFileSync('public/resources.json', routesString);
  });
};
