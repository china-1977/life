export const DEFAULT_NAME = 'Umi Max';

export const villageStatusEnum = {
  true: { text: '是' },
  false: { text: '否' },
};

export const houseTypeEnum = {
  ProFormSelect: {
    住宅: '住宅',
    商铺: '商铺',
    公寓: '公寓',
    办公室: '办公室',
  },
  columns: {
    住宅: { text: '住宅' },
    商铺: { text: '商铺' },
    公寓: { text: '公寓' },
    办公室: { text: '办公室' },
  },
};

export const houseCustomerTypeEnum = {
  ProFormSelect: {
    房东: '房东',
    租户: '租户',
  },
  columns: {
    房东: { text: '房东' },
    租户: { text: '租户' },
  },
};

export const ParkSpaceStatusEnum = {
  ProFormSelect: {
    出售: '出售',
    出租: '出租',
    闲置: '闲置',
  },
  columns: {
    出售: { text: '出售' },
    出租: { text: '出租' },
    闲置: { text: '闲置' },
  },
};

export const ParkSpaceStateEnum = {
  ProFormSelect: {
    有车: '有车',
    无车: '无车',
  },
  columns: {
    有车: { text: '有车' },
    无车: { text: '无车' },
  },
};

export const AccessStatusEnum = {
  ProFormSelect: {
    在线: '在线',
    离线: '离线',
  },
  columns: {
    在线: { text: '在线' },
    离线: { text: '离线' },
  },
};

export const DoorStatusEnum = {
  ProFormSelect: {
    在线: '在线',
    离线: '离线',
  },
  columns: {
    在线: { text: '在线' },
    离线: { text: '离线' },
  },
};

export const productStatusEnum = {
  true: { text: '是', status: 'Processing' },
  false: { text: '否', status: 'Default' },
};
export const scoreStatusEnum = {
  WAIT_PAY: {
    text: '待支付',
    status: 'Processing',
  },
  WAIT_PACKAGE: {
    text: '待配货',
    status: 'Default',
  },
  WAIT_DELIVER: {
    text: '待配送',
    status: 'Default',
  },
  WAIT_SIGN: {
    text: '待签收',
    status: 'Default',
  },
  REFUND_SUCCESS: {
    text: '退款成功',
    status: 'Default',
  },
  REFUND_CLOSED: {
    text: '退款关闭',
    status: 'Default',
  },
  REFUND_PROCESSING: {
    text: '退款处理中',
    status: 'Default',
  },
  REFUND_ABNORMAL: {
    text: '退款异常',
    status: 'Default',
  },
  FINISH: { text: '已完成', status: 'Default' },
};

export const OrderHouseStatusEnum = {
  ProFormSelect: {
    WAIT_CONFIRM: '待确认',
    WAIT_PAY: '待支付',
    FINISH: '已支付',
  },
  columns: {
    WAIT_CONFIRM: { text: '待确认' },
    WAIT_PAY: { text: '待支付' },
    FINISH: { text: '已支付' },
  },
};

export const OrderWaterStatusEnum = {
  ProFormSelect: {
    WAIT_CONFIRM: '待确认',
    WAIT_PAY: '待支付',
    FINISH: '已支付',
  },
  columns: {
    WAIT_CONFIRM: { text: '待确认' },
    WAIT_PAY: { text: '待支付' },
    FINISH: { text: '已支付' },
  },
};

export const OrderHeatStatusEnum = {
  ProFormSelect: {
    WAIT_CONFIRM: '待确认',
    WAIT_PAY: '待支付',
    FINISH: '已支付',
  },
  columns: {
    WAIT_CONFIRM: { text: '待确认' },
    WAIT_PAY: { text: '待支付' },
    FINISH: { text: '已支付' },
  },
};

export const IdentityStatusEnum = {
  ProFormSelect: {
    待处理: '待处理',
    已通过: '已通过',
    已拒绝: '已拒绝',
  },
  columns: {
    待处理: { text: '待处理' },
    已通过: { text: '已通过' },
    已拒绝: { text: '已拒绝' },
  },
};
