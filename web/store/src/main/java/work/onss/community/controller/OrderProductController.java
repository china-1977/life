package work.onss.community.controller;


import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.shop.OrderProduct;
import work.onss.community.domain.shop.OrderProductRepository;
import work.onss.community.domain.shop.QOrderProduct;

@Log4j2
@RestController
public class OrderProductController {

    @Autowired
    private OrderProductRepository orderProductRepository;


    @GetMapping(value = {"orderProducts/{id}"}, name = "商品订单详情")
    public OrderProduct orderProduct(@PathVariable String id, @RequestHeader(name = "mid") String mid) {
        return orderProductRepository.findByIdAndStoreId(id, mid).orElse(null);
    }

    @GetMapping(value = {"orderProducts"}, name = "商品订单分页")
    public Page<OrderProduct> search(@RequestHeader(name = "mid") String mid,
                                     @RequestParam(name = "year") Integer year,
                                     @RequestParam(name = "keyword", required = false) String keyword,
                                     @QuerydslPredicate(bindings = OrderProductRepository.class) Predicate predicate,
                                     @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QOrderProduct qOrderProduct = QOrderProduct.orderProduct;

        BooleanExpression booleanExpression = qOrderProduct.storeId.eq(mid).and(qOrderProduct.insertDatetime.year().eq(year));
        if (StringUtils.hasLength(keyword) && !"undefined".equals(keyword)) {
            booleanExpression = Expressions.allOf(booleanExpression,
                    qOrderProduct.userName.contains(keyword).or(qOrderProduct.userPhone.contains(keyword)));
        }
        return orderProductRepository.findAll(booleanExpression.and(predicate), pageable);
    }

}
