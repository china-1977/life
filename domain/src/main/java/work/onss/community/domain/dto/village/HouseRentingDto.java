package work.onss.community.domain.dto.village;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.format.annotation.NumberFormat;
import work.onss.community.domain.village.HouseRenting;

import java.math.BigDecimal;

@Log4j2
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HouseRentingDto extends HouseRenting {

    @NumberFormat(pattern = "#.00", style = NumberFormat.Style.CURRENCY)
    private BigDecimal distance;
    @NumberFormat(pattern = "#.00", style = NumberFormat.Style.CURRENCY)
    private BigDecimal desiredDistance;

    public HouseRentingDto(HouseRenting houseRenting, BigDecimal distance, BigDecimal desiredDistance) {
        super(houseRenting);
        this.distance = distance;
        this.desiredDistance = desiredDistance;
    }

    public HouseRentingDto(HouseRenting houseRenting, BigDecimal distance) {
        super(houseRenting);
        this.distance = distance;
    }
}
