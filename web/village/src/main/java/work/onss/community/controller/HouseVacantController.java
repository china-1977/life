package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.HouseVacant;
import work.onss.community.domain.village.HouseVacantRepository;
import work.onss.community.domain.village.QHouseVacant;

import java.util.Collection;
import java.util.List;

@Log4j2
@RestController
public class HouseVacantController {
    @Autowired
    private HouseVacantRepository houseVacantRepository;

    @GetMapping(value = {"houseVacants/{id}"}, name = "空置房详情")
    public HouseVacant detail(@RequestHeader("mid") String mid, @PathVariable String id) {
        QHouseVacant qHouseVacant = QHouseVacant.houseVacant;
        return houseVacantRepository.findOne(qHouseVacant.id.eq(id).and(qHouseVacant.merchantId.eq(mid))).orElse(null);
    }

    @GetMapping(value = {"houseVacants"}, name = "空置房列表")
    public Page<HouseVacant> page(@RequestHeader("mid") String mid,
                                  @QuerydslPredicate(bindings = HouseVacantRepository.class) Predicate predicate,
                                  @PageableDefault(sort = {"startDate"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QHouseVacant qHouseVacant = QHouseVacant.houseVacant;
        return houseVacantRepository.findAll(qHouseVacant.merchantId.eq(mid).and(predicate), pageable);
    }

    @PostMapping(value = {"houseVacants/checkout"}, name = "空置房-结账")
    public void reply(@RequestHeader("mid") String mid, @RequestBody List<String> ids) {

    }

    @PostMapping(value = {"houseVacants/delete"}, name = "空置房批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        houseVacantRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @PostMapping(value = {"houseVacants/export"}, name = "空置房导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = HouseVacantRepository.class, root = HouseVacant.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QHouseVacant qHouseVacant = QHouseVacant.houseVacant;
        Collection<HouseVacant> data = (Collection<HouseVacant>) houseVacantRepository.findAll(qHouseVacant.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), HouseVacant.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        excelWriter.finish();
    }
}