import { domain, checkToken, wxRequest, orderProductStatus, size, orderProductWay } from '../../../utils/util.js';
Page({
  data: {
    orderProductWay, domain, orderProducts: [], orderProductStatus, year: new Date().getFullYear()
  },

  onLoad: function () {
    checkToken().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderProducts?size=${size}&year=${this.data.year}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then(({ content, number }) => {
        this.setData({
          orderProducts: content, number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    checkToken().then(({ authorization, info }) => {
      const { year, keyword } = this.data;
      wxRequest({
        url: `${domain}/village/orderProducts`,
        data: { size, year, keyword },
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then(({ content, number }) => {
        this.setData({
          orderProducts: content, number
        });
        wx.stopPullDownRefresh()
      });
    });
  },

  onReachBottom: function () {
    checkToken().then(({ authorization, info }) => {
      const { year, number, keyword } = this.data;
      wxRequest({
        url: `${domain}/village/orderProducts`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { size, year, keyword, page: number + 1 },
      }).then(({ content, number }) => {
        if (!content.last)
          this.setData({
            orderProducts: [...this.data.orderProducts, ...content], number
          });
      });
    });
  },

  yearChange: function (e) {
    checkToken().then(({ authorization, info }) => {
      const year = e.detail.value;
      const { keyword } = this.data;
      wxRequest({
        url: `${domain}/village/orderProducts`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { size, year, keyword },
      }).then(({ content, number }) => {
        this.setData({
          orderProducts: content, number, year
        });
      });
    });
  },

  keywordOnChage: function (e) {
    const keyword = e.detail.value;
    checkToken().then(({ authorization, info }) => {
      const year = this.data.year;
      wxRequest({
        url: `${domain}/village/orderProducts`,
        data: { size, year, keyword },
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then(({ content, number }) => {
        this.setData({
          orderProducts: content, number, keyword
        });
      });
    });
  },

  reset: function (e) {
    const year = new Date().getFullYear();
    const keyword = '';
    this.setData({
      year, keyword
    })
    checkToken().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderProducts`,
        data: { size, year, keyword },
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then(({ content, number }) => {
        this.setData({
          orderProducts: content, number, keyword
        });
      });
    });
  }
})