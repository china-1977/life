import { wxLogin, domain, wxRequest, AdviceStatus } from '../../utils/util.js';
Page({

  data: {
    AdviceStatus
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/advices/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((advice) => {
        const status = AdviceStatus.range.findIndex((currentValue) => currentValue.key == advice.status)
        this.setData({
          status,
          advice
        });
      });
    });
  },

  statusChange: function (e) {
    const status = AdviceStatus.range[e.detail.value].key;
    this.setData({
      status: e.detail.value,
      ['advice.status']: status
    })
  },
  adviceSubmit: function (e) {
    const status = AdviceStatus.range[e.detail.value.status].key;
    const remark = e.detail.value.remark;
    const advice = this.data.advice;
    wxLogin().then(({ authorization, info }) => {
      wx.showModal({
        title: '提示',
        content: '是否提交？',
        success: (res) => {
          if (res.confirm) {
            wxRequest({
              url: `${domain}/village/advices`,
              header: { authorization, cid: info.cid, mid: info.mid },
              method: 'POST',
              data: { status, remark, id: advice.id }
            }).then((advice) => {
              wx.showModal({
                title: '提示',
                content: '提交成功',
                showCancel: false,
                success: (res) => {
                  wx.navigateBack({
                    delta: 2
                  });
                }
              })
            });
          }
        }
      })
    });
  }
})