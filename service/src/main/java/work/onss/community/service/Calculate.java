package work.onss.community.service;

import org.springframework.stereotype.Service;
import work.onss.community.domain.park.OrderPark;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.function.Function;

@Service
public class Calculate implements Function<OrderPark, BigDecimal> {
    @Override
    public BigDecimal apply(OrderPark orderPark) {
        Duration duration = Duration.between(orderPark.getInDatetime(), orderPark.getOutDatetime());
        return BigDecimal.valueOf(duration.toHours()).multiply(BigDecimal.valueOf(2));
    }
}
