package work.onss.community.domain.converters;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import work.onss.community.domain.village.HouseIdentity;

public class IdentityStatusConverter implements Converter<HouseIdentity.Status> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return HouseIdentity.Status.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public HouseIdentity.Status convertToJavaData(ReadConverterContext<?> context) {
        for (HouseIdentity.Status status : HouseIdentity.Status.values()) {
            if (status.getValue().equals(context.getReadCellData().getStringValue())) {
                return status;
            }
        }
        return null;
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<HouseIdentity.Status> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }

}
