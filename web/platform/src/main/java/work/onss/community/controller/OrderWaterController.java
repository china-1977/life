package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.OrderWater;
import work.onss.community.domain.village.OrderWaterRepository;
import work.onss.community.domain.village.QOrderWater;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class OrderWaterController {
    @Autowired
    private OrderWaterRepository orderWaterRepository;

    @GetMapping(value = {"orderWaters/{id}"}, name = "水费详情")
    public OrderWater detail(@PathVariable String id) {
        return orderWaterRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"orderWaters"}, name = "水费列表")
    public Page<OrderWater> page(
            @RequestHeader(name = "mid") String mid,
            @RequestParam Integer year,
            @QuerydslPredicate(bindings = OrderWaterRepository.class) Predicate predicate,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QOrderWater qOrderWater = QOrderWater.orderWater;
        return orderWaterRepository.findAll(qOrderWater.merchantId.eq(mid).and(qOrderWater.startDate.year().eq(year)).and(predicate), pageable);
    }

    @PostMapping(value = {"orderWaters/export"}, name = "水费导出")
    public void export(@QuerydslPredicate(bindings = OrderWaterRepository.class, root = OrderWater.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        Collection<OrderWater> data = (Collection<OrderWater>) orderWaterRepository.findAll(predicate);
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderWater.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<OrderWater>> floorNumberData = data.stream().collect(Collectors.groupingBy(OrderWater::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}