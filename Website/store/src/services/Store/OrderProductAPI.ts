import {sort} from '@/utils/format';
import {request} from '@@/exports';

export default {
    page: (params: any, sorter: any, filter: any) =>
        request<any>('/store/orderProducts', {
            params: {
                ...params,
                sort: sort(sorter),
                ...filter,
            },
        }).then((res: any) => {
            return {
                data: res.content,
                total: res.totalElements,
            };
        }),
    updateStatus: async (body: any[], status: boolean) =>
        request<any>(`/store/orderProducts/${status}/updateStatus`, {
            method: 'POST',
            data: body,
        }),
}