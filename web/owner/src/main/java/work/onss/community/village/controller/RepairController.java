package work.onss.community.village.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.QRepair;
import work.onss.community.domain.village.Repair;
import work.onss.community.domain.village.RepairRepository;

@Log4j2
@RestController
public class RepairController {
    @Autowired
    private RepairRepository repairRepository;

    @GetMapping(value = {"repairs/{id}"})
    public Repair detail(@RequestHeader("cid") String cid, @PathVariable String id) {
        QRepair qRepair = QRepair.repair;
        return repairRepository.findOne(qRepair.id.eq(id).and(qRepair.customerId.eq(cid))).orElse(null);
    }

    @GetMapping(value = {"repairs"})
    public Page<Repair> page(@RequestHeader("cid") String cid, @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QRepair qRepair = QRepair.repair;
        return repairRepository.findAll(qRepair.customerId.eq(cid), pageable);
    }

    @PostMapping(value = {"repairs"})
    public void save(@RequestHeader("cid") String cid, @RequestBody Repair repair) {
        repair.setCustomerId(cid);
        repair.setId(null);
        repair.setStatus(Repair.Status.WAIT_ALLOT);
        repairRepository.save(repair);
    }
}