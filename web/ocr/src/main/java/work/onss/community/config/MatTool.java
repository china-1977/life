package work.onss.community.config;

import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OrtEnvironment;
import org.opencv.core.*;
import org.opencv.dnn.Dnn;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * mat 工具类
 */
public class MatTool {

    /**
     * 根据4个点裁剪图像
     *
     * @param image
     * @param plateBox
     * @return
     */
    public static Mat crop(Mat image, LicensePlatePosition plateBox) {
        Mat endM = null;
        Mat startM = null;
        Mat perspectiveTransform = null;
        try {
            List<Point> dest = new ArrayList<>();
            dest.add(new Point(plateBox.getLeftTop().getX(), plateBox.getLeftTop().getY()));
            dest.add(new Point(plateBox.getRightTop().getX(), plateBox.getRightTop().getY()));
            dest.add(new Point(plateBox.getRightBottom().getX(), plateBox.getRightBottom().getY()));
            dest.add(new Point(plateBox.getLeftBottom().getX(), plateBox.getLeftBottom().getY()));

            startM = Converters.vector_Point2f_to_Mat(dest);
            List<Point> ends = new ArrayList<>();
            ends.add(new Point(0, 0));
            ends.add(new Point(plateBox.width(), 0));
            ends.add(new Point(plateBox.width(), plateBox.height()));
            ends.add(new Point(0, plateBox.height()));
            endM = Converters.vector_Point2f_to_Mat(ends);
            perspectiveTransform = Imgproc.getPerspectiveTransform(startM, endM);
            Mat outputMat = new Mat((int) plateBox.height(), (int) plateBox.width(), CvType.CV_8UC4);
            Imgproc.warpPerspective(image, outputMat, perspectiveTransform, new Size((int) plateBox.width(), (int) plateBox.height()), Imgproc.INTER_CUBIC);
            return outputMat;
        } finally {
            if (null != endM) {
                endM.release();
            }
            if (null != startM) {
                startM.release();
            }
            if (null != perspectiveTransform) {
                perspectiveTransform.release();
            }
        }
    }

    /**
     * 对图像进行预处理,并释放原始图片数据
     *
     * @param scale  图像各通道数值的缩放比例
     * @param mean   用于各通道减去的值，以降低光照的影响
     * @param swapRB 交换RB通道，默认为False.
     * @param mat    要处理的图片
     * @return 处理后的图片
     */
    public static Mat blobFromImageAndDoReleaseMat(double scale, Scalar mean, boolean swapRB, Mat mat) {
        return blobFromImage(scale, mean, swapRB, true, mat);
    }

    /**
     * 对图像进行预处理
     *
     * @param scale   图像各通道数值的缩放比例
     * @param mean    用于各通道减去的值，以降低光照的影响
     * @param swapRB  交换RB通道，默认为False.
     * @param release 是否释放参数mat
     * @param mat     要处理的图片
     * @return 处理后的图片
     */
    public static Mat blobFromImage(double scale, Scalar mean, boolean swapRB, boolean release, Mat mat) {
        try {
            Mat dst = Dnn.blobFromImage(mat, scale, new Size(mat.cols(), mat.rows()), mean, swapRB);
            List<Mat> mats = new ArrayList<>();
            Dnn.imagesFromBlob(dst, mats);
            dst.release();
            return mats.get(0);
        } finally {
            if (release) {
                ReleaseTool.release(mat);
            }
        }
    }

    /**
     * 转换为单精度形OnnxTensor,不释放原始图片数据
     *
     * @param firstChannel
     * @return
     */
    public static OnnxTensor to4dFloatOnnxTensorAndNoReleaseMat(float[] std, boolean firstChannel, Mat mat) {
        try {
            return OnnxTensor.createTensor(OrtEnvironment.getEnvironment(), to4dFloatArray(firstChannel, std, false, mat));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 转换为单精度形数组
     *
     * @param firstChannel 通道
     * @param release      是否释放参数mat
     * @param mat          要处理的图片
     * @return
     */
    public static float[][][][] to4dFloatArray(boolean firstChannel, float[] std, boolean release, Mat mat) {
        try {
            int width = mat.cols();
            int height = mat.rows();
            int channel = mat.channels();
            float[][][][] array;
            if (firstChannel) {
                array = new float[1][channel][height][width];
                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        double[] c = mat.get(i, j);
                        for (int k = 0; k < channel; k++) {
                            array[0][k][i][j] = (float) c[k] / std[k];
                        }
                    }
                }
            } else {
                array = new float[1][height][width][channel];
                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        double[] c = mat.get(i, j);
                        for (int k = 0; k < channel; k++) {
                            array[0][i][j][k] = (float) c[k] / std[k];
                        }
                    }
                }
            }
            return array;
        } finally {
            if (release) {
                ReleaseTool.release(mat);
            }
        }
    }

    public static String matToBase64(Mat mat) {
        // 将Mat转换为字节
        MatOfByte bytemat = new MatOfByte();
        Imgcodecs.imencode(".jpg", mat, bytemat);
        byte[] bytes = bytemat.toArray();
        // 将字节转换为Base64字符串
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static boolean saveMatAsImage(Mat mat, String filePath) throws IOException {
        if (mat == null) {
            throw new IllegalArgumentException("Mat object cannot be null");
        }

        if (filePath == null || filePath.isEmpty()) {
            throw new IllegalArgumentException("File path cannot be null or empty");
        }

        try {
            // 将 Mat 对象转换为字节数组
            MatOfByte matOfByte = new MatOfByte();
            Imgcodecs.imencode(".jpg", mat, matOfByte);
            byte[] byteArray = matOfByte.toArray();

            // 使用 Files.write 方法将字节数组写入文件
            Files.write(Paths.get(filePath), byteArray);
        } catch (Exception e) {
            // 记录异常信息
            throw new IOException("Error converting Mat to file: " + e.getMessage(), e);
        }
        return true;
    }

    public static Mat convertMultipartFileToMat(MultipartFile file) throws IOException {
        try (InputStream inputStream = file.getInputStream()) {
            byte[] bytes = file.getBytes();
            MatOfByte matOfByte = new MatOfByte(bytes);
            Mat mat = Imgcodecs.imdecode(matOfByte, Imgcodecs.IMREAD_UNCHANGED);
            if (mat.empty()) {
                throw new IOException("Failed to decode image");
            }
            return mat;
        }
    }

}
