/**
 * 意见列表
 */
import {AdviceStatusEnum} from '@/constants';
import {Advice} from '@/services/AdviceController';
import services from '@/services/api';
import {
    ActionType,
    ModalForm,
    ProFormDependency,
    ProFormDigit,
    ProFormSelect,
    ProFormText,
    ProFormTextArea,
    ProTable
} from '@ant-design/pro-components';
import {Avatar, Button, DatePicker, Dropdown, Form, Image, Popconfirm, Typography} from 'antd';
import {useRef, useState} from 'react';

const {page, setStatus, save} = services.AdviceController;
const {exportData} = services.FileController;
const AdviceList = () => {
    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},

        {title: '姓名', align: 'center', dataIndex: 'name', copyable: true},
        {title: '电话', align: 'center', dataIndex: 'phone', copyable: true},
        {title: '标题', align: 'center', dataIndex: 'title', copyable: true},
        {title: '描述', align: 'center', dataIndex: 'description', copyable: true},
        {
            title: '图片', align: 'center', dataIndex: 'descriptionPictures', copyable: true,
            render: (text: object, record: { descriptionPictures: [string] }) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={record?.descriptionPictures?.map((value, index) => (
                                <Image src={value}/>
                            ))}
                        />
                    }/>
            )
        },
        {title: '备注', align: 'center', dataIndex: 'remark', copyable: true},
        {
            title: '状态',
            align: 'center',
            dataIndex: 'status',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: AdviceStatusEnum.columns,
        },
        {
            title: '创建时间',
            dataIndex: 'insertDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '更新时间',
            dataIndex: 'updateDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, advice: Advice) => (
                <Dropdown.Button
                    key={advice.id}
                    onClick={async () => {
                        form.resetFields();
                        form.setFieldsValue({...advice});
                        setModalVisit('EDIT');
                    }}
                    menu={{
                        items: [
                            {
                                key: '关闭',
                                label: <Popconfirm
                                    title="关闭确认"
                                    description={`是否关闭【${advice.title}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        setStatus('CLOSED', [advice.id]).then(() => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>关闭</Button>
                                </Popconfirm>,
                            },
                            {
                                key: '未解决',
                                label: <Popconfirm
                                    title="未解决确认"
                                    description={`是否通过【${advice.name}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        setStatus('ERROR', [advice.id]).then(() => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button danger>未解决</Button>
                                </Popconfirm>,
                            },
                            {
                                key: '已解决',
                                label: <Popconfirm
                                    title="已解决确认"
                                    description={`是否通过【${advice.name}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        setStatus('SUCCESS', [advice.id]).then(() => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button danger>已解决</Button>
                                </Popconfirm>,
                            },
                            {
                                key: '待处理',
                                label: <Popconfirm
                                    title="待处理确认"
                                    description={`是否待处理【${advice.title}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        setStatus('WAIT', [advice.id]).then(() => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button danger>待处理</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(advice)}}>编辑</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const ref = useRef<ActionType>();
    const [form] = Form.useForm();
    const [modalVisit, setModalVisit] = useState<any>(false);

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        houseId: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params: any, sorter, filter) => page(params, sorter, filter)}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/advices/export', values, '意见.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
            />

            <ModalForm
                title="意见"
                form={form}
                open={modalVisit === 'EDIT'}
                grid={true}
                colProps={{span: 8}}
                onOpenChange={(visible) => {
                    if (visible && modalVisit === 'EDIT') {
                        setModalVisit('EDIT');
                    } else {
                        form.resetFields();
                        setModalVisit(false);
                    }
                }}
                onFinish={async (values) => {
                    values.id = form.getFieldValue('id');
                    return save(values).then((res) => {
                        form.resetFields();
                        ref.current?.reload();
                        return true;
                    });
                }}
            >
                <ProFormText width="sm" name="name" label="姓名" disabled/>
                <ProFormDigit width="sm" name="phone" label="电话" disabled/>
                <ProFormSelect width="sm" name="title" label="标题" disabled/>
                <ProFormDependency style={{width: '100%'}} name={['descriptionPictures']}>
                    {(values, form) => (
                        <Image.PreviewGroup
                            children={values?.descriptionPictures?.map((value: string, index: number) => (
                                <Image width={100} height={100} src={value}/>
                            ))}
                        />
                    )}
                </ProFormDependency>
                <ProFormTextArea colProps={{span: 24}} fieldProps={{autoSize: true}} name={'description'} label="描述"
                                 disabled/>
                <ProFormSelect width="sm" name="status" label="状态" valueEnum={AdviceStatusEnum.ProFormSelect}/>
                <ProFormTextArea colProps={{span: 24}} fieldProps={{autoSize: true}} name={'remark'} label="备注"/>
            </ModalForm>
        </div>
    );
};

export default AdviceList;
