import React, {useEffect, useState} from 'react';
import {Card, Input, Typography} from 'antd';
import {useRequest} from '@@/exports';
import {chat} from '@/services/LoginController';

const {Title} = Typography;

const Chat = () => {
    const [messages, setMessages] = useState<string>('');

    useEffect(() => {
        const eventSource = new EventSource('http://127.0.0.1:7000/ocr/chat');

        eventSource.onmessage = (event) => {
            setMessages(prevMessages => prevMessages.concat(' ').concat(event.data));
        };
        eventSource.onopen = () => {
            console.log('EventSource connected');
        };
        eventSource.onerror = (e) => {
            console.log(e)
        };
        return () => {
            eventSource.close();
        };
    }, []);


    return (
        <Card title={<Title level={3}>Server-Sent Events Chat</Title>}>
            <div>{messages}</div>
            <Input.Search
                placeholder="input search text"
                allowClear
                enterButton="Search"
                size="large"
                onSearch={(value) => {
                    const {run: chatRun} = useRequest(chat, {
                        manual: true,
                    });
                    chatRun({msg: value});
                }}
            />
        </Card>
    );
};

export default Chat;
   