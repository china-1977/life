import { defineConfig } from '@umijs/max';
import routes from './routes';

export default defineConfig({
  define: {
    'process.env.baseURL': 'http://112.241.95.100:7000',
    'process.env.file': 'http://112.241.95.100:7000',
  },
  access: {},
  model: {},
  initialState: {},
  request: {
    dataField: '',
  },
  antd: {},
  layout: {
    title: '服务商平台',
    locale: false,
  },
  hash: true,
  history: { type: 'hash' },
  routes: routes,
  npmClient: 'pnpm',
});
