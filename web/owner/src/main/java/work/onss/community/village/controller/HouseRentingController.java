package work.onss.community.village.controller;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.dto.village.HouseRentingDto;
import work.onss.community.domain.village.HouseRenting;
import work.onss.community.domain.village.HouseRentingRepository;
import work.onss.community.domain.village.QHouseRenting;
import work.onss.community.service.exception.BusinessException;

import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.util.List;

@Log4j2
@RestController
public class HouseRentingController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private HouseRentingRepository houseRentingRepository;

    @GetMapping(value = {"houseRentings/{id}"})
    public HouseRenting detail(@PathVariable String id) {
        HouseRenting houseRenting = houseRentingRepository.findById(id).orElse(null);
        if (houseRenting != null) {
            houseRenting.setFloorNumber(null);
            houseRenting.setUnit(null);
            houseRenting.setRoomNumber(null);
        }
        return houseRenting;
    }

    @GetMapping(value = {"houseRentings/{x}-{y}/near"})
    public Page<HouseRentingDto> page(
            @PathVariable(name = "x") Double x,
            @PathVariable(name = "y") Double y,
            @RequestParam(name = "r", defaultValue = "100000") Double r,
            @RequestParam(required = false) String title,
            @RequestParam(required = false) List<String> modes,
            @RequestParam(required = false) List<String> types,
            @PageableDefault Pageable pageable) {

        BooleanBuilder booleanBuilder = new BooleanBuilder();

        QHouseRenting qHouseRenting = QHouseRenting.houseRenting;
        if (StringUtils.hasLength(title)) {
            booleanBuilder.and(qHouseRenting.title.containsIgnoreCase(title));
        }
        if (modes != null && !modes.isEmpty()) {
            booleanBuilder.and(qHouseRenting.mode.in(modes));
        } else {
            booleanBuilder.and(qHouseRenting.mode.ne(HouseRenting.Mode.ZH.getMessage()));
        }
        if (types != null && !types.isEmpty()) {
            booleanBuilder.and(qHouseRenting.type.in(types));
        }

        SimpleExpression<BigDecimal> distance = Expressions.template(BigDecimal.class, "st_distance(geometry({0}),st_point({1},{2}))", qHouseRenting.location, x, y);
        OrderSpecifier<BigDecimal> distanceOrder = new OrderSpecifier<>(Order.ASC, distance);

        List<HouseRentingDto> houseRentingDtos = jpaQueryFactory.select(Projections.constructor(HouseRentingDto.class,
                        qHouseRenting,
                        distance.as("distance")
                )).from(qHouseRenting)
                .where(qHouseRenting.customerId.isNotNull(),
                        Expressions.predicate(Ops.LOE, distance, Expressions.constant(r)),
                        booleanBuilder
                )
                .orderBy(distanceOrder)
                .limit(pageable.getPageSize())
                .offset(pageable.getOffset())
                .fetch();

        return new PageImpl<>(houseRentingDtos, pageable, houseRentingDtos.size());
    }

    @GetMapping(value = {"houseRentings/{id}/supersede"})
    public Page<HouseRentingDto> page(@PathVariable String id, @RequestParam(name = "r", defaultValue = "100000") Double r, @PageableDefault Pageable pageable) {
        HouseRenting houseRenting = houseRentingRepository.findById(id).orElseThrow(() -> new BusinessException("房屋发布信息不存在"));
        QHouseRenting qHouseRenting = QHouseRenting.houseRenting;

        Point2D.Double desiredPosition = houseRenting.getDesiredPosition();
        Point2D.Double location = houseRenting.getLocation();

        SimpleExpression<BigDecimal> distance = Expressions.template(BigDecimal.class, "st_distance(geometry({0}),st_point({1},{2}))", qHouseRenting.location, desiredPosition.x, desiredPosition.y);
        SimpleExpression<BigDecimal> desiredDistance = Expressions.template(BigDecimal.class, "st_distance(geometry({0}),st_point({1},{2}))", qHouseRenting.desiredPosition, location.x, location.y);

        OrderSpecifier<BigDecimal> distanceOrder = new OrderSpecifier<>(Order.ASC, distance);
        OrderSpecifier<BigDecimal> desiredDistanceOrder = new OrderSpecifier<>(Order.ASC, desiredDistance);

        List<HouseRentingDto> houseRentingDtos = jpaQueryFactory.select(Projections.constructor(HouseRentingDto.class,
                        qHouseRenting,
                        distance.as("distance"),
                        desiredDistance.as("desiredDistance")
                )).from(qHouseRenting)
                .where(qHouseRenting.customerId.isNotNull(),
                        qHouseRenting.mode.eq(HouseRenting.Mode.ZH.getMessage()),
                        Expressions.predicate(Ops.LOE, distance, Expressions.constant(r)),
                        Expressions.predicate(Ops.LOE, desiredDistance, Expressions.constant(r))
                )
                .orderBy(distanceOrder, desiredDistanceOrder)
                .limit(pageable.getPageSize())
                .offset(pageable.getOffset())
                .fetch();

        return new PageImpl<>(houseRentingDtos, pageable, houseRentingDtos.size());
    }


}