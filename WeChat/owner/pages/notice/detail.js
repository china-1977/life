Page({
  mixins: [require('../../mixin/common')],
  data: {
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/notices/${options.id}`,
        header: { authorization },
      }).then((notice) => {
        this.setData({
          notice
        });
      });
    });
  },
})