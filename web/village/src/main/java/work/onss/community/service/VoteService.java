package work.onss.community.service;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import work.onss.community.domain.dto.village.DataCount;
import work.onss.community.domain.village.QVoter;

import java.util.List;
import java.util.StringJoiner;

@Log4j2
@Service
public class VoteService {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private QuerydslService querydslService;

    @Async("executorA")
    public void syncResult(String id, String mid) {
        QVoter qVoter = QVoter.voter;
        List<DataCount> dataCounts = jpaQueryFactory
                .select(Projections.constructor(DataCount.class, qVoter.option, qVoter.option.count()))
                .from(qVoter)
                .where(qVoter.voteId.eq(id), qVoter.merchantId.eq(mid))
                .groupBy(qVoter.option)
                .fetch();
        StringJoiner result = new StringJoiner(",");
        if (dataCounts.isEmpty()) {
            querydslService.setResult(id, "");
        } else {
            for (DataCount dataCount : dataCounts) {
                String data = String.join(":", dataCount.getKey().toString(), dataCount.getValue().toString());
                result.add(data);
                querydslService.setResult(id, result.toString());
            }
        }
    }
}
