import { request } from '@@/exports';

export const applications = (params: any) =>
  request<any>('/platform/applications', {
    params,
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });
