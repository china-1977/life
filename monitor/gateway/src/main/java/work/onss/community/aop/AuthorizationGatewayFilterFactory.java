package work.onss.community.aop;


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.config.SystemConfig;
import work.onss.community.domain.vo.Code;
import work.onss.community.domain.vo.Info;
import work.onss.community.domain.vo.Work;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
@Log4j2
public class AuthorizationGatewayFilterFactory extends AbstractGatewayFilterFactory<AuthorizationGatewayFilterFactory.Config> {

    @Autowired
    private SystemConfig systemConfig;

    public AuthorizationGatewayFilterFactory() {
        super(Config.class);
    }


    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            ServerHttpRequest oldRequest = exchange.getRequest();
            ServerHttpResponse response = exchange.getResponse();
            String url = oldRequest.getURI().getPath();
            List<String> excludePatterns = config.getExcludePatterns();
            AntPathMatcher antPathMatcher = new AntPathMatcher();
            for (String pattern : excludePatterns) {
                if (antPathMatcher.match(pattern, url)) {
                    return chain.filter(exchange.mutate().request(oldRequest).build());
                }
            }
            HttpHeaders headers = oldRequest.getHeaders();
            String authorization = headers.getFirst("authorization");
            Code sessionExpire = Code.SESSION_EXPIRE;
            Work<Object> work = new Work<>(sessionExpire.name(), sessionExpire.getValue(), null);
            String s = JacksonUtils.writeValueAsString(work);
            if (StringUtils.hasLength(authorization)) {
                Algorithm algorithm = Algorithm.HMAC256(systemConfig.getSecret());
                com.auth0.jwt.JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                try {
                    jwtVerifier.verify(authorization);
                } catch (JWTVerificationException e) {
                    response.setStatusCode(HttpStatus.UNAUTHORIZED);
                    DataBuffer wrap = response.bufferFactory().wrap(s.getBytes(StandardCharsets.UTF_8));
                    return response.writeWith(Mono.just(wrap));
                }
                DecodedJWT decode = JWT.decode(authorization);
                String subject = decode.getSubject();
                Info info = JacksonUtils.readValue(subject, Info.class);
                ServerHttpRequest request = oldRequest.mutate().headers((httpHeaders) -> {
                    if (StringUtils.hasLength(info.getCid())) {
                        httpHeaders.set("cid", info.getCid());
                    }
                    if (StringUtils.hasLength(info.getMid())) {
                        httpHeaders.set("mid", info.getMid());
                    }
                    if (StringUtils.hasLength(info.getOpenid())) {
                        httpHeaders.set("openid", info.getOpenid());
                    }
                    if (!decode.getAudience().isEmpty()) {
                        httpHeaders.set("source", decode.getAudience().getFirst());
                    }
                }).build();
                return chain.filter(exchange.mutate().request(request).build());
            } else {
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                DataBuffer wrap = response.bufferFactory().wrap(s.getBytes());
                return response.writeWith(Mono.just(wrap));
            }

        };
    }

    @Data
    public static class Config {
        private List<String> excludePatterns;
    }
}
