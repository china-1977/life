import { wxLogin, domain, wxRequest, RepairStatus, chooseImagesToBase64 } from '../../utils/util.js';

Page({
  data: {
    domain,
    windowWidth: wx.getSystemInfoSync().windowWidth,
    RepairStatus
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/repairs/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((repair) => {
        this.setData({
          repair
        });
      });
    });
  },

  chooseImages: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count
    const pictures = this.data[id] == null ? [] : this.data[id];
    count = count - pictures.length;
    chooseImagesToBase64('front', count).then((files) => {
      this.setData({
        [`${id}`]: [...pictures, ...files]
      })
    })
  },

  deletePictures: function (e) {
    const index = e.currentTarget.dataset?.index;
    const id = e.currentTarget.id;
    if (index) {
      let replyPictures = this.data.replyPictures;
      replyPictures.splice(index, 1);
      this.setData({
        [id]: replyPictures
      })
    } else {
      this.setData({
        [id]: []
      })
    }
  },

  statusChange: function (e) {
    const status = RepairStatus.range[e.detail.value];
    this.setData({
      status
    })
  },

  reply: function (e) {
    const reply = e.detail.value.reply;
    const index = e.detail.value.status;
    let { replyPictures, repair } = this.data;
    if (index && reply && replyPictures) {
      const status = RepairStatus.range[index].key;
      wxLogin().then(({ authorization, info }) => {
        wx.showModal({
          title: '提示',
          content: '是否确定？',
          success: (res) => {
            if (res.confirm) {
              wxRequest({
                url: `${domain}/village/repairs/reply`,
                header: { authorization, cid: info.cid, mid: info.mid },
                data: { id: repair.id, reply, replyPictures, status },
                method: "POST",
              }).then(() => {
                wx.showModal({
                  title: '提示',
                  content: '操作成功',
                  showCancel: false,
                  success: (res) => {
                    wx.navigateBack({
                      delta: 2
                    });
                  }
                })
              })
            }
          }
        })
      });
    } else {
      wx.showModal({
        title: '提示',
        content: '请完善信息',
        showCancel: false,
      })
    }
  }
})