import services from '@/services/api';
import { useRequest } from '@@/exports';
import { ActionType, ProTable } from '@ant-design/pro-components';
import {Dropdown, Form, Input, Modal, Select, Typography, message, DatePicker, Button} from 'antd';
import { useRef, useState } from 'react';

const { getMembers, delMember, getApplications, updateApplications } = services.MemberController;

const MemberList = () => {
  const actionRef = useRef<ActionType>();
  const [form] = Form.useForm();
  const [modalVisible, setModalVisible] = useState<any>(false);
  const [applications, setApplications] = useState<any>([]);
  let { loading: getApplicationsLoading, run: getApplicationsRun } = useRequest(getApplications, { manual: true });
  let { loading: delMemberLoading, run: delMemberRun } = useRequest(delMember, {
    manual: true,
  });
  let { loading: updateApplicationsLoading, run: updateApplicationsRun } = useRequest(updateApplications, { manual: true });

  const columns: any = [
    {
      title: '序号',
      dataIndex: 'index',
      align: 'center',
      valueType: 'indexBorder',
    },
    { title: '主键', dataIndex: 'id', align: 'center', copyable: true },
    { title: '姓名', dataIndex: 'name', align: 'center', copyable: true },
    { title: '身份证号', dataIndex: 'idCard', align: 'center', copyable: true },
    { title: '手机号', dataIndex: 'phone', align: 'center', copyable: true },
    { title: '账号', dataIndex: 'username', align: 'center', copyable: true },
    {
      title: '注册时间',
      dataIndex: 'insertDate',
      align: 'center',
      copyable: true,
      fieldProps: { format: 'YYYY-MM-DDTHH:mm:ss' },
      sorter: { multiple: 1 },
      onSorter: false,
      renderFormItem: () => <DatePicker.RangePicker showTime />,
    },
    {
      title: '更新日期',
      dataIndex: 'updateDate',
      align: 'center',
      copyable: true,
      fieldProps: { format: 'YYYY-MM-DDTHH:mm:ss' },
      sorter: { multiple: 1 },
      onSorter: false,
      renderFormItem: () => <DatePicker.RangePicker showTime />,
    },
    { title: 'openid', dataIndex: 'openid', align: 'center', copyable: true },
    {
      title: '操作',
      valueType: 'option',
      align: 'center',
      render: (text: any, record: any) => (
        <Dropdown.Button
          key={record.id}
          loading={getApplicationsLoading || delMemberLoading}
          menu={{
            items: [
              {
                key: 'AUTHORIZE',
                label: '授权',
                onClick: () => {
                  getApplicationsRun(record.id)
                    .then((res: any) => {
                      let applicationsId = [];
                      let newApplications: { label: any; value: any }[] = [];
                      for (const item of res) {
                        if (item.merchantCustomerApplication) {
                          applicationsId.push(item.application.id);
                        }
                        newApplications.push({
                          label: item.application.name,
                          value: item.application.id,
                        });
                      }
                      setApplications(newApplications);
                      return applicationsId;
                    })
                    .then((applicationsId) => {
                      form.setFieldsValue({ ...record, applicationsId });
                      setModalVisible('AUTHORIZE');
                    });
                },
              },
              {
                key: 'DEL',
                label: '删除',
                onClick: () => {
                  delMemberRun(record.id).then(() => {
                    message.success('删除成功').then(() => {
                      actionRef.current?.reload();
                    });
                  });
                },
              },
            ],
          }}
        >
          <Typography.Text
            copyable={{
              text: JSON.stringify(record, ['id', 'name', 'idCard', 'phone', 'openid', 'insertDate', 'updateDate']),
            }}
          >
            操作
          </Typography.Text>
        </Dropdown.Button>
      ),
    },
  ];
  return (
    <div>
      <ProTable actionRef={actionRef} rowKey="id" headerTitle="查询表格" columns={columns} request={(params: any) => getMembers(params)} search={{ defaultCollapsed: false }} />
    </div>
  );
};

export default MemberList;
