package work.onss.community.domain.dto.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.MerchantCustomer;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MerchantCustomerDto {
    private Merchant merchant;
    private Customer customer;
    private MerchantCustomer merchantCustomer;
}
