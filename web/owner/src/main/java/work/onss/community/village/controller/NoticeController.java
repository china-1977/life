package work.onss.community.village.controller;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.village.Notice;
import work.onss.community.domain.village.NoticeRepository;
import work.onss.community.domain.village.QNotice;
import work.onss.community.domain.village.relational.QHouseCustomer;

import java.util.List;

@Log4j2
@RestController
public class NoticeController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private NoticeRepository noticeRepository;

    @GetMapping(value = {"notices/{id}"})
    public Notice detail(@PathVariable String id) {
        return noticeRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"notices"})
    public Page<Notice> page(@RequestHeader("cid") String cid, @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        List<String> mids = jpaQueryFactory.selectDistinct(qHouseCustomer.merchantId)
                .from(qHouseCustomer)
                .where(qHouseCustomer.customerId.eq(cid))
                .fetch();
        if (mids.isEmpty()) {
            return null;
        } else {
            QNotice qNotice = QNotice.notice;
            return noticeRepository.findAll(qNotice.merchantId.in(mids), pageable);
        }
    }
}