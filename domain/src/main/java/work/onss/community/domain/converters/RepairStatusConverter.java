package work.onss.community.domain.converters;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import work.onss.community.domain.village.Repair;

public class RepairStatusConverter implements Converter<Repair.Status> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return Repair.Status.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Repair.Status convertToJavaData(ReadConverterContext<?> context) {
        for (Repair.Status status : Repair.Status.values()) {
            if (status.getValue().equals(context.getReadCellData().getStringValue())) {
                return status;
            }
        }
        return null;
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Repair.Status> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }

}
