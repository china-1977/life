package work.onss.community.domain.converters;

import jakarta.persistence.Converter;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.StandardConverter;
import org.hibernate.type.descriptor.java.JavaType;
import work.onss.community.domain.config.AESUtils;
import work.onss.community.domain.config.SystemConfig;

import java.util.Optional;

@Converter
public class SensitiveFieldConverter implements StandardConverter<String, String> {

    private final SystemConfig systemConfig;

    public SensitiveFieldConverter(SystemConfig systemConfig) {
        this.systemConfig = systemConfig;
    }

    @Override
    public String convertToDatabaseColumn(String attribute) {
        return this.toRelationalValue(attribute);
    }

    @Override
    public String convertToEntityAttribute(String dbData) {
        return this.toDomainValue(dbData);
    }

    @Override
    public String toDomainValue(String s) {
        return Optional.ofNullable(s).map(d -> {
            try {
                return AESUtils.decrypt(d, systemConfig.getAccessKey());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).orElse(null);
    }

    @Override
    public String toRelationalValue(String s) {
        return Optional.ofNullable(s).map(a -> {
            try {
                return AESUtils.encrypt(a, systemConfig.getAccessKey());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).orElse(null);
    }

    @Override
    public JavaType<String> getDomainJavaType() {
        return StandardBasicTypes.STRING.getConverter().getDomainJavaType();
    }

    @Override
    public JavaType<String> getRelationalJavaType() {
        return StandardBasicTypes.STRING.getConverter().getDomainJavaType();
    }
}