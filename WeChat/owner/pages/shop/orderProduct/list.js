Page({
  mixins: [require('../../../mixin/common')],
  data: {
    orderProducts: []
  },

  onLoad: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderProducts?size=${this.data.size}&year=${this.data.year}`,
        header: { authorization },
      }).then(({ content, number }) => {
        this.setData({
          orderProducts: content, number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderProducts?size=${this.data.size}&year=${this.data.year}`,
        header: { authorization },
      }).then(({ content, number }) => {
        this.setData({
          orderProducts: content, number
        });
        wx.stopPullDownRefresh()
      });
    });
  },

  onReachBottom: function () {
    this.wxLogin().then(({ authorization }) => {
      const number = this.data.number + 2;
      this.wxRequest({
        url: `${this.data.domain}/owner/orderProducts?page=${number}&size=${this.data.size}&year=${this.data.year}`,
        header: { authorization },
      }).then(({ content, number }) => {
        if (!content.last)
          this.setData({
            orderProducts: [...this.data.orderProducts, ...content], number
          });
      });
    });
  },

  bindDateChange: function (e) {
    const year = e.detail.value;
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderProducts?size=${this.data.size}&year=${year}`,
        header: { authorization },
      }).then(({ content, number }) => {
        this.setData({
          orderProducts: content, number, year
        });
      });
    });
  }
})