import React from 'react';
import { history, useModel, useRequest } from '@@/exports';
import { login } from '@/services/LoginController';
import { LoginForm, ProFormText } from '@ant-design/pro-components';
import { LockOutlined, UserOutlined } from '@ant-design/icons';

const Login: React.FC = () => {
  const { refresh } = useModel('@@initialState');
  let { loading: loginLoading, run: loginRun } = useRequest(login, {
    manual: true,
  });
  const onFinish = (values: any) => {
    loginRun(values)
      .then((res: any) => {
        if(res.info.shortname){
            localStorage.setItem('shortname', res.info.shortname);
        }
        localStorage.setItem('name', res.info.name);
        localStorage.setItem('lastTime', res.info.lastTime);
        localStorage.setItem('authorization', res.authorization);
        refresh();
      })
      .then(() => {
        history.push('/');
      });
  };

  return (
    <div>
      <div
        style={{
          flex: '1',
          padding: '300px 0',
        }}
      >
        <LoginForm
          contentStyle={{
            minWidth: 280,
            maxWidth: '75vw',
          }}
          logo={<img alt="logo" src="/logo2.png" />}
          title={'社区平台'}
          subTitle={'房屋、车位、门禁、道闸、业主服务、停车服务'}
          onFinish={async (values) => {
            onFinish(values);
          }}
        >
          <ProFormText
            name="username"
            initialValue={'wangchangkang'}
            fieldProps={{
              size: 'large',
              prefix: <UserOutlined />,
            }}
            placeholder={'请输入登录账号'}
            rules={[
              {
                required: true,
                message: '请输入用户名!',
              },
            ]}
          />
          <ProFormText.Password
            name="password"
            initialValue={'123456'}
            fieldProps={{
              size: 'large',
              prefix: <LockOutlined />,
            }}
            placeholder={'请输入6-12密码'}
            rules={[
              {
                required: true,
                message: '请输入密码！',
              },
            ]}
          />

        </LoginForm>
      </div>
    </div>
  );
};

export default Login;
