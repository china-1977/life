import { wxLogin, domain, wxRequest,status } from '../../utils/util.js';

Page({
  data: {
    domain,status
  },
  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/owner/orderHeats/${options.id}`,
        header: { authorization, cid: info.cid },
      }).then((orderHeat) => {
        this.setData({
          orderHeat
        });
      });
    });
  },

  pay: function (e) {
    wxLogin().then(({ authorization, info }) => {
      const orderHeat = this.data.orderHeat;
      wxRequest({
        url: `${domain}/owner/orderHeats/${orderHeat.id}/pay`,
        header: { authorization, cid: info.cid, openid: info.openid },
        method: 'POST',
      }).then((orderHeat) => {
        this.setData({
          orderHeat
        });
      })
    })
  },
})