import {sort} from '@/utils/format';
import {request} from '@@/exports';

export default {
    page: (params: any, sorter: any, filter: any) =>
        request<any>('/village/productPanels', {
            params: {
                ...params,
                sort: sort(sorter),
                ...filter,
            },
        }).then((res: any) => {
            return {
                data: res.content,
                total: res.totalElements,
            };
        }),
    all: () =>
        request<any>('/village/productPanels/all'),
    save: async (body: any) =>
        request<any>('/village/productPanels/save', {
            method: 'POST',
            data: body,
        }),
    del: async (body: any[]) =>
        request<any>('/village/productPanels/delete', {
            method: 'POST',
            data: body,
        }),
    panel: async (body: any[]) =>
        request<any>('/village/products/panel', {
            method: 'POST',
            data: body,
        }),
}