package work.onss.community.domain.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class JacksonUtils {

    private final static ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper().findAndRegisterModules();
    }

    public static String writeValueAsString(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T readValue(String text, Class<T> clazz) {
        try {
            return objectMapper.readValue(text, clazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T readValue(String text, TypeReference<T> valueTypeRef) {
        try {
            return objectMapper.readValue(text, valueTypeRef);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
