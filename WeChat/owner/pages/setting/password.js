

Page({
  mixins: [require('../../mixin/common')],
  data: {
  },

  changeLoginAccount(e) {
    this.wxLogin().then(({ authorization }) => {
      wx.showModal({
        title: '提示',
        content: '是否设置账号？',
        success: (res) => {
          if (res.confirm) {
            this.wxRequest({
              url: `${this.data.domain}/account/customers/loginAccount`,
              header: { authorization },
              data: { username: e.detail.value.username, password: [e.detail.value.password, e.detail.value.confirmPassword] },
              method: "POST",
            }).then((res) => {
              wx.showModal({
                title: '提示',
                content: '设置成功',
                showCancel: false,
                success: (res) => {
                  wx.navigateBack({
                    delta: 1
                  });
                }
              })
            });
          }
        }
      })
    })
  },
})