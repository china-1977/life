package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import work.onss.community.domain.account.MerchantRepository;
import work.onss.community.domain.village.OrderHouse;
import work.onss.community.domain.village.OrderHouseRepository;
import work.onss.community.domain.village.PayOrder;
import work.onss.community.domain.village.QOrderHouse;
import work.onss.community.service.exception.BusinessException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class OrderHouseController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private OrderHouseRepository orderHouseRepository;
    @Autowired
    private MerchantRepository merchantRepository;

    @GetMapping(value = {"orderHouses/{id}"}, name = "物业费详情")
    public OrderHouse detail(@PathVariable String id) {
        return orderHouseRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"orderHouses"}, name = "物业费列表")
    public Page<OrderHouse> page(
            @RequestHeader(name = "mid") String mid,
            @RequestParam Integer year,
            @QuerydslPredicate(bindings = OrderHouseRepository.class, root = OrderHouse.class) Predicate predicate,
            @PageableDefault(sort = {"startDate"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QOrderHouse qOrderHouse = QOrderHouse.orderHouse;
        return orderHouseRepository.findAll(qOrderHouse.merchantId.eq(mid).and(qOrderHouse.startDate.year().eq(year)).and(predicate), pageable);
    }

    @DeleteMapping(value = {"orderHouses"}, name = "物业费批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        List<OrderHouse> orderHouses = orderHouseRepository.findAllById(ids);
        for (OrderHouse orderHouse : orderHouses) {
            if (!orderHouse.getStatus().equals(PayOrder.Status.WAIT_CONFIRM)) {
                StringJoiner message = new StringJoiner(",");
                message.add(orderHouse.getFloorNumber());
                message.add(orderHouse.getUnit());
                message.add(orderHouse.getRoomNumber());
                message.add(orderHouse.getStatus().getMessage());
                message.add("无法删除");
                throw new BusinessException(message.toString());
            }
        }
        orderHouseRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @Transactional
    @PostMapping(value = {"orderHouses/import"}, name = "物业费导入")
    public void importWater(@RequestHeader(name = "mid") String mid, @RequestParam(value = "file") MultipartFile file) throws Exception {
        ExcelReader excelReader = EasyExcel.read(file.getInputStream()).build();
        List<ReadSheet> readSheets = excelReader.excelExecutor().sheetList();
        for (ReadSheet readSheet : readSheets) {
            List<OrderHouse> orderHouses = EasyExcel.read(file.getInputStream()).head(OrderHouse.class).sheet(readSheet.getSheetNo()).doReadSync();
            for (OrderHouse next : orderHouses) {
                next.setMerchantId(mid);
                if (next.getId() != null) {
                    orderHouseRepository.findById(next.getId()).ifPresentOrElse(orderHouse -> {
                        if (!orderHouse.getMerchantId().equals(mid)) {
                            throw new BusinessException("物业费主键错误:".concat(orderHouse.getId()));
                        }
                    }, () -> next.setId(null));
                }
                orderHouseRepository.save(next);
            }
        }
    }

    @Transactional
    @PostMapping(value = {"orderHouses/confirm"}, name = "物业费确认")
    public void confirm(@RequestHeader(name = "mid") String mid, @RequestBody OrderHouse orderHouse) {
        QOrderHouse qOrderHouse = QOrderHouse.orderHouse;
        jpaQueryFactory.update(qOrderHouse)
                .set(qOrderHouse.status, PayOrder.Status.WAIT_PAY)
                .where(
                        qOrderHouse.merchantId.eq(mid),
                        qOrderHouse.startDate.eq(orderHouse.getStartDate().withDayOfMonth(1)),
                        qOrderHouse.endDate.eq(orderHouse.getEndDate().withDayOfMonth(1)),
                        qOrderHouse.status.eq(PayOrder.Status.WAIT_CONFIRM))
                .execute();
    }

    @PostMapping(value = {"orderHouses/export"}, name = "物业费导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = OrderHouseRepository.class, root = OrderHouse.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QOrderHouse qOrderHouse = QOrderHouse.orderHouse;
        Collection<OrderHouse> data = (Collection<OrderHouse>) orderHouseRepository.findAll(qOrderHouse.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderHouse.class).autoCloseStream(true).build();
        Map<String, List<OrderHouse>> floorNumberData = data.stream().collect(Collectors.groupingBy(OrderHouse::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}