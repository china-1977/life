package work.onss.community.domain.wechat;

import lombok.Data;

import java.io.Serializable;

@Data
public class WXLogin implements Serializable {
    private String appid;
    private String code;
}
