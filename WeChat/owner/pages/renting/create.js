Page({
  mixins: [require('../../mixin/common')],
  data: {
    housesPicker: [], housesPickerIndex: -1, descriptionPictures: [], point: {},
  },

  onLoad(options) {
    const pages = getCurrentPages();
    const prevPage = pages[pages.length - 2];//上一页面
    const house = prevPage.data.house;
    this.setData({
      house
    });
  },

  bindModeChange: function (e) {
    this.setData({
      mode: this.data.rentingMode[e.detail.value]
    })
  },

  chooseImages: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count
    const pictures = this.data[id] == null ? [] : this.data[id];
    count = count - pictures.length;
    this.chooseImagesToBase64('front', count).then((files) => {
      this.setData({
        [`${id}`]: [...pictures, ...files]
      })
    })
  },

  deletePictures: function (e) {
    const index = e.currentTarget.dataset?.index;
    const id = e.currentTarget.id;
    if (index) {
      let descriptionPictures = this.data.descriptionPictures;
      descriptionPictures.splice(index, 1);
      this.setData({
        [id]: descriptionPictures
      })
    } else {
      this.setData({
        [id]: []
      })
    }
  },

  create: function (e) {
    this.wxLogin().then(({ authorization }) => {
      wx.showModal({
        title: '提示',
        content: '是否发布？',
        success: (res) => {
          if (res.confirm) {
            const house = this.data.house;
            const point = this.data.point;
            const descriptionPictures = this.data.descriptionPictures;
            this.wxRequest({
              url: `${this.data.domain}/owner/houses/renting`,
              data: {
                ...e.detail.value, id: house.id, descriptionPictures,
                desiredPosition: { x: point.longitude, y: point.latitude },
                desiredName: point?.name,
                desiredAddress: point?.address,
              },
              method: "POST",
              header: { authorization },
            }).then(() => {
              wx.showModal({
                title: '提示',
                content: '发布成功',
                showCancel: false,
                success: (res) => {
                  wx.navigateBack({
                    delta: 1
                  });
                }
              })
            })
          }
        }
      })
    })
  },

  chooseLocation: function () {
    wx.authorize({
      scope: 'scope.userLocation',
      success: (res) => {
        wx.chooseLocation({
          latitude: this.data.point.latitude,
          longitude: this.data.point.longitude,
          success: (point) => {
            if (point.address || point.name) {
              this.setData({ point });
            } else {
              wx.showModal({
                title: '提示',
                content: '请选择期望位置',
                confirmColor: '#e64340',
                showCancel: false,
              })
            }
          },

        })
      },
      fail: (res) => {
        wx.showModal({
          title: '提示',
          content: '请允许小程序使用位置消息',
          confirmColor: '#e64340',
          showCancel: false,
          success: () => {
            wx.openSetting();
          }
        })
      }
    })
  },

})