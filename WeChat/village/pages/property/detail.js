import { wxLogin, domain, wxRequest,PayOrderstatus } from '../../utils/util.js';

Page({
  data: {
    domain,PayOrderstatus
  },
  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderHouses/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((orderHouse) => {
        this.setData({
          orderHouse
        });
      });
    });
  },
})