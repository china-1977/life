package work.onss.community.domain.vo;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import work.onss.community.domain.shop.OrderProduct;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfirmScore implements Serializable {
    @NotBlank(message = "请输入联系姓名")
    private String userName;
    @NotBlank(message = "请输入联系电话")
    private String userPhone;
    @NotBlank(message = "缺少微信服务商APPID")
    private String subAppId;
    @NotEmpty(message = "商品不能为空")
    private List<String> cart;
    @NotNull(message = "请选择购买方式")
    @Enumerated(value = EnumType.STRING)
    private OrderProduct.Way way;

    @NotNull(message = "缺少商户ID")
    private String villageId;

    private String code;

    private String userAddressName;
    private String userAddress;
    private String userAddressDetail;
    private Point2D.Double userAddressPoint;
}
