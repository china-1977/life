/**
 * 商户列表
 */
import { villageStatusEnum } from '@/constants';
import services from '@/services/api';
import { replacer } from '@/utils/format';
import { history, useRequest } from '@@/exports';
import { ProTable } from '@ant-design/pro-components';
import { Avatar, Dropdown, Image, Typography } from 'antd';

const file = process.env.file;

const { getVillages, bind } = services.VillageController;

const VillageList = () => {
  let { loading: bindLoading, run: bindRun } = useRequest(bind, {
    manual: true,
  });

  const columns: any = [
    {
      title: '序号',
      dataIndex: 'index',
      valueType: 'indexBorder',
      align: 'center',
    },
    { title: '主键', dataIndex: 'id', copyable: true },
    {
      title: 'LOGO',
      dataIndex: 'trademark',
      align: 'center',
      render: (text: string, record: any) => <Avatar shape={'square'} src={<Image src={file?.concat(text)} />} />,
    },
    { title: '简称', dataIndex: 'shortname', copyable: true },
    {
      title: '营业时间',
      copyable: true,
      align: 'center',
      render: (text: string, record: any) => <Typography.Text copyable={{ text: [record.openTime, record.closeTime].toString() }}>{[record.openTime, '~', record.closeTime]}</Typography.Text>,
    },
    {
      title: '客服信息',
      copyable: true,
      align: 'center',
      render: (text: string, record: any) => <Typography.Text copyable={{ text: [record.username, record.phone].toString() }}>{[record.username, ':', record.phone]}</Typography.Text>,
    },
    {
      title: '是否经营',
      dataIndex: 'status',
      align: 'center',
      fieldProps: { mode: 'multiple' },
      valueEnum: villageStatusEnum,
    },
    {
      title: '操作',
      valueType: 'option',
      render: (text: any, record: any) => (
        <Dropdown.Button
          key={record.id}
          loading={bindLoading}
          menu={{
            items: [
              {
                key: 'AUTHORIZE',
                label: '授权',
                onClick: () => {
                  bindRun(record.id)
                    .then((res: any) => {
                      localStorage.setItem('info', JSON.stringify(res.info, replacer));
                      localStorage.setItem('authorization', res.authorization);
                      localStorage.setItem('name', res.name);
                    })
                    .then(() => {
                      history.push('/');
                    });
                },
              },
            ],
          }}
        >
          <Typography.Text
            copyable={{
              text: JSON.stringify(record, ['id', 'shortname', 'openTime', 'closeTime', 'username', 'phone', 'status']),
            }}
          >
            操作
          </Typography.Text>
        </Dropdown.Button>
      ),
    },
  ];

  return (
    <div>
      <ProTable pagination={false} rowKey="id" headerTitle="查询表格" columns={columns} request={(params: any) => getVillages(params)} search={false} />
    </div>
  );
};

export default VillageList;
