Page({
  mixins: [require('../../mixin/common')],
  data: {
    villagesIndex: -1,
    villages: [],
    point: {},
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      if (options.id) {
        this.wxRequest({
          url: `${this.data.domain}/owner/parkSpaceIdentitys/${options.id}`,
          header: { authorization },
        }).then((parkSpaceIdentity) => {
          this.setData({
            ...parkSpaceIdentity
          });
        });
      }
    });
  },

  create: function (e) {
    const identity = e.detail.value;
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/parkSpaceIdentitys`,
        data: { ...e.detail.value, id: this.data.id, timeInterval: [identity.startTime, identity.endTime] },
        method: "POST",
        header: { authorization },
      }).then(() => {
        wx.showModal({
          title: '提示',
          content: '申请成功，请耐心等待',
          showCancel: false,
          success: (res) => {
            wx.navigateBack({
              delta: 2
            });
          }
        })
      })
    })
  },

  startDatetimeChange: function (e) {
    this.setData({
      startTime: e.detail.value
    });
  },

  endDatetimeChange: function (e) {
    this.setData({
      endTime: e.detail.value
    });
  }

})