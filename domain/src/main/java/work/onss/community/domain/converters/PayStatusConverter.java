package work.onss.community.domain.converters;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import work.onss.community.domain.village.PayOrder;

public class PayStatusConverter implements Converter<PayOrder.PayStatus> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return PayOrder.PayStatus.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public PayOrder.PayStatus convertToJavaData(ReadConverterContext<?> context) {
        for (PayOrder.PayStatus status : PayOrder.PayStatus.values()) {
            if (status.getMessage().equals(context.getReadCellData().getStringValue())) {
                return status;
            }
        }
        return null;
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<PayOrder.PayStatus> context) {
        return new WriteCellData<>(context.getValue().getMessage());
    }

}
