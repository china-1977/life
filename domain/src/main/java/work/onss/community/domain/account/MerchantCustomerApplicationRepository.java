package work.onss.community.domain.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface MerchantCustomerApplicationRepository extends JpaRepository<MerchantCustomerApplication, String>, JpaSpecificationExecutor<MerchantCustomerApplication>, QuerydslPredicateExecutor<MerchantCustomerApplication>, QuerydslBinderCustomizer<QMerchantCustomerApplication> {

    default void customize(QuerydslBindings bindings, QMerchantCustomerApplication qVillageCustomerApplication) {
        bindings.bind(qVillageCustomerApplication.id).withDefaultBinding();
    }


    Optional<MerchantCustomerApplication> findByApplicationIdAndCustomerIdAndMerchantId(String rid, String cid, String vid);

    @Modifying
    @Transactional
    void deleteByApplicationIdNotInAndCustomerIdAndMerchantId(List<String> rids, String cid, String vid);

    @Modifying
    @Transactional
    void deleteByCustomerIdAndMerchantId(String cid, String vid);
}
