import { request } from '@@/exports';

export const getMembers = (params: any) =>
  request<any>('/account/merchantCustomers', {
    params,
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const delMember = (id: any) => request<any>(`/account/merchantCustomers/${id}`, { method: 'DELETE' });
export const getApplications = (id: any) =>
  request<any>(`/account/merchantCustomers/${id}/applications`, {
    headers: { contextPath: '/village' },
  });
export const updateApplications = (member: any) =>
  request<any>(`/account/merchantCustomers/${member.id}/authorize`, {
    method: 'POST',
    data: member.applicationsId,
  });
