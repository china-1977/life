import { wxLogin, domain, wxRequest, size } from '../../utils/util.js';
Page({

  data: {
    keyword: ''
  },

  searchReset: function (e) {
    console.log(e.detail.value);
    this.setData({
      keyword: ''
    })
  },

  searchSubmit: function (e) {
    const keyword = e.detail.value.keyword
    this.setData({
      keyword
    })

    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/visitors`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { size, keyword }
      }).then((data) => {
        this.setData({
          visitors: data.content, number: data.number
        });
      });
    });

  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/visitors`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { size }
      }).then((data) => {
        this.setData({
          visitors: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      const keyword = this.data.keyword;
      wxRequest({
        url: `${domain}/village/visitors`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { size, keyword }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          visitors: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      const keyword = this.data.keyword;
      wxRequest({
        url: `${domain}/village/visitors`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { page: this.data.number + 2, size, keyword }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let visitors = this.data.visitors;
          visitors.push(...data.content)
          this.setData({
            visitors, number: data.number
          });
        }
      });
    });
  },
})