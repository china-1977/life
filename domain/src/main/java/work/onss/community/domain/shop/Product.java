package work.onss.community.domain.shop;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import work.onss.community.domain.usertype.StringArrayType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;


/**
 * 商品
 *
 * @author wangchanghao
 */
@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@EntityListeners(AuditingEntityListener.class)
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键", index = 0)
    private String id;
    private String merchantId;
    @ExcelProperty(value = "名称", index = 1)
    @NotBlank(message = "商品名称不能为空")
    private String name;
    @ExcelProperty(value = "描述", index = 2)
    @NotBlank(message = "商品描述不能为空")
    private String description;
    @ExcelProperty(value = "单价", index = 3)
    @NotNull(message = "单价不能为空")
    @DecimalMin(value = "0.00", message = "商品单价不能小于{value}元")
    @Digits(fraction = 2, integer = 10, message = "单价小数位不能大于{fraction},整数不能大于{integer}")
    @NumberFormat(pattern = "#.00", style = NumberFormat.Style.CURRENCY)
    private BigDecimal price;
    @ExcelProperty(value = "单位", index = 4)
    @NotBlank(message = "单位不能为空")
    private String priceUnit;
    @ExcelProperty(value = "库存", index = 5)
    private BigInteger stock = BigInteger.ZERO;
    @ExcelProperty(value = "至少购买量", index = 6)
    @NotNull(message = "请填写最小购买数量")
    @DecimalMin(value = "1", message = "最小购买数量不能小于{value}")
    private BigInteger min;
    @ExcelProperty(value = "最多购买量", index = 7)
    @NotNull(message = "请填写最大购买数量")
    @DecimalMin(value = "1", message = "最大购买数量不能小于{value}")
    private BigInteger max;
    @ExcelProperty(value = "标签", index = 8)
    @NotBlank(message = "商品标签不能为空")
    private String label;
    @ExcelProperty(value = "排序", index = 9)
    @NotBlank(message = "排序不能为空")
    private String orderLabel;
    @ExcelProperty(value = "是否上架", index = 10)
    private Boolean status = false;
    @Type(value = StringArrayType.class)
    @NotEmpty(message = "请上传商品图片")
    @Size(min = 1, max = 9, message = "仅限上传{min}-{max}张图片")
    private String[] pictures;
    @ExcelProperty(value = "视频ID", index = 11)
    private String vid;

    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;
    @LastModifiedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateDatetime;
}
