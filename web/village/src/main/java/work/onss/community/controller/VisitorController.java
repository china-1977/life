package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.config.RegexUtils;
import work.onss.community.domain.village.QVisitor;
import work.onss.community.domain.village.Visitor;
import work.onss.community.domain.village.VisitorRepository;

import java.util.Collection;

@Log4j2
@RestController
public class VisitorController {
    @Autowired
    private VisitorRepository visitorRepository;

    @GetMapping(value = {"visitors/{id}"}, name = "访客详情")
    public Visitor detail(@PathVariable String id) {
        return visitorRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"visitors"}, name = "访客列表")
    public Page<Visitor> page(
            @RequestHeader(name = "mid") String mid,
            @RequestParam(required = false) String keyword,
            @QuerydslPredicate(bindings = VisitorRepository.class) Predicate predicate, @PageableDefault(sort = {"visitDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QVisitor qVisitor = QVisitor.visitor;

        Predicate predicate1 = null;
        if (StringUtils.hasLength(keyword)) {
            if (RegexUtils.isPlateNumber(keyword)) {
                predicate1 = qVisitor.carCode.eq(keyword);
            } else if (RegexUtils.isIdCard(keyword)) {
                predicate1 = qVisitor.idCard.eq(keyword);
            } else if (RegexUtils.isPhoneNumber(keyword)) {
                predicate1 = qVisitor.phone.eq(keyword);
            } else if (RegexUtils.isName(keyword)) {
                predicate1 = qVisitor.name.eq(keyword);
            } else {
                predicate1 = ExpressionUtils.anyOf(qVisitor.idCard.contains(keyword), qVisitor.carCode.contains(keyword), qVisitor.phone.contains(keyword), qVisitor.name.contains(keyword));
            }
        }

        return visitorRepository.findAll(qVisitor.merchantId.eq(mid).and(predicate).and(predicate1), pageable);
    }

    @PostMapping(value = "visitors/export", name = "访客导出")
    public void export(
            HttpServletResponse httpServletResponse,
            @RequestHeader(name = "mid") String mid,
            @QuerydslPredicate(bindings = VisitorRepository.class, root = Visitor.class) Predicate predicate) throws Exception {
        QVisitor qVisitor = QVisitor.visitor;
        Iterable<Visitor> visitors = visitorRepository.findAll(qVisitor.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        EasyExcel.write(httpServletResponse.getOutputStream(), Visitor.class)
                .sheet().doWrite((Collection<?>) visitors);
    }
}