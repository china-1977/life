export const DEFAULT_NAME = 'Umi Max';

export const StatusEnum = {
  columns:{
    true: { text: '是' },
    false: { text: '否' },
  }
};
export const merchantStatusEnum = {
  true: { text: '是' },
  false: { text: '否' },
};

export const OrderStatusEnum = {
  ProFormSelect: {
    WAIT_CONFIRM: '待确认',
    WAIT_PAY: '待支付',
    FINISH: '已支付',
  },
  columns: {
    WAIT_CONFIRM: { text: '待确认' },
    WAIT_PAY: { text: '待支付' },
    FINISH: { text: '已支付' },
  },
};
export const PayStatusEnum = {
  ProFormSelect: {
    SUCCESS: '支付成功',
    REFUND: '转入退款',
    NOTPAY: '未支付',
    CLOSED: '已关闭',
    REVOKED: '已撤销',
    USERPAYING: '用户支付中',
    PAYERROR: '支付失败',
  },
  columns: {
    SUCCESS: { text: '支付成功' },
    REFUND: { text: '转入退款' },
    NOTPAY: { text: '未支付' },
    CLOSED: { text: '已关闭' },
    REVOKED: { text: '已撤销' },
    USERPAYING: { text: '用户支付中' },
    PAYERROR: { text: '支付失败' },
  },
};

export const OrderProductWayEnum = {
  ProFormSelect: {
    DDZX: '到店自选',
    MDBG: '门店帮购',
    PSSM: '配送上门',
  },
  columns: {
    DDZX: { text: '到店自选' },
    MDBG: { text: '门店帮购' },
    PSSM: { text: '配送上门' },
  },
};
