package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.QCustomer;
import work.onss.community.domain.dto.village.DataCount;
import work.onss.community.domain.dto.village.HouseVoterDto;
import work.onss.community.domain.village.QHouse;
import work.onss.community.domain.village.QVoter;
import work.onss.community.domain.village.Vote;
import work.onss.community.domain.village.VoteRepository;
import work.onss.community.service.QuerydslService;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class VoteController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private QuerydslService querydslService;

    @GetMapping(value = {"votes"}, name = "投票列表")
    public Page<Vote> page(@QuerydslPredicate(bindings = VoteRepository.class) Predicate predicate, @PageableDefault Pageable pageable) {
        return voteRepository.findAll(predicate, pageable);
    }

    @PostMapping(value = {"votes/syncResult"}, name = "投票结果同步")
    public void syncResult(@RequestBody Collection<String> ids) {
        QVoter qVoter = QVoter.voter;
        for (String id : ids) {
            List<DataCount> dataCounts = jpaQueryFactory
                    .select(Projections.constructor(DataCount.class, qVoter.option, qVoter.option.count()))
                    .from(qVoter)
                    .where(qVoter.voteId.eq(id))
                    .groupBy(qVoter.option)
                    .fetch();
            StringJoiner result = new StringJoiner(",");
            if (dataCounts.isEmpty()) {
                querydslService.setResult(id, null);
            } else {
                for (DataCount dataCount : dataCounts) {
                    String data = String.join(":", dataCount.getKey().toString(), dataCount.getValue().toString());
                    result.add(data);
                    querydslService.setResult(id, result.toString());
                }
            }
        }
    }

    @PostMapping(value = "votes/export", name = "投票导出")
    public void export(HttpServletResponse httpServletResponse, @QuerydslPredicate(bindings = VoteRepository.class, root = Vote.class) Predicate predicate) throws Exception {
        Iterable<Vote> votes = voteRepository.findAll(predicate);
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        EasyExcel.write(httpServletResponse.getOutputStream(), Vote.class)
                .sheet().doWrite((Collection<?>) votes);
    }

    @PostMapping(value = "votes/{id}/detailExport", name = "投票导出详情")
    public void detailExport(HttpServletResponse httpServletResponse, @PathVariable String id) throws Exception {
        QVoter qVoter = QVoter.voter;
        QCustomer qCustomer = QCustomer.customer;
        QHouse qHouse = QHouse.house;
        List<HouseVoterDto> data = jpaQueryFactory.select(Projections.constructor(HouseVoterDto.class, qHouse, qCustomer, qVoter)).from(qVoter)
                .leftJoin(qHouse).on(qHouse.id.eq(qVoter.houseId))
                .leftJoin(qCustomer).on(qCustomer.id.eq(qVoter.customerId))
                .where(qVoter.voteId.eq(id))
                .fetch();
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), HouseVoterDto.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<HouseVoterDto>> floorNumberData = data.stream().collect(Collectors.groupingBy(HouseVoterDto::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}