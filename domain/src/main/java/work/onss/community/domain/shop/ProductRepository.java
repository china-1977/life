package work.onss.community.domain.shop;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, String>, QuerydslPredicateExecutor<Product>, QuerydslBinderCustomizer<QProduct> {

    default void customize(QuerydslBindings bindings, QProduct qProduct) {
        bindings.bind(qProduct.name).first((path, s) -> Objects.requireNonNull(path).contains(s));
        bindings.bind(qProduct.insertDatetime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
    }

    Optional<Product> findByIdAndMerchantId(String id, String sid);

    @Modifying
    @Transactional
    void deleteByIdInAndMerchantId(Collection<String> ids, String sid);
}
