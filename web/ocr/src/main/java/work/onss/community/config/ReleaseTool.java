package work.onss.community.config;

import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OrtSession;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.Mat;

/**
 * 释放资源的工具类
 */
@Slf4j
public class ReleaseTool {

    /**
     * 释放边框信息
     *
     * @param borderMats 边框
     */
    public static void release(BorderMat... borderMats) {
        for (BorderMat borderMat : borderMats) {
            if (null != borderMat) {
                try {
                    borderMat.release();
                } catch (Exception e) {
                    log.error("释放资源失败", e);

                } finally {
                    borderMat = null;
                }
            }
        }
    }

    /**
     * 释放 mat 图片资源
     *
     * @param mats 图片
     */
    public static void release(Mat... mats) {
        for (Mat mat : mats) {
            if (null != mat) {
                try {
                    mat.release();
                } catch (Exception e) {
                    e.fillInStackTrace();
                } finally {
                    mat = null;
                }
            }
        }
    }

    /**
     * 释放张量信息
     *
     * @param tensors 张量
     */
    public static void release(OnnxTensor... tensors) {
        if (null == tensors || tensors.length == 0) {
            return;
        }
        try {
            for (OnnxTensor tensor : tensors) {
                try {
                    if (null != tensor) {
                        tensor.close();
                    }
                } catch (Exception e) {
                    e.fillInStackTrace();
                } finally {
                    tensor = null;
                }
            }
        } catch (Exception e) {
            log.error("释放资源失败", e);

        } finally {
            tensors = null;
        }
    }


    /**
     * 释放结果信息
     *
     * @param results 结果
     */
    public static void release(OrtSession.Result... results) {
        if (null == results || results.length == 0) {
            return;
        }
        try {
            for (OrtSession.Result result : results) {
                try {
                    if (null != result) {
                        result.close();
                    }

                } catch (Exception e) {
                    log.error("释放结果资源失败", e);

                } finally {
                    result = null;
                }
            }
        } catch (Exception e) {
            log.error("释放结果资源失败", e);

        } finally {
            results = null;
        }
    }
}
