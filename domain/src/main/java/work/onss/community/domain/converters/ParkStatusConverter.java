package work.onss.community.domain.converters;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import work.onss.community.domain.park.OrderPark;

public class ParkStatusConverter implements Converter<OrderPark.Status> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return OrderPark.Status.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public OrderPark.Status convertToJavaData(ReadConverterContext<?> context) {
        for (OrderPark.Status status : OrderPark.Status.values()) {
            if (status.getValue().equals(context.getReadCellData().getStringValue())) {
                return status;
            }
        }
        return null;
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<OrderPark.Status> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }
}