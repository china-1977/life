package work.onss.community.config;

import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OrtSession;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * 车牌识别
 * 获取车牌上的信息
 */
public class LicensePlateInfoInterfaceImpl extends AbstractBaseOnnx {

    /**
     *
     */
    private final static float STD_VALUE = 0.193f;
    /**
     * 平均值
     */
    private final static float MEAN_VALUE = 0.588f * 255;
    /**
     * 车牌的颜色
     */
    private final static String[] PLATE_COLOR = new String[]{"黑色", "蓝色", "绿色", "白色", "黄色"};
    /**
     * 车牌中文字
     */
    private final static String PLATE_NAME = "#京沪津渝冀晋蒙辽吉黑苏浙皖闽赣鲁豫鄂湘粤桂琼川贵云藏陕甘青宁新学警港澳挂使领民航危0123456789ABCDEFGHJKLMNPQRSTUVWXYZ险品";

    /**
     * 构造函数
     *
     * @param modelPath 模型的路径
     * @param thread    开启的线程数
     */
    public LicensePlateInfoInterfaceImpl(String modelPath, int thread) {
        super(modelPath, thread);
    }

    /**
     * 解码分析颜色
     *
     * @param indexes
     * @return
     */
    private static Double[] decodeColor(double[] indexes) {
        double index = -1;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < indexes.length; i++) {
            if (max < indexes[i]) {
                max = indexes[i];
                index = i;
            }
        }
        return new Double[]{index, max};
    }

    /**
     * float 转 double
     *
     * @param input 浮动类型
     * @return double 类型
     */
    private static double[] floatToDouble(float[] input) {
        if (input == null) {
            return null;
        }
        double[] output = new double[input.length];
        for (int i = 0; i < input.length; i++) {
            output[i] = input[i];
        }
        return output;
    }

    /**
     * softmax变换
     *
     * @param tensor 要变换的数据
     * @return
     */
    private static double[] softMax(double[] tensor) {
        if (Arrays.stream(tensor).max().isPresent()) {
            double maxValue = Arrays.stream(tensor).max().getAsDouble();
            double[] value = Arrays.stream(tensor).map(y -> Math.exp(y - maxValue)).toArray();
            double total = Arrays.stream(value).sum();
            return Arrays.stream(value).map(p -> p / total).toArray();
        } else {
            throw new NoSuchElementException("不存在要变换的值(No value present)");
        }
    }

    /**
     * 解码识别对应数组
     *
     * @param indexes
     * @return
     */
    private static String decodePlate(int[] indexes) {
        int pre = 0;
        StringBuffer sb = new StringBuffer();
        for (int index : indexes) {
            if (index != 0 && pre != index) {
                sb.append(PLATE_NAME.charAt(index));
            }
            pre = index;
        }
        return sb.toString();
    }

    /**
     * 获取最大得分指数
     *
     * @param result 结果
     * @return
     */
    private static int[] maxScoreIndex(float[][] result) {
        int[] indexes = new int[result.length];
        for (int i = 0; i < result.length; i++) {
            int index = 0;
            float max = Float.MIN_VALUE;
            for (int j = 0; j < result[i].length; j++) {
                if (max < result[i][j]) {
                    max = result[i][j];
                    index = j;
                }
            }
            indexes[i] = index;
        }
        return indexes;
    }

    /**
     * 拆分合并为一张图片
     *
     * @param plate 要拆分合并的图片
     * @return 合并为一行后的图片
     */
    private static Mat splitAndMergePlate(Mat plate) {
        Mat upperImageTemp = null, upperImage = null;
        Mat lowerImageTemp = null, lowerImage = null;
        Mat upperReSize = null;
        try {
            int width = (int) plate.size().width;
            int height = (int) plate.size().height;

            // 上半部分
            int upperSplit = Double.valueOf(5.0f / 12 * height).intValue();
            Rect upperRect = new Rect(0, 0, width, upperSplit);
            upperImageTemp = new Mat(plate, upperRect);
            upperImage = new Mat();
            upperImageTemp.copyTo(upperImage);

            // 下半部分
            int lowerSplit = Double.valueOf(1.0f / 3 * height).intValue();
            Rect lowerRect = new Rect(0, lowerSplit, width, height - lowerSplit);
            lowerImageTemp = new Mat(plate, lowerRect);
            lowerImage = new Mat();
            lowerImageTemp.copyTo(lowerImage);

            // 合并图像
            upperReSize = new Mat();
            int heightSize = height - lowerSplit;
            Imgproc.resize(upperImage, upperReSize, new Size(width, heightSize), 0, 0, Imgproc.INTER_AREA);
            Mat concatMat = concat(upperReSize, lowerImage);
            // 返回合并后的图像
            return concatMat;

        } finally {
            ReleaseTool.release(upperImageTemp, upperImage);
            ReleaseTool.release(lowerImageTemp, lowerImage);
            ReleaseTool.release(upperReSize);
        }
    }

    /**
     * 横向拼接两个图像的数据（Mat），该两个图像的类型必须是相同的类型，如：均为CvType.CV_8UC3类型
     *
     * @param m1 要合并的图像1（左图）
     * @param m2 要合并的图像2（右图）
     * @return 拼接好的Mat图像数据集。其高度等于两个图像中高度较大者的高度；其宽度等于两个图像的宽度之和。类型与两个输入图像相同。
     * @throws Exception 当两个图像数据的类型不同时，抛出异常
     */
    private static Mat concat(Mat m1, Mat m2) {
        if (m1.type() != m2.type()) {
            throw new RuntimeException("concat:两个图像数据的类型不同！");
        }
        // 宽度为两图的宽度之和
        double width = m1.size().width + m2.size().width;
        // 高度取两个矩阵中的较大者的高度
        double height = Math.max(m1.size().height, m2.size().height);
        // 创建一个大矩阵对象
        Mat des = Mat.zeros((int) height, (int) width, m1.type());
        // 在最终的大图上标记一块区域，用于存放复制图1（左图）的数据，大小为从第0列到m1.cols()列
        Mat rectForM1 = des.colRange(new Range(0, m1.cols()));
        // 标记出位于rectForM1的垂直方向上中间位置的区域，高度为图1的高度，此时该区域的大小已经和图1的大小相同。（用于存放复制图1（左图）的数据）
        int rowOffset1 = (int) (rectForM1.size().height - m1.rows()) / 2;
        rectForM1 = rectForM1.rowRange(rowOffset1, rowOffset1 + m1.rows());
        // 在最终的大图上标记一块区域，用于存放复制图2（右图）的数据
        Mat rectForM2 = des.colRange(new Range(m1.cols(), des.cols()));
        // 标记出位于rectForM2的垂直方向上中间位置的区域，高度为图2的高度，此时该区域的大小已经和图2的大小相同。（用于存放复制图2（右图）的数据）
        int rowOffset2 = (int) (rectForM2.size().height - m2.rows()) / 2;
        rectForM2 = rectForM2.rowRange(rowOffset2, rowOffset2 + m2.rows());
        // 将图1拷贝到des的指定区域 rectForM1
        m1.copyTo(rectForM1);
        // 将图2拷贝到des的指定区域 rectForM2
        m2.copyTo(rectForM2);
        return des;
    }

    /**
     * 推理识别车牌上的信息
     *
     * @param mat          要识别的图片
     * @param single       是否为单行车牌
     * @param params       额外参数
     * @param licensePlate 识别到的车辆信息，也就是 接口得到的数据，
     * @return 返回的 LicensePlate 和这个对象是一样的。但即时没有这个对象，只要new一个，作用只是存储一些数据
     */
    public LicensePlate inference(Mat mat, Boolean single, Map<String, Object> params, LicensePlate licensePlate) {
        OnnxTensor tensor = null;
        OrtSession.Result output = null;
        // 克隆的图片
        Mat imageMatClone = mat.clone();

        try {
            // 图片处理
            if (null == single || single) {
                imageMatClone = mat.clone();
            } else {
                splitAndMergePlate(mat);
            }
            // 转换数据为张量
            Mat matHandlerOne = resizeAndNoReleaseMat(168, 48, imageMatClone);
            Mat matHandlerTwo = MatTool.blobFromImageAndDoReleaseMat(1.0 / 255, new Scalar(MEAN_VALUE, MEAN_VALUE, MEAN_VALUE), false, matHandlerOne);
            tensor = MatTool.to4dFloatOnnxTensorAndNoReleaseMat(new float[]{STD_VALUE, STD_VALUE, STD_VALUE}, true, matHandlerTwo);

            // ONNX推理
            // 推理
            // 获取输入端的名称
            String inputName = getInputNameArray()[0];
            OrtSession ortSession = getSessionArray()[0];
            output = ortSession.run(Collections.singletonMap(inputName, tensor));
            // 车牌识别
            float[][][] result = (float[][][]) output.get(0).getValue();
            int[] indexes = new int[result[0].length];
            for (int i = 0; i < result[0].length; i++) {
                int index = 0;
                float max = Float.MIN_VALUE;
                for (int j = 0; j < result[0][i].length; j++) {
                    if (max < result[0][i][j]) {
                        max = result[0][i][j];
                        index = j;
                    }
                }
                indexes[i] = index;
            }

            int pre = 0;
            StringBuffer sb = new StringBuffer();
            for (int index : indexes) {
                if (index != 0 && pre != index) {
                    sb.append(PLATE_NAME.charAt(index));
                }
                pre = index;
            }
            String plateNo = sb.toString();

            // 车牌颜色识别
            float[][] color = (float[][]) output.get(1).getValue();
            double[] doubles = floatToDouble(color[0]);
            double[] colorSoftMax = softMax(doubles);
            Double[] colorRResult = decodeColor(colorSoftMax);

            // MatTool.saveMatAsImage(mat, "web/ocr/src/main/resources/model/".concat(plateNo).concat(".jpg"));
            // 返回解析到的数据
            licensePlate.setBaseImage(MatTool.matToBase64(mat));
            licensePlate.setPlateNo(plateNo);
            licensePlate.setPlateColor(PLATE_COLOR[colorRResult[0].intValue()]);
            licensePlate.setColorScore(colorRResult[1].floatValue());
            return licensePlate;

        } catch (Exception e) {
            throw new RuntimeException("识别车牌异常", e);

        } finally {
            // 释放资源
            if (null != tensor) {
                ReleaseTool.release(tensor);
            }
            if (null != output) {
                ReleaseTool.release(output);
            }
            if (null != imageMatClone) {
                ReleaseTool.release(imageMatClone);
            }
        }
    }

    /**
     * 重新设置图片尺寸,不释放原始图片数据
     *
     * @param width  图片宽度
     * @param height 图片高度
     * @param mat    图片
     * @return
     */
    private Mat resizeAndNoReleaseMat(int width, int height, Mat mat) {
        return resize(width, height, false, mat);
    }

    /**
     * 重新设置图片尺寸
     *
     * @param width   图片宽度
     * @param height  图片高度
     * @param release 是否释放参数mat
     * @param mat     图片
     * @return
     */
    private Mat resize(int width, int height, boolean release, Mat mat) {
        try {
            Mat dst = new Mat();
            Imgproc.resize(mat, dst, new Size(width, height), 0, 0, Imgproc.INTER_AREA);
            return dst;
        } finally {
            if (release) {
                ReleaseTool.release(mat);
            }
        }
    }
}
