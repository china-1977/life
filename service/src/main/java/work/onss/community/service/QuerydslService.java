package work.onss.community.service;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import work.onss.community.domain.account.*;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.park.OrderPark;
import work.onss.community.domain.park.ParkRegion;
import work.onss.community.domain.park.QOrderPark;
import work.onss.community.domain.shop.Product;
import work.onss.community.domain.shop.QProduct;
import work.onss.community.domain.village.*;
import work.onss.community.domain.wechat.WXNotify;
import work.onss.community.service.config.CollectionRunner;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Log4j2
@Service
public class QuerydslService {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    /**
     * @param id  商户ID
     * @param cid 营业员ID
     * @return 商户详情
     */
    public Merchant get(String id, String cid) {
        QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
        QMerchant qMerchant = QMerchant.merchant;
        return jpaQueryFactory.select(qMerchant).from(qMerchant)
                .innerJoin(qMerchantCustomer).on(qMerchantCustomer.merchantId.eq(qMerchant.id))
                .where(qMerchant.id.eq(id), qMerchantCustomer.customerId.eq(cid))
                .fetchOne();
    }

    /**
     * @param cid    营业员ID
     * @param limit  数量
     * @param offset 偏移
     * @return 商户详情
     */
    public List<Merchant> get(String cid, Integer limit, Long offset) {
        QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
        QMerchant qMerchant = QMerchant.merchant;
        return jpaQueryFactory.select(qMerchant).from(qMerchant)
                .innerJoin(qMerchantCustomer).on(qMerchant.id.eq(qMerchantCustomer.merchantId))
                .where(qMerchantCustomer.customerId.eq(cid))
                .limit(limit)
                .offset(offset)
                .fetch();
    }

    /**
     * @param cid 营业员ID
     * @return 商户详情
     */
    public List<Merchant> get(String cid, List<Merchant.Category> categories, QMerchant qMerchant, Expression<?>... exprs) {
        QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
        return jpaQueryFactory.select(Projections.fields(Merchant.class, exprs)).from(qMerchant)
                .innerJoin(qMerchantCustomer).on(qMerchant.id.eq(qMerchantCustomer.merchantId))
                .where(qMerchantCustomer.customerId.eq(cid), qMerchant.category.in(categories))
                .fetch();
    }

    public List<Customer> getCustomers(String merchantId, Predicate predicate) {
        QCustomer qCustomer = QCustomer.customer;
        QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
        return jpaQueryFactory.select(qCustomer).from(qMerchantCustomer)
                .innerJoin(qCustomer).on(qMerchantCustomer.customerId.eq(qCustomer.id))
                .where(predicate, qMerchantCustomer.merchantId.eq(merchantId))
                .orderBy(qMerchantCustomer.id.desc())
                .fetch();
    }

    public List<Customer> getCustomers(String merchantId, Predicate predicate, Integer limit, Long offset) {
        QCustomer qCustomer = QCustomer.customer;
        QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
        return jpaQueryFactory.select(qCustomer).from(qMerchantCustomer)
                .innerJoin(qCustomer).on(qMerchantCustomer.customerId.eq(qCustomer.id))
                .where(predicate, qMerchantCustomer.merchantId.eq(merchantId))
                .orderBy(qMerchantCustomer.id.desc())
                .limit(limit)
                .offset(offset)
                .fetch();
    }

    public List<Customer> get(String merchantId, List<String> applications) {
        QCustomer qCustomer = QCustomer.customer;
        QMerchantCustomerApplication qMerchantCustomerApplication = QMerchantCustomerApplication.merchantCustomerApplication;
        QApplication qApplication = QApplication.application;
        return jpaQueryFactory.select(qCustomer).from(qMerchantCustomerApplication)
                .innerJoin(qCustomer).on(qCustomer.id.eq(qMerchantCustomerApplication.customerId))
                .innerJoin(qApplication).on(qCustomer.id.eq(qMerchantCustomerApplication.applicationId))
                .where(qMerchantCustomerApplication.merchantId.eq(merchantId), qApplication.contextPath.concat(qApplication.type).concat(qApplication.value).in(applications))
                .groupBy(qCustomer.id)
                .fetch();
    }

    /**
     * @param id       商户ID
     * @param merchant 商户
     */
    @Transactional
    public void setMerchant(String id, Merchant merchant) {
        QMerchant qMerchant = QMerchant.merchant;
        jpaQueryFactory.update(qMerchant)
                .set(qMerchant.shortname, merchant.getShortname())
                .set(qMerchant.subMchId, merchant.getSubMchId())
                .set(qMerchant.description, merchant.getDescription())
                .set(qMerchant.trademark, merchant.getTrademark())
                .set(qMerchant.pictures, merchant.getPictures())
                .set(qMerchant.videos, merchant.getVideos())
                .set(qMerchant.openTime, merchant.getOpenTime())
                .set(qMerchant.closeTime, merchant.getCloseTime())
                .set(qMerchant.addressValue, merchant.getAddressValue())
                .set(qMerchant.addressCode, merchant.getAddressCode())
                .set(qMerchant.addressName, merchant.getAddressName())
                .set(qMerchant.addressDetail, merchant.getAddressDetail())
                .set(qMerchant.username, merchant.getUsername())
                .set(qMerchant.phone, merchant.getPhone())
                .set(qMerchant.updateDatetime, LocalDateTime.now())
                .where(qMerchant.id.eq(id))
                .execute();
    }

    @Transactional
    public void setMerchant(String id, Boolean status) {
        QMerchant qMerchant = QMerchant.merchant;
        jpaQueryFactory.update(qMerchant)
                .set(qMerchant.status, status)
                .where(qMerchant.id.eq(id))
                .execute();
    }

    @Transactional
    public void setCustomer(Customer customer) {
        QCustomer qCustomer = QCustomer.customer;
        jpaQueryFactory.update(qCustomer)
                .set(qCustomer.phone, customer.getPhone())
                .set(qCustomer.name, customer.getName())
                .set(qCustomer.idCard, customer.getIdCard())
                .set(qCustomer.face, customer.getFace())
                .where(qCustomer.id.eq(customer.getId()))
                .execute();
    }

    @Transactional
    public void setPeopleNumberZero(String merchantId) {
        QHouse qHouse = QHouse.house;
        jpaQueryFactory.update(qHouse)
                .set(qHouse.peopleNumber, 0)
                .set(qHouse.parkSpaceCount, 0)
                .where(qHouse.merchantId.eq(merchantId))
                .execute();
    }

    @Transactional
    public void setPeopleNumber(String houseId, Integer count) {
        QHouse qHouse = QHouse.house;
        jpaQueryFactory.update(qHouse)
                .set(qHouse.peopleNumber, count)
                .where(qHouse.id.eq(houseId))
                .execute();
    }

    @Transactional
    public void setParkSpaceNumber(String houseId, Integer count) {
        QHouse qHouse = QHouse.house;
        jpaQueryFactory.update(qHouse)
                .set(qHouse.parkSpaceCount, count)
                .where(qHouse.id.eq(houseId))
                .execute();
    }

    @Transactional
    public void setResult(String id, String result) {
        QVote qVote = QVote.vote;
        jpaQueryFactory.update(qVote)
                .set(qVote.result, result)
                .where(qVote.id.eq(id))
                .execute();
    }

    @Transactional
    public void updateHouse(House house, String mid) {
        QHouse qHouse = QHouse.house;
        jpaQueryFactory.update(qHouse)
                .set(qHouse.floor, house.getFloor())
                .set(qHouse.floorNumber, house.getFloorNumber())
                .set(qHouse.unit, house.getUnit())
                .set(qHouse.roomNumber, house.getRoomNumber())
                .set(qHouse.type, house.getType())
                .set(qHouse.area, house.getArea())
                .set(qHouse.remarks, house.getRemarks())
                .where(qHouse.id.eq(house.getId()), qHouse.merchantId.eq(mid))
                .execute();
    }

    @Transactional
    public void updateCustomer(String id, LocalDateTime now) {
        QCustomer qCustomer = QCustomer.customer;
        jpaQueryFactory.update(qCustomer)
                .set(qCustomer.updateDatetime, now)
                .where(qCustomer.id.eq(id))
                .execute();
    }

    @Transactional
    public void updatePayStatus(WXNotify.WXRefund wxRefund, PayOrder payOrder) {
        QPayOrder qPayOrder = QPayOrder.payOrder;
        List<PayOrder.Refund> refunds = payOrder.getRefunds();
        if (refunds == null) {
            refunds = List.of();
        }
        PayOrder.Refund refund = new PayOrder.Refund(wxRefund.getOutRefundNo(), BigDecimal.valueOf(wxRefund.getAmount().getRefund()));
        refunds.add(refund);
        jpaQueryFactory.update(qPayOrder)
                .set(qPayOrder.refunds, refunds)
                .where(qPayOrder.id.eq(payOrder.getId()))
                .execute();
    }

    @Transactional
    public void updatePayStatus(String decryptToString) {
        WXNotify.WXTransaction wxTransaction = JacksonUtils.readValue(decryptToString, WXNotify.WXTransaction.class);
        if (wxTransaction.getTradeState().equals(WXNotify.WXTransaction.Status.SUCCESS)) {
            LocalDateTime successTime = LocalDateTime.parse(wxTransaction.getSuccessTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX"));
            String outTradeNo = wxTransaction.getOutTradeNo();
            QPayOrder qPayOrder = QPayOrder.payOrder;
            PayOrder payOrder = jpaQueryFactory.select(qPayOrder).from(qPayOrder).where(qPayOrder.outTradeNo.eq(outTradeNo)).fetchOne();
            if (payOrder == null) {
                log.error("订单丢失:{}", decryptToString);
            } else {
                jpaQueryFactory.update(qPayOrder)
                        .set(qPayOrder.status, PayOrder.PayStatus.SUCCESS)
                        .set(qPayOrder.payDatetime, successTime)
                        .where(qPayOrder.id.eq(payOrder.getId()))
                        .execute();
                List<PayOrder.Detail> details = payOrder.getDetails();
                for (PayOrder.Detail detail : details) {
                    switch (detail.getOrderType()) {
                        case ORDER_HOUSE -> {
                            QOrderHouse qOrderHouse = QOrderHouse.orderHouse;
                            OrderHouse oldOrderHouse = jpaQueryFactory.select(qOrderHouse).from(qOrderHouse).where(qOrderHouse.id.eq(detail.getId())).fetchOne();
                            if (oldOrderHouse == null) {
                                log.error("物业费订单丢失:{}", decryptToString);
                            } else {
                                jpaQueryFactory.update(qOrderHouse)
                                        .set(qOrderHouse.status, PayOrder.Status.FINISH)
                                        .set(qOrderHouse.payDatetime, successTime)
                                        .where(qOrderHouse.id.eq(detail.getId()))
                                        .execute();
                            }
                        }
                        case ORDER_HEAT -> {
                            QOrderWater qOrderWater = QOrderWater.orderWater;
                            OrderWater oldOrderWater = jpaQueryFactory.select(qOrderWater).from(qOrderWater).where(qOrderWater.id.eq(detail.getId())).fetchOne();
                            if (oldOrderWater == null) {
                                log.error("水费订单丢失:{}", decryptToString);
                            } else {
                                jpaQueryFactory.update(qOrderWater)
                                        .set(qOrderWater.status, PayOrder.Status.FINISH)
                                        .set(qOrderWater.payDatetime, successTime)
                                        .where(qOrderWater.id.eq(detail.getId()))
                                        .execute();
                            }
                        }
                        case ORDER_WATER -> {
                            QOrderHeat qOrderHeat = QOrderHeat.orderHeat;
                            OrderHeat oldOrderHeat = jpaQueryFactory.select(qOrderHeat).from(qOrderHeat).where(qOrderHeat.id.eq(detail.getId())).fetchOne();
                            if (oldOrderHeat == null) {
                                log.error("采暖费订单丢失:{}", decryptToString);
                            } else {
                                jpaQueryFactory.update(qOrderHeat)
                                        .set(qOrderHeat.status, PayOrder.Status.FINISH)
                                        .set(qOrderHeat.payDatetime, successTime)
                                        .where(qOrderHeat.id.eq(detail.getId()))
                                        .execute();
                            }
                        }
                        case ORDER_PARK -> {
                            QOrderPark qOrderPark = QOrderPark.orderPark;
                            OrderPark oldOrderPark = jpaQueryFactory.select(qOrderPark).from(qOrderPark).where(qOrderPark.id.eq(detail.getId())).fetchOne();
                            if (oldOrderPark == null) {
                                log.error("停车费订单丢失:{}", decryptToString);
                            } else {
                                jpaQueryFactory.update(qOrderPark)
                                        .set(qOrderPark.moneyPayment, qOrderPark.moneyPayment.add(detail.getTotal()))
                                        .where(qOrderPark.id.eq(detail.getId()))
                                        .execute();
                            }
                        }
                        default -> log.error("订单丢失:{}", decryptToString);
                    }
                }
            }
        }
    }

    public void getTotal(PayOrder.Detail detail, OrderPark orderPark, ParkRegion parkRegion) {
        String description = String.join(",", "停车费", orderPark.getCarNumber());
        detail.setDescription(description);
        detail.setStartDate(orderPark.getInDatetime());
        if (orderPark.getParkStatus().equals(OrderPark.Status.I)) {
            // TODO 出场时间
            LocalDateTime now = LocalDateTime.now();
            detail.setEndDate(now);
            orderPark.setOutDatetime(now);
            Duration duration = Duration.between(orderPark.getInDatetime(), orderPark.getOutDatetime());
            long minutes = duration.toMinutes();
            BigDecimal moneyMeet = BigDecimal.ZERO;
            if (minutes > parkRegion.getFreeTime()) {
                // 停车时长 > 减免时长时，执行计费函数
                moneyMeet = (BigDecimal) CollectionRunner.invoke(parkRegion.getFormula(), "apply", duration, parkRegion.getPrice());
            }
            orderPark.setMoneyMeet(moneyMeet); // 应付金额
            orderPark.setMoneyReduction(BigDecimal.ZERO); // 减免金额
            // TODO 发起支付时，停车订单ID占用优惠券，在没有驶离之前可以重复使用，驶离之后核销优惠券
            BigDecimal total = moneyMeet.subtract(orderPark.getMoneyPayment()); // 剩余应付金额 = 应付金额 - 实付金额
            detail.setTotal(total);
        } else {
            detail.setTotal(orderPark.getMoneyMeet().subtract(orderPark.getMoneyReduction()).subtract(orderPark.getMoneyPayment()));
            detail.setEndDate(orderPark.getOutDatetime());
        }
    }

    /**
     * 商品编辑
     *
     * @param sid     商户ID
     * @param product 商品
     * @return 更新数量
     */
    @Transactional
    public Long setProduct(String sid, Product product) {
        QProduct qProduct = QProduct.product;
        return jpaQueryFactory.update(qProduct)
                .set(qProduct.name, product.getName())
                .set(qProduct.label, product.getLabel())
                .set(qProduct.orderLabel, product.getOrderLabel())
                .set(qProduct.price, product.getPrice())
                .set(qProduct.priceUnit, product.getPriceUnit())
                .set(qProduct.stock, product.getStock())
                .set(qProduct.min, product.getMin())
                .set(qProduct.max, product.getMax())
                .set(qProduct.vid, product.getVid())
                .set(qProduct.status, product.getStatus())
                .set(qProduct.description, product.getDescription())
                .set(qProduct.pictures, product.getPictures())
                .set(qProduct.updateDatetime, LocalDateTime.now())
                .where(qProduct.id.eq(product.getId()), qProduct.merchantId.eq(sid))
                .execute();
    }

    public Merchant get(String id) {
        QMerchant qMerchant = QMerchant.merchant;
        return jpaQueryFactory.select(Projections.fields(Merchant.class,
                        qMerchant.id,
                        qMerchant.shortname,
                        qMerchant.category,
                        qMerchant.trademark,
                        qMerchant.description,
                        qMerchant.openTime,
                        qMerchant.closeTime,
                        qMerchant.subMchId
                )).from(qMerchant)
                .where(qMerchant.id.eq(id))
                .fetchOne();
    }
}
