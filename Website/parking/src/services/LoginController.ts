import { request } from '@@/exports';

export const login = (body?: any) =>
  request<any>('/account/login', {
    method: 'POST',
    data: body,
  });
