package work.onss.community.domain.converters;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.village.PayOrder;

public class MerchantCategoryConverter implements Converter<Merchant.Category> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return PayOrder.Status.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Merchant.Category convertToJavaData(ReadConverterContext<?> context) {
        for (Merchant.Category category : Merchant.Category.values()) {
            if (category.getMessage().equals(context.getReadCellData().getStringValue())) {
                return category;
            }
        }
        return null;
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Merchant.Category> context) {
        return new WriteCellData<>(context.getValue().getMessage());
    }

}
