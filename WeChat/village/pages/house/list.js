import { wxLogin, domain, wxRequest, size } from '../../utils/util.js';

Page({
  data: {
    domain, houses: [],
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/account/merchants/${info.mid}/allHouse`,
        header: { authorization, cid: info.cid },
        data: { 'sort': 'floorNumber,asc' }
      }).then((c0) => {
        if (c0) {
          const c1 = c0[0].children;
          this.setData({
            dataTree: c0,
            housesPicker: [c0, c1, []]
          });
        }
      });
      wxRequest({
        url: `${domain}/village/houses`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { sort: 'floorNumber,unit,roomNumber,asc', size }
      }).then((data) => {
        this.setData({
          houses: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      const data = { sort: 'floorNumber,unit,roomNumber,asc', size }
      const houseKeyword = this.data.houseKeyword;
      if (houseKeyword) {
        const keyword = houseKeyword.split(",");
        if (keyword[0]) {
          data.floorNumber = keyword[0];
        }
        if (keyword[1]) {
          data.unit = keyword[1];
        }
        if (keyword[2]) {
          data.roomNumber = keyword[2];
        }
      }
      wxRequest({
        url: `${domain}/village/houses`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          houses: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      const data = { sort: 'floorNumber,unit,roomNumber,asc', page: this.data.number + 2, size }
      const houseKeyword = this.data.houseKeyword;
      if (houseKeyword) {
        const keyword = houseKeyword.split(",");
        if (keyword[0]) {
          data.floorNumber = keyword[0];
        }
        if (keyword[1]) {
          data.unit = keyword[1];
        }
        if (keyword[2]) {
          data.roomNumber = keyword[2];
        }
      }

      wxRequest({
        url: `${domain}/village/houses`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let houses = this.data.houses;
          houses.push(...data.content)
          this.setData({
            houses, number: data.number
          });
        }
      });
    });
  },

  hoseColumnChange: function (e) {
    let c0;
    let c1;
    let c2;
    switch (e.detail.column) {
      case 0:
        c0 = this.data?.housesPicker[0];
        c1 = c0[e.detail.value]?.children;
        if (c1) {
          this.setData({
            ['housesPicker[1]']: c1,
            ['housesPicker[2]']: [],
          })
        }
        break;
      case 1:
        c1 = this.data?.housesPicker[1];
        c2 = c1[e.detail.value]?.children;
        if (c2) {
          this.setData({
            [`housesPicker[2]`]: c2
          })
        }
        break;
      default:
        break;
    }
  },

  hoseChange: function (e) {
    const housesPicker = this.data.housesPicker;
    if (housesPicker) {
      const housesIndex = e.detail.value;
      let houseKeyword;
      if (housesPicker[2].length > 0) {
        houseKeyword = housesPicker[2][housesIndex[2]]?.value
      } else if (housesPicker[1].length > 0) {
        houseKeyword = housesPicker[1][housesIndex[1]]?.value
      } else {
        houseKeyword = housesPicker[0][housesIndex[0]]?.value
      }

      if (houseKeyword) {
        wxLogin().then(({ authorization, info }) => {
          const data = { sort: 'floorNumber,unit,roomNumber,asc', size }
          const keyword = houseKeyword.split(",");
          if (keyword[0]) {
            data.floorNumber = keyword[0];
          }
          if (keyword[1]) {
            data.unit = keyword[1];
          }
          if (keyword[2]) {
            data.roomNumber = keyword[2];
          }
          wxRequest({
            url: `${domain}/village/houses`,
            header: { authorization, cid: info.cid, mid: info.mid },
            data: data
          }).then((data) => {
            this.setData({
              houses: data.content, number: data.number, houseKeyword
            });
          });
        });
      }
    }
  },

  restHousesIndex: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/houses`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { sort: 'floorNumber,unit,roomNumber,asc', size }
      }).then((data) => {
        let c1;
        let c0 = this.data.dataTree;
        if (c0) {
          c1 = c0[0].children;
        } else {
          c0 = []
          c1 = []
        }
        this.setData({
          houses: data.content, number: data.number, housesPicker: [c0, c1, []], houseKeyword: null
        });
      });
    });
  }
})