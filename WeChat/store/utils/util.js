

function checkToken() {
  return new Promise((resolve) => {
    const authorization = wx.getStorageSync('authorization');
    if (authorization) {
      const info = wx.getStorageSync('info');
      const seonds = new Date(info.lastTime).getTime();
      const now = Date.now();
      const seond = (now - seonds) / 1000;
      if (seond > 18000) {
        wx.reLaunch({
          url: '/pages/login'
        })
      } else {
        resolve({ authorization, info });
      }
    } else {
      wx.reLaunch({
        url: '/pages/login'
      })
    }
  })
}

function wxLogin() {
  return new Promise((resolve) => {
    const authorization = wx.getStorageSync('authorization');
    if (authorization) {
      const info = wx.getStorageSync('info');
      if (info.mid) {
        const seonds = new Date(info.lastTime).getTime();
        const now = Date.now();
        const seond = (now - seonds) / 1000;
        if (seond > 18000) {
          wx.reLaunch({
            url: '/pages/login'
          })
        } else {
          resolve({ authorization, info });
        }
      } else {
        wx.reLaunch({
          url: '/pages/merchant/list'
        })
      }
    } else {
      wx.reLaunch({
        url: '/pages/login'
      })
    }
  })
}

/** 根据经纬度分页获取商户
 * @param {Number} longitude 经度
 * @param {Number} latitude 维度
 * @param {Number} number 分页数
 * @param {string} keyword 关键字
 */
function getStores(longitude, latitude, number = 0, keyword, categories) {
  let url = `${domain}/account/merchants/${longitude}-${latitude}/near?page=${number}`;
  if (keyword) {
    url = `${url}&keyword=${keyword}`
  }
  if (categories) {
    url = `${url}&categories=${categories}`
  }
  return wxRequest({ url })
}

/** 根据商户ID获取商户信息
 * @param {String} id 商户主键
 */
function getStore(id) {
  return wxRequest({ url: `${domain}/consumer/stores/${id}` });
}

/**
 * 单文件上传
 * @param {string} camera 使用前置或后置摄像头
 */
function chooseImageToBase64(camera) {
  return new Promise((resolve, reject) => {
    wx.chooseMedia({
      count: 1,
      mediaType: ['image'],
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      camera,
      success: res => {
        const fs = wx.getFileSystemManager()
        fs.readFile({
          filePath: res.tempFiles[0].tempFilePath,
          encoding: 'base64',
          success: (res) => {
            resolve(`data:image/jpeg;base64,${res.data}`)
          },
          fail: (res) => {
            wx.showModal({
              title: '警告',
              content: '上传失败',
              confirmColor: '#e64340',
              showCancel: false,
            })
          }
        })
      }
    })
  });
}

/**
 * 多文件文件上传
 * @param {string} camera 使用前置或后置摄像头
 * @param {number} count 上传数量
 */
function chooseImagesToBase64(camera, count) {
  return new Promise((resolve, reject) => {
    wx.chooseMedia({
      count: count,
      mediaType: ['image'],
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      camera,
      success: res => {
        const data = res.tempFiles.map((value) => {
          const fs = wx.getFileSystemManager()
          const file = fs.readFileSync(value.tempFilePath, 'base64');
          return `data:image/jpeg;base64,${file}`;
        })
        resolve(data);
      }
    })
  })
}


/**单文件上传
 * @param {string} camera 使用前置或后置摄像头
 * @param {string} authorization 秘钥
 * @param {object} info 用户信息
 * @param {number} url 上传地址
 */
function chooseImage(camera, authorization, info, url) {
  return new Promise((resolve, reject) => {
    wx.chooseMedia({
      count: 1,
      mediaType: ['image'],
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      camera,
      success: res => {
        wx.showLoading({
          title: '加载中',
          mask: true
        })
        wx.uploadFile({
          header: { authorization, ...info },
          url,
          filePath: res.tempFilePaths[0],
          name: 'file',
          success: res => {
            if (res.statusCode === 200) {
              resolve(res.data)
            } else {
              const { code, message } = JSON.parse(res.data);
              switch (code) {
                case 'SESSION_EXPIRE':
                  wx.removeStorageSync('authorization');
                  wx.removeStorageSync('info');
                  wx.reLaunch({
                    url: '/pages/login/login'
                  });
                  break;
                default:
                  wx.showModal({
                    title: '警告',
                    content: message,
                    confirmColor: '#e64340',
                    showCancel: false,
                  })
                  break;
              }
            }
          },
          complete: (data) => {
            wx.hideLoading()
          }
        })
      }
    })
  });
}

/**多个文件上传
 * @param {string} authorization 秘钥
 * @param {object} info 用户信息
 * @param {number} count 上传数量
 * @param {number} url 上传地址
 */
function chooseImages(authorization, info, count, url) {
  return new Promise((resolve, reject) => {
    wx.chooseMedia({
      count,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: res => {
        wx.showLoading({ title: '加载中', mask: true })
        for (let filePath of res.tempFilePaths) {
          wx.uploadFile({
            header: { authorization, ...info },
            url,
            filePath: filePath,
            name: 'file',
            success: res => {
              if (res.statusCode === 200) {
                resolve(res.data)
              } else {
                const { code, message } = JSON.parse(res.data);
                switch (code) {
                  case 'SESSION_EXPIRE':
                    wx.removeStorageSync('authorization');
                    wx.removeStorageSync('info');
                    wx.reLaunch({
                      url: '/pages/login/login'
                    });
                    break;
                  default:
                    wx.showModal({
                      title: '警告',
                      content: message,
                      confirmColor: '#e64340',
                      showCancel: false,
                    })
                    break;
                }
              }
            },
            fail: (data) => {
              const { code, message, content } = data;
              wx.showModal({
                title: '提示',
                content: message,
                showCancel: false,
              })
            },
            complete: (data) => {
              wx.hideLoading()
            }
          })
        }
      }
    })
  })
}

function wxRequest({ url, data = {}, dataType = 'json', header, method = 'GET', responseType = 'text', timeout = 0 }) {
  return new Promise((resolve) => {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url,
      data,
      dataType,
      method,
      responseType,
      timeout,
      header: { 'Content-Type': 'application/json;charset=UTF-8', ...header },
      success: ({ data, statusCode }) => {
        if (statusCode === 200) {
          resolve(data);
        } else {
          const { code, message, content } = data;
          switch (code) {
            case 'SESSION_EXPIRE':
              wx.removeStorageSync('authorization');
              wx.removeStorageSync('info');
              wx.reLaunch({
                url: '/pages/login'
              })
              break;
            case 'NOT_REGISTER_CUSTOMER':
              wx.reLaunch({
                url: '/pages/login'
              })
              break;
            default:
              wx.showModal({
                title: '警告',
                content: message,
                confirmColor: '#e64340',
                showCancel: false,
              })
              break;
          }
        }
      },
      fail: (data) => {
        wx.hideLoading()
        const { code, message, content } = data;
        switch (code) {
          case 'SESSION_EXPIRE':
            wx.removeStorageSync('authorization');
            wx.removeStorageSync('info');
            wx.reLaunch({
              url: '/pages/login'
            })
            break;
          default:
            wx.showModal({
              title: '警告',
              content: message,
              confirmColor: '#e64340',
              showCancel: false,
            })
            break;
        }
      },
      complete: (res) => {
        wx.hideLoading()
      },
    })
  })
}

module.exports = {
  checkToken,
  wxLogin,
  getStores,
  getStore,
  wxRequest,
  chooseImage,
  chooseImageToBase64,
  chooseImagesToBase64,
  windowWidth: getApp().globalData.windowWidth,
  domain: 'http://1977.work:7000',
  // domain: 'http://127.0.0.1:7000',
  appid: "wx095ba1a3f9396476",
  size: 20,
  relations: ['房东', '租户', '员工'],
  PayOrderstatus: {
    range: [
      { key: 'WAIT_CONFIRM', value: '待确认' },
      { key: 'WAIT_PAY', value: '待支付' },
      { key: 'FINISH', value: '已支付' }
    ],
    data: {
      WAIT_CONFIRM: '待确认',
      WAIT_PAY: '待支付',
      FINISH: '已支付'
    },
  },

  AdviceStatus: {
    range: [
      { key: 'WAIT', value: '待处理' },
      { key: 'CLOSED', value: '已关闭' },
      { key: 'ERROR', value: '未解决' },
      { key: 'SUCCESS', value: '已解决' }
    ],
    data: {
      WAIT: '待处理',
      CLOSED: '已关闭',
      ERROR: '未解决',
      SUCCESS: '已解决'
    },
  },

  ParkSpaceIdentityStatus: {
    range: [
      { key: 'WAIT', value: '待处理' },
      { key: 'YES', value: '已通过' },
      { key: 'NO', value: '已驳回' },
      { key: 'FINISHED', value: '已结束' }
    ],
    data: {
      WAIT: '待处理',
      YES: '已通过',
      NO: '已驳回',
      FINISHED: '已结束'
    },

  },

  RepairStatus: {
    range: [
      { key: 'WAIT_ALLOT', value: '待分配' },
      { key: 'WAIT_REPAIR', value: '待维修' },
      { key: 'FINISHED', value: '已完成' }
    ],
    data: {
      WAIT_ALLOT: '待分配',
      WAIT_REPAIR: '待维修',
      FINISHED: '已完成'
    },
  },

  HouseIdentityStatus: {
    range: [
      { key: 'WAIT', value: '待处理' },
      { key: 'YES', value: '已通过' },
      { key: 'NO', value: '已拒绝' }
    ],
    data: {
      WAIT: '待处理',
      YES: '已通过',
      NO: '已拒绝'
    },
  },

  OrderParkStatus: {
    range: [
      { key: 'IN_PARKING', value: '在场' },
      { key: 'UN_PAY', value: '未付' },
      { key: 'ZERO', value: '零元' },
      { key: 'FREE', value: '全免' },
      { key: 'PART_PAY', value: '部分' },
      { key: 'FULL_PAY', value: '全额' },
    ],
    data: {
      IN_PARKING: "在场",
      UN_PAY: "未付",
      ZERO: "零元",
      FREE: "全免",
      PART_PAY: "部分",
      FULL_PAY: "全额"
    },
  },
  scoreStatus : {
    WAIT_PAY: "待支付",
    FINISH_PAY: "已支付",
    REFUND_SUCCESS: "退款成功",
    REFUND_CLOSED: "退款关闭",
    REFUND_PROCESSING: "退款处理中",
    REFUND_ABNORMAL: "退款异常"
  },

   scoreWay : {
    DNYC: "店内用餐",
    DBDZ: "打包带走",
    PSSM: "配送上门",
  },

   storeState : {
    APPLYMENT_STATE_EDITTING: "编辑中",
    APPLYMENT_STATE_AUDITING: "审核中",
    APPLYMENT_STATE_REJECTED: "已驳回",
    APPLYMENT_STATE_TO_BE_CONFIRMED: "待账户验证",
    APPLYMENT_STATE_TO_BE_SIGNED: "待签约",
    APPLYMENT_STATE_SIGNING: "开通权限中",
    APPLYMENT_STATE_FINISHED: "已完成",
    APPLYMENT_STATE_CANCELED: "已作废",
  }
}
