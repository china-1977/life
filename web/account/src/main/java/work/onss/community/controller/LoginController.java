package work.onss.community.controller;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.account.*;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.config.SystemConfig;
import work.onss.community.domain.vo.Info;
import work.onss.community.domain.wechat.Jscode2SessionResult;
import work.onss.community.domain.wechat.WXLogin;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.exception.BusinessException;
import work.onss.community.service.utils.Utils;
import work.onss.community.service.wechat.MiniAppService;
import work.onss.community.service.wechat.WechatMpProperties;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
public class LoginController {

    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private SystemConfig systemConfig;
    @Autowired
    private WechatMpProperties wechatMpProperties;
    @Autowired
    private MiniAppService miniAppService;
    @Autowired
    private QuerydslService querydslService;

    @Transactional
    @PostMapping(value = {"register"})
    public void realName(@Validated @RequestBody Customer customer) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode(customer.getPassword());
        customer.setPassword(encode);
        customerRepository.save(customer);
    }

    @PostMapping(value = {"wxLogin"})
    public Map<String, Object> wxLogin(@RequestBody WXLogin wxLogin) {
        WechatMpProperties.AppConfig appConfig = wechatMpProperties.getAppConfigs().get(wxLogin.getAppid());
        String jscode2Session = miniAppService.jscode2session(wxLogin.getAppid(), appConfig.getSecret(), wxLogin.getCode(), "authorization_code");
        Jscode2SessionResult jscode2SessionResult = JacksonUtils.readValue(jscode2Session, Jscode2SessionResult.class);
        QCustomer qCustomer = QCustomer.customer;
        LocalDateTime now = LocalDateTime.now();
        List<Customer> customers = jpaQueryFactory.select(qCustomer).from(qCustomer)
                .where(qCustomer.openid.eq(jscode2SessionResult.getOpenid()))
                .fetch();
        if (customers.isEmpty()) {
            Customer customer = new Customer(jscode2SessionResult.getOpenid(), jscode2SessionResult.getOpenid(), now);
            customerRepository.save(customer);
            Info info = new Info(customer.getId(), customer.getName(), now);
            String subject = JacksonUtils.writeValueAsString(info);
            String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.toInstant(ZoneOffset.ofHours(8)), subject, customer.getId(), "");
            Map<String, Object> result = new HashMap<>();
            result.put("authorization", authorization);
            result.put("info", info);
            return result;
        } else {
            if (customers.size() > 1) {
                Info info = new Info(jscode2SessionResult.getOpenid(), now);
                String subject = JacksonUtils.writeValueAsString(info);
                String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.toInstant(ZoneOffset.ofHours(8)), subject, jscode2SessionResult.getOpenid(), "");
                Map<String, Object> result = new HashMap<>();
                result.put("authorization", authorization);
                result.put("info", info);
                return result;
            } else {
                Customer customer = customers.getFirst();
                Info info = new Info(customer.getId(), customer.getName(), now);
                String subject = JacksonUtils.writeValueAsString(info);
                String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.toInstant(ZoneOffset.ofHours(8)), subject, customer.getId(), "");
                Map<String, Object> result = new HashMap<>();
                result.put("authorization", authorization);
                result.put("info", info);
                querydslService.updateCustomer(customer.getId(), now);
                return result;
            }

        }
    }

    /**
     * @param customer 成员信息
     * @return 密钥及营业员信息
     */
    @PostMapping(value = {"login"})
    public Map<String, Object> login(@RequestBody Customer customer) {
        QCustomer qCustomer = QCustomer.customer;
        Customer oldCustomer = customerRepository.findOne(qCustomer.username.eq(customer.getUsername())).orElseThrow(() -> new BusinessException("账号不存在或者密码错误"));
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        boolean matches = bCryptPasswordEncoder.matches(customer.getPassword(), oldCustomer.getPassword());
        if (!matches) {
            throw new BusinessException("账号不存在或者密码错误");
        }
        LocalDateTime now = LocalDateTime.now();
        Info info = new Info(oldCustomer.getId(), oldCustomer.getName(), now);

        QMerchantCustomer qVillageCustomer = QMerchantCustomer.merchantCustomer;
        QMerchant qMerchant = QMerchant.merchant;
        List<Merchant> merchants = jpaQueryFactory.select(qMerchant).from(qMerchant)
                .innerJoin(qVillageCustomer).on(qMerchant.id.eq(qVillageCustomer.merchantId))
                .where(qVillageCustomer.customerId.eq(oldCustomer.getId()))
                .limit(2)
                .offset(0)
                .fetch();
        String shortname = "请选择主体";
        String audience = "";
        if (merchants.size() == 1) {
            Merchant merchant = merchants.getFirst();
            info = new Info(oldCustomer.getId(), oldCustomer.getName(), merchant.getId(), merchant.getShortname(), now);
            shortname = merchants.getFirst().getShortname();
            audience = merchant.getCategory().name();
        }
        String subject = JacksonUtils.writeValueAsString(info);
        String authorization = Utils.authorization(
                systemConfig.getSecret(),
                "1977",
                now.toInstant(ZoneOffset.ofHours(8)),
                subject,
                oldCustomer.getId(),
                audience);
        Map<String, Object> result = new HashMap<>();
        result.put("authorization", authorization);
        result.put("info", info);
        result.put("shortname", shortname);
        querydslService.updateCustomer(oldCustomer.getId(), now);
        return result;
    }
}

