import { wxLogin, domain, wxRequest } from '../../utils/util.js';

Page({
  data: {
    domain
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/houses/getCustomers`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((customers) => {
        this.setData({
          customers
        });
      });
    });
  },

  radioChange: function (e) {
    const customer = this.data.customers[e.detail.value];
    var pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];//上一页面
    prevPage.setData({
      ['repair.repairerCustomerId']: customer.id,
      ['repair.repairerName']: customer.name,
      ['repair.repairerPhone']: customer.phone
    })
    wx.navigateBack({
      delta: 1,
    })
  }
})