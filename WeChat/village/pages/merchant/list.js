import { domain, wxRequest, checkToken } from '../../utils/util.js';

Page({
  data: {
    domain,
    merchants: [],
    currentID: -1,
    slideButtons: [
      {
        text: '编辑',
      },
      {
        text: '授权',
      }
    ],
  },


  /**获取所有商户
   *
   * @param {*} options
   */
  onLoad: function (options) {
    checkToken().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/account/bindings/getMerchants`,
        header: { authorization, cid: info.cid },
      }).then((merchants) => {
        this.setData({
          info,
          merchants
        })
      })
    })
  },

  onPullDownRefresh: function () {
    checkToken().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/account/bindings/getMerchants`,
        header: { authorization, cid: info.cid },
      }).then((merchants) => {
        wx.stopPullDownRefresh();
        this.setData({
          merchants
        })
      })
    })
  },

  bindShow: function (e) {
    this.setData({
      currentID: e.target.id
    });
    setTimeout(() => {
      this.setData({
        currentID: -1
      })
    }, 3000)
  },


  bindButtonTap: function (e) {
    const index = e.currentTarget.id;
    const merchant = this.data.merchants[index];
    switch (e.detail.index) {
      case 0:
        wx.navigateTo({
          url: `/pages/merchant/edit?id=${merchant.id}`,
        }); break;
      case 1:
        checkToken().then(({ authorization, info }) => {
          wxRequest({
            url: `${domain}/account/bindings/${merchant.id}`,
            header: { authorization, cid: info.cid },
            method: 'POST',
          }).then((content) => {
            wx.setStorageSync('authorization', content.authorization);
            wx.setStorageSync('info', content.info);
            wx.showModal({
              title: '提示',
              content: '授权成功',
              showCancel: false,
              success: (res) => {
                wx.navigateBack({
                  delta: 2
                });
              }
            })
          })
        }); break;
    }
  },
})
