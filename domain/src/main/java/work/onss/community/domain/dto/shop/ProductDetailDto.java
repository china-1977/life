package work.onss.community.domain.dto.shop;

import lombok.Data;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.NumberFormat;
import work.onss.community.domain.shop.Cart;
import work.onss.community.domain.shop.Product;
import work.onss.community.domain.usertype.StringArrayType;
import work.onss.community.domain.village.ProductPanel;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
public class ProductDetailDto {
    private String id;
    private String cartId;
    private String label;
    private String orderLabel;
    private String name;
    private String description;
    @NumberFormat(pattern = "#.00", style = NumberFormat.Style.CURRENCY)
    private BigDecimal price;
    private String priceUnit;
    private String merchantId;
    @Type(value = StringArrayType.class)
    private String[] pictures;
    private BigInteger num;
    private Boolean checked = false;
    @NumberFormat(pattern = "#.00", style = NumberFormat.Style.CURRENCY)
    private BigDecimal total;

    public ProductDetailDto(Product product, Cart cart, BigDecimal total) {
        this.id = product.getId();
        this.label = product.getLabel();
        this.orderLabel = product.getOrderLabel();
        this.name = product.getName();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.priceUnit = product.getPriceUnit();
        this.merchantId = product.getMerchantId();
        this.pictures = product.getPictures();
        if (cart != null) {
            this.cartId = cart.getId();
            this.num = cart.getNum();
            this.checked = cart.getChecked();
        }
        this.total = total;
    }

    public ProductDetailDto(Product product, ProductPanel productPanel, Cart cart, BigDecimal total) {
        this.id = product.getId();
        this.label = productPanel.getLabel();
        this.orderLabel = productPanel.getOrderLabel();
        this.name = product.getName();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.priceUnit = product.getPriceUnit();
        this.merchantId = product.getMerchantId();
        this.pictures = product.getPictures();
        if (cart != null) {
            this.cartId = cart.getId();
            this.num = cart.getNum();
            this.checked = cart.getChecked();
        }
        this.total = total;
    }

    public ProductDetailDto(Product product) {
        this.id = product.getId();
        this.label = product.getLabel();
        this.name = product.getName();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.priceUnit = product.getPriceUnit();
        this.merchantId = product.getMerchantId();
        this.pictures = product.getPictures();
    }
}
