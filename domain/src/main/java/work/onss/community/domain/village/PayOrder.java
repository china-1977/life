package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.converters.PayStatusConverter;
import work.onss.community.domain.usertype.JsonStringType;
import work.onss.community.domain.usertype.StringArrayType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Log4j2
@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class PayOrder implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @NotBlank(message = "商户ID不能为空")
    private String merchantId;
    @NotBlank(message = "商户号不能为空")
    private String subMchId;
    @NotBlank(message = "商户简称不能为空")
    private String merchantShortname;
    @NotBlank(message = "用户ID不能为空")
    private String customerId;
    @NotBlank(message = "描述不能为空")
    private String description;
    @Type(value = StringArrayType.class)
    private String[] orderIds;
    @Type(value = JsonStringType.class)
    private List<Detail> details;
    private BigDecimal total;
    private String remarks;
    @Enumerated(EnumType.STRING)
    @ExcelProperty(value = "支付状态", converter = PayStatusConverter.class)
    private PayStatus status;
    private String prepayId;
    private String outTradeNo;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateDatetime;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime payDatetime;
    @Type(value = JsonStringType.class)
    private List<Refund> refunds;

    public PayOrder(String merchantId, String subMchId, String merchantShortname, String customerId, String description,
                    String[] orderIds, List<Detail> details, BigDecimal total, PayStatus status, String prepayId,
                    String outTradeNo) {
        this.merchantId = merchantId;
        this.subMchId = subMchId;
        this.merchantShortname = merchantShortname;
        this.customerId = customerId;
        this.description = description;
        this.orderIds = orderIds;
        this.details = details;
        this.total = total;
        this.status = status;
        this.prepayId = prepayId;
        this.outTradeNo = outTradeNo;
    }

    @Getter
    @AllArgsConstructor
    public enum Status implements Serializable {
        WAIT_CONFIRM("待确认"), WAIT_PAY("待支付"), FINISH("已支付");
        private final String message;
    }

    @Getter
    @AllArgsConstructor
    public enum PayStatus implements Serializable {
        SUCCESS("支付成功"), REFUND("转入退款"), NOTPAY("未支付"), CLOSED("已关闭"), REVOKED("已撤销"), USERPAYING("用户支付中"), PAYERROR("支付失败");
        private final String message;
    }

    @Getter
    @AllArgsConstructor
    public enum PayType implements Serializable {
        WECHAT_MINI("微信小程序"), ALIPAY_MINI("支付宝小程序"), CASH("现金支付");
        private final String message;
    }


    @Getter
    public enum OrderType implements Serializable {
        ORDER_PARK("停车费"), ORDER_HEAT("采暖费"), ORDER_WATER("水费"), ORDER_HOUSE("物业费");
        private final String value;

        OrderType(String value) {
            this.value = value;
        }
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Detail implements Serializable {
        private String id;
        private OrderType orderType;
        private String description;
        private BigDecimal total;
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        private LocalDateTime startDate;
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        private LocalDateTime endDate;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Refund implements Serializable {
        private String outRefundNo;
        private BigDecimal refund;
    }
}
