package work.onss.community.domain.account;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.query.sqm.mutation.internal.UpdateHandler;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.converters.SensitiveFieldConverter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@EntityListeners(AuditingEntityListener.class)
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    @ExcelProperty(value = "姓名")
    @NotBlank(message = "请输入真实姓名", groups = UpdateHandler.class)
    @Convert(converter = SensitiveFieldConverter.class)
    private String name;
    @ExcelProperty(value = "身份证")
    @Size(min = 18, max = 18, message = "请输入18位身份证", groups = UpdateHandler.class)
    @Convert(converter = SensitiveFieldConverter.class)
    private String idCard;
    private String idCardFace;
    private String idCardNational;
    private String face;
    @ExcelProperty(value = "手机号")
    @NotBlank(message = "请输入手机号", groups = UpdateHandler.class)
    @Convert(converter = SensitiveFieldConverter.class)
    private String phone;
    @ExcelProperty(value = "账号")
    @NotBlank(message = "请输入登录账户")
    private String username;
    @Size(min = 6, max = 12, message = "请输入6~12位密码", groups = UpdateHandler.class)
    private String password;
    @ExcelProperty(value = "注册时间")
    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;
    @ExcelProperty(value = "更新时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @LastModifiedDate
    private LocalDateTime updateDatetime;
    @ExcelProperty(value = "OPENID")
    private String openid;

    @Transient
    private Boolean status = false;

    public Customer(String username, String openid, LocalDateTime insertDatetime) {
        this.username = username;
        this.openid = openid;
        this.insertDatetime = insertDatetime;
    }
}
