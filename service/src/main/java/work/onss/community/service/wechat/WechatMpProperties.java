package work.onss.community.service.wechat;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@RefreshScope
@Log4j2
@Data
@ConfigurationProperties(prefix = "wechat.mp")
@EnableConfigurationProperties(WechatMpProperties.class)
@Configuration
public class WechatMpProperties {
    private String appid;
    private String secret;
    private String mchId;
    private String mchkey;
    private String apiV3Key;
    private String certSerialNo;
    private String keyPath;
    private String privateKey;
    private String privateCert;
    private String notifyUrl;
    private String refundNotifyUrl;
    private Map<String, AppConfig> appConfigs;

    @Data
    public static class AppConfig {
        private String appid;
        private String secret;
    }
}
