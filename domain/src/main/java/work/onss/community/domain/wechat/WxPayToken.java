package work.onss.community.domain.wechat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

import java.io.Serializable;

@Log4j2
@Data
@Builder
public class WxPayToken implements Serializable {
    private String mchid;
    @JsonProperty("nonce_str")
    private String nonceStr;
    private String signature;
    private String timestamp;
    @JsonProperty("serial_no")
    private String serialNo;
}
