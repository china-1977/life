package work.onss.community.domain.shop;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart, String>, QuerydslPredicateExecutor<Cart>, QuerydslBinderCustomizer<QCart> {

    default void customize(QuerydslBindings bindings, QCart qCart) {
        bindings.bind(qCart.id).withDefaultBinding();
    }

    @Modifying
    @Transactional
    void deleteByIdAndCustomerId(String id, String aid);

    Optional<Cart> findByMerchantIdAndCustomerIdAndProductId(String storeId, String aid, String pid);

    @Transactional
    @Modifying
    void deleteByMerchantIdAndCustomerId(String sid, String cid);

    @Modifying
    @Transactional
    void deleteByMerchantId(String storeId);
}
