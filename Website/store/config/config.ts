import { defineConfig } from '@umijs/max';
import routes from './routes';

export default defineConfig({
  define: {
    'process.env.baseURL': 'http://127.0.0.1:7000',
  },
  access: {},
  model: {},
  initialState: {},
  request: {
    dataField: '',
  },
  antd: {},
  layout: {
    title: '商城',
    locale: false,
  },
  hash: true,
  history: { type: 'hash' },
  routes: routes,
  npmClient: 'pnpm',
});
