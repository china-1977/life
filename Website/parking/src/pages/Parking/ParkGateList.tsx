/**
 * 道闸列表
 */
import {AccessTypeEnum, DeviceStatusEnum} from '@/constants';
import {exportData} from '@/services/FileController';
import services from '@/services/api';
import {ActionType, ProTable} from '@ant-design/pro-components';
import {Button, DatePicker, Dropdown, Popconfirm, Typography} from 'antd';
import {useRef} from 'react';

const {page, del} = services.ParkGateController;

const ParkGateList = () => {
    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {title: '设备序号', align: 'center', dataIndex: 'imei', copyable: true},
        {title: '名称', align: 'center', dataIndex: 'title', copyable: true},
        {title: 'IP', align: 'center', dataIndex: 'ip', copyable: true},
        {
            title: '类型',
            dataIndex: 'type',
            copyable: true,
            align: 'center',
            fieldProps: {mode: 'multiple'},
            valueEnum: AccessTypeEnum.columns,
        },
        {
            title: '更新日期',
            dataIndex: 'updateDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: true,
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {title: '备注', dataIndex: 'remarks', copyable: true, search: false},
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, record: any) => (
                <Popconfirm
                    title="删除确认"
                    description={`是否删除【${record.title}】？`}
                    okText="确认"
                    cancelText="取消"
                    onConfirm={() => {
                        del([record.id]).then((res) => {
                            ref.current?.reload();
                        });
                    }}
                >
                    <Button type="primary" danger>删除</Button>
                </Popconfirm>
            ),
        },
    ];

    const ref = useRef<ActionType>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/parkGates/export', values, '道闸信息.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
                request={(params, sorter, filter) => page(params, sorter, filter)}
            />
        </div>
    );
};

export default ParkGateList;
