package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.QCustomer;
import work.onss.community.domain.dto.village.HouseCustomerDto;
import work.onss.community.domain.village.*;
import work.onss.community.domain.village.relational.HouseCustomer;
import work.onss.community.domain.village.relational.QHouseCustomer;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class HouseController {
    @Autowired
    private ParkSpaceRepository parkSpaceRepository;
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private HouseRepository houseRepository;

    @GetMapping(value = {"houses/{id}/customers"}, name = "房屋成员")
    public List<HouseCustomer> customers(@PathVariable String id) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        QCustomer qCustomer = QCustomer.customer;
        return jpaQueryFactory.select(Projections.fields(HouseCustomer.class,
                        qCustomer,
                        qHouseCustomer.id,
                        qHouseCustomer.houseId,
                        qHouseCustomer.customerId,
                        qHouseCustomer.merchantId,
                        qHouseCustomer.relation
                ))
                .from(qHouseCustomer)
                .innerJoin(qCustomer).on(qCustomer.id.eq(qHouseCustomer.customerId))
                .where(qHouseCustomer.houseId.eq(id))
                .fetch();
    }

    @GetMapping(value = {"houses/{id}/parkSpaces"}, name = "房屋车位")
    public List<ParkSpace> parkSpaces(@RequestHeader(name = "mid") String mid, @PathVariable String id) {
        QParkSpace qParkSpace = QParkSpace.parkSpace;
        return jpaQueryFactory.select(qParkSpace)
                .from(qParkSpace)
                .where(qParkSpace.merchantId.eq(mid), qParkSpace.houseId.eq(id))
                .fetch();
    }

    @GetMapping(value = {"houses/{id}/detail"}, name = "房屋信息")
    public HouseCustomerDto detail(@PathVariable String id) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        QCustomer qCustomer = QCustomer.customer;
        House house = houseRepository.findById(id).orElse(null);
        List<HouseCustomer> houseCustomers = jpaQueryFactory.select(
                        Projections.fields(HouseCustomer.class,
                                qCustomer,
                                qHouseCustomer.id,
                                qHouseCustomer.houseId,
                                qHouseCustomer.customerId,
                                qHouseCustomer.merchantId,
                                qHouseCustomer.relation
                        )
                )
                .from(qHouseCustomer)
                .leftJoin(qCustomer).on(qCustomer.id.eq(qHouseCustomer.customerId))
                .where(qHouseCustomer.houseId.eq(id))
                .fetch();
        List<ParkSpace> parkSpaces = parkSpaceRepository.findAllByHouseId(id);
        return new HouseCustomerDto(house, houseCustomers, parkSpaces);
    }

    @GetMapping(value = {"houses"}, name = "房屋列表")
    public Page<House> page(@QuerydslPredicate(bindings = HouseRepository.class) Predicate predicate, @PageableDefault Pageable pageable) {
        return houseRepository.findAll(predicate, pageable);
    }

    @PostMapping(value = {"houses/export"}, name = "房屋导出")
    public void export(@QuerydslPredicate(bindings = HouseRepository.class, root = House.class) Predicate predicate, HttpServletResponse httpServletResponse) throws Exception {
        Collection<House> data = (Collection<House>) houseRepository.findAll(predicate);
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), House.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<House>> floorNumberData = data.stream().collect(Collectors.groupingBy(House::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}