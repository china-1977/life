Page({
  mixins: [require('../../mixin/common')],
  data: {
    houseIdentitys: [],
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/houseIdentitys`,
        header: { authorization },
        data: { sort: 'floorNumber,unit,roomNumber,asc', size: this.data.size }
      }).then((data) => {
        this.setData({
          houseIdentitys: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      let data = { sort: 'floorNumber,unit,roomNumber,asc', size: this.data.size }
      this.wxRequest({
        url: `${this.data.domain}/owner/houseIdentitys`,
        header: { authorization },
        data
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          houseIdentitys: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    this.wxLogin().then(({ authorization }) => {
      let data = { sort: 'floorNumber,unit,roomNumber,asc', size: this.data.size }
      const number = this.data.number;
      if (number) {
        data.page = number + 2;
      }
      const houseIdentityKeyword = this.data.houseIdentityKeyword;
      if (houseIdentityKeyword) {
        data.status = houseIdentityKeyword.key;
      }
      this.wxRequest({
        url: `${this.data.domain}/owner/houseIdentitys`,
        header: { authorization },
        data
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let houseIdentitys = this.data.houseIdentitys;
          houseIdentitys.push(...data.content)
          this.setData({
            houseIdentitys, number: data.number
          });
        }
      });
    });
  },







})