package work.onss.community.domain.pay;

import lombok.Data;

import java.util.Map;

@Data
public class LePosPayRequest {
    // 接口名
    private String service;
    // auth_code 条形码
    private String authCode;
    // merchant_id 商户号
    private String merchantId;
    // 公众号APPID
    private String appId;
    // 用户子标识
    private String subOpenid;
    /**
     * 支付类型
     * 0-支付宝扫码支付；
     * 1-微信公众号、支付宝服务窗支付<原生支付>；
     * 2-微信公众号、支付宝服务窗支付<简易支付>；
     * 3-微信小程序支付、支付宝小程序支付
     */
    private String jspayFlag;
    // 支付方式
    private String payWay;
    // 金额
    private String amount;


    public static Map<String, String> merchantKey = Map.of("1234567890", "192006250b4c09247ec02edce69f6a2d");
}
