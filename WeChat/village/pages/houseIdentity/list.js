import { wxLogin, domain, wxRequest, HouseIdentityStatus, size } from '../../utils/util.js';

Page({
  data: {
    domain, HouseIdentityStatus, houseIdentitys: [],
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/account/merchants/${info.mid}/allHouse`,
        header: { authorization, cid: info.cid },
      }).then((c0) => {
        if (c0) {
          const c1 = c0[0].children;
          this.setData({
            dataTree: c0,
            housesPicker: [c0, c1, []]
          });
        }
      });
      wxRequest({
        url: `${domain}/village/houseIdentitys`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { sort: 'floorNumber,unit,roomNumber,asc', size }
      }).then((data) => {
        this.setData({
          houseIdentitys: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      let data = { sort: 'floorNumber,unit,roomNumber,asc', size }
      const houseKeyword = this.data.houseKeyword;
      if (houseKeyword) {
        const keyword = houseKeyword.split(',')
        if (keyword[0]) {
          data.floorNumber = keyword[0];
        }
        if (keyword[1]) {
          data.unit = keyword[1];
        }
        if (keyword[2]) {
          data.roomNumber = keyword[2];
        }
      }
      const houseIdentityKeyword = this.data.houseIdentityKeyword;
      if (houseIdentityKeyword) {
        data.status = houseIdentityKeyword.key;
      }
      wxRequest({
        url: `${domain}/village/houseIdentitys`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          houseIdentitys: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      let data = { sort: 'floorNumber,unit,roomNumber,asc', size }
      const houseKeyword = this.data.houseKeyword;
      if (houseKeyword) {
        const keyword = houseKeyword.split(",");
        if (keyword[0]) {
          data.floorNumber = keyword[0];
        }
        if (keyword[1]) {
          data.unit = keyword[1];
        }
        if (keyword[2]) {
          data.roomNumber = keyword[2];
        }
      }
      const number = this.data.number;
      if (number) {
        data.page = number + 2;
      }

      const houseIdentityKeyword = this.data.houseIdentityKeyword;
      if (houseIdentityKeyword) {
        data.status = houseIdentityKeyword.key;
      }
      wxRequest({
        url: `${domain}/village/houseIdentitys`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let houseIdentitys = this.data.houseIdentitys;
          houseIdentitys.push(...data.content)
          this.setData({
            houseIdentitys, number: data.number
          });
        }
      });
    });
  },

  hoseColumnChange: function (e) {
    let c0;
    let c1;
    let c2;
    switch (e.detail.column) {
      case 0:
        c0 = this.data?.housesPicker[0];
        c1 = c0[e.detail.value]?.children;
        if (c1) {
          this.setData({
            ['housesPicker[1]']: c1,
            ['housesPicker[2]']: [],
          })
        }
        break;
      case 1:
        c1 = this.data?.housesPicker[1];
        c2 = c1[e.detail.value]?.children;
        if (c2) {
          this.setData({
            [`housesPicker[2]`]: c2
          })
        }
        break;
      default:
        break;
    }
  },

  hoseChange: function (e) {
    const housesPicker = this.data.housesPicker;
    if (housesPicker) {
      const housesIndex = e.detail.value;
      let houseKeyword;
      if (housesPicker[2].length > 0) {
        houseKeyword = housesPicker[2][housesIndex[2]]?.value
      } else if (housesPicker[1].length > 0) {
        houseKeyword = housesPicker[1][housesIndex[1]]?.value
      } else {
        houseKeyword = housesPicker[0][housesIndex[0]]?.value
      }

      if (houseKeyword) {
        wxLogin().then(({ authorization, info }) => {
          let data = { sort: 'floorNumber,unit,roomNumber,asc', size }
          const keyword = houseKeyword.split(",");
          if (keyword[0]) {
            data.floorNumber = keyword[0];
          }
          if (keyword[1]) {
            data.unit = keyword[1];
          }
          if (keyword[2]) {
            data.roomNumber = keyword[2];
          }
          const houseIdentityKeyword = this.data.houseIdentityKeyword;
          if (houseIdentityKeyword) {
            data.status = houseIdentityKeyword.key;
          }

          wxRequest({
            url: `${domain}/village/houseIdentitys`,
            header: { authorization, cid: info.cid, mid: info.mid },
            data: data
          }).then((data) => {
            this.setData({
              houseIdentitys: data.content, number: data.number, houseKeyword
            });
          });
        });
      }
    }
  },

  restHousesIndex: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/houseIdentitys`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { sort: 'floorNumber,unit,roomNumber,asc', size }
      }).then((data) => {
        let c1;
        let c0 = this.data.dataTree;
        if (c0) {
          c1 = c0[0].children;
        } else {
          c0 = []
          c1 = []
        }
        this.setData({
          houseIdentitys: data.content, number: data.number, housesPicker: [c0, c1, []], houseKeyword: null, houseIdentityKeyword: null,
        });
      });
    });
  },

  houseIdentityChange: function (e) {
    wxLogin().then(({ authorization, info }) => {
      let data = { sort: 'floorNumber,unit,roomNumber,asc', size }
      const houseKeyword = this.data.houseKeyword;
      if (houseKeyword) {
        const keyword = houseKeyword.split(',')
        if (keyword[0]) {
          data.floorNumber = keyword[0];
        }
        if (keyword[1]) {
          data.unit = keyword[1];
        }
        if (keyword[2]) {
          data.roomNumber = keyword[2];
        }
      }
      const houseIdentityKeyword = HouseIdentityStatus.range[e.detail.value];
      if (houseIdentityKeyword) {
        data.status = houseIdentityKeyword.key;
      }
      wxRequest({
        url: `${domain}/village/houseIdentitys`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: data
      }).then((data) => {
        this.setData({
          houseIdentitys: data.content, number: data.number, houseIdentityKeyword
        });
      });
    });
  },
})