import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/village/orderWaters', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const save = async (body?: any) =>
  request<any>('/village/orderWaters', {
    method: 'POST',
    data: body,
  });

export const del = async (body?: any) =>
  request<any>('/village/orderWaters', {
    method: 'DELETE',
    data: body,
  });

export const sedSubscribeMessage = async (params?: any) =>
  request<any>('/village/orderWaters/sedSubscribeMessage', { params });

export interface OrderWater {
  id: string;
  code: string;
  merchantId: string;
  floorNumber: string;
  unit: string;
  roomNumber: string;
  waterPrice: number;
  waterInitial: number;
  waterCurrent: number;
  waterCount: number;
  waterRatio: number;
  waterLoss: number;
  waterTotal: number;
  type: string;
  remarks: string;
  startDate: any;
  endDate: any;
  status: string;
  payDatetime: any;
  updateDatetime: any;
}
