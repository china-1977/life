package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.account.QCustomer;
import work.onss.community.domain.account.QMerchantCustomer;
import work.onss.community.domain.dto.village.HouseCustomerDto;
import work.onss.community.domain.dto.village.ParkSpaceNumber;
import work.onss.community.domain.dto.village.PeopleNumber;
import work.onss.community.domain.village.*;
import work.onss.community.domain.village.relational.HouseCustomer;
import work.onss.community.domain.village.relational.QHouseCustomer;
import work.onss.community.domain.vo.HouseFastCreate;
import work.onss.community.service.QuerydslService;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class HouseController {
    @Autowired
    private ParkSpaceRepository parkSpaceRepository;
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private QuerydslService querydslService;
    @Autowired
    private HouseRepository houseRepository;

    @GetMapping(value = {"houses/{id}"}, name = "房屋详情")
    public House score(@PathVariable String id) {
        return houseRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"houses/{id}/customers"}, name = "房屋成员")
    public List<HouseCustomer> customers(@RequestHeader(name = "mid") String mid, @PathVariable String id) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        QCustomer qCustomer = QCustomer.customer;
        return jpaQueryFactory.select(Projections.fields(HouseCustomer.class,
                        qCustomer,
                        qHouseCustomer.id,
                        qHouseCustomer.houseId,
                        qHouseCustomer.customerId,
                        qHouseCustomer.merchantId,
                        qHouseCustomer.relation
                ))
                .from(qHouseCustomer)
                .innerJoin(qCustomer).on(qCustomer.id.eq(qHouseCustomer.customerId))
                .where(qHouseCustomer.houseId.eq(id), qHouseCustomer.merchantId.eq(mid))
                .fetch();
    }

    @GetMapping(value = {"houses/getCustomers"}, name = "工作人员")
    public List<Customer> getCustomers(@RequestHeader(name = "mid") String mid) {
        QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
        QCustomer qCustomer = QCustomer.customer;
        return jpaQueryFactory.select(qCustomer)
                .from(qMerchantCustomer)
                .innerJoin(qCustomer).on(qCustomer.id.eq(qMerchantCustomer.customerId))
                .where(qMerchantCustomer.merchantId.eq(mid))
                .fetch();
    }

    @GetMapping(value = {"houses/{id}/parkSpaces"}, name = "房屋车位")
    public List<ParkSpace> parkSpaces(@RequestHeader(name = "mid") String mid, @PathVariable String id) {
        QParkSpace qParkSpace = QParkSpace.parkSpace;
        return jpaQueryFactory.select(qParkSpace)
                .from(qParkSpace)
                .where(qParkSpace.merchantId.eq(mid), qParkSpace.houseId.eq(id))
                .fetch();
    }

    @GetMapping(value = {"houses/{id}/detail"}, name = "房屋信息")
    public HouseCustomerDto detail(@RequestHeader(name = "mid") String mid, @PathVariable String id) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        QCustomer qCustomer = QCustomer.customer;
        House house = houseRepository.findById(id).orElse(null);
        List<HouseCustomer> houseCustomers = jpaQueryFactory.select(
                        Projections.fields(HouseCustomer.class,
                                qCustomer,
                                qHouseCustomer.id,
                                qHouseCustomer.houseId,
                                qHouseCustomer.customerId,
                                qHouseCustomer.merchantId,
                                qHouseCustomer.relation
                        )
                )
                .from(qHouseCustomer)
                .leftJoin(qCustomer).on(qCustomer.id.eq(qHouseCustomer.customerId))
                .where(qHouseCustomer.houseId.eq(id), qHouseCustomer.merchantId.eq(mid))
                .fetch();
        List<ParkSpace> parkSpaces = parkSpaceRepository.findAllByHouseId(id);
        return new HouseCustomerDto(house, houseCustomers, parkSpaces);
    }

    @GetMapping(value = {"houses"}, name = "房屋列表")
    public Page<House> page(@RequestHeader(name = "mid") String mid, @QuerydslPredicate(bindings = HouseRepository.class) Predicate predicate, @PageableDefault Pageable pageable) {
        QHouse qHouse = QHouse.house;
        return houseRepository.findAll(qHouse.merchantId.eq(mid).and(predicate), pageable);
    }

    @PostMapping(value = {"houses"}, name = "房屋创建")
    public void insert(@RequestHeader(name = "mid") String mid, @RequestBody @Validated House house) {
        house.setId(null);
        house.setMerchantId(mid);
        houseRepository.save(house);
    }

    @PostMapping(value = {"houses/houseFastCreate"}, name = "房屋快速创建")
    public void houseFastCreate(@RequestHeader(name = "mid") String mid, @RequestBody @Validated HouseFastCreate houseFastCreate) {
        for (HouseFastCreate.Floor floor : houseFastCreate.getFloors()) {
            for (int i = 1; i <= floor.getCount(); i++) {
                int roomNumber = 1;
                for (HouseFastCreate.HouseType houseType : houseFastCreate.getHouseTypes()) {
                    House house = new House();
                    house.setMerchantId(mid);
                    house.setFloorNumber(floor.getFloorNumber());
                    house.setUnit(houseType.getUnit());
                    house.setType(houseType.getType());
                    house.setArea(houseType.getArea());
                    house.setFloor(i);
                    house.setRoomNumber(String.join("-", String.valueOf(i), String.valueOf(roomNumber)));
                    house.setMerchantId(mid);
                    try {
                        houseRepository.save(house);
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                    roomNumber++;
                }
            }
        }

    }

    /**
     * house_customer 房屋成员 删除、更换
     * house_identity 入驻申请 删除、更换
     * house_renting 房屋租赁 删除、更换
     * house_vacant 空置房屋 删除、更换
     * order_heat 采暖订单 删除、更换
     * order_house 物业费 删除、更换
     * order_water 水费 删除、更换
     * parkSpace 车位 删除、更换
     * visitor 访客 删除、更换
     * voter 投票者 删除、更换
     *
     * @param mid
     * @param house
     */
    @PostMapping(value = {"houses/updateHouse"}, name = "房屋编辑")
    public void updateHouse(@RequestHeader(name = "mid") String mid, @RequestBody @Validated House house) {
        querydslService.updateHouse(house, mid);
    }

    @PostMapping(value = {"houses/syncPeopleNumber"}, name = "房屋人数和车位同步")
    public void syncPeopleNumber(@RequestHeader(name = "mid") String mid) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        List<PeopleNumber> peopleNumbers = jpaQueryFactory
                .select(Projections.constructor(PeopleNumber.class, qHouseCustomer.houseId, qHouseCustomer.houseId.count().intValue()))
                .from(qHouseCustomer)
                .where(qHouseCustomer.merchantId.eq(mid))
                .groupBy(qHouseCustomer.houseId)
                .fetch();

        QParkSpace qParkSpace = QParkSpace.parkSpace;
        List<ParkSpaceNumber> parkSpaceNumbers = jpaQueryFactory
                .select(Projections.constructor(ParkSpaceNumber.class, qParkSpace.houseId, qParkSpace.houseId.count().intValue()))
                .from(qParkSpace)
                .where(qParkSpace.merchantId.eq(mid))
                .groupBy(qParkSpace.houseId)
                .fetch();

        querydslService.setPeopleNumberZero(mid);
        for (PeopleNumber peopleNumber : peopleNumbers) {
            querydslService.setPeopleNumber(peopleNumber.getHouseId(), peopleNumber.getCount());
        }

        for (ParkSpaceNumber parkSpaceNumber : parkSpaceNumbers) {
            querydslService.setParkSpaceNumber(parkSpaceNumber.getHouseId(), parkSpaceNumber.getSize());
        }
    }

    /**
     * house_customer 用户迁移
     * identity 入住申请迁移
     * order_water 水费迁移
     * parkSpace 车位迁移
     * voter 投票者迁移
     *
     * @param mid 主体ID
     * @param ids 房屋ID
     */
    @DeleteMapping(value = {"houses"}, name = "房屋批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        // TODO 删除房屋前，迁移房屋相关数据
        houseRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @PostMapping(value = "houses/imports", name = "房屋导入")
    public void imports(@RequestHeader(name = "mid") String mid, @RequestParam(value = "file") MultipartFile file) throws Exception {
        List<House> houses = EasyExcel.read(file.getInputStream()).head(House.class).doReadAllSync();
        for (House house : houses) {
            if (house.getId() == null) {
                house.setMerchantId(mid);
                houseRepository.save(house);
            } else {
                querydslService.updateHouse(house, mid);
            }
        }
    }

    @PostMapping(value = {"houses/export"}, name = "房屋导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = HouseRepository.class, root = House.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QHouse qHouse = QHouse.house;
        Collection<House> data = (Collection<House>) houseRepository.findAll(qHouse.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), House.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<House>> floorNumberData = data.stream().collect(Collectors.groupingBy(House::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }

    @PostMapping(value = {"houses/exportOrderHouse"}, name = "物业费模板导出")
    public void exportOrderHouse(@RequestHeader(name = "mid") String mid,
                                 @QuerydslPredicate(bindings = HouseRepository.class, root = House.class) Predicate predicate,
                                 HttpServletResponse httpServletResponse) throws Exception {
        QHouse qHouse = QHouse.house;
        Collection<House> data = (Collection<House>) houseRepository.findAll(qHouse.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderHouse.class).build();
        Map<String, List<OrderHouse>> floorNumberData = data.stream().map(OrderHouse::new).collect(Collectors.groupingBy(OrderHouse::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }

    @PostMapping(value = {"houses/exportOrderWater"}, name = "水费模板导出")
    public void exportOrderWater(@RequestHeader(name = "mid") String mid,
                                 @QuerydslPredicate(bindings = HouseRepository.class, root = House.class) Predicate predicate,
                                 HttpServletResponse httpServletResponse) throws Exception {
        QHouse qHouse = QHouse.house;
        Collection<House> data = (Collection<House>) houseRepository.findAll(qHouse.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderWater.class).build();
        Map<String, List<OrderWater>> floorNumberData = data.stream().map(OrderWater::new).collect(Collectors.groupingBy(OrderWater::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }

    @PostMapping(value = {"houses/exportOrderHeat"}, name = "采暖费模板导出")
    public void exportOrderHeat(@RequestHeader(name = "mid") String mid,
                                @QuerydslPredicate(bindings = HouseRepository.class, root = House.class) Predicate predicate,
                                HttpServletResponse httpServletResponse) throws Exception {
        QHouse qHouse = QHouse.house;
        Collection<House> data = (Collection<House>) houseRepository.findAll(qHouse.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderHeat.class).autoCloseStream(true).build();
        Map<String, List<OrderHeat>> floorNumberData = data.stream().map(OrderHeat::new).collect(Collectors.groupingBy(OrderHeat::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }

    @Transactional
    @PostMapping(value = {"houses/{id}/bindingParkSpace"}, name = "房屋绑定车位")
    public void bindingParkSpace(@PathVariable String id, @RequestHeader(name = "mid") String mid, @RequestBody List<String> parkSpaces) {
        QParkSpace qParkSpace = QParkSpace.parkSpace;
        if (parkSpaces.isEmpty()) {
            jpaQueryFactory.update(qParkSpace)
                    .set(qParkSpace.houseId, Expressions.nullExpression())
                    .where(qParkSpace.merchantId.eq(mid), qParkSpace.houseId.eq(id))
                    .execute();
        } else {
            jpaQueryFactory.update(qParkSpace)
                    .set(qParkSpace.houseId, id)
                    .where(qParkSpace.id.in(parkSpaces), qParkSpace.merchantId.eq(mid), qParkSpace.houseId.ne(id).or(qParkSpace.houseId.isNull()))
                    .execute();
            jpaQueryFactory.update(qParkSpace)
                    .set(qParkSpace.houseId, Expressions.nullExpression())
                    .where(qParkSpace.merchantId.eq(mid), qParkSpace.houseId.eq(id), qParkSpace.id.notIn(parkSpaces))
                    .execute();
        }
    }

    @GetMapping(value = {"houses/emptyCustomer"}, name = "房屋待授权列表")
    public List<House> customers(@RequestHeader(name = "mid") String mid) {
        QHouse qHouse = QHouse.house;
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        return jpaQueryFactory.select(qHouse)
                .from(qHouse)
                .leftJoin(qHouseCustomer).on(qHouse.id.eq(qHouseCustomer.houseId))
                .where(qHouse.merchantId.eq(mid), qHouseCustomer.id.isNull())
                .fetch();
    }

}