package work.onss.community.domain.wechat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AccessTokenResult implements Serializable {

    private String access_token;
    private Long expires_in;
    private Integer errcode;
    private String errmsg;
}
