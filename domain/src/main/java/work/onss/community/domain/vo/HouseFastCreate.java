package work.onss.community.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HouseFastCreate {

    private List<Floor> floors;
    private List<HouseType> houseTypes;


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Floor {
        private String floorNumber;
        private Integer count;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class HouseType {
        private String unit;
        private String type;
        private BigDecimal area;
    }

}
