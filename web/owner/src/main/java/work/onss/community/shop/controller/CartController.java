package work.onss.community.shop.controller;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.QMerchant;
import work.onss.community.domain.dto.shop.ProductDetailDto;
import work.onss.community.domain.shop.*;
import work.onss.community.service.exception.BusinessException;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
public class CartController {
    @Autowired
    protected JPAQueryFactory jpaQueryFactory;
    @Autowired
    protected CartRepository cartRepository;
    @Autowired
    protected ProductRepository productRepository;

    /**
     * @param cart 购物车
     */
    @PostMapping(value = {"carts"})
    public void updateNum(@Validated @RequestBody Cart cart, @RequestHeader String cid) {
        if (cart.getNum().intValue() == 0) {
            cartRepository.deleteByIdAndCustomerId(cart.getId(), cid);
        } else {
            Product product = productRepository.findById(cart.getProductId()).orElseThrow(() -> new RuntimeException("该商品已下架"));
            if (cart.getNum().compareTo(product.getMax()) > 0) {
                throw new BusinessException("FAIL", MessageFormat.format("每次仅限购买{0}至{1}", product.getMin(), product.getMax()), product);
            } else if (cart.getNum().compareTo(product.getStock()) > 0) {
                throw new BusinessException("FAIL", "库存不足", product);
            } else if (!product.getStatus()) {
                throw new BusinessException("该商品已下架");
            }
            BigDecimal total = product.getPrice().multiply(new BigDecimal(cart.getNum()));
            Cart oldCart = cartRepository.findByMerchantIdAndCustomerIdAndProductId(cart.getMerchantId(), cid, product.getId()).orElse(null);
            if (oldCart == null) {
                cart.setChecked(true);
                cart.setCustomerId(cid);
                cart.setTotal(total);
                cartRepository.save(cart);
            } else {
                oldCart.setNum(cart.getNum());
                oldCart.setTotal(total);
                cartRepository.save(oldCart);
            }
        }
    }

    /**
     * @param merchantId 商户ID
     * @return 购物车
     */
    @GetMapping(value = {"carts"})
    public Map<String, Object> getCarts(@RequestParam(name = "merchantId") String merchantId, @RequestHeader String cid) {
        QMerchant qMerchant = QMerchant.merchant;
        Merchant merchant = jpaQueryFactory.select(Projections.fields(
                        Merchant.class,
                        qMerchant.id,
                        qMerchant.shortname,
                        qMerchant.username,
                        qMerchant.phone,
                        qMerchant.addressName,
                        qMerchant.addressDetail,
                        qMerchant.location,
                        qMerchant.postcode,
                        qMerchant.addressCode,
                        qMerchant.addressValue
                ))
                .from(qMerchant).where(qMerchant.id.eq(merchantId)).fetchFirst();
        if (merchant == null) {
            throw new BusinessException("该商户不存在");
        }
        QCart qCart = QCart.cart;
        QProduct qProduct = QProduct.product;
        List<ProductDetailDto> productDetailDtos = jpaQueryFactory
                .select(Projections.constructor(
                        ProductDetailDto.class,
                        qProduct,
                        qCart,
                        (qProduct.price.multiply(qCart.num)).as(qCart.total)
                ))
                .from(qCart)
                .innerJoin(qProduct).on(qProduct.id.eq(qCart.productId), qCart.merchantId.eq(merchantId), qCart.customerId.eq(cid))
                .orderBy(qCart.id.desc())
                .fetch();
        BigDecimal sum = BigDecimal.ZERO;
        boolean checkAll = true;
        for (ProductDetailDto productDetailDto : productDetailDtos) {
            if (null != productDetailDto.getCartId() && productDetailDto.getChecked()) {
                sum = sum.add(productDetailDto.getTotal());
            } else {
                checkAll = false;
            }
        }
        return Map.of("store", merchant, "products", productDetailDtos, "checkAll", checkAll, "sum", sum);
    }

    @Transactional
    @PostMapping(value = {"carts/{id}/setChecked"})
    public void setChecked(@PathVariable String id, @RequestParam(name = "checked") Boolean checked, @RequestHeader String cid) {
        QCart qCart = QCart.cart;
        jpaQueryFactory.update(qCart)
                .set(qCart.checked, !checked)
                .where(qCart.id.eq(id), qCart.customerId.eq(cid))
                .execute();
    }

    @DeleteMapping(value = {"carts"})
    public void clear(@RequestBody Cart cart, @RequestHeader String cid) {
        cartRepository.deleteByMerchantIdAndCustomerId(cart.getMerchantId(), cid);
    }
}
