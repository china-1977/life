Page({
  mixins: [require('../../mixin/common')],
  data: {
    orderHeats: [],
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderHeats`,
        header: { authorization },
        data: { year: this.data.year, size: this.data.size }
      }).then((data) => {
        this.setData({
          orderHeats: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderHeats`,
        header: { authorization },
        data: { year: this.data.year, size: this.data.size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          orderHeats: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderHeats`,
        header: { authorization },
        data: { page: this.data.number + 2, year: this.data.year, size: this.data.size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let orderHeats = this.data.orderHeats;
          orderHeats.push(...data.content)
          this.setData({
            orderHeats, number: data.number
          });
        }
      });
    });
  },

  yearChange: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderHeats`,
        header: { authorization },
        data: { year: e.detail.value }
      }).then((data) => {
        this.setData({
          orderHeats: data.content, number: data.number, year: e.detail.value
        });
      });
    });
  }
})