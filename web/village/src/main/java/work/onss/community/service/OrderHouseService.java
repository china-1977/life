package work.onss.community.service;

import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.account.QCustomer;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.config.SystemConfig;
import work.onss.community.domain.village.*;
import work.onss.community.domain.village.relational.QHouseCustomer;
import work.onss.community.domain.vo.Info;
import work.onss.community.domain.wechat.SubscribeMessage;
import work.onss.community.service.utils.Utils;
import work.onss.community.service.wechat.MiniAppService;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Log4j2
@Service
public class OrderHouseService {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private MiniAppService miniAppService;
    @Autowired
    private SystemConfig systemConfig;

    @Async
    @Transactional
    public void sedSubscribeMessage(OrderHouse orderHouse, String shortname, String accessToken, LocalDateTime now) {
        if (orderHouse.getNoticeDatetime() == null || orderHouse.getNoticeDatetime().until(now, ChronoUnit.DAYS) >= 0) {
            QCustomer qCustomer = QCustomer.customer;
            QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
            List<Customer> customers = jpaQueryFactory.select(qCustomer).from(qHouseCustomer)
                    .innerJoin(qCustomer).on(qHouseCustomer.customerId.eq(qCustomer.id))
                    .where(qHouseCustomer.houseId.eq(orderHouse.getHouseId()))
                    .fetch();
            customers.forEach(customer -> {
                Info info = new Info(now, customer.getId());
                String subject = JacksonUtils.writeValueAsString(info);
                String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.plusDays(15).toInstant(ZoneOffset.of("+8")), subject, customer.getId(), "WeChat");
                SubscribeMessage subscribeMessage = SubscribeMessage.getSubscribeMessage1(
                        orderHouse.getId(),
                        customer.getId(),
                        authorization,
                        customer.getOpenid(),
                        shortname,
                        String.join(",", orderHouse.getFloorNumber(), orderHouse.getUnit(), orderHouse.getRoomNumber()),
                        String.join("~", orderHouse.getStartDate().toString(), orderHouse.getEndDate().toString()),
                        orderHouse.getTotal().toPlainString().concat("元"),
                        "待缴费"
                );
                String data = JacksonUtils.writeValueAsString(subscribeMessage);
                String message = miniAppService.sendSubscribeMessage(data, accessToken);
                log.error("message:{}", message);
            });
            QOrderHouse qOrderHouse = QOrderHouse.orderHouse;
            jpaQueryFactory.update(qOrderHouse)
                    .set(qOrderHouse.noticeDatetime, now)
                    .where(qOrderHouse.id.eq(orderHouse.getId()))
                    .execute();
        }
    }

    @Async
    @Transactional
    public void sedSubscribeMessage(OrderWater orderWater, String shortname, String accessToken, LocalDateTime now) {
        if (orderWater.getNoticeDatetime() == null || orderWater.getNoticeDatetime().until(now, ChronoUnit.DAYS) >= 0) {
            QCustomer qCustomer = QCustomer.customer;
            QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
            List<Customer> customers = jpaQueryFactory.select(qCustomer).from(qHouseCustomer)
                    .innerJoin(qCustomer).on(qHouseCustomer.customerId.eq(qCustomer.id))
                    .where(qHouseCustomer.houseId.eq(orderWater.getHouseId()))
                    .fetch();
            customers.forEach(customer -> {
                Info info = new Info(now, customer.getId());
                String subject = JacksonUtils.writeValueAsString(info);
                String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.plusDays(15).toInstant(ZoneOffset.of("+8")), subject, customer.getId(), "WeChat");

                SubscribeMessage subscribeMessage = SubscribeMessage.getSubscribeMessage6(
                        orderWater.getId(),
                        customer.getId(),
                        authorization,
                        customer.getOpenid(),
                        shortname,
                        String.join("-", orderWater.getFloorNumber(), orderWater.getUnit(), orderWater.getRoomNumber()),
                        orderWater.getWaterCount().toPlainString(),
                        orderWater.getWaterTotal().toPlainString().concat("元"),
                        "待缴费"
                );
                String data = JacksonUtils.writeValueAsString(subscribeMessage);
                String message = miniAppService.sendSubscribeMessage(data, accessToken);
                log.error("message:{}", message);
            });
            QOrderWater qOrderWater = QOrderWater.orderWater;
            jpaQueryFactory.update(qOrderWater)
                    .set(qOrderWater.noticeDatetime, now)
                    .where(qOrderWater.id.eq(orderWater.getId()))
                    .execute();
        }
    }

    @Async
    @Transactional
    public void sedSubscribeMessage(Vote vote, String accessToken, LocalDateTime now) {
        if (vote.getNoticeDatetime() == null || vote.getNoticeDatetime().until(now, ChronoUnit.DAYS) >= 0) {
            QCustomer qCustomer = QCustomer.customer;
            QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
            QVoter qVoter = QVoter.voter;
            List<Customer> customers = jpaQueryFactory.select(qCustomer).from(qHouseCustomer)
                    .innerJoin(qCustomer).on(qHouseCustomer.customerId.eq(qCustomer.id))
                    .leftJoin(qVoter).on(qVoter.houseId.eq(qHouseCustomer.houseId))
                    .where(qHouseCustomer.merchantId.eq(vote.getMerchantId()), qVoter.id.isNull())
                    .fetch();
            customers.forEach(customer -> {
                Info info = new Info(now, customer.getId());
                String subject = JacksonUtils.writeValueAsString(info);
                String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.plusDays(15).toInstant(ZoneOffset.of("+8")), subject, customer.getId(), "WeChat");

                SubscribeMessage subscribeMessage = SubscribeMessage.getSubscribeMessage5(
                        vote.getId(),
                        customer.getId(),
                        authorization,
                        customer.getOpenid(),
                        vote.getTitle(),
                        vote.getDescription()
                );
                String data = JacksonUtils.writeValueAsString(subscribeMessage);
                String message = miniAppService.sendSubscribeMessage(data, accessToken);
                log.error("message:{}", message);
            });
            QVote qVote = QVote.vote;
            jpaQueryFactory.update(qVote)
                    .set(qVote.noticeDatetime, now)
                    .where(qVote.id.eq(vote.getId()))
                    .execute();
        }
    }

    @Async
    @Transactional
    public void sedSubscribeMessage(Notice notice, String shortname, String accessToken, LocalDateTime now) {
        if (notice.getNoticeDatetime() == null || notice.getNoticeDatetime().until(now, ChronoUnit.DAYS) >= 0) {
            QCustomer qCustomer = QCustomer.customer;
            QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
            List<Customer> customers = jpaQueryFactory.select(qCustomer).from(qHouseCustomer)
                    .innerJoin(qCustomer).on(qHouseCustomer.customerId.eq(qCustomer.id))
                    .where(qHouseCustomer.merchantId.eq(notice.getMerchantId()))
                    .fetch();
            customers.forEach(customer -> {
                Info info = new Info(now, customer.getId());
                String subject = JacksonUtils.writeValueAsString(info);
                String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.plusDays(15).toInstant(ZoneOffset.of("+8")), subject, customer.getId(), "WeChat");

                SubscribeMessage subscribeMessage = SubscribeMessage.getSubscribeMessage7(
                        notice.getId(),
                        customer.getId(),
                        authorization,
                        customer.getOpenid(),
                        shortname,
                        notice.getTitle(),
                        notice.getInsertDatetime().toString(),
                        notice.getDescription()
                );
                String data = JacksonUtils.writeValueAsString(subscribeMessage);
                String message = miniAppService.sendSubscribeMessage(data, accessToken);
                log.error("message:{}", message);
            });
            QNotice qNotice = QNotice.notice;
            jpaQueryFactory.update(qNotice)
                    .set(qNotice.noticeDatetime, now)
                    .where(qNotice.id.eq(notice.getId()))
                    .execute();
        }
    }
}
