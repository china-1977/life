import { wxLogin, domain, wxRequest, RepairStatus, size } from '../../utils/util.js';
Page({

  data: {
    RepairStatus
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/repairs`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { status: 'WAIT_REPAIR', repairerCustomerId: info.cid, size }
      }).then((data) => {
        this.setData({
          repairs: data.content, number: data.number
        });
      });
    });
  },

  onShow() {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/repairs`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { status: 'WAIT_REPAIR', repairerCustomerId: info.cid, size }
      }).then((data) => {
        this.setData({
          repairs: data.content, number: data.number
        });
      });
    });
  },
  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/repairs`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { status: 'WAIT_REPAIR', repairerCustomerId: info.cid, size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          repairs: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/repairs`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { page: this.data.number + 2, status: 'WAIT_REPAIR', repairerCustomerId: info.cid, size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let repairs = this.data.repairs;
          repairs.push(...data.content)
          this.setData({
            repairs, number: data.number
          });
        }
      });
    });
  },

})