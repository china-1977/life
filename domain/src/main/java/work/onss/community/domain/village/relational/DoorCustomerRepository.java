package work.onss.community.domain.village.relational;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

public interface DoorCustomerRepository extends JpaRepository<DoorCustomer, String>, JpaSpecificationExecutor<DoorCustomer>, QuerydslPredicateExecutor<DoorCustomer>, QuerydslBinderCustomizer<QDoorCustomer> {

    default void customize(QuerydslBindings bindings, QDoorCustomer qDoorCustomer) {
        bindings.bind(qDoorCustomer.id).withDefaultBinding();
    }

}
