package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.MerchantRepository;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.park.OrderPark;
import work.onss.community.domain.park.OrderParkRepository;
import work.onss.community.domain.village.*;
import work.onss.community.domain.vo.RefundReason;
import work.onss.community.domain.vo.Work;
import work.onss.community.domain.wechat.WXNotify;
import work.onss.community.domain.wechat.WXRefundRequest;
import work.onss.community.domain.wechat.WXRefundResult;
import work.onss.community.domain.wechat.WxPayToken;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.exception.BusinessException;
import work.onss.community.service.utils.Utils;
import work.onss.community.service.wechat.WechatMpProperties;
import work.onss.community.service.wechat.WechatPayService;

import java.net.URI;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static work.onss.community.domain.village.PayOrder.OrderType.*;

@Log4j2
@RestController
public class PayOrderController {
    @Autowired
    private PayOrderRepository payOrderRepository;
    @Autowired
    private OrderWaterRepository orderWaterRepository;
    @Autowired
    private OrderHeatRepository orderHeatRepository;
    @Autowired
    private OrderHouseRepository orderHouseRepository;
    @Autowired
    private OrderParkRepository orderParkRepository;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private WechatMpProperties wechatMpProperties;
    @Autowired
    private WechatPayService wechatPayService;
    @Autowired
    private QuerydslService querydslService;

    @GetMapping(value = {"payOrders/{id}"}, name = "支付记录详情")
    public Map<String, Object> detail(@PathVariable String id) {
        PayOrder payOrder = payOrderRepository.findById(id).orElse(null);
        if (payOrder == null) {
            return null;
        } else {
            Map<PayOrder.OrderType, List<String>> orderTypeListMap = payOrder.getDetails().stream()
                    .collect(Collectors.groupingBy(PayOrder.Detail::getOrderType, Collectors.mapping(PayOrder.Detail::getId, Collectors.toList())));
            Map<String, Object> data = new HashMap<>(orderTypeListMap.size() + 1);
            for (Map.Entry<PayOrder.OrderType, List<String>> orderTypeListEntry : orderTypeListMap.entrySet()) {
                List<String> value = orderTypeListEntry.getValue();
                PayOrder.OrderType key = orderTypeListEntry.getKey();
                switch (key) {
                    case ORDER_WATER -> {
                        List<OrderWater> orderWaters = orderWaterRepository.findAllById(value);
                        data.put(ORDER_WATER.name(), orderWaters);
                    }
                    case ORDER_HEAT -> {
                        List<OrderHeat> orderHeats = orderHeatRepository.findAllById(value);
                        data.put(ORDER_HEAT.name(), orderHeats);
                    }
                    case ORDER_HOUSE -> {
                        List<OrderHouse> orderHouses = orderHouseRepository.findAllById(value);
                        data.put(ORDER_HOUSE.name(), orderHouses);
                    }
                    case ORDER_PARK -> {
                        List<OrderPark> orderParks = orderParkRepository.findAllById(value);
                        data.put(ORDER_PARK.name(), orderParks);
                    }
                }
            }
            data.put("PAY_ORDER", payOrder);
            return data;
        }
    }

    @GetMapping(value = {"payOrders"}, name = "支付记录列表")
    public Page<PayOrder> page(
            @RequestHeader(name = "mid") String mid,
            @RequestParam Integer year,
            @QuerydslPredicate(bindings = PayOrderRepository.class) Predicate predicate,
            @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QPayOrder qPayOrder = QPayOrder.payOrder;
        return payOrderRepository.findAll(qPayOrder.merchantId.eq(mid).and(qPayOrder.payDatetime.year().eq(year)).and(predicate), pageable);
    }

    @PostMapping(value = {"payOrders/export"}, name = "支付记录导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = PayOrderRepository.class, root = PayOrder.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QPayOrder qPayOrder = QPayOrder.payOrder;
        Collection<PayOrder> data = (Collection<PayOrder>) payOrderRepository.findAll(qPayOrder.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), PayOrder.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        excelWriter.finish();
    }

    @PostMapping(value = {"payOrders/{id}/refundOrderHouse"}, name = "物业费退款")
    public WXRefundResult refundOrderHouse(@RequestHeader(name = "mid") String mid, @PathVariable String id, @RequestBody RefundReason refundReason) {

        QPayOrder qPayOrder = QPayOrder.payOrder;
        PayOrder payOrder = payOrderRepository.findOne(qPayOrder.id.eq(id).and(qPayOrder.merchantId.eq(mid))).orElseThrow(() -> new BusinessException("支付订单不存在"));
        Merchant merchant = merchantRepository.findById(payOrder.getMerchantId()).orElseThrow(() -> new BusinessException("商户不存在"));

        LocalDateTime now = LocalDateTime.now();
        String nowStr = now.format(DateTimeFormatter.ofPattern("yyMMddHHmmssSS"));
        int i = RandomUtils.nextInt(9999);
        String outRefundNo = String.format("%s%04d", nowStr, i);

        WXRefundRequest.Amount amount = WXRefundRequest.Amount.builder()
                .refund(refundReason.getRefund().movePointRight(2).longValue())
                .currency("CNY")
                .total(payOrder.getTotal().movePointRight(2).longValue())
                .build();

        WXRefundRequest wxRefundRequest = WXRefundRequest.builder()
                .subMchid(merchant.getSubMchId())
                .outTradeNo(payOrder.getOutTradeNo())
                .outRefundNo(outRefundNo)
                .reason("物业费退款")
                .notifyUrl("https://1977.work/owner/order/refund")
                .amount(amount)
                .build();

        String json = JacksonUtils.writeValueAsString(wxRefundRequest);

        URI uri = URI.create("https://api.mch.weixin.qq.com/v3/refund/domestic/refunds");
        String nonceStr = UUID.randomUUID().toString().replace("-", "");
        String timestamp = String.valueOf(now.toEpochSecond(ZoneOffset.ofHours(8)));
        String signature = Utils.sign(wechatMpProperties.getPrivateKey(), "POST", uri.getPath(), timestamp, nonceStr, json);
        WxPayToken wxPayToken = WxPayToken.builder()
                .serialNo(wechatMpProperties.getCertSerialNo())
                .mchid(wechatMpProperties.getMchId())
                .signature(signature)
                .nonceStr(nonceStr)
                .timestamp(timestamp)
                .build();

        String authorization = Utils.getWxPayToken(wxPayToken);

        return wechatPayService.refunds(json, authorization);
    }

    @PostMapping(value = {"payOrders/{id}/refundOrderWater"}, name = "水费退款")
    public WXRefundResult refundOrderHouses(@RequestHeader(name = "mid") String mid, @PathVariable String id, @RequestBody RefundReason refundReason) {

        QPayOrder qPayOrder = QPayOrder.payOrder;
        PayOrder payOrder = payOrderRepository.findOne(qPayOrder.id.eq(id).and(qPayOrder.merchantId.eq(mid))).orElseThrow(() -> new BusinessException("支付订单不存在"));
        Merchant merchant = merchantRepository.findById(payOrder.getMerchantId()).orElseThrow(() -> new BusinessException("商户不存在"));

        LocalDateTime now = LocalDateTime.now();
        String nowStr = now.format(DateTimeFormatter.ofPattern("yyMMddHHmmssSS"));
        int i = RandomUtils.nextInt(9999);
        String outRefundNo = String.format("%s%04d", nowStr, i);

        WXRefundRequest.Amount amount = WXRefundRequest.Amount.builder()
                .refund(refundReason.getRefund().movePointRight(2).longValue())
                .currency("CNY")
                .total(payOrder.getTotal().movePointRight(2).longValue())
                .build();

        WXRefundRequest wxRefundRequest = WXRefundRequest.builder()
                .subMchid(merchant.getSubMchId())
                .outTradeNo(payOrder.getOutTradeNo())
                .outRefundNo(outRefundNo)
                .reason("水费退款")
                .notifyUrl("https://1977.work/owner/order/refund")
                .amount(amount)
                .build();

        String json = JacksonUtils.writeValueAsString(wxRefundRequest);

        URI uri = URI.create("https://api.mch.weixin.qq.com/v3/refund/domestic/refunds");
        String nonceStr = UUID.randomUUID().toString().replace("-", "");
        String timestamp = String.valueOf(now.toEpochSecond(ZoneOffset.ofHours(8)));
        String signature = Utils.sign(wechatMpProperties.getPrivateKey(), "POST", uri.getPath(), timestamp, nonceStr, json);
        WxPayToken wxPayToken = WxPayToken.builder()
                .serialNo(wechatMpProperties.getCertSerialNo())
                .mchid(wechatMpProperties.getMchId())
                .signature(signature)
                .nonceStr(nonceStr)
                .timestamp(timestamp)
                .build();

        String authorization = Utils.getWxPayToken(wxPayToken);

        return wechatPayService.refunds(json, authorization);
    }

    @PostMapping(value = {"payOrders/refundNotify"})
    public Work<String> refundNotify(@RequestBody WXNotify wxNotify) throws GeneralSecurityException {
        WXNotify.Resource resource = wxNotify.getResource();
        String decryptToString = Utils.decryptToString(resource.getAssociatedData(), resource.getNonce(), resource.getCiphertext(), wechatMpProperties.getApiV3Key());
        WXNotify.WXRefund wxRefund = JacksonUtils.readValue(decryptToString, WXNotify.WXRefund.class);
        QPayOrder qPayOrder = QPayOrder.payOrder;
        PayOrder payOrder = payOrderRepository.findOne(qPayOrder.outTradeNo.eq(wxRefund.getOutTradeNo())).orElse(null);
        if (payOrder == null) {
            log.error("订单丢失:{}", decryptToString);
            return Work.success();
        }
        querydslService.updatePayStatus(wxRefund, payOrder);
        return Work.success();
    }
}