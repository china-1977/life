package work.onss.community.controller;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import work.onss.community.domain.config.SystemConfig;
import work.onss.community.service.utils.Utils;

import java.nio.file.Path;
import java.util.Objects;

/**
 * 用户管理
 *
 * @author wangchanghao
 */
@Log4j2
@RestController
public class UploadController {
    @Autowired
    private SystemConfig systemConfig;

    /**
     * @param file 文件
     * @return 文件存储路径
     * @throws Exception 文件上传失败异常
     */
    @PostMapping(value = "upload/face", name = "上传人脸图像")
    public String uploadPicture(@RequestParam(value = "file") MultipartFile file) throws Exception {
        String subtype = MediaType.valueOf(Objects.requireNonNull(file.getContentType())).getSubtype();
        String fileName = DigestUtils.sha256Hex(file.getBytes()).concat(".").concat(subtype);
        Path path = Utils.uploadFile(file, systemConfig.getFilePath(), "customer", "face", fileName);
        return StringUtils.cleanPath(path.toString());
    }
}

