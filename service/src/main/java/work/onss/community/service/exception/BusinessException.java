package work.onss.community.service.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import work.onss.community.domain.vo.Code;

/**
 * @author wangchanghao
 */
@EqualsAndHashCode(callSuper = true)
@ResponseStatus(code = HttpStatus.NOT_EXTENDED)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BusinessException extends RuntimeException {
    private String code;
    private String message;
    private Object data;

    public BusinessException(String message) {
        this.code = Code.FAIL.name();
        this.message = message;
        this.data = null;
    }
}
