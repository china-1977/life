Page({
  mixins: [require('../../mixin/common')],
  data: {
    notices: []
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/notices`,
        header: { authorization },
        data: { size: this.data.size }
      }).then((data) => {
        if (data) {
          this.setData({
            notices: data.content, number: data.number
          });
        }
      });
    });
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/notices`,
        header: { authorization },
        data: { size: this.data.size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        if (data) {
          this.setData({
            notices: data.content, number: data.number
          });
        }
      });
    });
  },

  onReachBottom: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/notices`,
        header: { authorization },
        data: { page: this.data.number + 2, size: this.data.size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let notices = this.data.notices;
          notices.push(...data.content)
          this.setData({
            notices, number: data.number
          });
        }
      });
    });
  }
})