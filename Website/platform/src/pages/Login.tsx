import { Button, Card, Col, Form, Input, Layout, Row, Space } from 'antd';

import services from '@/services/api';
import { replacer } from '@/utils/format';
import { history, useModel, useRequest } from '@@/exports';

const { login } = services.LoginController;

const Login = () => {
  const [form] = Form.useForm();
  const { refresh } = useModel('@@initialState');
  let { loading: loginLoading, run: loginRun } = useRequest(login, {
    manual: true,
  });
  const onFinish = (values: any) => {
    loginRun(values)
      .then((res: any) => {
        console.log(res);
        localStorage.setItem('info', JSON.stringify(res.info, replacer));
        localStorage.setItem('authorization', res.authorization);
        refresh();
      })
      .then(() => {
        history.push('/');
      });
  };

  return (
    <Layout>
      <Layout.Header>Header</Layout.Header>
      <Layout.Content style={{ paddingBlock: 140 }}>
        <Row justify="space-around" align="middle">
          <Col span={6}>
            <Card title="登录" bordered={false} style={{ textAlign: 'center' }}>
              <Form form={form} onFinish={onFinish} disabled={loginLoading}>
                <Form.Item label="账号" name="username" rules={[{ required: true, message: '请输入登录账号' }]}>
                  <Input />
                </Form.Item>
                <Form.Item label="密码" name="password" rules={[{ required: true, message: '请输入6~12位密码' }]}>
                  <Input.Password />
                </Form.Item>
                <Form.Item>
                  <Space>
                    <Button type="primary" htmlType="submit" loading={loginLoading} size="large">
                      登录
                    </Button>
                  </Space>
                </Form.Item>
              </Form>
            </Card>
          </Col>
        </Row>
      </Layout.Content>
      <Layout.Footer>Footer</Layout.Footer>
    </Layout>
  );
};

export default Login;
