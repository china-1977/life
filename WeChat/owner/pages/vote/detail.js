Page({
  mixins: [require('../../mixin/common')],
  data: {
    housesPicker: [], housesPickerIndex: -1
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/votes/${options.id}`,
        header: { authorization },
      }).then((vote) => {
        this.setData({
          vote
        });
      });

      this.wxRequest({
        url: `${this.data.domain}/owner/houses`,
        header: { authorization },
        data: { merchantId: options.merchantId }
      }).then((houses) => {
        const housesPicker = houses.map((value) => {
          return { houseId: value.id, value: [value.floorNumber, value.unit, value.roomNumber].join(",") }
        })
        this.setData({
          houses, housesPicker
        });
      });

    });
  },
  optionChange: function (e) {
    this.setData({
      option: this.data.vote.options[e.detail.value]
    })
  },
  houseChange: function (e) {
    this.setData({
      housesPickerIndex: e.detail.value
    })
  },
  optionSelect: function (e) {
    const { vote, housesPicker, housesPickerIndex } = this.data;
    if (e.detail.value.option && housesPickerIndex != -1) {
      wx.showModal({
        title: e.detail.value.option,
        content: '是否投票？',
        success: (res) => {
          if (res.confirm) {
            this.wxLogin().then(({ authorization }) => {
              this.wxRequest({
                url: `${this.data.domain}/owner/votes`,
                header: { authorization },
                data: { voteId: vote.id, merchantId: vote.merchantId, houseId: housesPicker[housesPickerIndex].houseId, ...e.detail.value },
                method: "POST",
              }).then((res) => {
                wx.showModal({
                  title: '提示',
                  content: '投票成功',
                  showCancel: false, success: (res) => {
                    wx.navigateBack({
                      delta: 2
                    });
                  }
                })
              });
            })
          }
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '请选择投票和房屋',
        showCancel: false,
      })
    }
  }
})