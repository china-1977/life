import { domain, appid, wxLogin, wxRequest } from '../../utils/util.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  setOpenid: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wx.showModal({
        title: '提示',
        content: '是否绑定？',
        success: (res) => {
          if (res.confirm) {
            wx.login({
              success: ({ code }) => {
                wxRequest({
                  url: `${domain}/account/customers/setOpenid`,
                  header: { authorization, cid: info.cid },
                  method: 'POST',
                  data: { code, appid }
                }).then((content) => {
                  wx.showModal({
                    title: '提示',
                    content: '绑定成功',
                    showCancel: false,
                  })
                });
              },
              fail: () => {
                wx.showModal({
                  title: '警告',
                  content: '获取微信code失败',
                  confirmColor: '#e64340',
                  showCancel: false,
                })
              },
            })
          }
        }
      })
    })
  },

  

})