import { wxLogin, domain, wxRequest, size } from '../../utils/util.js';

Page({
  data: {
    domain,
    notices: []
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/notices`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data:{size}
      }).then((data) => {
        this.setData({
          notices: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/notices`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data:{size}
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          notices: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/notices`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { page: this.data.number + 2, size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let notices = this.data.notices;
          notices.push(...data.content)
          this.setData({
            notices, number: data.number
          });
        }
      });
    });
  }
})