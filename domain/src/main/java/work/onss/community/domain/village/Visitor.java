package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class Visitor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String customerId;
    private String name;
    private String idCard;
    private String phone;
    private String carCode;
    private String merchantId;
    private String shortname;
    private String houseId;
    private String floorNumber;
    private String unit;
    private String roomNumber;
    private LocalDateTime visitDatetime;
    private LocalDateTime leaveDatetime;
    private LocalDateTime applicationTime;
    private String remarks;
}
