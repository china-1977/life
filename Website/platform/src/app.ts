import { errorHandler, logout, paramsSerializer, requestInterceptor1, requestInterceptor2 } from '@/utils/format';

export const request: any = {
  paramsSerializer,
  baseURL: process.env.baseURL,
  requestInterceptors: [[requestInterceptor1, requestInterceptor2]],
  errorConfig: {
    errorHandler,
  },
};

export async function getInitialState(): Promise<any> {
  let shortname = '请选择主体';
  let username = '姓名';
  const infoStr = localStorage.getItem('info');
  if (infoStr) {
    const info = JSON.parse(infoStr);
    shortname = info.shortname;
    username = info.name;
  }
  return { name: `${shortname}-${username}` };
}

export const layout = () => {
  return {
    logout,
  };
};
