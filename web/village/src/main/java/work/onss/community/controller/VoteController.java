package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.QCustomer;
import work.onss.community.domain.dto.village.DataCount;
import work.onss.community.domain.dto.village.HouseVoterDto;
import work.onss.community.domain.village.*;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.VoteService;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class VoteController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private VoteService voteService;

    @GetMapping(value = {"votes/{id}"}, name = "投票详情")
    public Vote detail(@PathVariable String id) {
        return voteRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"votes"}, name = "投票列表")
    public Page<Vote> page(
            @RequestHeader(name = "mid") String mid,
            @QuerydslPredicate(bindings = VoteRepository.class) Predicate predicate, @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QVote qVote = QVote.vote;
        return voteRepository.findAll(qVote.merchantId.eq(mid).and(predicate), pageable);
    }

    @Transactional
    @PostMapping(value = {"votes"}, name = "投票[创建、编辑]")
    public void saveOrInsert(@RequestHeader(name = "mid") String mid, @RequestBody @Validated Vote vote) {
        if (vote.getId() == null) {
            vote.setMerchantId(mid);
            voteRepository.save(vote);
        } else {
            QVote qVote = QVote.vote;
            jpaQueryFactory.update(qVote)
                    .set(qVote.title, vote.getTitle())
                    .set(qVote.description, vote.getDescription())
                    .set(qVote.pictures, vote.getPictures())
                    .set(qVote.videos, vote.getVideos())
                    .set(qVote.options, vote.getOptions())
                    .set(qVote.updateDatetime, LocalDateTime.now())
                    .where(qVote.id.eq(vote.getId()), qVote.merchantId.eq(mid))
                    .execute();
        }
    }

    @DeleteMapping(value = {"votes"}, name = "投票批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        voteRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @PostMapping(value = {"votes/syncResult"}, name = "投票结果同步")
    public void syncResult(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        for (String id : ids) {
            voteService.syncResult(id, mid);
        }
    }

    @PostMapping(value = "votes/export", name = "投票导出")
    public void export(
            HttpServletResponse httpServletResponse,
            @RequestHeader(name = "mid") String mid,
            @QuerydslPredicate(bindings = VoteRepository.class, root = Vote.class) Predicate predicate) throws Exception {
        QVote qVote = QVote.vote;
        Iterable<Vote> votes = voteRepository.findAll(qVote.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        EasyExcel.write(httpServletResponse.getOutputStream(), Vote.class)
                .sheet().doWrite((Collection<?>) votes);
    }

    @PostMapping(value = "votes/{id}/detailExport", name = "投票导出详情")
    public void detailExport(
            HttpServletResponse httpServletResponse,
            @RequestHeader(name = "mid") String mid,
            @PathVariable String id) throws Exception {
        QVoter qVoter = QVoter.voter;
        QCustomer qCustomer = QCustomer.customer;
        QHouse qHouse = QHouse.house;
        List<HouseVoterDto> data = jpaQueryFactory.select(Projections.constructor(HouseVoterDto.class, qHouse, qCustomer, qVoter)).from(qVoter)
                .leftJoin(qHouse).on(qHouse.id.eq(qVoter.houseId))
                .leftJoin(qCustomer).on(qCustomer.id.eq(qVoter.customerId))
                .where(qVoter.voteId.eq(id), qVoter.merchantId.eq(mid))
                .fetch();
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), HouseVoterDto.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<HouseVoterDto>> floorNumberData = data.stream().collect(Collectors.groupingBy(HouseVoterDto::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}