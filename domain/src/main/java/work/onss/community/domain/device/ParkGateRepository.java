package work.onss.community.domain.device;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.util.Collection;
import java.util.Optional;

public interface ParkGateRepository extends JpaRepository<ParkGate, String>, JpaSpecificationExecutor<ParkGate>, QuerydslPredicateExecutor<ParkGate>, QuerydslBinderCustomizer<QParkGate> {

    default void customize(QuerydslBindings bindings, QParkGate qParkGate) {
        bindings.bind(qParkGate.id).withDefaultBinding();
    }

    @Modifying
    @Transactional
    void deleteByMerchantIdAndIdIn(String vid, Collection<String> id);

    Optional<ParkGate> findByImei(String s);
}
