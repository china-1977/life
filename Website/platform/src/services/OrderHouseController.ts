import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/village/orderHouses', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const save = async (body?: any) =>
  request<any>('/village/orderHouses', {
    method: 'POST',
    data: body,
  });

export const del = async (body?: any) =>
  request<any>('/village/orderHouses', {
    method: 'DELETE',
    data: body,
  });

export interface OrderHouse {
  id: string;
  code: string;
  merchantId: string;
  floorNumber: string;
  unit: string;
  roomNumber: string;
  area: number;
  areaPrice: number;
  propertyTotal: number;
  rubbishPrice: number;
  parkSpaceCount: number;
  parkSpaceTotal: number;
  total: number;
  type: string;
  remarks: string;
  startDate: any;
  endDate: any;
  monthCount: number;
  status: string;
  payDatetime: any;
  updateDatetime: any;
}
