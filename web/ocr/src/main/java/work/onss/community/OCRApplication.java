package work.onss.community;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author wangchanghao
 */
@Log4j2
@EnableAsync
@EnableJpaAuditing
@EnableDiscoveryClient
@SpringBootApplication
public class OCRApplication {

    public static void main(String[] args) {
        SpringApplication.run(OCRApplication.class, args);
    }

}
