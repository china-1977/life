/**
 * 房屋列表
 */
import {houseTypeEnum} from '@/constants';
import {bindingParkSpace, customers, getParkSpaces, House, HouseCustomer} from '@/services/HouseController';
import {getParkSpaceByKeywords, ParkSpace} from '@/services/ParkSpaceController';
import services from '@/services/api';
import {
    ActionType,
    ModalForm,
    ProFormDigit,
    ProFormGroup,
    ProFormList,
    ProFormSelect,
    ProFormText,
    ProFormTextArea,
    ProTable
} from '@ant-design/pro-components';
import {
    Avatar,
    Button,
    Descriptions,
    Dropdown,
    Form,
    FormListFieldData,
    Image,
    List,
    message,
    Modal,
    Popconfirm, Space,
    Typography,
    Upload
} from 'antd';
import {useRef, useState} from 'react';

const {page, save, del, syncPeopleNumber, updateHouse, houseFastCreate} = services.HouseController;
const {exportData} = services.FileController;

const HouseList = () => {
    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {
            title: '楼号',
            align: 'center',
            dataIndex: 'floorNumber',
            copyable: true,
            onSorter: false,
            sorter: {multiple: 1},
        },
        {title: '单元', align: 'center', dataIndex: 'unit', copyable: true, sorter: {multiple: 1}, onSorter: false},
        {
            title: '楼层',
            align: 'center',
            dataIndex: 'floor',
            copyable: true,
            onSorter: false,
            sorter: {multiple: 1},
        },
        {
            title: '室',
            align: 'center',
            dataIndex: 'roomNumber',
            copyable: true,
            onSorter: false,
            sorter: {multiple: 1},
        },
        {
            title: '人数',
            align: 'center',
            dataIndex: 'peopleNumber',
            onSorter: false,
            copyable: true,
            sorter: {multiple: 1},
            search: false,
        },
        {title: '面积', dataIndex: 'area', copyable: true, search: false},
        {
            title: '类型',
            align: 'center',
            dataIndex: 'type',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: houseTypeEnum.columns,

        },
        {title: '备注', dataIndex: 'remarks', copyable: true},
        {
            title: '操作',
            align: 'center',
            dataIndex: 'option',
            valueType: 'option',
            render: (text: any, house: House) => (
                <Dropdown.Button
                    key={house.id}
                    onClick={async (res) => {
                        Promise.all([
                            customers(house.id).then((res) => {
                                house.houseCustomers = res;
                            }),
                            getParkSpaces(house.id).then((res) => {
                                house.parkSpaces = res;
                            }),
                        ]).then((res) => {
                            setHouse(house);
                            setModalVisit('DETAIL');
                        });
                    }}
                    menu={{
                        items: [
                            {
                                key: 'PARKSPACE',
                                label: (
                                    <ModalForm
                                        title="车位信息"
                                        grid={true}
                                        colProps={{span: 6}}
                                        onFinish={async (values) => {
                                            return bindingParkSpace(
                                                form.getFieldValue('id'),
                                                values.parkSpaces.map((res: any) => res.id),
                                            ).then((res) => {
                                                form.resetFields();
                                                return true;
                                            });
                                        }}
                                        form={form}
                                        onOpenChange={(visible) => {
                                            form.resetFields();
                                            if (visible) {
                                                getParkSpaces(house.id).then((parkSpaces) => {
                                                    form.setFieldsValue({...house, parkSpaces});
                                                });
                                            }
                                        }}
                                        trigger={<Button type="primary"> 车位 </Button>}
                                    >
                                        <ProFormText disabled width="sm" name="floorNumber" label="楼号"
                                                     placeholder="请输入楼号"/>
                                        <ProFormText disabled width="sm" name="unit" label="单元"
                                                     placeholder="请输入单元"/>
                                        <ProFormText disabled width="sm" name="roomNumber" label="室"
                                                     placeholder="请输入室"/>
                                        <ProFormSelect disabled debounceTime={1000} width="sm" name="type" label="类型"
                                                       valueEnum={houseTypeEnum.ProFormSelect}/>
                                        <ProFormList
                                            colProps={{span: 24}}
                                            name="parkSpaces"
                                            children={(field: FormListFieldData, index, action, count) => (
                                                <ProFormGroup key="parkSpace">
                                                    <ProFormSelect
                                                        colProps={{span: 24}}
                                                        name={['id']}
                                                        label={'车位'}
                                                        showSearch={true}
                                                        debounceTime={1000}
                                                        width="sm"
                                                        fieldProps={{filterOption: false}}
                                                        params={{id: action.getCurrentRowData()?.id}}
                                                        request={async (params: { keyWords: string; id: string }) => {
                                                            if (params.id || params.keyWords) {
                                                                return getParkSpaceByKeywords(params).then((values: [ParkSpace]) => {
                                                                    return values.map((parkSpace) => {
                                                                        return {
                                                                            value: parkSpace.id,
                                                                            label: `${parkSpace.region}-${parkSpace.code}-${parkSpace.carCode}`,
                                                                        };
                                                                    });
                                                                });
                                                            } else {
                                                                return [];
                                                            }
                                                        }}
                                                    />
                                                </ProFormGroup>
                                            )}
                                        />
                                    </ModalForm>
                                ),
                            },
                            {
                                key: 'EDIT',
                                label: (
                                    <ModalForm
                                        title="房屋编辑"
                                        grid={true}
                                        colProps={{span: 4}}
                                        onFinish={async (values) => {
                                            values.id = form.getFieldValue('id');
                                            return updateHouse(values).then((res) => {
                                                form.resetFields();
                                                ref.current?.reload();
                                                return true;
                                            });
                                        }}
                                        form={form}
                                        onOpenChange={(visible) => {
                                            form.resetFields();
                                            if (visible) {
                                                form.setFieldsValue(house);
                                            }
                                        }}
                                        trigger={<Button type="primary"> 编辑 </Button>}
                                    >
                                        <ProFormText name="floorNumber" label="楼号" placeholder="请输入楼号"/>
                                        <ProFormText name="unit" label="单元" placeholder="请输入单元"/>
                                        <ProFormText name="roomNumber" label="室" placeholder="请输入室"/>
                                        <ProFormSelect name="type" label="类型" debounceTime={1000}
                                                       valueEnum={houseTypeEnum.ProFormSelect}/>
                                        <ProFormDigit name="area" label="面积" placeholder="请输入面积"
                                                      extra={'平方米'}/>
                                        <ProFormTextArea name="remarks" label="备注" colProps={{span: 24}}
                                                         fieldProps={{autoSize: true}}/>
                                    </ModalForm>
                                ),
                            },
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${house.roomNumber}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        del([house.id]).then((res) => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(house)}}>详情</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const [previewProps, setPreviewProps] = useState<any>(false);
    const [form] = Form.useForm<House>();
    const [modalVisit, setModalVisit] = useState<string | boolean>(false);
    const ref = useRef<ActionType>();
    const [house, setHouse] = useState<House>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params, sorter, filter) => page(params, sorter, filter)}
                rowSelection={{}}
                tableAlertOptionRender={(data) => {
                    return (
                        <Space size={16}>
                            <Popconfirm
                                title="批量删除"
                                description={`是否批量删除？`}
                                okText="确认"
                                cancelText="取消"
                                onConfirm={() => {
                                    del(data?.selectedRowKeys).then(() => {
                                        ref.current?.reload();
                                    });
                                }}
                            >
                                <Button type="primary" danger>删除</Button>
                            </Popconfirm>
                        </Space>
                    );
                }}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/houses/export', values, '房屋信息.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        <Dropdown.Button
                            key={'export'}
                            menu={{
                                items: [
                                    {
                                        key: 'exportOrderHouse',
                                        label: '物业费',
                                        onClick: () => {
                                            const values = searchConfig?.form?.getFieldsValue();
                                            exportData('/village/houses/exportOrderHouse', values, '物业费模板.xlsx');
                                        },
                                    },
                                    {
                                        key: 'exportOrderWater',
                                        label: '水费',
                                        onClick: () => {
                                            const values = searchConfig?.form?.getFieldsValue();
                                            exportData('/village/houses/exportOrderWater', values, '水费模板.xlsx');
                                        },
                                    },
                                    {
                                        key: 'exportOrderHeat',
                                        label: '采暖费',
                                        onClick: () => {
                                            const values = searchConfig?.form?.getFieldsValue();
                                            exportData('/village/houses/exportOrderHeat', values, '采暖费模板.xlsx');
                                        },
                                    },
                                ],
                            }}
                        >
                            <Typography.Text>模板</Typography.Text>
                        </Dropdown.Button>,
                        ...dom.values(),
                    ],
                }}
                toolBarRender={() => [
                    <ModalForm
                        title="房屋信息"
                        form={form}
                        grid={true}
                        colProps={{span: 4}}
                        onOpenChange={(visible) => {
                            form.resetFields();
                        }}
                        onFinish={async (values) => {
                            houseFastCreate(values).then((res) => {
                                form.resetFields();
                                ref.current?.reload();
                                return true;
                            });
                        }}
                        trigger={<Button type="primary"> 快速创建 </Button>}
                    >
                        <ProFormList name="floors" colProps={{span: 24}} label={'楼宇'}>
                            {(meta, index, action, count) =>
                                <ProFormGroup key="floor" colProps={{span: 24}}>
                                    <ProFormText name={'floorNumber'} label={'楼号'}/>
                                    <ProFormText name={'count'} label={'楼层'}/>
                                </ProFormGroup>
                            }
                        </ProFormList>

                        <ProFormList colProps={{span: 24}} name="houseTypes" label={'户型'}>
                            {(meta, index, action, count) =>
                                <ProFormGroup key="houseType" colProps={{span: 24}}>
                                    <ProFormText name={'unit'} label={'单元'}/>
                                    <ProFormText name={'area'} label={'面积'}/>
                                    <ProFormSelect name="type" label="类型" debounceTime={1000}
                                                   valueEnum={houseTypeEnum.ProFormSelect}/>
                                </ProFormGroup>
                            }
                        </ProFormList>
                    </ModalForm>,
                    <ModalForm
                        title="房屋信息"
                        form={form}
                        grid={true}
                        colProps={{span: 4}}
                        onOpenChange={(visible) => {
                            form.resetFields();
                        }}
                        onFinish={async (values) => {
                            values.id = form.getFieldValue('id');
                            return save(values).then((res) => {
                                form.resetFields();
                                ref.current?.reload();
                                return true;
                            });
                        }}
                        trigger={<Button type="primary"> 新建 </Button>}
                    >
                        <ProFormText name="floorNumber" label="楼号" placeholder="请输入楼号"/>
                        <ProFormText name="unit" label="单元" placeholder="请输入单元"/>
                        <ProFormDigit name="floor" label="楼层" placeholder="请输入楼层" />
                        <ProFormText name="roomNumber" label="室" placeholder="请输入室"/>
                        <ProFormSelect name="type" label="类型" debounceTime={1000}
                                       valueEnum={houseTypeEnum.ProFormSelect}/>
                        <ProFormDigit name="area" label="面积" placeholder="请输入面积" extra={'平方米'}/>
                        <ProFormTextArea name="remarks" label="备注" colProps={{span: 24}}
                                         fieldProps={{autoSize: true}}/>
                    </ModalForm>,
                    <Upload
                      action={`${process.env.baseURL}/village/imports/startData`}
                      onChange={(e) => {
                          switch (e.file.status) {
                              case 'uploading':
                                  message.loading({content: '上传中', key: e.file.uid});
                                  break;
                              case 'error':
                                  message.error({content: '上传失败', key: e.file.uid});
                                  break;
                              case 'done':
                                  message.success({content: '上传成功', key: e.file.uid});
                                  break;
                          }
                      }}
                      showUploadList={false}
                      headers={{
                          authorization: localStorage.getItem('authorization') as string,
                          cid: localStorage.getItem('cid') as string,
                          mid: localStorage.getItem('mid') as string,
                      }}
                    >
                        <Button>初始导入</Button>
                    </Upload>,
                    <Upload
                        action={`${process.env.baseURL}/village/houses/imports`}
                        onChange={(e) => {
                            switch (e.file.status) {
                                case 'uploading':
                                    message.loading({content: '上传中', key: e.file.uid});
                                    break;
                                case 'error':
                                    message.error({content: '上传失败', key: e.file.uid});
                                    break;
                                case 'done':
                                    message.success({content: '上传成功', key: e.file.uid});
                                    break;
                            }
                        }}
                        showUploadList={false}
                        headers={{
                            authorization: localStorage.getItem('authorization') as string,
                            cid: localStorage.getItem('cid') as string,
                            mid: localStorage.getItem('mid') as string,
                        }}
                    >
                        <Button>导入</Button>
                    </Upload>,
                    <Button
                        onClick={() => {
                            syncPeopleNumber().then((res) => {
                                ref.current?.reload();
                            });
                        }}
                    >
                        同步人数
                    </Button>,
                ]}
            />

            <Modal
                width={'50%'}
                footer={false}
                open={modalVisit === 'DETAIL'}
                onCancel={() => {
                    setModalVisit(false);
                }}
            >
                <Descriptions size={'small'} title="房屋详情" bordered={true}>
                    <Descriptions.Item
                        label="房屋">{[house?.floorNumber, house?.unit, house?.roomNumber]}</Descriptions.Item>
                    <Descriptions.Item label="类型">{house?.type}</Descriptions.Item>
                    <Descriptions.Item label="面积">{house?.area}</Descriptions.Item>
                    <Descriptions.Item label="备注">{house?.remarks}</Descriptions.Item>
                </Descriptions>
                <List
                    header={'业主'}
                    bordered
                    dataSource={house?.houseCustomers}
                    renderItem={(item: HouseCustomer) => (
                        <List.Item>
                            <List.Item.Meta
                                avatar={
                                    <Avatar src={
                                        <Image src={item.customer.face}/>
                                    }/>
                                }
                                title={[item.relation, '-', item.customer.name]}
                                description={[
                                    <p>{item.customer.phone}</p>,
                                    <a onClick={() => {
                                        setPreviewProps({
                                            preview: {
                                                visible: true,
                                                onVisibleChange: (value: any) => {
                                                    setPreviewProps({
                                                        preview: {
                                                            visible: false,
                                                        }, items: []
                                                    });
                                                }
                                            }, items: [
                                                item?.customer?.idCardFace,
                                                item?.customer?.idCardNational
                                            ].filter(value => value != null)
                                        })

                                    }}>{item.customer.idCard}</a>,

                                ]}
                            />
                        </List.Item>
                    )}
                />
                <List
                    itemLayout={'vertical'}
                    header={'车位'}
                    bordered
                    dataSource={house?.parkSpaces}
                    renderItem={(item: ParkSpace) => (
                        <List.Item actions={[item.state, item.status]}>
                            <List.Item.Meta title={[item.region, item.code]} description={item.carCode}/>
                        </List.Item>
                    )}
                />
                <Image.PreviewGroup {...previewProps} />
            </Modal>
        </div>
    );
};

export default HouseList;
