package work.onss.community.domain.device;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.converters.ParkGateTypeConverter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
public class ParkGate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    private String merchantId;
    private String parkRegionId;
    @ExcelProperty(value = "设备序号")
    private String imei;
    @ExcelProperty(value = "名称")
    private String title;
    @ExcelProperty(value = "IP")
    private String ip;
    @Enumerated(EnumType.STRING)
    @ExcelProperty(value = "类型", converter = ParkGateTypeConverter.class)
    private Type type;
    @ExcelProperty(value = "更新时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateDatetime;
    @ExcelProperty(value = "备注")
    private String remarks;

    public ParkGate(String imei, String title, String ip, LocalDateTime updateDatetime) {
        this.imei = imei;
        this.title = title;
        this.ip = ip;
        this.updateDatetime = updateDatetime;
    }

    @Getter
    @AllArgsConstructor
    public enum Type implements Serializable {
        I("入口"),
        O("出口");

        private final String value;
    }


}
