import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/parking/orderParks', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const save = async (body?: any) =>
  request<any>('/parking/orderParks', {
    method: 'POST',
    data: body,
  });

export const del = async (body?: any) =>
  request<any>('/parking/orderParks', {
    method: 'DELETE',
    data: body,
  });