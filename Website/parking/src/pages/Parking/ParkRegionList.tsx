/**
 * 停车区域列表
 */
import services from '@/services/api';
import {ActionType, ModalForm, ProFormDigit, ProFormText, ProFormTextArea, ProTable} from '@ant-design/pro-components';
import {Button, DatePicker, Dropdown, Form, Popconfirm, Typography} from 'antd';
import {useRef, useState} from 'react';

const {page, save, del} = services.ParkRegionController;
const {exportData} = services.FileController;

const ParkRegionList = () => {
    const columns: any = [
        {title: '序号', align: 'center', dataIndex: 'index', valueType: 'indexBorder'},
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {title: '名称', align: 'center', dataIndex: 'name', copyable: true},
        {title: '单价(元/小时)', align: 'center', dataIndex: 'price', copyable: true, search: false,},
        {title: '减免时长(分钟)', align: 'center', dataIndex: 'freeTime', copyable: true, search: false,},
        {
            title: '创建日期',
            dataIndex: 'insertDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '更新日期',
            dataIndex: 'updateDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, record: any) => (
                <Dropdown.Button
                    key={record.id}
                    onClick={async () => {
                        form.resetFields();
                        form.setFieldsValue({...record});
                        setModalVisit('EDIT');
                    }}
                    menu={{
                        items: [
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${record.code}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        del([record.id]).then((res) => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(record)}}>编辑</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const [form] = Form.useForm();
    const [modalVisit, setModalVisit] = useState<any>(false);
    const ref = useRef<ActionType>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params, sorter, filter) => page(params, sorter, filter)}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/parkRegions/export', values, '停车区域.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
                toolBarRender={() => [
                    <ModalForm
                        title="停车区域信息"
                        form={form}
                        open={modalVisit === 'EDIT'}
                        grid={true}
                        colProps={{span: 8}}
                        onOpenChange={(visible) => {
                            if (visible && modalVisit === 'EDIT') {
                                setModalVisit('EDIT');
                            } else {
                                form.resetFields();
                                setModalVisit(false);
                            }
                        }}
                        onFinish={async (values) => {
                            values.id = form.getFieldValue('id');
                            return save(values).then((res) => {
                                form.resetFields();
                                ref.current?.reload();
                                return true;
                            });
                        }}
                        trigger={<Button type="primary"> 新建 </Button>}
                    >
                        <ProFormText width="sm" name="name" label="名称" placeholder="请输入名称"/>
                        <ProFormDigit width="sm" name="price" label="单价" placeholder="请输入单价" min={0}
                                      extra={"元/小时"}/>
                        <ProFormDigit width="sm" name="freeTime" label="减免时长" placeholder="请输入减免时长"
                                      extra="分钟" min={0}/>
                        <ProFormTextArea colProps={{span: 24,}} filedConfig={{valueType: 'code'}}
                                         fieldProps={{autoSize: true}} name="formula" label="计费方式"
                                         placeholder="请输入计费方式"/>
                    </ModalForm>,
                ]}
            />
        </div>
    );
};

export default ParkRegionList;
