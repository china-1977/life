package work.onss.community.config;

import lombok.Data;

/**
 * 坐标点
 */
@Data
public class Position {

    /**
     * x 坐标
     */
    private float x;
    /**
     * y 坐标
     */
    private float y;

    /**
     * 构造函数
     *
     * @param x 坐标x
     * @param y 坐标y
     */
    private Position(Float x, Float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * 构造一个点
     *
     * @param x 坐标x
     * @param y 坐标y
     * @return
     */
    public static Position build(float x, float y) {
        return new Position(x, y);
    }

    /**
     * 对点进行中心旋转
     *
     * @param center 中心点
     * @param angle  旋转角度
     * @return 旋转后的角
     */
    public Position rotation(Position center, float angle) {
        double k = Math.toRadians(angle);
        float nx1 = (float) ((this.x - center.x) * Math.cos(k) + (this.y - center.y) * Math.sin(k) + center.x);
        float ny1 = (float) (-(this.x - center.x) * Math.sin(k) + (this.y - center.y) * Math.cos(k) + center.y);
        return new Position(nx1, ny1);
    }

    /**
     * 计算两点之间的距离
     *
     * @param that 点
     * @return 距离
     */
    public float distance(Position that) {
        return (float) Math.sqrt(Math.pow((this.x - that.x), 2) + Math.pow((this.y - that.y), 2));
    }

}
