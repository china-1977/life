Page({
  mixins: [require('../../mixin/common')],
  data: {
    customers: [],
  },

  onLoad(options) {
    this.wxRequest({
      url: `${this.data.domain}/account/customers/getAllByOpenid`,
      header: options,
    }).then((customers) => {
      this.setData({
        options,
        customers
      })
    });
  },

  binding: function (e) {
    this.wxRequest({
      url: `${this.data.domain}/account/customers/${e.currentTarget.id}/binding`,
      header: this.data.options,
      method: "POST",
    }).then(({ authorization, info }) => {
      wx.setStorageSync('authorization', authorization);
      wx.setStorageSync('info', info);
      wx.showModal({
        title: '提示',
        content: '登陆成功',
        showCancel: false,
        success: () => {
          wx.switchTab({
            url: '/pages/main/index',
          })
        }
      })
    });
  },
})