package work.onss.community.controller;

import com.querydsl.core.types.Predicate;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.MerchantRepository;

import java.util.Map;


/**
 * 商户管理-列表、详情（主体、充值统计、消费统计、余额统计）、会员卡、充值记录、消费记录
 *
 * @author wangchanghao
 */
@Log4j2
@RestController
public class MerchantController {
    @Autowired
    private MerchantRepository merchantRepository;

    @GetMapping(value = {"merchants"}, name = "主体列表")
    public Page<Merchant> merchants(@QuerydslPredicate(bindings = MerchantRepository.class) Predicate predicate,
                                    @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return merchantRepository.findAll(predicate, pageable);
    }

    @GetMapping(value = {"merchants/{id}"}, name = "主体详情")
    public Merchant detail(@PathVariable String id) {
        return merchantRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"merchants/{id}/statistics"}, name = "主体统计")
    public Map<String, Object> statistics(@PathVariable String id) {
        Merchant merchant = merchantRepository.findById(id).orElse(new Merchant());
        return Map.of("merchant", merchant);
    }
}

