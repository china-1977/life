Page({
  mixins: [require('../../mixin/common')],
  data: {},

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/advices/${options.id}`,
        header: { authorization },
      }).then((advice) => {
        this.setData({
          advice
        });
      });
    });
  },



})