package work.onss.community.domain.dto.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import work.onss.community.domain.account.Application;
import work.onss.community.domain.account.MerchantCustomerApplication;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationDto {

    private Application application;
    private MerchantCustomerApplication merchantCustomerApplication;
}
