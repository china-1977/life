package work.onss.community.domain.dto.village;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import work.onss.community.domain.village.House;
import work.onss.community.domain.village.ParkSpace;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HouseParkSpaceDto {

    private House house;
    private ParkSpace parkSpace;
}
