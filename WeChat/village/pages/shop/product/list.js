import { domain, checkToken, wxRequest } from '../../../utils/util';
Page({
  data: {
    domain, products: [], labels: [], checkAll: false, intoIndex: 0
  },

  onLoad: function (options) {
    checkToken().then(({ authorization, info }) => {
      this.init(authorization, info);
    })
  },

  init: function (authorization, info) {
    wxRequest({
      url: `${domain}/village/products/all`,
      header: { authorization, cid: info.cid, mid: info.mid },
    }).then((data) => {
      this.setData({
        ...data
      })
    })
  },

  scrollInto: function (e) {
    this.setData({
      intoIndex: e.currentTarget.id
    })
  },

  submit: function (e) {
    checkToken().then(({ authorization, info }) => {
      wx.showModal({
        title: '提示',
        content: e.detail.target.dataset.message,
        complete: (res) => {
          if (res.confirm) {
            if (e.detail.target.id === 'delete') {
              wxRequest({
                url: `${domain}/village/products/delete`,
                header: { authorization, cid: info.cid, mid: info.mid },
                method: 'POST',
                data: e.detail.value.ids
              }).then((data) => {
                this.init(authorization, info);
                this.setData({
                  checkAll: false
                });
              })
            } else if (e.detail.target.id === 'up') {
              wxRequest({
                url: `${domain}/village/products/updateStatus?status=true`,
                header: { authorization, cid: info.cid, mid: info.mid },
                method: 'POST',
                data: e.detail.value.ids
              }).then((data) => {
                this.init(authorization, info);
                this.setData({
                  checkAll: false
                });
              })
            } else if (e.detail.target.id === 'down') {
              wxRequest({
                url: `${domain}/village/products/updateStatus?status=false`,
                header: { authorization, cid: info.cid, mid: info.mid },
                method: 'POST',
                data: e.detail.value.ids
              }).then((data) => {
                this.init(authorization, info);
                this.setData({
                  checkAll: false
                });
              })
            }
          }
        }
      })

    })
  },

  checkAllTap: function (e) {
    console.log();
    this.setData({
      checkAll: !this.data.checkAll
    })
  },

  navigatorTo: function (e) {
    console.log(e.currentTarget.dataset.url);
    wx.navigateTo({
      url: e.currentTarget.dataset.url,
    })
  }
})
