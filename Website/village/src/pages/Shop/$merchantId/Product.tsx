import ProductPanelAPI from '@/services/Store/ProductPanelAPI';
import MerchantAPI from '@/services/Store/MerchantAPI';
import {Avatar, Button, Image, message, Popconfirm, Space, Tooltip} from 'antd';
import {ActionType, ProTable} from '@ant-design/pro-components';
import {useRef, useState, useEffect} from 'react';
import {ProductStatusEnum} from "@/constants";
import {useParams} from "@@/exports";

const ProductList = () => {
    const params = useParams();
    const [merchant, setMerchant] = useState<any>(false);
    useEffect(() => {
        MerchantAPI.detail(params.id).then((res)=>{
            setMerchant(res);
        })
    }, [params.id]);
    const columns: any = [
        {title: '序号', dataIndex: 'index', valueType: 'indexBorder', align: 'center',},
        {
            title: '图集', dataIndex: 'pictures', copyable: true, search: false, align: 'center',
            render: (text: object, record: { pictures: [string] }) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={record?.pictures?.map((value, index) => (
                                <Image src={value}/>
                            ))}
                        />
                    }/>
            )
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {
            title: '名称', dataIndex: 'name', copyable: true,
            render: (text: any, record: any) => (
                <Tooltip title={record.description}>
                    <Button type="link">{text}</Button>
                </Tooltip>
            )
        },
        {title: '价格', dataIndex: 'price', copyable: true, search: false},
        {title: '单位', dataIndex: 'priceUnit', copyable: true, search: false},
        {title: '库存', dataIndex: 'stock', copyable: true, search: false},
        {title: '最小', dataIndex: 'min', search: false},
        {title: '最大', dataIndex: 'max', search: false},
        {title: '标签', dataIndex: 'label', copyable: true, align: 'center',},
        {title: '排序', dataIndex: 'orderLabel', copyable: true, align: 'center',},
        {title: '状态', dataIndex: 'status', valueEnum: ProductStatusEnum.columns, align: 'center',},
        {title: '视频', dataIndex: 'vid', copyable: true},
        {
            title: '操作',
            align: 'center',
            dataIndex: 'option',
            valueType: 'option',
            render: (text: any, value: any) => (
                <Popconfirm
                    title="加入橱窗确认"
                    description={`是否加入橱窗？`}
                    okText="确认"
                    cancelText="取消"
                    onConfirm={() => {
                        ProductPanelAPI.panel([value.id]).then((res) => {
                            message.success("加入橱窗成功")
                            ref.current?.reload();
                        });
                    }}
                >
                    <Button type="primary">加入橱窗</Button>
                </Popconfirm>)
        }
    ];

    const ref = useRef<ActionType>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle={merchant ? `${merchant.shortname},${merchant.username},${merchant.phone},${merchant.addressValue},${merchant.addressName}` : merchant}
                columns={columns}
                params={{merchantId: params.id}}
                request={(params: any, sorter, filter) => MerchantAPI.products(params.merchantId, params, sorter, filter)}
                rowSelection={{}}
                tableAlertOptionRender={(data) => {
                    return (
                        <Space size={16}>
                            <Popconfirm
                                title="批量加入橱窗"
                                description={`是否批量加入橱窗？`}
                                okText="确认"
                                cancelText="取消"
                                onConfirm={() => {
                                    ProductPanelAPI.panel(data?.selectedRowKeys).then(() => {
                                        message.success("加入橱窗成功")
                                        // @ts-ignore
                                        ref.current.clearSelected();
                                        ref.current?.reload();
                                    });
                                }}
                            >
                                <Button danger>批量加入橱窗</Button>

                            </Popconfirm>
                        </Space>
                    );
                }}
            />
        </div>
    );
};

export default ProductList;
