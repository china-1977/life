package work.onss.community.parking.rabbit;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.config.SystemConfig;
import work.onss.community.domain.park.OrderPark;
import work.onss.community.domain.park.OrderParkRepository;
import work.onss.community.domain.park.ParkCapture;
import work.onss.community.domain.park.ParkCaptureRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;


@Log4j2
@Component
public class Consumer {

    @Autowired
    private ParkCaptureRepository parkCaptureRepository;
    @Autowired
    private OrderParkRepository orderParkRepository;
    @Autowired
    private SystemConfig systemConfig;

    @RabbitListener(
            bindings = @QueueBinding(
                    value = @Queue(value = "topic.queue.parking", durable = "true", autoDelete = "false"),
                    exchange = @Exchange(value = "amq.topic", type = ExchangeTypes.TOPIC),
                    key = "device.#.parking"
            )
    )
    public void parking(String message) {
        log.info("\n道闸上报:{}", message);
        ParkCapture parkCapture = JacksonUtils.readValue(message, ParkCapture.class);
        Optional<ParkCapture> captureOptional = parkCaptureRepository.findById(parkCapture.getId());
        parkCapture.setReceiveTime(LocalDateTime.now());
        parkCaptureRepository.save(parkCapture);
        if (captureOptional.isEmpty()) {
            OrderPark orderPark = new OrderPark();
            orderPark.setCarNumber(parkCapture.getCode());
            orderPark.setCarNumberPicture(parkCapture.getCodeImage());
            orderPark.setMerchantId(parkCapture.getMerchantId());
            orderPark.setParkRegionId(parkCapture.getParkRegionId());

            if (parkCapture.getDataType() == ParkCapture.DataType.I) {

                orderPark.setInDatetime(parkCapture.getCaptureTime());
                orderPark.setInPicture(parkCapture.getCaptureImage());
                orderPark.setInDeviceNumber(parkCapture.getDeviceNumber());
                orderPark.setParkStatus(OrderPark.Status.I);

                OrderPark oldOrderPark = orderParkRepository.findFirstByMerchantIdAndParkRegionIdAndCarNumberOrderByInDatetimeAsc(
                        parkCapture.getMerchantId(), parkCapture.getParkRegionId(), parkCapture.getCode()
                ).orElse(null);

                if (oldOrderPark == null || oldOrderPark.getParkStatus().equals(OrderPark.Status.O)) {
                    orderParkRepository.save(orderPark);
                } else {
                    Duration duration = Duration.between(oldOrderPark.getInDatetime(), parkCapture.getCaptureTime());

                    if (duration.toSeconds() <= systemConfig.getExpire()) {
                        // 短时间重复入场：只更新入场时间、入场图片、入场设备编号
                        oldOrderPark.setInDatetime(parkCapture.getCaptureTime());
                        oldOrderPark.setInPicture(parkCapture.getCaptureImage());
                        oldOrderPark.setInDeviceNumber(parkCapture.getDeviceNumber());
                        orderParkRepository.save(oldOrderPark);
                    } else {
                        // 僵尸记录-自动驶离僵尸记录，再创建新记录
                        oldOrderPark.setOutDatetime(parkCapture.getCaptureTime());
                        oldOrderPark.setOutPicture(parkCapture.getCaptureImage());
                        oldOrderPark.setParkStatus(OrderPark.Status.O);
                        orderParkRepository.save(oldOrderPark);
                        orderParkRepository.save(orderPark);
                    }
                }
            }

            if (parkCapture.getDataType() == ParkCapture.DataType.O) {
                OrderPark oldOrderPark = orderParkRepository.findFirstByMerchantIdAndParkRegionIdAndCarNumberOrderByInDatetimeAsc(
                        parkCapture.getMerchantId(), parkCapture.getParkRegionId(), parkCapture.getCode()
                ).orElse(null);
                if (oldOrderPark == null || oldOrderPark.getParkStatus().equals(OrderPark.Status.O)) {
                    // 无入场记录-将出场信息作为入场信息
                    orderPark.setInDatetime(parkCapture.getCaptureTime());
                    orderPark.setInPicture(parkCapture.getCaptureImage());
                    orderPark.setInDeviceNumber(parkCapture.getDeviceNumber());

                    orderPark.setOutDatetime(parkCapture.getCaptureTime());
                    orderPark.setOutPicture(parkCapture.getCaptureImage());
                    orderPark.setOutDeviceNumber(parkCapture.getDeviceNumber());

                    orderPark.setParkStatus(OrderPark.Status.O);
                    orderParkRepository.save(orderPark);
                } else {
                    oldOrderPark.setOutDatetime(parkCapture.getCaptureTime());
                    oldOrderPark.setOutPicture(parkCapture.getCaptureImage());
                    oldOrderPark.setParkStatus(OrderPark.Status.O);
                    oldOrderPark.setOutDeviceNumber(parkCapture.getDeviceNumber());
                    orderParkRepository.save(oldOrderPark);
                }
            }
        }
    }
}
