package work.onss.community.village.controller;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.OrderHeat;
import work.onss.community.domain.village.OrderHeatRepository;
import work.onss.community.domain.village.QOrderHeat;
import work.onss.community.domain.village.relational.QHouseCustomer;

import java.util.List;

@Log4j2
@RestController
public class OrderHeatController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private OrderHeatRepository orderHeatRepository;

    @GetMapping(value = {"orderHeats/{id}"})
    public OrderHeat detail(@PathVariable String id, @RequestHeader(name = "cid") String cid) {
        OrderHeat orderHeat = orderHeatRepository.findById(id).orElse(null);
        if (orderHeat == null) {
            return null;
        }
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        String houseId = jpaQueryFactory.select(qHouseCustomer.houseId).from(qHouseCustomer)
                .where(qHouseCustomer.customerId.eq(cid), qHouseCustomer.houseId.eq(orderHeat.getHouseId()))
                .fetchOne();
        if (houseId == null) {
            return null;
        } else {
            return orderHeat;
        }
    }

    @GetMapping(value = {"orderHeats"})
    public Page<OrderHeat> page(
            @RequestHeader(name = "cid") String cid,
            @RequestParam Integer year,
            @QuerydslPredicate(bindings = OrderHeatRepository.class) Predicate predicate,
            @PageableDefault(sort = {"startDate"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        List<String> houseIds = jpaQueryFactory.select(qHouseCustomer.houseId).from(qHouseCustomer)
                .where(qHouseCustomer.customerId.eq(cid))
                .fetch();
        QOrderHeat qOrderHeat = QOrderHeat.orderHeat;
        return orderHeatRepository.findAll(qOrderHeat.houseId.in(houseIds).and(qOrderHeat.startDate.year().eq(year)).and(predicate), pageable);
    }

}