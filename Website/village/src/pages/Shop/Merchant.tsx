import MerchantAPI from '@/services/Store/MerchantAPI';
import {Avatar, Button, Cascader, Image} from 'antd';
import {ActionType, ProTable} from '@ant-design/pro-components';
import {useRef} from 'react';
import {StatusEnum} from "@/constants";
import _meta from '../../../public/_meta.json'

const MerchantList = () => {

    const columns: any = [
        {title: '序号', dataIndex: 'index', valueType: 'indexBorder', align: 'center',},
        {
            title: '图集', dataIndex: 'pictures', copyable: true, search: false,
            render: (text: object, record: { pictures: [string] }) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={record?.pictures?.map((value, index) => (
                                <Image src={value}/>
                            ))}
                        />
                    }/>
            )
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {
            title: 'LOGO', align: 'center', dataIndex: 'trademark', copyable: true, search: false,
            render: (text: object, record: any) => (
                <Avatar
                    shape="square"
                    src={
                        <Image src={record.trademark}/>
                    }/>
            )
        },
        {
            title: '简称', align: 'center', dataIndex: 'shortname', copyable: true,
            render: (text: string, record: any) =>
                <Button color="primary" variant="link" href={`#/shop/${record.id}/products`}>
                    {record.shortname}
                </Button>,
        },
        {
            title: '描述', align: 'center', dataIndex: 'description', copyable: true, order: 10,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: '此项为必填项',
                    },
                ],
            },
        },
        {title: '姓名', align: 'center', dataIndex: 'username', copyable: true},
        {title: '手机号', align: 'center', dataIndex: 'phone', copyable: true},
        {title: '地址名称', align: 'center', dataIndex: 'addressName', copyable: true},
        {title: '详细地址', align: 'center', dataIndex: 'addressDetail', copyable: true},
        {
            title: '省市区',
            align: 'center',
            dataIndex: 'addressCode',
            copyable: true,
            order: 90,
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: '此项为必填项',
                    },
                ],
            },
            render: (text: any, record: any) => (
                record.addressValue.join()
            ),
            renderFormItem: () => {
                return <Cascader
                    showSearch={{
                        filter: (inputValue: string, path: any[]) =>
                            path.some(
                                (option) => (option.fullname as string).toLowerCase().indexOf(inputValue.toLowerCase()) > -1,
                            )
                    }}
                    fieldNames={{label: 'fullname', value: 'code'}}
                    options={_meta.children} multiple/>
            },
        },
        {title: '状态', dataIndex: 'status', valueEnum: StatusEnum.columns,},
    ];

    const ref = useRef<ActionType>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params, sorter, filter) => MerchantAPI.page(params, sorter, filter)}
                rowSelection={{}}
                form={{
                    ignoreRules: false,
                }}
                manualRequest={true}
            />

        </div>
    );
};

export default MerchantList;
