import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/village/repairs', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const setStatus = async (status: string, ids: any[]) =>
  request<any>(`/village/repairs/${status}/setStatus`, {
    method: 'POST',
    data: ids,
  });

export const save = async (body?: any) =>
  request<any>('/village/repairs', {
    method: 'POST',
    data: body,
  });

export const del = async (body?: any) =>
  request<any>('/village/repairs', {
    method: 'DELETE',
    data: body,
  });

export interface Repair {
  id: string;
  customerId: string;
  name: string;
  phone: string;
  address: string;
  title: string;
  description: string;
  descriptionPictures: [string];
  reply: string;
  replyPictures: [string];
  status: string;
  label: string;
  repairerCustomerId: string;
  repairerName: string;
  merchantId: string;
  merchantShortname: string;
  insertDatetime: string;
  updateDatetime: string;
}
