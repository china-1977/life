dir=/home/application;

# 如果目录不存在，创建目录
if [ ! -d "$dir" ]; then
  mkdir -p "$dir"
fi

cd "$dir" || exit ;

if [ -d "$dir/life" ]; then
    cd "$dir"/life || exit ;
    git pull;
else
    cd "$dir" || exit ;
    git clone git@gitee.com:china-1977/life.git;
    cd "$dir"/life || exit ;
fi

if which java; then
  java -version
else
  curl -O https://download.oracle.com/java/22/archive/jdk-22.0.1_linux-x64_bin.deb;
  dpkg -i jdk-22.0.1_linux-x64_bin.deb;
  java -version
fi

if which mvn; then
    mvn -version
else
  if [ ! -d /usr/local/apache-maven-3.9.9 ]; then
    curl -O https://dlcdn.apache.org/maven/maven-3/3.9.9/binaries/apache-maven-3.9.9-bin.tar.gz;
    tar -zxvf apache-maven-3.9.9-bin.tar.gz -C /usr/local/;
  fi
  sudo touch /etc/profile.d/maven.sh
  export M2_HOME=/usr/local/apache-maven-3.9.9 >> /etc/profile.d/maven.sh
  export PATH=$PATH:$M2_HOME/bin >> /etc/profile.d/maven.sh
  sudo chmod +x /etc/profile.d/maven.sh
  source /etc/profile
  mvn -version
fi



mvn clean package -'Dmaven.test.skip=true';

find "$dir"/life -name 'service-*-0.0.1-SNAPSHOT.jar' -exec mv {} "$dir" \;

find "$dir"/life -name 'service-*.service' -exec cp {} '/etc/systemd/system' \;
sudo systemctl daemon-reload
sudo systemctl restart service-eureka.service
sudo systemctl restart service-gateway.service
sudo systemctl restart service-account.service
sudo systemctl restart service-owner.service
sudo systemctl restart service-platform.service
sudo systemctl restart service-shop.service
sudo systemctl restart service-village.service
