Page({
  mixins: [require('../../../mixin/common')],
  data: {
    options: {},
    store: {},
    sum: '0.00',
    products: [],
    checkAll: false,
    currentID: -1,
    deleteCart: [
      {
        type: "default",
        text: '删除',
        extClass: 'default',
      }
    ],
  },
  onLoad: function (options) {
    if (options.storeId) {
      this.wxLogin().then(({ authorization }) => {
        getCarts(info.cid, options.storeId, authorization).then((data) => {
          this.setData({
            ...data
          })
        })
      })
    } else {
      wx.reLaunch({
        url: '/pages/cart/cart'
      });
    }
  },

  addCount: function (e) {
    const index = e.currentTarget.id;
    this.updateCart(index, 1);
  },

  subtractCount: function (e) {
    const index = e.currentTarget.id;
    this.updateCart(index, -1);
  },

  updateCart: function (index, count) {
    this.wxLogin().then(({ authorization }) => {
      let product = this.data.products[index];
      this.wxRequest({
        url: `${this.data.domain}/owner/carts`,
        method: 'POST',
        header: { authorization },
        data: { id: product.cartId, storeId: product.storeId, productId: product.id, num: product.num + count },
      }).then((data) => {
        getCarts(info.cid, product.storeId, authorization).then((data) => {
          this.setData({
            ...data
          })
        })
      });
    })
  },

  checkedChange: function (e) {
    const index = e.currentTarget.dataset.index;
    const product = this.data.products[index];
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/carts/${product.cartId}/setChecked?checked=${product.checked}&storeId=${product.storeId}`,
        method: 'POST',
        header: { authorization },
      }).then((data) => {
        getCarts(info.cid, product.storeId, authorization).then((data) => {
          this.setData({
            ...data
          })
        })
      })
    })
  },

  switchChange: function (e) {
    this.wxLogin().then(({ authorization }) => {
      const store = this.data.store;
      const checkAll = e.detail.value;
      this.wxRequest({
        url: `${this.data.domain}/owner/carts/setCheckAll?checkAll=${checkAll}&storeId=${store.id}`,
        method: 'POST',
        header: { authorization },
      }).then((data) => {
        getCarts(info.cid, store.id, authorization).then((data) => {
          this.setData({ ...data });
        });
      })
    })
  },

  bindButtonTap: function (e) {
    this.wxLogin().then(({ authorization }) => {
      let { products } = this.data;
      const index = e.currentTarget.id;
      const product = products[index];
      this.wxRequest({
        url: `${this.data.domain}/owner/carts/${product.cartId}`,
        method: 'DELETE',
        header: { authorization },
      }).then((data) => {
        getCarts(info.cid, product.storeId, authorization).then((data) => {
          this.setData({
            ...data
          })
        })
      })
    })
  }
})
