#  社区服务小程序 SaaS版本

#### 介绍
![社区平台.png](%E7%A4%BE%E5%8C%BA%E5%B9%B3%E5%8F%B0.png)
#### 系统架构
![system.png](系统架构.png)
#### 技术清单
1. Postgresql 和 PostGIS 扩展
2. JPA
3. QueryDSL
4. Spring Boot 3.4.3 
5. Spring Cloud 2024.0.0
6. JDK 23
7. Eureka
8. Gateway
8. Config Server
9. 微信小程序原生组件
10. React
11. UmiJS
12. Ant Design
13. RabbitMQ
14. MQTT
15. Nginx
16. JWT

#### 项目启动
1. 初始化数据库 [schema.sql](schema.sql)
2. 安装 RabbitMQ
4. 将 [domain/target/generated-sources](domain%2Ftarget%2Fgenerated-sources) 目录置为 Generate Sources Root
5. 先启动 [eureka](monitor%2Feureka) 再启动其他模块
6. [nginx.conf](nginx.conf) 配置文件，线上部署时使用，仅供参考
7. [前端服务启动](Website%2Fvillage%2Fpackage.json)(先安装 npm install --g pnpm)

