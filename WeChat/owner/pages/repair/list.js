
Page({
  mixins: [require('../../mixin/common')],
  data: {
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/repairs`,
        header: { authorization },
        data: { size: this.data.size }
      }).then((data) => {
        this.setData({
          repairs: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/repairs`,
        header: { authorization },
        data: { size: this.data.size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          repairs: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/repairs`,
        header: { authorization },
        data: { page: this.data.number + 2, size: this.data.size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let repairs = this.data.repairs;
          repairs.push(...data.content)
          this.setData({
            repairs, number: data.number
          });
        }
      });
    });
  },
})