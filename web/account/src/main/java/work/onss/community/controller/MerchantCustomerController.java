package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.*;
import work.onss.community.domain.dto.account.ApplicationDto;
import work.onss.community.domain.dto.account.MerchantCustomerDto;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.exception.BusinessException;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 营业员管理
 *
 * @author wangchanghao
 */
@Log4j2
@RestController
public class MerchantCustomerController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private QuerydslService querydslService;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private MerchantCustomerRepository merchantCustomerRepository;
    @Autowired
    private MerchantCustomerApplicationRepository merchantCustomerApplicationRepository;

    /**
     * @param mid 主体ID
     * @return 营业员列表
     */
    @GetMapping(value = {"merchantCustomers"}, name = "营业员列表")
    public Page<Customer> customers(@RequestHeader(name = "mid") String mid,
                                    @QuerydslPredicate(bindings = CustomerRepository.class) Predicate predicate,
                                    @PageableDefault Pageable pageable) {
        QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
        long count = merchantCustomerRepository.count(qMerchantCustomer.merchantId.eq(mid));
        List<Customer> customers = querydslService.getCustomers(mid, predicate, pageable.getPageSize(), pageable.getOffset());
        return new PageImpl<>(customers, pageable, count);
    }

    /**
     * @param cid              营业员Id
     * @param mid              主体ID
     * @param merchantCustomer 邀请营业员
     */
    @PostMapping(value = {"merchantCustomers/inviteCustomer"}, name = "营业成员邀请")
    public void inviteCustomer(@RequestHeader(name = "cid") String cid, @RequestHeader(name = "mid") String mid, @RequestBody MerchantCustomer merchantCustomer) {
        if (cid.equals(merchantCustomer.getCustomerId())) {
            throw new BusinessException("无法邀请自己");
        }
        merchantCustomer.setMerchantId(mid);
        merchantCustomerRepository.save(merchantCustomer);
    }

    /**
     * @param id  待删除营业成员ID
     * @param mid 主体ID
     * @param cid 当前登录营业成员ID
     */
    @Transactional
    @DeleteMapping(value = {"merchantCustomers/{id}"}, name = "营业成员删除")
    public void deleteCustomer(@RequestHeader(name = "mid") String mid, @RequestHeader(name = "cid") String cid, @PathVariable String id) throws BusinessException {
        Merchant merchant = merchantRepository.findById(mid).orElseThrow(() -> new BusinessException("FAIL", "主体不存在，请联系客服", MessageFormat.format("主体ID:{0}", id)));
        if (id.equals(merchant.getCustomerId())) {
            throw new BusinessException("无法删除超级管理员!");
        }
        if (id.equals(cid)) {
            throw new BusinessException("无法删除自己!");
        }
        merchantCustomerRepository.deleteByMerchantIdAndCustomerId(mid, id);
        merchantCustomerApplicationRepository.deleteByCustomerIdAndMerchantId(id, mid);
    }

    /**
     * @param id             营业员ID
     * @param mid            主体ID
     * @param applicationIds 待授权资源ID
     */
    @PostMapping(value = {"merchantCustomers/{id}/authorize"}, name = "营业成员权限设置")
    public void updateApplications(@PathVariable String id, @RequestHeader(name = "mid") String mid, @Validated @RequestBody List<String> applicationIds) {
        if (applicationIds.isEmpty()) {
            merchantCustomerApplicationRepository.deleteByCustomerIdAndMerchantId(id, mid);
        } else {
            QApplication qApplication = QApplication.application;
            applicationIds = jpaQueryFactory.select(qApplication.id)
                    .from(qApplication)
                    .where(qApplication.contextPath.concat("/").concat(qApplication.name).in(applicationIds))
                    .fetch();
            merchantCustomerApplicationRepository.deleteByApplicationIdNotInAndCustomerIdAndMerchantId(applicationIds, id, mid);
            List<MerchantCustomerApplication> applicationCustomers = new ArrayList<>();
            for (String applicationId : applicationIds) {
                MerchantCustomerApplication applicationCustomer = merchantCustomerApplicationRepository.findByApplicationIdAndCustomerIdAndMerchantId(applicationId, id, mid).orElse(new MerchantCustomerApplication(id, applicationId, mid));
                if (null == applicationCustomer.getId()) {
                    applicationCustomers.add(applicationCustomer);
                }
            }
            if (!applicationCustomers.isEmpty()) {
                merchantCustomerApplicationRepository.saveAll(applicationCustomers);
            }
        }
    }

    /**
     * @param id  营业员ID
     * @param mid 主体ID
     * @return 资源与营业员关系列表
     */
    @PostMapping(value = {"merchantCustomers/{id}/applications"}, name = "营业成员资源列表")
    public List<ApplicationDto> applications(@PathVariable String id, @RequestHeader(name = "mid") String mid, @RequestBody List<String> data) {
        QApplication qApplication = QApplication.application;
        QMerchantCustomerApplication qVillageCustomerApplication = QMerchantCustomerApplication.merchantCustomerApplication;
        return jpaQueryFactory.select(
                        Projections.fields(ApplicationDto.class, qApplication, qVillageCustomerApplication))
                .from(qApplication)
                .leftJoin(qVillageCustomerApplication).on(
                        qVillageCustomerApplication.applicationId.eq(qApplication.id),
                        qVillageCustomerApplication.merchantId.eq(mid),
                        qVillageCustomerApplication.customerId.eq(id)
                ).where(qApplication.contextPath.in(data)).fetch();
    }

    /**
     * @param mid  主体ID
     * @param cid  营业员ID
     * @param data 服务路径
     * @return 资源与营业员关系列表
     */
    @PostMapping(value = {"merchantCustomers/myApplications"})
    public List<String> myApplications(@RequestHeader(name = "mid", required = false) String mid, @RequestHeader(name = "cid") String cid, @RequestBody List<String> data) {
        if (StringUtils.hasLength(mid)) {
            Merchant merchant = merchantRepository.findById(mid).orElseThrow(() -> new BusinessException("主体不存在，请联系平台客服"));
            QApplication qApplication = QApplication.application;
            if (merchant.getCustomerId().equals(cid)) {
                return jpaQueryFactory.select(qApplication.contextPath.concat("/").concat(qApplication.name))
                        .from(qApplication)
                        .where(qApplication.contextPath.in(data))
                        .fetch();
            }
            QMerchantCustomerApplication qVillageCustomerApplication = QMerchantCustomerApplication.merchantCustomerApplication;
            return jpaQueryFactory.select(qApplication.contextPath.concat("/").concat(qApplication.name)).from(qVillageCustomerApplication)
                    .innerJoin(qApplication).on(qVillageCustomerApplication.applicationId.eq(qApplication.id)).where(
                            qVillageCustomerApplication.merchantId.eq(mid),
                            qVillageCustomerApplication.customerId.eq(cid),
                            qApplication.contextPath.in(data)
                    ).fetch();
        } else {
            return null;
        }
    }

    /**
     * @param idCard 身份证
     * @param mid    主体ID
     * @return 营业员详情
     */
    @GetMapping(value = {"merchantCustomers/getCustomer"}, name = "营业成员身份证查询")
    public MerchantCustomerDto getCustomer(@RequestParam String idCard, @RequestHeader(name = "mid") String mid) {
        QCustomer qCustomer = QCustomer.customer;
        QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
        return jpaQueryFactory.select(
                        Projections.fields(MerchantCustomerDto.class, qCustomer, qMerchantCustomer)
                ).from(qCustomer)
                .leftJoin(qMerchantCustomer).on(qMerchantCustomer.customerId.eq(qCustomer.id), qMerchantCustomer.merchantId.eq(mid))
                .where(qCustomer.idCard.eq(idCard))
                .fetchOne();
    }

    /**
     * @param mid 主体ID
     */
    @PostMapping(value = {"merchantCustomers/export"}, name = "营业员导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = CustomerRepository.class, root = Customer.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws IOException {
        List<Customer> customers = querydslService.getCustomers(mid, predicate);
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), Customer.class).build();
        excelWriter.write(customers, EasyExcel.writerSheet("全部").build());
        excelWriter.finish();
    }

}

