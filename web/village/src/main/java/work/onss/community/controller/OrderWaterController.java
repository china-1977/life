package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import work.onss.community.domain.account.MerchantRepository;
import work.onss.community.domain.village.OrderWater;
import work.onss.community.domain.village.OrderWaterRepository;
import work.onss.community.domain.village.PayOrder;
import work.onss.community.domain.village.QOrderWater;
import work.onss.community.service.exception.BusinessException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class OrderWaterController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private OrderWaterRepository orderWaterRepository;
    @Autowired
    private MerchantRepository merchantRepository;
    @GetMapping(value = {"orderWaters/{id}"}, name = "水费详情")
    public OrderWater detail(@PathVariable String id) {
        return orderWaterRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"orderWaters"}, name = "水费列表")
    public Page<OrderWater> page(
            @RequestHeader(name = "mid") String mid,
            @RequestParam Integer year,
            @QuerydslPredicate(bindings = OrderWaterRepository.class) Predicate predicate,
            @PageableDefault(sort = {"startDate"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QOrderWater qOrderWater = QOrderWater.orderWater;
        return orderWaterRepository.findAll(qOrderWater.merchantId.eq(mid).and(qOrderWater.startDate.year().eq(year)).and(predicate), pageable);
    }

    @DeleteMapping(value = {"orderWaters"}, name = "水费批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        List<OrderWater> orderWaters = orderWaterRepository.findAllById(ids);
        for (OrderWater orderWater : orderWaters) {
            if (!orderWater.getStatus().equals(PayOrder.Status.WAIT_CONFIRM)) {
                StringJoiner message = new StringJoiner(",");
                message.add(orderWater.getFloorNumber());
                message.add(orderWater.getUnit());
                message.add(orderWater.getRoomNumber());
                message.add(orderWater.getStatus().getMessage());
                message.add("无法删除");
                throw new BusinessException(message.toString());
            }
        }
        orderWaterRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @Transactional
    @PostMapping(value = {"orderWaters/import"}, name = "水费导入")
    public void importWater(@RequestHeader(name = "mid") String mid, @RequestParam(value = "file") MultipartFile file) throws Exception {
        ExcelReader excelReader = EasyExcel.read(file.getInputStream()).build();
        List<ReadSheet> readSheets = excelReader.excelExecutor().sheetList();
        for (ReadSheet readSheet : readSheets) {
            List<OrderWater> data = EasyExcel.read(file.getInputStream()).head(OrderWater.class).sheet(readSheet.getSheetNo()).doReadSync();
            for (OrderWater next : data) {
                next.setMerchantId(mid);
                if (next.getId() != null) {
                    orderWaterRepository.findById(next.getId()).ifPresentOrElse(item -> {
                        if (!item.getMerchantId().equals(mid)) {
                            throw new BusinessException("水费主键错误:".concat(item.getId()));
                        }
                    }, () -> next.setId(null));
                }
                orderWaterRepository.save(next);
            }
        }
    }

    @Transactional
    @PostMapping(value = {"orderWaters/confirm"}, name = "水费确认")
    public void confirm(@RequestHeader(name = "mid") String mid, @RequestBody OrderWater orderWater) {
        QOrderWater qOrderWater = QOrderWater.orderWater;
        jpaQueryFactory.update(qOrderWater)
                .set(qOrderWater.status, PayOrder.Status.WAIT_PAY)
                .where(
                        qOrderWater.merchantId.eq(mid),
                        qOrderWater.startDate.eq(orderWater.getStartDate().withDayOfMonth(1)),
                        qOrderWater.endDate.eq(orderWater.getEndDate().withDayOfMonth(1)),
                        qOrderWater.status.eq(PayOrder.Status.WAIT_CONFIRM))
                .execute();
    }

    @PostMapping(value = {"orderWaters/export"}, name = "水费导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = OrderWaterRepository.class, root = OrderWater.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QOrderWater qOrderWater = QOrderWater.orderWater;
        Collection<OrderWater> data = (Collection<OrderWater>) orderWaterRepository.findAll(qOrderWater.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderWater.class).autoCloseStream(true).build();
        Map<String, List<OrderWater>> floorNumberData = data.stream().collect(Collectors.groupingBy(OrderWater::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}