package work.onss.community.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import work.onss.community.domain.village.House;

import java.io.Serializable;

@Log4j2
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ExcelIgnoreUnannotated
public class HouseVo implements Serializable {
    @ExcelProperty(value = "主键")
    private String id;
    @ExcelProperty(value = "编号")
    private String name;

    public HouseVo(House house) {
        this.id = house.getId();
        this.name = house.getFloorNumber() + house.getUnit() + house.getRoomNumber();
    }
}
