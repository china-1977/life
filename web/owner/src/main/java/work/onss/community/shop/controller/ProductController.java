package work.onss.community.shop.controller;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.QMerchant;
import work.onss.community.domain.dto.shop.ProductDetailDto;
import work.onss.community.domain.shop.*;
import work.onss.community.domain.village.QProductPanel;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class ProductController {

    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private ProductRepository productRepository;

    /**
     * @param id 主键
     * @return 商品信息
     */
    @GetMapping(value = {"products/{id}"})
    public Product product(@PathVariable String id) {
        return productRepository.findById(id).orElse(null);
    }

    /**
     * @param merchantId 商户ID
     * @return 商品列表
     */
    @GetMapping(value = {"products"})
    public Map<String, Object> products(@RequestParam String merchantId, @RequestHeader String cid) {
        QMerchant qStore = QMerchant.merchant;
        Merchant store = jpaQueryFactory.select(Projections.fields(
                        Merchant.class,
                        qStore.id,
                        qStore.shortname,
                        qStore.username,
                        qStore.phone,
                        qStore.addressName,
                        qStore.addressDetail,
                        qStore.location,
                        qStore.postcode,
                        qStore.addressCode,
                        qStore.addressValue
                ))
                .from(qStore).where(qStore.id.eq(merchantId)).fetchFirst();

        QProductPanel qProductPanel = QProductPanel.productPanel;
        QProduct qProduct = QProduct.product;
        QCart qCart = QCart.cart;

        List<ProductDetailDto> productDetailDtos = jpaQueryFactory
                .select(Projections.constructor(
                        ProductDetailDto.class,
                        qProduct,
                        qProductPanel,
                        qCart,
                        (qProduct.price.multiply(qCart.num)).as(qCart.total)
                ))
                .from(qProduct)
                .innerJoin(qProductPanel).on(qProductPanel.productId.eq(qProduct.id))
                .leftJoin(qCart).on(qCart.productId.eq(qProduct.id), qCart.customerId.eq(cid))
                .where(qProductPanel.merchantId.eq(merchantId), qProduct.status.eq(true))
                .orderBy(qProduct.orderLabel.asc())
                .fetch();
        BigDecimal sum = BigDecimal.ZERO;
        boolean checkAll = true;
        for (ProductDetailDto productDetailDto : productDetailDtos) {
            if (null != productDetailDto.getCartId() && productDetailDto.getChecked()) {
                sum = sum.add(productDetailDto.getTotal());
            } else {
                checkAll = false;
            }
        }
        Map<String, List<ProductDetailDto>> map = productDetailDtos.stream().collect(Collectors.groupingBy(ProductDetailDto::getLabel));
        Map<String, String> labels = new HashMap<>(map.size());
        map.forEach((k, v) -> labels.put(k, v.getFirst().getLabel()));
        return Map.of("store", store, "labels", labels, "products", map, "checkAll", checkAll, "sum", sum.toPlainString());
    }
}
