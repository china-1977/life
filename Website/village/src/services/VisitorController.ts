import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/village/visitors', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const setStatus = async (status: string, ids: any[]) =>
  request<any>(`/village/visitors/${status}/setStatus`, {
    method: 'POST',
    data: ids,
  });

export const save = async (body?: any) =>
  request<any>('/village/visitors', {
    method: 'POST',
    data: body,
  });

export const del = async (body?: any) =>
  request<any>('/village/visitors', {
    method: 'DELETE',
    data: body,
  });

export interface HouseIdentity {
  id: string;
  name: string;
  merchantId: string;
  floorNumber: string;
  unit: string;
  roomNumber: string;
  area: number;
  areaPrice: number;
  rubbishPrice: number;
  total: number;
  type: string;
  relation: string;
  remarks: string;
}
