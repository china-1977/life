import { domain, wxRequest } from '../utils/util.js'

Page({
  data: {},
  login: function (e) {
    wxRequest({
      url: `${domain}/account/login`,
      method: 'POST',
      data: e.detail.value
    }).then((content) => {
      wx.setStorageSync('authorization', content.authorization);
      wx.setStorageSync('info', content.info);
      if (content.info.mid) {
        wx.reLaunch({
          url: '/pages/main/index',
        });
      } else {
        wx.reLaunch({
          url: '/pages/merchant/list',
        });
      }
    });
  },
})