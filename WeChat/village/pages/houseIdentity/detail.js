import { domain, wxRequest, wxLogin, HouseIdentityStatus } from '../../utils/util.js';
Page({

  data: {
    HouseIdentityStatus
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/houseIdentitys/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((houseIdentity) => {
        this.setData({
          houseIdentity
        });
      });
    });
  },

  updateStatus: function (e) {
    wxLogin().then(({ authorization, info }) => {
      const status = e.currentTarget.id;
      let content = '是否同意入住？'
      if (status == 'NO') {
        content: '是否拒绝入住？'
      }
      wx.showModal({
        title: '提示',
        content,
        success: (res) => {
          if (res.confirm) {
            wxRequest({
              url: `${domain}/village/houseIdentitys/${status}/setStatus`,
              method: 'POST',
              header: { authorization, cid: info.cid, mid: info.mid },
              data: [this.data.houseIdentity.id]
            }).then(() => {
              wx.showModal({
                title: '提示',
                content: '操作成功',
                showCancel: false,
                success: (res) => {
                  wx.navigateBack({
                    delta: 1
                  });
                }
              })
            });
          }
        }
      })
    });
  },
})