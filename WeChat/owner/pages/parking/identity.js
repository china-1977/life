Page({
  mixins: [require('../../mixin/common')],
  data: {
    villagesIndex: -1,
    villages: [],
    point: {},
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      if (options.id) {
        this.wxRequest({
          url: `${this.data.domain}/owner/parkSpaceIdentitys/${options.id}`,
          header: { authorization },
        }).then((parkSpaceIdentity) => {
          this.setData({
            parkSpaceIdentity
          });
        });
      }
    });
  },

  create: function (e) {
    const identity = e.detail.value;
    if (identity.villagesIndex < 0) {
      wx.showModal({
        title: '提示',
        content: '请选择社区',
        confirmColor: '#e64340',
        showCancel: false,
      })
    } else {
      const merchant = this.data.villages[identity.villagesIndex];
      this.wxLogin().then(({ authorization }) => {
        wx.showModal({
          title: '提示',
          content: '是否申请？',
          success: (res) => {
            if (res.confirm) {
              this.wxRequest({
                url: `${this.data.domain}/owner/parkSpaceIdentitys`,
                data: { ...e.detail.value, timeInterval: [identity.startTime, identity.endTime], merchantId: merchant.id, merchantShortname: merchant.shortname },
                method: "POST",
                header: { authorization },
              }).then(() => {
                wx.showModal({
                  title: '提示',
                  content: '申请成功，请耐心等待',
                  showCancel: false,
                  success: (res) => {
                    wx.navigateBack({
                      delta: 2
                    });
                  }
                })
              })
            }
          }
        })
      })
    }
  },

  chooseLocation: function () {
    wx.authorize({
      scope: 'scope.userLocation',
      success: (res) => {
        wx.chooseLocation({
          latitude: this.data.point.latitude,
          longitude: this.data.point.longitude,
          success: (point) => {
            this.setData({ point });
            const url = `${this.data.domain}/account/merchants/${point.longitude}-${point.latitude}/near?categories=${categories}&keyword=${this.data.keyword}`;
            this.wxRequest({ url }).then((villages) => {
              this.setData({
                villages
              })
            })
          },
          fail: (res) => {
            wx.getLocation({
              type: 'gcj02',
              success: (res) => {
                const point = {
                  address: '默认地址',
                  latitude: res.latitude,
                  longitude: res.longitude,
                  name: '默认地址',
                };
                this.setData({ point });
                const url = `${this.data.domain}/account/merchants/${point.longitude}-${point.latitude}/near?categories=${categories}&keyword=${this.data.keyword}`;
                this.wxRequest({ url }).then((villages) => {
                  this.setData({
                    villages
                  })
                })
              }
            })
          }
        })
      },
      fail: (res) => {
        wx.showModal({
          title: '提示',
          content: '请允许小程序使用位置消息',
          confirmColor: '#e64340',
          showCancel: false,
          success: () => {
            wx.openSetting();
          }
        })
      }
    })
  },

  villageChange: function (e) {
    this.setData({
      villagesIndex: e.detail.value
    });
  },

  startDatetimeChange: function (e) {
    this.setData({
      startTime: e.detail.value
    });
  },

  endDatetimeChange: function (e) {
    this.setData({
      endTime: e.detail.value
    });
  }
})