import {sort} from '@/utils/format';
import {request} from '@@/exports';

export default {
    page: (params: any, sorter: any, filter: any) =>
        request<any>('/store/productPanels', {
            params: {
                ...params,
                sort: sort(sorter),
                ...filter,
            },
        }).then((res: any) => {
            return {
                data: res.content,
                total: res.totalElements,
            };
        }),
    all: () =>
        request<any>('/store/productPanels/all'),
    save: async (body: any) =>
        request<any>('/store/productPanels/save', {
            method: 'POST',
            data: body,
        }),
    del: async (body: any[]) =>
        request<any>('/store/productPanels/delete', {
            method: 'POST',
            data: body,
        }),
}