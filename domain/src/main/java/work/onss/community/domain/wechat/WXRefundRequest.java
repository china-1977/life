package work.onss.community.domain.wechat;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Builder
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class WXRefundRequest implements Serializable {

    private String subMchid;
    private String transactionId;
    private String outTradeNo;
    private String outRefundNo;
    private String reason;
    private String notifyUrl;
    private String fundsAccount;
    private Amount amount;
    private List<GoodsDetail> goodsDetail;

    @Data
    @Builder
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class Amount {
        private Long refund;
        private List<From> from;
        private Long total;
        private String currency;
    }

    @Data
    @NoArgsConstructor
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class From implements Serializable {
        private String account;
        private Long amount;
    }

    @Data
    @NoArgsConstructor
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class GoodsDetail implements Serializable {
        private String merchantGoodsId;
        private String wechatpayGoodsId;
        private String goodsName;
        private Long unitPrice;
        private Long refundAmount;
        private Long refundQuantity;
    }
}
