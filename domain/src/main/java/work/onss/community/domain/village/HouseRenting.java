package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.config.PointType;
import work.onss.community.domain.usertype.StringArrayType;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class HouseRenting implements Serializable {

    @Id
    @ExcelProperty(value = "房屋ID")
    private String id;
    private String merchantId;
    @Type(value = PointType.class)
    private Point2D.Double location;
    @ExcelProperty(value = "账户ID")
    private String customerId;
    @ExcelProperty(value = "楼号")
    private String floorNumber;
    @ExcelProperty(value = "单元")
    private String unit;
    @ExcelProperty(value = "室")
    private String roomNumber;
    @ExcelProperty(value = "方式")
    private String mode;
    @ExcelProperty(value = "户型")
    private String type;
    @ExcelProperty(value = "价格")
    private BigDecimal price;
    @ExcelProperty(value = "姓名")
    private String name;
    @ExcelProperty(value = "电话")
    private String phone;
    @ExcelProperty(value = "标题")
    private String title;
    @ExcelProperty(value = "描述")
    private String description;
    @Type(value = StringArrayType.class)
    private String[] descriptionPictures;
    @ExcelProperty(value = "创建时间")
    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;

    @Type(value = PointType.class)
    private Point2D.Double desiredPosition;
    @ExcelProperty(value = "期望地名")
    private String desiredName;
    @ExcelProperty(value = "期望地址")
    private String desiredAddress;

    public HouseRenting(HouseRenting houseRenting) {
        this.id = houseRenting.id;
        this.merchantId = houseRenting.merchantId;
        this.location = houseRenting.location;
        this.customerId = houseRenting.customerId;
        this.floorNumber = houseRenting.floorNumber;
        this.unit = houseRenting.unit;
        this.roomNumber = houseRenting.roomNumber;
        this.mode = houseRenting.mode;
        this.type = houseRenting.type;
        this.price = houseRenting.price;
        this.name = houseRenting.name;
        this.phone = houseRenting.phone;
        this.title = houseRenting.title;
        this.description = houseRenting.description;
        this.descriptionPictures = houseRenting.descriptionPictures;
        this.insertDatetime = houseRenting.insertDatetime;
        this.desiredPosition = houseRenting.desiredPosition;
        this.desiredName = houseRenting.desiredName;
        this.desiredAddress = houseRenting.desiredAddress;
    }

    @Getter
    @AllArgsConstructor
    public static enum Mode implements Serializable {
        ZH("置换"), CS("出售"), ZZ("整租"), HZ("合租"), GY("公寓");
        private final String message;
    }
}
