import { errorHandler, paramsSerializer, requestInterceptor1, requestInterceptor2 } from '@/utils/format';
import { history } from '@umijs/max';
import { InitDataType } from '@@/plugin-layout/types';
import { MenuDataItem } from '@umijs/route-utils';
import { useRequest } from '@@/exports';
import { myApplications } from '@/services/MemberController';

export const request: any = {
  paramsSerializer,
  baseURL: process.env.baseURL,
  requestInterceptors: [[requestInterceptor1, requestInterceptor2]],
  errorConfig: {
    errorHandler,
  },
};

export async function getInitialState(): Promise<any> {
  let shortname = localStorage.getItem('shortname');
  let username = localStorage.getItem('name');
  return { name: `${shortname}-${username}`, shortname };
}

export const layout = (initialState: InitDataType) => {
  const { data } = useRequest(myApplications);
  return {
    title: initialState.initialState.shortname,
    menuDataRender: (menuDataItems: MenuDataItem[]) => {
      if (data) {
        return filterMenuDataRender(data, menuDataItems);
      } else {
        return menuDataItems;
      }
    },
    logout: () => {
      localStorage.clear();
      history.push('/login');
    },
  };
};

const filterMenuDataRender = (myResources: string[], menuDataItems: MenuDataItem[]) => {
  for (let menuDataItem of menuDataItems) {
    if (!menuDataItem.hideInMenu) {
      if (menuDataItem.resources) {
        menuDataItem.hideInMenu = true;
        for (let resource of menuDataItem.resources) {
          const hideInMenu = myResources.includes(resource.path);
          if (hideInMenu) {
            menuDataItem.hideInMenu = false;
            break;
          }
        }
      } else {
        if (menuDataItem.children) {
          menuDataItem.hideInMenu = false;
          let children = filterMenuDataRender(myResources, menuDataItem.children);
          children = children.filter(item => item.hideInMenu == false);
          children.length === 0 ? menuDataItem.hideInMenu = true : menuDataItem.hideInMenu = false;
        } else {
          menuDataItem.hideInMenu = false;
        }
      }
    }
  }
  return menuDataItems;
};