Page({
  mixins: [require('../../../mixin/common')],
  data: {
    sum: 0.00
  },

  onLoad: function (options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/carts`,
        header: { authorization },
        data: options
      }).then((data) => {
        this.setData({
          ...data, options
        })
      })
    })
  },

  chooseLocation() {
    const point = this.data.point;
    if (point) {
      wx.chooseLocation({
        latitude: point.latitude,
        longitude: point.longitude,
        success: (res) => {
          this.setData({
            point: {
              latitude: res.latitude,
              longitude: res.longitude,
              name: res.name,
              address: res.address,
            }
          })
        }
      })
    } else {
      wx.getLocation({
        success: (res) => {
          wx.chooseLocation({
            latitude: res.latitude,
            longitude: res.longitude,
            success: (res) => {
              this.setData({
                point: {
                  latitude: res.latitude,
                  longitude: res.longitude,
                  name: res.name,
                  address: res.address,
                }
              })
            }
          })
        }
      })
    }
  },

  changeWay: function (e) {
    if (e.detail.value === 'DDZX' || e.detail.value === 'MDBG') {
      this.setData({
        way: e.detail.value,
      })
    } else {
      this.setData({
        way: e.detail.value,
      })
    }
  },

  createOrderProduct: function (e) {
    const { userName, userPhone, userAddressDetail, way, cart } = e.detail.value
    if (way) {
      this.wxLogin().then(({ authorization }) => {
        const { options, point } = this.data;
        wx.login({
          success: ({ code }) => {
            this.wxRequest({
              url: `${this.data.domain}/owner/orderProducts`,
              header: { authorization },
              method: "POST",
              data: {
                userName, userPhone, userAddressDetail, way, type: 'LS', cart,
                subAppId: this.data.appid, code,
                villageId: options.merchantId,
                userAddressName: point?.name,
                userAddress: point?.address,
                userAddressPoint: { x: point?.latitude, y: point?.longitude }
              },
            }).then((data) => {
              wx.reLaunch({
                url: `/pages/shop/orderProduct/list`,
              })
              // wx.requestPayment(
              //   {
              //     ...data.order, package: data.order.packageValue,
              //     'success': (res) => {
              //       setTimeout(() => {
              //         wx.reLaunch({
              //           url: `/pages/shop/orderProduct/detail?id=${data.orderProduct.id}`,
              //         })
              //       }, 300);
              //     },
              //     'fail': (res) => {
              //       wx.reLaunch({
              //         url: `/pages/shop/orderProduct/detail?id=${data.orderProduct.id}`,
              //       })
              //     }
              //   })
            })
          },
          fail: () => {
            wx.showModal({
              title: '警告',
              content: '获取微信code失败',
              confirmColor: '#e64340',
              showCancel: false,
            })
          },
        })
      })
    } else {
      wx.showModal({
        title: '警告',
        content: "请选择购买方式",
        confirmColor: '#e64340',
        showCancel: false
      })
    }
  },
})
