package work.onss.community.service.wechat;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import work.onss.community.domain.wechat.WXMerchantScore;
import work.onss.community.domain.wechat.WXRefundResult;


@HttpExchange(value = "https://api.mch.weixin.qq.com")
public interface WechatPayService {

    /**
     * JSAPI支付
     *
     * @param wxMerchantScore
     * @param authorization
     * @return
     */
    @PostExchange(value = "/v3/pay/partner/transactions/jsapi", accept = {"application/json"}, contentType = "application/json")
    WXMerchantScore.Result jsapi(@RequestBody String wxMerchantScore, @RequestHeader("authorization") String authorization);

    @PostExchange(value = "/v3/refund/domestic/refunds", accept = {"application/json"}, contentType = "application/json")
    WXRefundResult refunds(@RequestBody String wxRefundRequest, @RequestHeader("authorization") String authorization);
}
