import ProductAPI from '@/services/Store/ProductAPI';
import {Avatar, Button, Dropdown, Form, Image, message, Popconfirm, Space, Tooltip, Typography} from 'antd';
import {
    ActionType,
    ModalForm,
    ProFormSwitch,
    ProFormText,
    ProFormTextArea,
    ProFormUploadButton,
    ProTable
} from '@ant-design/pro-components';
import {useRef, useState} from 'react';
import {StatusEnum} from "@/constants";

const ProductList = () => {
    const columns: any = [
        {title: '序号', dataIndex: 'index', valueType: 'indexBorder', align: 'center',},
        {
            title: '图集', dataIndex: 'pictures', copyable: true, search: false, align: 'center',
            render: (text: object, record: { pictures: [string] }) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={record?.pictures?.map((value, index) => (
                                <Image src={value}/>
                            ))}
                        />
                    }/>
            )
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {
            title: '名称', dataIndex: 'name', copyable: true,
            render: (text: any, record: any) => (
                <Tooltip title={record.description}>
                    <Button type="link">{text}</Button>
                </Tooltip>
            )
        },
        {title: '价格', dataIndex: 'price', copyable: true, search: false},
        {title: '单位', dataIndex: 'priceUnit', copyable: true, search: false},
        {title: '库存', dataIndex: 'stock', copyable: true, search: false},
        {title: '最小', dataIndex: 'min', search: false},
        {title: '最大', dataIndex: 'max', search: false},
        {title: '标签', dataIndex: 'label', copyable: true, align: 'center',},
        {title: '排序', dataIndex: 'orderLabel', copyable: true, align: 'center',},
        {title: '状态', dataIndex: 'status', valueEnum: StatusEnum.columns, align: 'center',},
        {title: '视频', dataIndex: 'vid', copyable: true},
        {
            title: '操作',
            align: 'center',
            dataIndex: 'option',
            valueType: 'option',
            render: (text: any, value: any) => (
                <Dropdown.Button
                    key={value.id}
                    children={
                        productForm(<Typography.Text
                            onClick={() => {
                                const pictures = value?.pictures?.map((value: any) => {
                                    return {thumbUrl: value};
                                });
                                form.setFieldsValue({...value, pictures})
                            }}
                            copyable={{text: JSON.stringify(value)}}>编辑</Typography.Text>, form, '编辑商品')
                    }
                    menu={{
                        items: [
                            {
                                key: 'PANEL',
                                label: <Popconfirm
                                    title="加入橱窗确认"
                                    description={`是否加入橱窗？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        ProductAPI.panel([value.id]).then((res) => {
                                            ref.current?.reload();
                                            message.success("加入橱窗成功")
                                        });
                                    }}
                                >
                                    <Button type="primary">橱窗</Button>
                                </Popconfirm>,
                            },
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        ProductAPI.del([value.id]).then((res) => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                </Dropdown.Button>)
        }
    ];

    const ref = useRef<ActionType>();
    const [form] = Form.useForm();
    const [preview, setPreview] = useState<any>(false);

    const productForm: any = (trigger: any, form: any, title: string) =>
        <ModalForm
            width={1200}
            title={title}
            form={form}
            grid={true}
            colProps={{span: 8}}
            onFinish={async (values) => {
                await form.validateFields();
                values.pictures = values.pictures?.map((res: any) => res.thumbUrl);
                await ProductAPI.save(values)
                form.resetFields()
                ref.current?.reload();
                return true
            }}
            trigger={trigger}
        >

            <ProFormText name="id" label="主键" hidden={true} colProps={{span: 0}}/>
            <ProFormText width="sm" name="name" label="名称" placeholder="请输入名称"
                         rules={[{required: true, message: '名称不能为空'}]}/>
            <ProFormText width="sm" name="price" label="单价" placeholder="请输入单价"
                         rules={[{required: true, message: '单价不能为空'}]}/>
            <ProFormText width="sm" name="priceUnit" label="单位" placeholder="请输入单位"
                         rules={[{required: true, message: '单位不能为空'}]}/>
            <ProFormTextArea name="description" label="描述" colProps={{span: 24}} placeholder="请输入描述"
                             rules={[{required: true, message: '描述不能为空'}]}/>
            <ProFormText width="sm" name="stock" label="库存" placeholder="请输入库存"
                         rules={[{required: true, message: '库存不能为空'}]}/>
            <ProFormText width="sm" name="min" label="最小" placeholder="请输入最小"
                         rules={[{required: true, message: '最小购买量不能为空'}]}/>
            <ProFormText width="sm" name="max" label="最大" placeholder="请输入最大"
                         rules={[{required: true, message: '最大购买量不能为空'}]}/>
            <ProFormText width="sm" name="label" label="标签名称" placeholder="请输入标签名称"
                         rules={[{required: true, message: '标签名称不能为空'}]}/>
            <ProFormText width="sm" name="orderLabel" label="标签排序" placeholder="请输入标签排序"
                         rules={[{required: true, message: '标签排序不能为空'}]}/>
            <ProFormSwitch label="状态" name="status"/>
            <ProFormText width="sm" name="vid" label="视频" placeholder="请输入视频ID"/>

            <ProFormUploadButton
                colProps={{span: 24}}
                name="pictures"
                label="图片"
                rules={[{required: true, message: '名称不能为空'}]}
                fieldProps={{
                    onPreview: (file) => {
                        setPreview(
                            {
                                visible: true,
                                src: file.thumbUrl,
                                onVisibleChange: (value: any) => {
                                    setPreview(value);
                                }
                            }
                        )
                    },
                    name: 'file',
                    listType: 'picture-card',
                }}
            />
        </ModalForm>


    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params, sorter, filter) => ProductAPI.page(params, sorter, filter)}
                rowSelection={{}}
                toolBarRender={() => {
                    return [
                        productForm(<Button type="primary" onClick={() => {
                            form.resetFields();
                        }}> 创建 </Button>, form, '创建商品')
                    ]

                }}
                tableAlertOptionRender={(data) => {
                    return (
                        <Space size={16}>
                            <Popconfirm
                                title="批量删除"
                                description={`是否批量删除？`}
                                okText="确认"
                                cancelText="取消"
                                onConfirm={() => {
                                    ProductAPI.del(data?.selectedRowKeys).then(() => {
                                        // @ts-ignore
                                        ref.current.clearSelected();
                                        ref.current?.reload();
                                    });
                                }}
                            >
                                <Button type="primary" danger>删除</Button>
                            </Popconfirm>

                            <Popconfirm
                                title="批量上架"
                                description={`是否批量上架？`}
                                okText="确认"
                                cancelText="取消"
                                onConfirm={() => {
                                    ProductAPI.updateStatus(data?.selectedRowKeys, true).then(() => {
                                        // @ts-ignore
                                        ref.current.clearSelected();
                                        ref.current?.reload();
                                    });
                                }}
                            >
                                <Button type="primary">上架</Button>
                            </Popconfirm>

                            <Popconfirm
                                title="批量下架"
                                description={`是否批量下架？`}
                                okText="确认"
                                cancelText="取消"
                                onConfirm={() => {
                                    ProductAPI.updateStatus(data?.selectedRowKeys, false).then(() => {
                                        // @ts-ignore
                                        ref.current.clearSelected();
                                        ref.current?.reload();

                                    });
                                }}
                            >
                                <Button danger>下架</Button>

                            </Popconfirm>
                        </Space>
                    );
                }}
            />

            <Image preview={preview}/>
        </div>
    );
};

export default ProductList;
