package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.OrderHouse;
import work.onss.community.domain.village.OrderHouseRepository;
import work.onss.community.domain.village.QOrderHouse;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class OrderHouseController {
    @Autowired
    private OrderHouseRepository orderHouseRepository;

    @GetMapping(value = {"orderHouses/{id}"}, name = "账单详情")
    public OrderHouse detail(@PathVariable String id) {
        return orderHouseRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"orderHouses"}, name = "账单列表")
    public Page<OrderHouse> page(
            @RequestParam Integer year,
            @QuerydslPredicate(bindings = OrderHouseRepository.class) Predicate predicate,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QOrderHouse qOrderHouse = QOrderHouse.orderHouse;
        return orderHouseRepository.findAll(qOrderHouse.startDate.year().eq(year).and(predicate), pageable);
    }

    @PostMapping(value = {"orderHouses/export"}, name = "账单导出")
    public void export(@QuerydslPredicate(bindings = OrderHouseRepository.class, root = OrderHouse.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        Collection<OrderHouse> data = (Collection<OrderHouse>) orderHouseRepository.findAll(predicate);
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderHouse.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<OrderHouse>> floorNumberData = data.stream().collect(Collectors.groupingBy(OrderHouse::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}