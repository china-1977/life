package work.onss.community.config;

import lombok.extern.log4j.Log4j2;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;

import java.io.IOException;

@Log4j2
public class ScoreMNIST {

    public static void main(String[] args) throws IOException {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        VideoCapture camera = new VideoCapture(0);
        if (!camera.isOpened()) {
            System.out.println("Error: Could not open the camera.");
            return;
        }

        Mat frame = new Mat();
        while (true) {
            if (camera.read(frame)) {
                Imgcodecs.imwrite("frame.png", frame);
//                 HighGui.imshow("Frame", frame);
            } else {
                System.out.println("Error: Could not read a frame.");
                break;
            }
        }

        camera.release();
    }
}