Page({
  mixins: [require('../../mixin/common')],
  data: {
    orderParks: [],
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderParks`,
        header: { authorization },
        data: { year: this.data.year, size: this.data.size }
      }).then((data) => {
        this.setData({
          orderParks: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderParks`,
        header: { authorization },
        data: { year: this.data.year, size: this.data.size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          orderParks: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderParks`,
        header: { authorization },
        data: { page: this.data.number + 2, year: this.data.year, size: this.data.size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let orderParks = this.data.orderParks;
          orderParks.push(...data.content)
          this.setData({
            orderParks, number: data.number
          });
        }
      });
    });
  },

  yearChange: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderParks`,
        header: { authorization },
        data: { year: e.detail.value }
      }).then((data) => {
        this.setData({
          orderParks: data.content, number: data.number, year: e.detail.value
        });
      });
    });
  }
})