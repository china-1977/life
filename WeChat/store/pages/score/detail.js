import { wxLogin, domain, scoreStatus, scoreWay, wxRequest } from '../../utils/util.js';

Page({
  data: {
    domain,
    scoreStatus, scoreWay
  },
  onLoad: function (options) {
    if (options.q) {
      const link = decodeURIComponent(options.q);
      const paramStr = link.match(/\?param=(.*)/)[1];
      const param = JSON.parse(paramStr);
      options.id = param.id;
    }
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/store/scores/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((score) => {
        this.setData({
          index: options.index,
          score
        });
      });
    })
  },

  clipBoard: function (e) {
    wx.setClipboardData({
      data: e.currentTarget.dataset.value,
    })
  },

  waitPackage: function (e) {
    const oldScore = this.data.score;
    wxLogin().then(({ authorization, info }) => {
      wx.showModal({
        title: '订单状态',
        content: scoreStatus[oldScore.status],
        confirmColor: '#e64340',
        success: (res) => {
          if (res.confirm) {
            wxRequest({
              url: `${domain}/store/scores/${oldScore.id}/waitPackage`,
              header: { authorization, cid: info.cid, mid: info.mid },
              method: 'POST',
              data: oldScore
            }).then((score) => {
              wx.navigateBack({
                delta: 2,
                fail: (res) => {
                  wx.reLaunch({
                    url: '/pages/score/score'
                  })
                }
              })
            });
          }
        }
      })
    })
  },

  waitDeliver: function (e) {
    const oldScore = this.data.score;
    wxLogin().then(({ authorization, info }) => {
      wx.showModal({
        title: '订单状态',
        content: scoreStatus[oldScore.status],
        confirmColor: '#e64340',
        success: (res) => {
          if (res.confirm) {
            wxRequest({
              url: `${domain}/store/scores/${oldScore.id}/waitDeliver`,
              header: { authorization, cid: info.cid, mid: info.mid },
              method: 'POST',
              data: oldScore
            }).then((score) => {
              wx.navigateBack({
                delta: 2
              })
            });
          }
        }
      })
    })
  },

  refund: function (e) {
    const oldScore = this.data.score;
    wxLogin().then(({ authorization, info }) => {
      wx.showModal({
        title: '警示',
        content: '确定退货?',
        confirmColor: '#e64340',
        success: (res) => {
          if (res.confirm) {
            wxRequest({
              url: `${domain}/store/scores/${oldScore.id}-${e.detail.value.outTradeNo}/refund`,
              header: { authorization, cid: info.cid, mid: info.mid },
              method: 'POST',
              data: oldScore
            }).then((score) => {
              wx.navigateBack({
                delta: 2
              })
            });
          }
        }
      })
    })
  },

  makePhoneCall: function (e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone
    })
  },

  printer: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wx.showModal({
        title: '警示',
        content: '确定再次打印?',
        confirmColor: '#e64340',
        success: (res) => {
          if (res.confirm) {
            wxRequest({
              url: `${domain}/store/scores/${this.data.score.id}/printer`,
              header: { authorization, cid: info.cid, mid: info.mid },
            });
          }
        }
      })
    })
  }
})
