import * as FileController from './FileController';
import * as LoginController from './LoginController';
import * as MemberController from './MemberController';
import * as MerchantController from './MerchantController';
import * as PayOrderController from './Store/PayOrderController';

export default {
  MerchantController,
  LoginController,
  MemberController,
  FileController,
  PayOrderController,
};
