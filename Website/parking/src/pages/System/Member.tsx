import {exportData} from '@/services/FileController';
import services from '@/services/api';
import {useRequest} from '@@/exports';
import {ActionType, ProTable} from '@ant-design/pro-components';
import {
    Button,
    DatePicker,
    Dropdown,
    Form,
    Input,
    message,
    Modal,
    Popconfirm,
    TreeSelect,
    Typography,
} from 'antd';
import {useRef, useState} from 'react';
import resources from '../../../public/resources.json';

const {getMembers, delMember, getApplications, updateApplications} = services.MemberController;

const MemberList = () => {
    const actionRef = useRef<ActionType>();
    const [form] = Form.useForm();
    const [modalVisible, setModalVisible] = useState<any>(false);
    const [applications, setApplications] = useState<any>(resources);
    let {loading: getApplicationsLoading, run: getApplicationsRun} = useRequest(getApplications, {manual: true});
    let {loading: delMemberLoading, run: delMemberRun} = useRequest(delMember, {
        manual: true,
    });
    let {
        loading: updateApplicationsLoading,
        run: updateApplicationsRun
    } = useRequest(updateApplications, {manual: true});

    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            align: 'center',
            valueType: 'indexBorder',
        },
        {title: '主键', dataIndex: 'id', align: 'center', copyable: true},
        {title: '姓名', dataIndex: 'name', align: 'center', copyable: true},
        {title: '身份证号', dataIndex: 'idCard', align: 'center', copyable: true},
        {title: '手机号', dataIndex: 'phone', align: 'center', copyable: true},
        {title: '账号', dataIndex: 'username', align: 'center', copyable: true},
        {title: 'appid', dataIndex: 'appid', align: 'center', copyable: true},
        {title: 'openid', dataIndex: 'openid', align: 'center', copyable: true},
        {
            title: '注册时间',
            dataIndex: 'insertDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '更新日期',
            dataIndex: 'updateDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '操作',
            valueType: 'option',
            align: 'center',
            render: (text: any, record: any) => (
                <Dropdown.Button
                    key={record.id}
                    loading={getApplicationsLoading || delMemberLoading}
                    menu={{
                        items: [
                            {
                                key: 'AUTHORIZE',
                                label: <Button type="primary">授权</Button>,
                                onClick: () => {
                                    getApplicationsRun(record.id)
                                        .then((res: any) => {
                                            let applicationsId = [];
                                            let newApplications:{path: string, name: string, routes: any[]} = {
                                                path: 'ALL',
                                                name: '其他接口',
                                                routes: []
                                            };
                                            const paths = JSON.stringify(resources);
                                            for (const item of res) {
                                                if (item.merchantCustomerApplication) {
                                                    applicationsId.push(item.application.contextPath.concat("/").concat(item.application.name));
                                                }
                                                if (!paths.includes(item.application.contextPath.concat("/").concat(item.application.name))) {
                                                    newApplications.routes.push({
                                                        name: item.application.name,
                                                        path: item.application.contextPath.concat("/").concat(item.application.name),
                                                    });
                                                }
                                            }
                                            setApplications([...resources,newApplications])
                                            console.log(resources);
                                            console.log(applicationsId);
                                            return applicationsId;
                                        })
                                        .then((applicationsId: any) => {
                                            form.setFieldsValue({...record, applicationsId});
                                            setModalVisible('AUTHORIZE');
                                        });
                                },
                            },
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${record.name}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        delMemberRun(record.id).then(() => {
                                            message.success('删除成功').then(() => {
                                                actionRef.current?.reload();
                                            });
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text
                        copyable={{
                            text: JSON.stringify(record, ['id', 'name', 'idCard', 'phone', 'openid', 'insertDate', 'updateDate']),
                        }}
                    >
                        操作
                    </Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];
    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        appid: {show: false},
                        openid: {show: false},
                    }
                }}
                actionRef={actionRef}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params: any) => getMembers(params)}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/account/merchantCustomers/export', values, '员工信息.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
            />
            <Modal
                centered={true}
                width={'35%'}
                title="授权"
                open={modalVisible === 'AUTHORIZE'}
                confirmLoading={updateApplicationsLoading}
                onOk={() => {
                    form.submit();
                }}
                onCancel={() => {
                    form.resetFields();
                    setModalVisible(false);
                }}
            >
                <Form
                    labelCol={{span: 6}}
                    wrapperCol={{span: 16}}
                    form={form}
                    onFinish={(values) => {
                        updateApplicationsRun(values).then(() => {
                            setModalVisible(false);
                            form.resetFields();
                            message.success('授权成功');
                        });
                    }}
                >
                    <Form.Item hidden={true} name="id">
                        <Input/>
                    </Form.Item>
                    <Form.Item label="姓名" name="name" rules={[{required: true, message: '请输入描述'}]}>
                        <Input disabled/>
                    </Form.Item>
                    <Form.Item label="手机号" name="phone" rules={[{required: true, message: '请输入描述'}]}>
                        <Input disabled/>
                    </Form.Item>
                    <Form.Item label="接口权限" name="applicationsId">
                        <TreeSelect
                            treeCheckable= {true}
                            fieldNames={
                                {
                                    value: 'path',
                                    label: 'name',
                                    children: 'routes',
                                }
                            }
                            treeData={applications}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
};

export default MemberList;
