Page({
  mixins: [require('../../mixin/common')],
  data: {
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/houses/${options.id}/detail`,
        header: { authorization },
      }).then((data) => {
        this.setData({
          ...data
        });
      });
    });
  },

  unbinding: function (e) {
    wx.showModal({
      title: '警示',
      content: '是否真的解除？',
      success: (res) => {
        if (res.confirm) {
          this.wxLogin().then(({ authorization }) => {
            const house = this.data.house;
            this.wxRequest({
              url: `${this.data.domain}/owner/houses/unbinding`,
              method: 'POST',
              header: { authorization },
              data: [house.id]
            }).then((data) => {
              wx.showModal({
                title: '提示',
                content: '解除成功',
                showCancel: false,
                success: (res) => {
                  wx.navigateBack({
                    delta: 2
                  });
                }
              })
            })
          })
        }
      }
    })
  },

  onShareAppMessage: function() {
    wx.downloadFile({
      url: 'https://res.wx.qq.com/wxdoc/dist/assets/img/demo.ef5c5bef.jpg',
      success: (res) => {
        wx.showShareImageMenu({
          path: res.tempFilePath
        })
      }
    })
  }
})