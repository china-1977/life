/**
 * 访客列表
 */
import services from '@/services/api';
import { ActionType, ProTable } from '@ant-design/pro-components';
import { Button, DatePicker } from 'antd';
import { useRef } from 'react';

const { page, del, setStatus } = services.VisitorController;
const { exportData } = services.FileController;
const VisitorList = () => {
  const columns: any = [
    { title: '序号', dataIndex: 'index', valueType: 'indexBorder', align: 'center' },
    { title: '主键', align: 'center', dataIndex: 'id', copyable: true },
    {
      title: '楼号',
      align: 'center',
      dataIndex: 'floorNumber',
      copyable: true,
      onSorter: false,
      sorter: {multiple: 1},
    },
    {title: '单元', align: 'center', dataIndex: 'unit', copyable: true, sorter: {multiple: 1}, onSorter: false},
    {
      title: '室',
      align: 'center',
      dataIndex: 'roomNumber',
      copyable: true,
      onSorter: false,
      sorter: {multiple: 1},
    },
    { title: '车牌号', align: 'center', dataIndex: 'carCode', copyable: true, onSorter: false },
    { title: '姓名', align: 'center', dataIndex: 'name', copyable: true },
    { title: '身份证', align: 'center', dataIndex: 'idCard', copyable: true },
    { title: '手机号', align: 'center', dataIndex: 'phone', copyable: true },
    {
      title: '访问时间',
      dataIndex: 'visitDatetime',
      align: 'center',
      copyable: true,
      fieldProps: { format: 'YYYY-MM-DDTHH:mm:ss' },
      sorter: { multiple: 1 },
      onSorter: false,
      renderFormItem: () => <DatePicker.RangePicker showTime />,
    },
    {
      title: '离开时间',
      dataIndex: 'leaveDatetime',
      align: 'center',
      copyable: true,
      fieldProps: { format: 'YYYY-MM-DDTHH:mm:ss' },
      sorter: { multiple: 1 },
      onSorter: false,
      renderFormItem: () => <DatePicker.RangePicker showTime />,
    },
    {
      title: '申请日期',
      dataIndex: 'applicationTime',
      align: 'center',
      copyable: true,
      fieldProps: { format: 'YYYY-MM-DDTHH:mm:ss' },
      sorter: { multiple: 1 },
      onSorter: false,
      renderFormItem: () => <DatePicker.RangePicker showTime />,
    },
  ];

  const ref = useRef<ActionType>();

  return (
    <div>
      <ProTable
        columnsState={{
          defaultValue: {
            id: { show: false },
            houseId: { show: false },
          },
        }}
        actionRef={ref}
        rowKey="id"
        headerTitle="查询表格"
        columns={columns}
        request={(params: any, sorter, filter) => page(params, sorter, filter)}
        search={{
          optionRender: (searchConfig, formProps, dom) => [
            <Button
              key="out"
              onClick={() => {
                const values = searchConfig?.form?.getFieldsValue();
                exportData('/village/visitors/export', values, '访客.xlsx');
              }}
            >
              导出
            </Button>,
            ...dom.values(),
          ],
        }}
      />
    </div>
  );
};

export default VisitorList;
