import { wxLogin, domain, wxRequest, RepairStatus, windowWidth } from '../../utils/util.js';

Page({
  data: {
    domain,
    windowWidth,
    RepairStatus
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/repairs/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((repair) => {
        this.setData({
          repair
        });
      });
    });
  },

  allot: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wx.showModal({
        title: '提示',
        content: '是否分配？',
        success: (res) => {
          if (res.confirm) {
            let repair = this.data.repair;
            repair.status = 'WAIT_REPAIR';
            repair.label = e.detail.value.label;
            wxRequest({
              url: `${domain}/village/repairs`,
              header: { authorization, cid: info.cid, mid: info.mid },
              data: repair,
              method: "POST",
            }).then(() => {
              wx.showModal({
                title: '提示',
                content: '分配成功',
                showCancel: false,
                success: (res) => {
                  wx.navigateBack({
                    delta: 2
                  });
                }
              })
            })
          }
        }
      })
    });
  }
})