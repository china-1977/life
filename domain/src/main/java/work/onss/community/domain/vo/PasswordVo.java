package work.onss.community.domain.vo;

import lombok.Data;

import java.util.Set;

@Data
public class PasswordVo {
    private Set<String> password;
    private String oldPassword;
}
