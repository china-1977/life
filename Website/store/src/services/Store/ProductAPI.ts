import {sort} from '@/utils/format';
import {request} from '@@/exports';

export default {
    page: (params: any, sorter: any, filter: any) =>
        request<any>('/store/products', {
            params: {
                ...params,
                sort: sort(sorter),
                ...filter,
            },
        }).then((res: any) => {
            return {
                data: res.content,
                total: res.totalElements,
            };
        }),

    all: () =>
        request<any>('/store/products/all'),
    save: async (body: any) =>
        request<any>('/store/products/save', {
            method: 'POST',
            data: body,
        }),
    del: async (body: any[]) =>
        request<any>('/store/products/delete', {
            method: 'POST',
            data: body,
        }),
    updateStatus: async (body: any[], status: boolean) =>
        request<any>(`/store/products/${status}/updateStatus`, {
            method: 'POST',
            data: body,
        }),
}