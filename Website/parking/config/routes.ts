export default [
  {
    path: '/',
    name: '首页',
    icon: 'dashboard',
    component: '@/pages/Index',
  },
  {
    path: '/parking',
    name: '停车管理',
    icon: 'dashboard',
    routes: [
      {
        path: '/parking/region',
        name: '停车区域',
        component: '@/pages/Parking/ParkRegionList',
        resources: [
          {
            name: '停车区域导出',
            path: '/parking/停车区域导出',
          },
          {
            name: '停车区域编辑',
            path: '/parking/停车区域编辑',
          },
          {
            name: '停车区域详情',
            path: '/parking/停车区域详情',
          },
          {
            name: '停车区域列表',
            path: '/parking/停车区域列表',
          },
          {
            name: '停车区域批量删除',
            path: '/parking/停车区域批量删除',
          },
        ],
      },
      {
        path: '/parking/order',
        name: '出入记录',
        component: '@/pages/Parking/OrderPark',
        resources: [
          {
            name: '停车费导出',
            path: '/parking/停车费导出',
          },
          {
            name: '停车费详情',
            path: '/parking/停车费详情',
          },
          {
            name: '停车费列表',
            path: '/parking/停车费列表',
          },
        ],
      },
      {
        path: '/parking/gate',
        name: '道闸管理',
        component: '@/pages/Parking/ParkGateList',
        resources: [
          {
            name: '道闸导出',
            path: '/parking/道闸导出',
          },
          {
            name: '道闸编辑',
            path: '/parking/道闸编辑',
          },
          {
            name: '道闸详情',
            path: '/parking/道闸详情',
          },
          {
            name: '道闸列表',
            path: '/parking/道闸列表',
          },
          {
            name: '道闸批量删除',
            path: '/parking/道闸批量删除',
          },
        ],
      },
    ],
  },
  {
    path: '/money',
    name: '财务管理',
    icon: 'dashboard',
    routes: [
      {
        path: '/money/orderParks',
        name: '停车费',
        icon: 'dashboard',
        component: '@/pages/Parking/OrderPark',
        resources: [
          {
            name: '停车费导出',
            path: '/parking/停车费导出',
          },
          {
            name: '停车费详情',
            path: '/parking/停车费详情',
          },
          {
            name: '停车费列表',
            path: '/parking/停车费列表',
          },
        ],
      },
      {
        path: '/money/payOrder',
        name: '支付记录',
        icon: 'dashboard',
        component: '@/pages/Order/PayOrder',
        resources: [
          {
            name: '支付记录导出',
            path: '/parking/支付记录导出',
          },
          {
            name: '支付记录详情',
            path: '/parking/支付记录详情',
          },
          {
            name: '支付记录列表',
            path: '/parking/支付记录列表',
          },
        ],
      },
    ],
  },
  {
    path: '/base',
    name: '账户设置',
    icon: 'dashboard',
    routes: [
      {
        path: '/base/merchants',
        name: '主体管理',
        component: '@/pages/System/Merchant',
        resources: [
          {
            name: '主体更新',
            path: '/account/主体更新',
          },
          {
            name: '主体图片',
            path: '/account/主体图片',
          },
          {
            name: '主体详情',
            path: '/account/主体详情',
          },
          {
            name: '主体状态',
            path: '/account/主体状态',
          },
        ],
      },
      {
        path: '/base/members',
        name: '员工列表',
        component: '@/pages/System/Member',
        resources: [
          {
            name: '营业成员权限设置',
            path: '/account/营业成员权限设置',
          },
          {
            name: '营业成员删除',
            path: '/account/营业成员删除',
          },
          {
            name: '营业成员身份证查询',
            path: '/account/营业成员身份证查询',
          },
          {
            name: '营业成员邀请',
            path: '/account/营业成员邀请',
          },
          {
            name: '营业成员资源列表',
            path: '/account/营业成员资源列表',
          },
          {
            name: '营业员导出',
            path: '/account/营业员导出',
          },
          {
            name: '营业员列表',
            path: '/account/营业员列表',
          },
        ],
      },
    ],
  },
  {
    path: '/login',
    name: '登录',
    component: '@/pages/Login',
    layout: false,
    hideInMenu: true,
  },
];