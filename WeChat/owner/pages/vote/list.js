Page({
  mixins: [require('../../mixin/common')],
  data: {
    currentID: -1,
    buttons: [
      {
        type: "warn",
        text: '解除',
      }
    ],
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/votes`,
        header: { authorization },
        data: { size: this.data.size }
      }).then((data) => {
        if (data) {
          this.setData({
            votes: data.content, number: data.number
          });
        }
      });
    });
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/votes`,
        header: { authorization },
        data: { size: this.data.size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        if (data) {
          this.setData({
            votes: data.content, number: data.number
          });
        }
      });
    });
  },

  onReachBottom: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/votes`,
        header: { authorization },
        data: { page: this.data.number + 2, size: this.data.size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let votes = this.data.votes;
          votes.push(...data.content)
          this.setData({
            votes, number: data.number
          });
        }
      });
    });
  }
})