export default [
  {
    path: '/',
    name: '首页',
    icon: 'dashboard',
    component: '@/pages/Index',
  },
  {
    path: '/products',
    name: '商品管理',
    component: '@/pages/Store/Product',
    resources: [
      {
        name: '商品详情',
        path: '/store/商品详情',
      },
      {
        name: '商品分页',
        path: '/store/商品分页',
      },
      {
        name: '商品保存',
        path: '/store/商品保存',
      },
      {
        name: '商品删除',
        path: '/store/商品删除',
      },
      {
        name: '商品状态更新',
        path: '/store/商品状态更新',
      },
      {
        name: '商品加入橱窗',
        path: '/store/商品加入橱窗',
      },
    ],
  },
  {
    path: '/orderProduct',
    name: '订单管理',
    component: '@/pages/Store/OrderProduct',
    resources: [
      {
        name: '商品订单分页',
        path: '/store/商品订单分页',
      },
    ],
  },
  {
    path: '/base',
    name: '账户设置',
    icon: 'dashboard',
    routes: [
      {
        path: '/base/merchants',
        name: '主体管理',
        component: '@/pages/System/Merchant',
        resources: [
          {
            name: '主体更新',
            path: '/account/主体更新',
          },
          {
            name: '主体图片',
            path: '/account/主体图片',
          },
          {
            name: '主体详情',
            path: '/account/主体详情',
          },
          {
            name: '主体状态',
            path: '/account/主体状态',
          },
        ],
      },
      {
        path: '/base/members',
        name: '员工列表',
        component: '@/pages/System/Member',
        resources: [
          {
            name: '营业成员权限设置',
            path: '/account/营业成员权限设置',
          },
          {
            name: '营业成员删除',
            path: '/account/营业成员删除',
          },
          {
            name: '营业成员身份证查询',
            path: '/account/营业成员身份证查询',
          },
          {
            name: '营业成员邀请',
            path: '/account/营业成员邀请',
          },
          {
            name: '营业成员资源列表',
            path: '/account/营业成员资源列表',
          },
          {
            name: '营业员导出',
            path: '/account/营业员导出',
          },
          {
            name: '营业员列表',
            path: '/account/营业员列表',
          },
        ],
      },
    ],
  },

  {
    path: '/login',
    name: '登录',
    component: '@/pages/Login',
    layout: false,
    hideInMenu: true,
  },
];