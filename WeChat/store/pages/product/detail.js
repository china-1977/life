import { windowWidth, wxLogin, domain, wxRequest } from '../../utils/util.js';
Page({
  data: {
    domain,
    windowWidth
  },

  onLoad: function (options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/store/products/${options.id}`,
        method: "GET",
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((product) => {
        this.setData({
          index: options.index,
          product
        })
      })
    });
  },
})