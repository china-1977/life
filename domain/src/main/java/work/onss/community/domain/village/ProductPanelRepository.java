package work.onss.community.domain.village;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

public interface ProductPanelRepository extends JpaRepository<ProductPanel, String>, QuerydslPredicateExecutor<ProductPanel>, QuerydslBinderCustomizer<QProductPanel> {
    default void customize(QuerydslBindings bindings, QProductPanel qProductPanel) {
        bindings.bind(qProductPanel.id).withDefaultBinding();
    }

    Optional<ProductPanel> findByIdAndMerchantId(String id, String sid);

    @Modifying
    @Transactional
    void deleteByIdInAndMerchantId(Collection<String> ids, String sid);
}
