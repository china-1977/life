package work.onss.community.service.wechat;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;


@HttpExchange(value = "https://api.weixin.qq.com")
public interface MiniAppService {

    /**
     * 获取小程序全局唯一后台接口调用凭据（access_token）
     *
     * @param appid      小程序 appid
     * @param secret     小程序 appsecret
     * @param js_code    登录时获取的 code
     * @param grant_type 授权类型，此处只需填写 authorization_code
     * @return access_token
     */
    @GetExchange(value = "/sns/jscode2session")
    String jscode2session(@RequestParam String appid,
                          @RequestParam String secret,
                          @RequestParam String js_code,
                          @RequestParam String grant_type
    );

    /**
     * 获取小程序全局唯一后台接口调用凭据（access_token）
     *
     * @param grant_type 填写 client_credential
     * @param appid      小程序唯一凭证
     * @param secret     小程序唯一凭证密钥
     * @return access_token
     */
    @GetExchange(value = "/cgi-bin/token")
    String token(@RequestParam String grant_type, @RequestParam String appid, @RequestParam String secret);

    /**
     * 发送微信小程序订阅消息
     *
     * @param access_token     凭证
     * @param subscribeMessage 消息
     * @return {"errcode":0,"errmsg":"ok"}
     */
    @PostExchange(value = "/cgi-bin/message/subscribe/send", accept = {"application/json"}, contentType = "application/json")
    String sendSubscribeMessage(@RequestBody String subscribeMessage, @RequestParam String access_token);
}
