import { wxLogin, domain, wxRequest } from '../../utils/util.js';
Page({

  data: {
    
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      if (options.id) {
        wxRequest({
          url: `${domain}/village/visitors/${options.id}`,
          header: { authorization, cid: info.cid, mid: info.mid },
        }).then((visitor) => {
          this.setData({
            ...visitor
          });
        });
      }
    });
  },
})