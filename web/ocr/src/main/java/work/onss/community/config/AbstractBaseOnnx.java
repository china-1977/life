package work.onss.community.config;

import ai.onnxruntime.OrtEnvironment;
import ai.onnxruntime.OrtException;
import ai.onnxruntime.OrtLoggingLevel;
import ai.onnxruntime.OrtSession;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.Core;

/**
 * 基础的onnx引擎初始化类
 */
@Slf4j
@Data
public abstract class AbstractBaseOnnx {

    static {
        // 加载动态链接库
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    /**
     * 用于创建和 管理ONNX运行时（onnx-runtime）会话（OrtSession）-初始化赋值
     * 这个类被设计成单例模式，意味着在一个JVM（Java Virtual Machine）的生命周期中，最多只能创建一个OrtEnvironment对象。
     * 此类的主要功能是创建和管理ONNX运行时的会话，这些会话用于封装特定的模型，并提供执行这些模型的能力。
     */
    private OrtEnvironment ortEnvironment;
    /**
     * 用于加载ONNX模型、执行推理以及管理会话状态-初始化赋值
     * OrtSession类是Java中ONNX运行时（onnx-runtime）的一部分，它包装了一个ONNX模型并允许进行推理调用。
     * 这个类允许你检查模型的前置和后置节点。它是由OrtEnvironment类的实例生成的。
     * 一旦会话被关闭，你就不能再使用这个会话。如果你尝试在会话关闭后执行任何操作（例如，获取输入或输出节点），将会抛出异常。
     */
    private OrtSession[] sessionArray;
    /**
     * OrtSession输入名称是指OrtSession模型中的输入层名称。在ONNX Runtime中，OrtSession是用于执行ONNX模型的类。输入名称是该模型中的输入层的名称，这些输入层是模型期望的输入
     * 数据。在模型训练后，这些输入层的名称是固定的，因此我们可以通过获取这些输入名称来了解模型需要什么样的输入数据。在代码中，this.inputNames保存了第一个OrtSession模型的输入名称。
     */
    private String[] inputNameArray;

    /**
     * 构造函数
     *
     * @param modelPath 模型的路径
     * @param thread    开启的线程数
     */
    public AbstractBaseOnnx(String modelPath, int thread) {
        try {
            this.ortEnvironment = OrtEnvironment.getEnvironment();
            OrtSession.SessionOptions option = new OrtSession.SessionOptions();
            // 开启线程数
            option.setInterOpNumThreads(thread);
            // 日志级别
            option.setSessionLogLevel(OrtLoggingLevel.ORT_LOGGING_LEVEL_ERROR);
            // 优化级别
            option.setOptimizationLevel(OrtSession.SessionOptions.OptLevel.BASIC_OPT);

            this.sessionArray = new OrtSession[]{ortEnvironment.createSession(modelPath, option)};
            this.inputNameArray = new String[]{this.sessionArray[0].getInputNames().iterator().next()};

        } catch (OrtException e) {
            log.warn("[onnx] 初始化模型异常", e);
            throw new RuntimeException("[onnx] 初始化模型异常");
        }
    }

    /**
     * 关闭服务
     */
    protected void close() {
        if (sessionArray != null) {
            for (OrtSession session : sessionArray) {
                try {
                    session.close();
                } catch (OrtException e) {
                    log.error("[onnx] 关闭服务失败");
                }
            }
        }
    }
}
