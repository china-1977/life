package work.onss.community.domain.dto.village;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.village.OrderHouse;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderHouseCustomerDto {
    private OrderHouse orderHouse;
    private Customer customer;
}
