package work.onss.community.controller;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.shop.*;
import work.onss.community.domain.village.ProductPanelRepository;
import work.onss.community.service.exception.BusinessException;

import java.util.Collection;

@Log4j2
@RestController
public class ProductController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductPanelRepository productPanelRepository;

    @GetMapping(value = {"products/{id}"}, name = "商品详情")
    public Product product(@PathVariable String id, @RequestHeader(name = "mid") String mid) {
        return productRepository.findByIdAndMerchantId(id, mid).orElse(null);
    }

    @GetMapping(value = {"products"}, name = "商品分页")
    public Page<Product> products(@RequestHeader(name = "mid") String mid,
                                  @QuerydslPredicate(bindings = ProductRepository.class) Predicate predicate,
                                  @PageableDefault(sort = {"orderLabel"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QProduct qProduct = QProduct.product;
        return productRepository.findAll(qProduct.merchantId.eq(mid).and(predicate), pageable);
    }

    @PostMapping(value = {"products/save"}, name = "商品保存")
    public Product insert(@RequestHeader(name = "mid") String mid, @Validated @RequestBody Product product) {
        product.setMerchantId(mid);
        if (product.getId() == null) {
            productRepository.save(product);
        } else {
            Product oldProduct = productRepository.findById(product.getId()).orElseThrow(() -> new BusinessException("商品不存在"));
            if (oldProduct.getMerchantId().equals(mid)) {
                productRepository.save(product);
            }
        }
        return product;
    }

    @PostMapping(value = {"products/delete"}, name = "商品删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        productRepository.deleteByIdInAndMerchantId(ids, mid);
    }

    @Transactional
    @PostMapping(value = {"products/{status}/updateStatus"}, name = "商品状态更新")
    public void updateStatus(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids, @PathVariable Boolean status) {
        QProduct qProduct = QProduct.product;
        jpaQueryFactory.update(qProduct)
                .set(qProduct.status, status)
                .where(qProduct.id.in(ids), qProduct.merchantId.eq(mid))
                .execute();
    }


}
