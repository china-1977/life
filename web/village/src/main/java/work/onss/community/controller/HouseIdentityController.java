package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.HouseIdentity;
import work.onss.community.domain.village.HouseIdentityRepository;
import work.onss.community.domain.village.QHouseIdentity;
import work.onss.community.domain.village.relational.HouseCustomer;
import work.onss.community.domain.village.relational.HouseCustomerRepository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class HouseIdentityController {
    @Autowired
    private HouseIdentityRepository houseIdentityRepository;
    @Autowired
    private HouseCustomerRepository houseCustomerRepository;

    @GetMapping(value = {"houseIdentitys"}, name = "入住申请列表")
    public Page<HouseIdentity> page(@RequestHeader(name = "mid") String mid,
                                    @QuerydslPredicate(bindings = HouseIdentityRepository.class) Predicate predicate,
                                    @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        return houseIdentityRepository.findAll(qHouseIdentity.merchantId.eq(mid).and(predicate), pageable);
    }

    @GetMapping(value = {"houseIdentitys/{id}"}, name = "入住申请详情")
    public HouseIdentity detail(@RequestHeader(name = "mid") String mid, @PathVariable String id) {
        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        return houseIdentityRepository.findOne(qHouseIdentity.merchantId.eq(mid).and(qHouseIdentity.id.eq(id))).orElse(null);
    }

    @DeleteMapping(value = {"houseIdentitys"}, name = "入住申请删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody List<String> ids) {
        houseIdentityRepository.deleteAllByMerchantIdAndIdIn(mid, ids);
    }

    @Transactional
    @PostMapping(value = {"houseIdentitys/{status}/setStatus"}, name = "入住申请审核")
    public void binding(@RequestHeader(name = "mid") String mid, @PathVariable HouseIdentity.Status status, @RequestBody List<String> ids) {
        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        BooleanExpression booleanExpression = qHouseIdentity.id.in(ids).and(qHouseIdentity.merchantId.eq(mid));
        Iterable<HouseIdentity> identities = houseIdentityRepository.findAll(booleanExpression);
        if (status.equals(HouseIdentity.Status.YES)) {
            // TODO 门禁-注册用户、关联房屋信息
            for (HouseIdentity houseIdentity : identities) {
                HouseCustomer oldHouseCustomer = houseCustomerRepository.findByMerchantIdAndHouseIdAndCustomerId(houseIdentity.getMerchantId(), houseIdentity.getHouseId(), houseIdentity.getCustomerId()).orElse(null);
                if (null == oldHouseCustomer) {
                    HouseCustomer houseCustomer = new HouseCustomer(houseIdentity.getRelation(), houseIdentity.getHouseId(), houseIdentity.getCustomerId(), houseIdentity.getMerchantId());
                    houseCustomerRepository.save(houseCustomer);
                } else {
                    if (!oldHouseCustomer.getRelation().equals(houseIdentity.getRelation())) {
                        oldHouseCustomer.setRelation(houseIdentity.getRelation());
                        houseCustomerRepository.save(oldHouseCustomer);
                    }
                }
                houseIdentity.setStatus(status);
                houseIdentityRepository.save(houseIdentity);
            }
        } else if (status.equals(HouseIdentity.Status.NO)) {
            // TODO 门禁-删除用户、解除房屋信息
            for (HouseIdentity houseIdentity : identities) {
                houseCustomerRepository.deleteByMerchantIdAndHouseIdAndCustomerId(houseIdentity.getMerchantId(), houseIdentity.getHouseId(), houseIdentity.getCustomerId());
                houseIdentity.setStatus(status);
                houseIdentityRepository.save(houseIdentity);
            }
        }
    }

    @PostMapping(value = {"houseIdentitys/export"}, name = "入住申请导出")
    public void export(
            @RequestHeader(name = "mid") String mid,
            @QuerydslPredicate(bindings = HouseIdentityRepository.class, root = HouseIdentity.class) Predicate predicate,
            HttpServletResponse httpServletResponse) throws Exception {
        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        Collection<HouseIdentity> data = (Collection<HouseIdentity>) houseIdentityRepository.findAll(qHouseIdentity.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), HouseIdentity.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<HouseIdentity>> floorNumberData = data.stream().collect(Collectors.groupingBy(HouseIdentity::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();

    }
}