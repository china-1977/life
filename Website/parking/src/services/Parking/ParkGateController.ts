import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: object, filter: any) => {
  return request<any>('/parking/parkGates', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });
};

export const save = async (body?: any) =>
  request<any>('/parking/parkGates', {
    method: 'POST',
    data: body,
  });

export const del = async (id: string[]) =>
  request<any>('/parking/parkGates', {
    method: 'DELETE',
    data: id,
  });
