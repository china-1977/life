package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.OrderHeat;
import work.onss.community.domain.village.OrderHeatRepository;
import work.onss.community.domain.village.QOrderHeat;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class OrderHeatController {
    @Autowired
    private OrderHeatRepository orderHeatRepository;

    @GetMapping(value = {"orderHeats/{id}"}, name = "采暖费详情")
    public OrderHeat detail(@PathVariable String id) {
        return orderHeatRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"orderHeats"}, name = "采暖费列表")
    public Page<OrderHeat> page(
            @RequestHeader(name = "mid") String mid,
            @RequestParam Integer year,
            @QuerydslPredicate(bindings = OrderHeatRepository.class) Predicate predicate,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QOrderHeat qOrderHeat = QOrderHeat.orderHeat;
        return orderHeatRepository.findAll(qOrderHeat.merchantId.eq(mid).and(qOrderHeat.startDate.year().eq(year)).and(predicate), pageable);
    }

    @PostMapping(value = {"orderHeats/export"}, name = "采暖费导出")
    public void export(@QuerydslPredicate(bindings = OrderHeatRepository.class, root = OrderHeat.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        Collection<OrderHeat> data = (Collection<OrderHeat>) orderHeatRepository.findAll(predicate);
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderHeat.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<OrderHeat>> floorNumberData = data.stream().collect(Collectors.groupingBy(OrderHeat::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}