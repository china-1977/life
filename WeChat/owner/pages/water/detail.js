Page({
  mixins: [require('../../mixin/common')],
  data: {
  },
  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderWaters/${options.id}`,
        header: { authorization },
      }).then((orderWater) => {
        this.setData({
          orderWater
        });
      });
    });
  },

  pay: function (e) {
    this.wxLogin().then(({ authorization }) => {
      wx.login({
        success: ({ code }) => {
          const orderWater = this.data.orderWater;
          this.wxRequest({
            url: `${this.data.domain}/owner/orderWaters/${orderWater.id}/pay`,
            header: { authorization },
            method: 'POST',
            data: { appid: this.data.appid, code }
          }).then((miniPayToken) => {
            wx.requestPayment({
              ...miniPayToken,
              success: (res) => {
                wx.showModal({
                  title: '提示',
                  content: '支付成功',
                  showCancel: false,
                  success: (res) => {
                    wx.navigateBack({
                      delta: 1
                    });
                  }
                })
              },
              fail: (res) => {
                console.log(res);
              }
            })
          })
        },
        fail: () => {
          wx.reLaunch({
            url: '/pages/login'
          })
        },
      })
    })
  },
})