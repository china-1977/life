package work.onss.community.domain.village.relational;

import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.query.sqm.mutation.internal.InsertHandler;
import work.onss.community.domain.account.Customer;

import java.io.Serializable;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@DynamicInsert
@DynamicUpdate
public class HouseCustomer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @NotBlank(groups = InsertHandler.class, message = "请选择类型")
    private String relation;
    @NotBlank(groups = InsertHandler.class, message = "请选择房屋")
    private String houseId;
    @NotBlank(groups = InsertHandler.class, message = "请选择业主")
    private String customerId;
    private String merchantId;
    @Transient
    private Customer customer;

    public HouseCustomer(String relation, String houseId, String customerId, String merchantId) {
        this.relation = relation;
        this.houseId = houseId;
        this.customerId = customerId;
        this.merchantId = merchantId;
    }
}
