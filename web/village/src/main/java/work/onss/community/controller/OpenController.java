package work.onss.community.controller;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.account.QCustomer;
import work.onss.community.domain.account.QMerchantCustomer;
import work.onss.community.domain.park.ParkRegion;
import work.onss.community.domain.park.ParkRegionRepository;
import work.onss.community.domain.park.QParkRegion;
import work.onss.community.domain.village.ParkSpace;
import work.onss.community.domain.village.ParkSpaceRepository;
import work.onss.community.domain.village.QHouse;
import work.onss.community.domain.village.QParkSpace;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
public class OpenController {
    @Autowired
    private ParkSpaceRepository parkSpaceRepository;
    @Autowired
    private ParkRegionRepository parkRegionRepository;
    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    /**
     * @param mid      主体ID
     * @param id       车位ID
     * @param keyWords 包含【区域、车牌号】
     * @return 车位列表
     */
    @GetMapping(value = {"parkSpaces/getParkSpaceByKeywords"})
    public List<ParkSpace> getParkSpaceByKeywords(@RequestHeader(name = "mid") String mid, @RequestParam(required = false) String id, @RequestParam(required = false) String keyWords) {
        if (StringUtils.hasLength(id)) {
            return parkSpaceRepository.findAllById(List.of(id));
        } else {
            QParkSpace qParkSpace = QParkSpace.parkSpace;
            return jpaQueryFactory.select(qParkSpace)
                    .from(qParkSpace)
                    .where(qParkSpace.merchantId.eq(mid), qParkSpace.region.concat(qParkSpace.code).concat(qParkSpace.carCode).contains(keyWords))
                    .fetch();
        }
    }

    @GetMapping(value = {"houses/getFloor"})
    public Map<String, List<String>> getFloor(@RequestHeader(name = "mid") String mid) {
        QHouse qHouse = QHouse.house;
        List<String> floorNumbers = jpaQueryFactory.select(qHouse.floorNumber).from(qHouse)
                .where(qHouse.merchantId.eq(mid))
                .groupBy(qHouse.floorNumber).fetch();

        List<String> units = jpaQueryFactory.select(qHouse.unit).from(qHouse)
                .where(qHouse.merchantId.eq(mid))
                .groupBy(qHouse.unit).fetch();

        Map<String, List<String>> data = new HashMap<>();
        data.put("floorNumbers", floorNumbers);
        data.put("units", units);
        return data;
    }


    /**
     * @param mid      主体ID
     * @param id       停车区域ID
     * @param keyWords 包含【名称】
     * @return 停车区域
     */
    @GetMapping(value = {"parkRegions/getParkRegion"})
    public Iterable<ParkRegion> getParkRegion(@RequestHeader(name = "mid") String mid, @RequestParam(required = false) String id, @RequestParam(required = false) String keyWords) {
        QParkRegion qParkRegion = QParkRegion.parkRegion;
        if (id == null && keyWords == null) {
            return parkRegionRepository.findAll(qParkRegion.merchantId.eq(mid));
        } else {
            if (StringUtils.hasLength(id)) {
                return jpaQueryFactory.select(qParkRegion)
                        .from(qParkRegion)
                        .where(qParkRegion.merchantId.eq(mid), qParkRegion.id.eq(id))
                        .fetch();
            }
            if (StringUtils.hasLength(keyWords)) {
                return jpaQueryFactory.select(qParkRegion)
                        .from(qParkRegion)
                        .where(qParkRegion.merchantId.eq(mid), qParkRegion.name.contains(keyWords))
                        .fetch();
            }
        }
        return null;
    }


    /**
     * @param mid      主体ID
     * @param id       账号ID
     * @param keyWords 包含【姓名、电话、登录账号】
     * @return 账户列表
     */
    @GetMapping(value = {"customers/geCustomerByKeywords"})
    public List<Customer> geCustomerByKeywords(@RequestHeader(name = "mid") String mid, @RequestParam(required = false) String id, @RequestParam(required = false) String keyWords) {
        QCustomer qCustomer = QCustomer.customer;
        if (StringUtils.hasLength(id)) {
            return jpaQueryFactory.select(Projections.fields(Customer.class,
                            qCustomer.id,
                            qCustomer.name,
                            qCustomer.phone
                    )).from(qCustomer)
                    .where(qCustomer.id.eq(id))
                    .fetch();
        } else {
            QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
            return jpaQueryFactory.select(qCustomer)
                    .from(qCustomer)
                    .innerJoin(qMerchantCustomer).on(qMerchantCustomer.customerId.eq(qCustomer.id), qMerchantCustomer.merchantId.eq(mid))
                    .where(qCustomer.name.concat(qCustomer.phone).concat(qCustomer.username).contains(keyWords))
                    .fetch();
        }
    }
}