

Page({
  mixins: [require('../../mixin/common')],
  data: {
    radioCheck: false
  },

  realNameForm: function (e) {
    wx.showModal({
      title: '提示',
      content: '是否修改个人信息？',
      success: (res) => {
        if (res.confirm) {
          const { face, idCardFace, idCardNational } = this.data;
          this.wxLogin().then(({ authorization }) => {
            this.wxRequest({
              url: `${this.data.domain}/account/customers/update`,
              header: { authorization },
              data: { ...e.detail.value, face, idCardFace, idCardNational },
              method: "POST",
            }).then((res) => {
              wx.showModal({
                title: '提示',
                content: '变更成功',
                showCancel: false,
                success: (res) => {
                  wx.navigateBack({
                    delta: 1
                  });
                }
              })
            });
          })
        }
      }
    })
  },

  chooseImage: function (e) {
    const id = e.currentTarget.id;
    this.chooseImageToBase64('front').then((file) => {
      this.setData({
        [`${id}`]: file
      })
    })
  },

  deletePicture: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: null
    })
  },

  checkboxChange: function (e) {
  }
})