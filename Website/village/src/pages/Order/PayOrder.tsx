/**
 * 支付记录列表
 */
import {OrderStatusEnum} from '@/constants';
import {exportData} from '@/services/FileController';
import services from '@/services/api';
import {ActionType, ProTable} from '@ant-design/pro-components';
import {Button, DatePicker, Dropdown, Typography} from 'antd';
import dayjs, {Dayjs} from 'dayjs';
import {useRef, useState} from 'react';

const {page, detail} = services.PayOrderController;

const PayOrderList = () => {
    const [year, setYear] = useState<Dayjs | any>(dayjs());

    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '订单编号', align: 'center', dataIndex: 'outTradeNo', copyable: true},
        {
            title: '支付状态', align: 'center', dataIndex: 'status', copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: OrderStatusEnum.columns,
            onFilter: false,
            filters: true,
            search: false,
        },
        {
            title: '支付时间', align: 'center', dataIndex: 'payDatetime', copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {title: '用户ID', align: 'center', dataIndex: 'customerId', copyable: true},
        {title: '支付金额', align: 'center', dataIndex: 'total', copyable: true, search: false},
        {title: '描述', align: 'center', dataIndex: 'description', copyable: true},
        {
            title: '创建时间', align: 'center', dataIndex: 'insertDatetime', copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, record: any) => (
                <Dropdown.Button
                    key={record.id}
                    onClick={async (res) => {
                        detail(record.id).then((res) => {
                            setPayOrder(res);
                        })
                    }}
                    menu={{
                        items: [
                            {
                                key: 'refund_all',
                                label: '全部退款',
                                onClick: () => {
                                    console.log(record)
                                },
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(payOrder)}}>详情</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const ref = useRef<ActionType>();
    const [payOrder, setPayOrder] = useState<any>();

    return (
        <div>
            <ProTable
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                params={{year: year?.year()}}
                columns={columns}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/payOrders/export', values, '支付记录信息.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
                request={(params: any, sorter, filter) => page(params, sorter, filter)}
                toolbar={{
                    filter: <DatePicker picker={'year'} defaultValue={year} onChange={(date, value) => setYear(date)}/>,
                }}
            />
        </div>
    );
};

export default PayOrderList;
