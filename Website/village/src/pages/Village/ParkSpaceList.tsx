/**
 * 车位列表
 */
import {ParkSpaceStateEnum, ParkSpaceStatusEnum} from '@/constants';
import services from '@/services/api';
import {ActionType, ModalForm, ProFormSelect, ProFormText, ProFormTextArea, ProTable} from '@ant-design/pro-components';
import {Button, Dropdown, Form, message, Popconfirm, Typography, Upload} from 'antd';
import {useRef, useState} from 'react';

const {page, save, del} = services.ParkSpaceController;
const {exportData} = services.FileController;

const ParkSpaceList = () => {
    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {title: '区域', align: 'center', dataIndex: 'region', copyable: true, sorter: {multiple: 1}, onSorter: false},
        {
            title: '编号',
            align: 'center',
            dataIndex: 'code',
            copyable: true,
            sorter: {multiple: 1},
            onSorter: false,
        },
        {title: '车牌号', dataIndex: 'carCode', copyable: true},
        {
            title: '设备状态',
            dataIndex: 'state',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: ParkSpaceStateEnum.columns,
        },
        {
            title: '使用状态',
            dataIndex: 'status',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: ParkSpaceStatusEnum.columns,
        },
        {title: '备注', dataIndex: 'remarks', copyable: true},
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, record: any) => (
                <Dropdown.Button
                    key={record.id}
                    onClick={(res) => {
                        form.setFieldsValue(record);
                        setModalVisit('EDIT');
                    }}
                    menu={{
                        items: [
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${record.code}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        del([record.id]).then((res) => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(record)}}>编辑</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const [form] = Form.useForm();
    const [modalVisit, setModalVisit] = useState<any>(false);
    const ref = useRef<ActionType>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        houseId: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params, sorter, filter) => page(params, sorter, filter)}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/parkSpaces/export', values, '车位信息.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
                toolBarRender={() => [
                    <ModalForm
                        title="车位信息"
                        form={form}
                        open={modalVisit === 'EDIT'}
                        grid={true}
                        colProps={{span: 6}}
                        onFinish={async (values) => {
                            values.id = form.getFieldValue('id');
                            return save(values).then((res) => {
                                form.resetFields();
                                ref.current?.reload();
                                return true;
                            });
                        }}
                        onOpenChange={(visible) => {
                            if (visible && modalVisit === 'EDIT') {
                                setModalVisit('EDIT');
                            } else {
                                form.resetFields();
                                setModalVisit(false);
                            }
                        }}
                        trigger={<Button type="primary">新建</Button>}
                    >
                        <ProFormText name="region" label="区域" placeholder="请输入区域"/>
                        <ProFormText name="code" label="编号" placeholder="请输入编号"/>
                        <ProFormText name="carCode" label="车牌号" placeholder="请输入车牌号"/>
                        <ProFormSelect width="sm" name="status" label="使用状态" debounceTime={1000}
                                       valueEnum={ParkSpaceStatusEnum.ProFormSelect}/>
                        <ProFormTextArea name="remarks" label="备注" colProps={{span: 24}}
                                         fieldProps={{autoSize: true}}/>
                    </ModalForm>,
                    <Upload
                        action={`${process.env.baseURL}/village/parkSpaces/imports`}
                        onChange={(e) => {
                            switch (e.file.status) {
                                case 'uploading':
                                    message.loading({content: '上传中', key: e.file.uid});
                                    break;
                                case 'error':
                                    message.error({content: '上传失败', key: e.file.uid});
                                    break;
                                case 'done':
                                    message.success({content: '上传成功', key: e.file.uid});
                                    break;
                            }
                        }}
                        showUploadList={false}
                        headers={{
                            authorization: localStorage.getItem('authorization') as string,
                            cid: localStorage.getItem('cid') as string,
                            mid: localStorage.getItem('mid') as string,
                        }}
                    >
                        <Button>导入</Button>
                    </Upload>,
                ]}
            />
        </div>
    );
};

export default ParkSpaceList;
