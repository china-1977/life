package work.onss.community.service.config;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import jakarta.servlet.ServletContext;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.codehaus.groovy.runtime.InvokerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import work.onss.community.domain.account.Application;
import work.onss.community.domain.account.ApplicationRepository;
import work.onss.community.domain.account.QApplication;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
@Component
@Order(value = 2)
@Service
public class CollectionRunner implements CommandLineRunner {
    public static final GroovyShell groovyShell = new GroovyShell();
    public static final Map<String, Script> scriptCache = new ConcurrentHashMap<>();
    public static WebApplicationContext webApplicationContext;
    Map<RequestMappingInfo, HandlerMethod> handlerMethods = new HashMap<>();
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private ApplicationRepository applicationRepository;

    public CollectionRunner(List<RequestMappingHandlerMapping> handlerMappings, WebApplicationContext webApplicationContext) {
        CollectionRunner.webApplicationContext = webApplicationContext;
        for (RequestMappingHandlerMapping handlerMapping : handlerMappings) {
            Map<RequestMappingInfo, HandlerMethod> handlerMethodMap = handlerMapping.getHandlerMethods();
            if (!handlerMethodMap.isEmpty()) {
                handlerMethods.putAll(handlerMethodMap);
            }
        }
    }

    public static <T> T getBean(Class<T> tClass) {
        return CollectionRunner.webApplicationContext.getBean(tClass);
    }

    public static Object invoke(String scriptText, String methodName, Object... objects) {
        Script script;
        String cacheKey = DigestUtils.md5Hex(scriptText);
        if (scriptCache.containsKey(cacheKey)) {
            script = scriptCache.get(cacheKey);
        } else {
            script = groovyShell.parse(scriptText);
            scriptCache.put(cacheKey, script);
        }
        return InvokerHelper.invokeMethod(script, methodName, objects);
    }

    /**
     * 更新所有接口资源的更新时间，再将旧数据删除
     *
     * @param args
     */
    @Override
    public void run(String... args) {
        ServletContext servletContext = webApplicationContext.getServletContext();
        assert servletContext != null;
        String contextPath = servletContext.getContextPath();
        LocalDateTime now = LocalDateTime.now().withNano(0);
        List<Application> applications = new ArrayList<>();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> m : handlerMethods.entrySet()) {
            HandlerMethod value = m.getValue();
            GetMapping getMapping = value.getMethodAnnotation(GetMapping.class);
            if (getMapping != null && !getMapping.name().isEmpty()) {
                QApplication qApplication = QApplication.application;
                String path = getMapping.value()[0];
                BooleanExpression booleanExpression = qApplication.contextPath.eq(contextPath).and(qApplication.name.eq(getMapping.name()));
                Application application = applicationRepository.findOne(booleanExpression).orElse(new Application(path, getMapping.name(), RequestMethod.GET.name(), contextPath, now));
                application.setValue(path);
                application.setType(RequestMethod.GET.name());
                application.setPath(String.join(":", RequestMethod.GET.name(), String.join("/", contextPath, path)));
                application.setUpdateDatetime(now);
                applications.add(application);
                continue;
            }
            PostMapping postMapping = value.getMethodAnnotation(PostMapping.class);
            if (postMapping != null && !postMapping.name().isEmpty()) {
                QApplication qApplication = QApplication.application;
                String path = postMapping.value()[0];
                BooleanExpression booleanExpression = qApplication.contextPath.eq(contextPath).and(qApplication.name.eq(postMapping.name()));
                Application application = applicationRepository.findOne(booleanExpression).orElse(new Application(path, postMapping.name(), RequestMethod.POST.name(), contextPath, now));
                application.setValue(path);
                application.setType(RequestMethod.POST.name());
                application.setPath(String.join(":", RequestMethod.POST.name(), String.join("/", contextPath, path)));
                application.setUpdateDatetime(now);
                applications.add(application);
                continue;
            }
            PutMapping putMapping = value.getMethodAnnotation(PutMapping.class);
            if (putMapping != null && !putMapping.name().isEmpty()) {
                QApplication qApplication = QApplication.application;
                String path = putMapping.value()[0];
                BooleanExpression booleanExpression = qApplication.contextPath.eq(contextPath).and(qApplication.name.eq(putMapping.name()));
                Application application = applicationRepository.findOne(booleanExpression).orElse(new Application(path, putMapping.name(), RequestMethod.PUT.name(), contextPath, now));
                application.setValue(path);
                application.setType(RequestMethod.PUT.name());
                application.setPath(String.join(":", RequestMethod.PUT.name(), String.join("/", contextPath, path)));
                application.setUpdateDatetime(now);
                applications.add(application);
                continue;
            }
            DeleteMapping deleteMapping = value.getMethodAnnotation(DeleteMapping.class);
            if (deleteMapping != null && !deleteMapping.name().isEmpty()) {
                QApplication qApplication = QApplication.application;
                String path = deleteMapping.value()[0];
                BooleanExpression booleanExpression = qApplication.contextPath.eq(contextPath).and(qApplication.name.eq(deleteMapping.name()));
                Application application = applicationRepository.findOne(booleanExpression).orElse(new Application(path, deleteMapping.name(), RequestMethod.DELETE.name(), contextPath, now));
                application.setValue(path);
                application.setType(RequestMethod.DELETE.name());
                application.setPath(String.join(":", RequestMethod.DELETE.name(), String.join("/", contextPath, path)));
                application.setUpdateDatetime(now);
                applications.add(application);
                continue;
            }
            PatchMapping patchMapping = value.getMethodAnnotation(PatchMapping.class);
            if (patchMapping != null && !patchMapping.name().isEmpty()) {
                QApplication qApplication = QApplication.application;
                String path = patchMapping.value()[0];
                BooleanExpression booleanExpression = qApplication.contextPath.eq(contextPath).and(qApplication.name.eq(patchMapping.name()));
                Application application = applicationRepository.findOne(booleanExpression).orElse(new Application(path, patchMapping.name(), RequestMethod.PATCH.name(), contextPath, now));
                application.setValue(path);
                application.setType(RequestMethod.PATCH.name());
                application.setPath(String.join(":", RequestMethod.PATCH.name(), String.join("/", contextPath, path)));
                application.setUpdateDatetime(now);
                applications.add(application);
            }
        }
        applicationRepository.saveAll(applications);
        QApplication qApplication = QApplication.application;
        List<String> applicationsId = jpaQueryFactory.select(qApplication.id).from(qApplication).where(qApplication.updateDatetime.ne(now), qApplication.contextPath.eq(contextPath)).fetch();
        if (!applicationsId.isEmpty()) {
            applicationRepository.deleteAllById(applicationsId);
        }
    }
}
