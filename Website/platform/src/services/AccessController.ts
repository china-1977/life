import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: object, filter: any) => {
  return request<any>('/platform/accesses', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });
};