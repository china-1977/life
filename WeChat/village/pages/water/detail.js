import { wxLogin, domain, wxRequest,PayOrderstatus } from '../../utils/util.js';

Page({
  data: {
    domain,PayOrderstatus
  },
  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderWaters/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((orderWater) => {
        this.setData({
          orderWater
        });
      });
    });
  },
})