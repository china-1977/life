
Page({
  mixins: [require('../../mixin/common')],
  data: {

  },

  onLoad(options) {
    this.init();
  },

  init: function () {
    const point = wx.getStorageSync('point');
    if (point) {
      this.setData({ point });
      this.wxRequest({
        url: `${this.data.domain}/owner/houseRentings/${point.longitude}-${point.latitude}/near`,
        data: { size: this.data.size },
      }).then((data) => {
        this.setData({
          houseRentings: data.content, number: data.number
        });
      });
    } else {
      wx.authorize({
        scope: 'scope.userLocation',
        success: (res) => {
          wx.getLocation({
            type: 'gcj02',
            success: (res) => {
              wx.chooseLocation({
                latitude: res.latitude,
                longitude: res.longitude,
                success: ({ address, latitude, longitude, name }) => {
                  const point = {
                    address: address === '' ? '默认地址' : address,
                    latitude,
                    longitude,
                    name: name === '' ? '默认地址' : name
                  };
                  wx.setStorageSync('point', point);
                  this.setData({ point });
                  this.wxRequest({
                    url: `${this.data.domain}/owner/houseRentings/${point.longitude}-${point.latitude}/near`,
                    data: { size: this.data.size },
                  }).then((data) => {
                    this.setData({
                      houseRentings: data.content, number: data.number
                    });
                  });
                },
                fail: () => {
                  const point = {
                    address: '默认地址',
                    latitude: res.latitude,
                    longitude: res.longitude,
                    name: '默认地址',
                  };
                  wx.setStorageSync('point', point);
                  this.setData({ point });
                  this.wxRequest({
                    url: `${this.data.domain}/owner/houseRentings/${point.longitude}-${point.latitude}/near`,
                    data: { size: this.data.size },
                  }).then((data) => {
                    this.setData({
                      houseRentings: data.content, number: data.number
                    });
                  });
                }
              })
            }
          })
        },
        fail: (res) => {
          wx.showModal({
            title: '提示',
            content: '请允许小程序使用位置消息',
            confirmColor: '#e64340',
            showCancel: false,
            success: () => {
              wx.openSetting();
            }
          })
        }
      })
    }
  },

  chooseLocation: function () {
    wx.authorize({
      scope: 'scope.userLocation',
      success: (res) => {
        let point = this.data.point
        wx.chooseLocation({
          latitude: point.latitude,
          longitude: point.longitude,
          success: ({ address, latitude, longitude, name }) => {
            point = this.data.point
            if (!address && !name) {
              address = point.address
              name = point.name
            }
            point = { address, latitude, longitude, name };
            wx.setStorageSync('point', point);
            this.setData({ point });
            this.wxRequest({
              url: `${this.data.domain}/owner/houseRentings/${point.longitude}-${point.latitude}/near`,
              data: { size: this.data.size },
            }).then((data) => {
              this.setData({
                houseRentings: data.content, number: data.number
              });
            });
          },
          fail: (res) => {
            wx.getLocation({
              type: 'gcj02',
              success: (res) => {
                const point = {
                  address: '默认地址',
                  latitude: res.latitude,
                  longitude: res.longitude,
                  name: '默认地址',
                };
                wx.setStorageSync('point', point);
                this.setData({ point });
                this.wxRequest({
                  url: `${this.data.domain}/owner/houseRentings/${point.longitude}-${point.latitude}/near`,
                  data: { size: this.data.size },
                }).then((data) => {
                  this.setData({
                    houseRentings: data.content, number: data.number
                  });
                });
              }
            })
          }
        })
      },
      fail: (res) => {
        wx.showModal({
          title: '提示',
          content: '请允许小程序使用位置消息',
          confirmColor: '#e64340',
          showCancel: false,
          success: () => {
            wx.openSetting();
          }
        })
      }
    })
  },

  onPullDownRefresh() {
    this.init();
    wx.stopPullDownRefresh();
  },

  onReachBottom() {
    const point = this.data.point
    this.wxRequest({
      url: `${this.data.domain}/owner/houseRentings/${point.longitude}-${point.latitude}/near`,
      data: { page: this.data.number + 2, size: this.data.size }
    }).then((data) => {
      if (data.empty) {
        console.log('空');
      } else {
        let houseRentings = this.data.houseRentings;
        houseRentings.push(...data.content)
        this.setData({
          houseRentings, number: data.number
        });
      }
    });
  },
})