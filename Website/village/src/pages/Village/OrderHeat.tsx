/**
 * 采暖费列表
 */
import {houseTypeEnum, OrderStatusEnum} from '@/constants';
import {exportData} from '@/services/FileController';
import {OrderHeat} from '@/services/OrderHeatController';
import services from '@/services/api';
import {ActionType, ProTable} from '@ant-design/pro-components';
import {Button, DatePicker, Descriptions, Dropdown, message, Modal, Popconfirm, Typography, Upload} from 'antd';
import dayjs, {Dayjs} from 'dayjs';
import {useRef, useState} from 'react';

const {page, del} = services.OrderHeatController;

const OrderHeatList = () => {
    const [year, setYear] = useState<Dayjs | any>(dayjs());

    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '编号', align: 'center', dataIndex: 'code', copyable: true},
        {title: '房屋ID', align: 'center', dataIndex: 'houseId', copyable: true},
        {title: '楼号', align: 'center', dataIndex: 'floorNumber', copyable: true},
        {title: '单元', align: 'center', dataIndex: 'unit', copyable: true},
        {title: '室', align: 'center', dataIndex: 'roomNumber', copyable: true},
        {
            title: '类型',
            align: 'center',
            dataIndex: 'type',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: houseTypeEnum.columns,
        },
        {title: '面积', dataIndex: 'area', copyable: true, search: false},
        {title: '采暖费', dataIndex: 'heatTotal', copyable: true, search: false},
        {
            title: '开始日期',
            dataIndex: 'startDate',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DD'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker/>,
        },
        {
            title: '结束日期',
            dataIndex: 'endDate',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DD'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker/>,
        },
        {
            title: '状态',
            align: 'center',
            dataIndex: 'status',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: OrderStatusEnum.columns,
        },
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, orderHeat: OrderHeat) => (
                <Dropdown.Button
                    key={orderHeat.id}
                    onClick={async (res) => {
                        setOrderHeat(orderHeat);
                        setModalVisit('DETAIL');
                    }}
                    menu={{
                        items: [
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${orderHeat.roomNumber}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        del([orderHeat.id]).then((res) => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(orderHeat)}}>详情</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const [modalVisit, setModalVisit] = useState<string | boolean>(false);
    const ref = useRef<ActionType>();
    const [orderHeat, setOrderHeat] = useState<OrderHeat>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        houseId: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                params={{year: year?.year()}}
                columns={columns}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/orderHeats/export', values, '采暖费信息.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
                toolBarRender={() => [
                    <Upload
                        action={`${process.env.baseURL}/village/orderHeats/import`}
                        onChange={(e) => {
                            switch (e.file.status) {
                                case 'uploading':
                                    message.loading({content: '上传中', key: e.file.uid});
                                    break;
                                case 'error':
                                    message.error({content: '上传失败', key: e.file.uid});
                                    break;
                                case 'done':
                                    message.success({content: '上传成功', key: e.file.uid});
                                    break;
                            }
                        }}
                        showUploadList={false}
                        headers={{
                            authorization: localStorage.getItem('authorization') as string,
                            cid: localStorage.getItem('cid') as string,
                            mid: localStorage.getItem('mid') as string,
                        }}
                    >
                        <Button>导入</Button>
                    </Upload>,
                ]}
                request={(params: any, sorter, filter) => page(params, sorter, filter)}
                toolbar={{
                    filter: <DatePicker picker={'year'} defaultValue={year} onChange={(date, value) => setYear(date)}/>,
                }}
            />

            <Modal
                width={'60%'}
                footer={false}
                open={modalVisit === 'DETAIL'}
                onCancel={() => {
                    setModalVisit(false);
                }}
            >
                <Descriptions size={'small'} title="房屋信息" bordered>
                    <Descriptions.Item
                        label="房屋">{[orderHeat?.floorNumber, orderHeat?.unit, orderHeat?.roomNumber, orderHeat?.type]}</Descriptions.Item>
                    <Descriptions.Item label="面积">{orderHeat?.area}</Descriptions.Item>
                    <Descriptions.Item label="采暖费">{orderHeat?.heatTotal}</Descriptions.Item>
                    <Descriptions.Item label="编号">{orderHeat?.code}</Descriptions.Item>
                    <Descriptions.Item label="开始日期">{orderHeat?.startDate}</Descriptions.Item>
                    <Descriptions.Item label="结束日期">{orderHeat?.endDate}</Descriptions.Item>
                    <Descriptions.Item label="支付状态">{orderHeat?.status}</Descriptions.Item>
                    <Descriptions.Item label="支付时间">{orderHeat?.payDatetime}</Descriptions.Item>
                    <Descriptions.Item label="更新时间">{orderHeat?.updateDatetime}</Descriptions.Item>
                </Descriptions>
            </Modal>
        </div>
    );
};

export default OrderHeatList;
