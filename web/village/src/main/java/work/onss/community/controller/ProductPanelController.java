package work.onss.community.controller;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.QMerchant;
import work.onss.community.domain.dto.shop.ProductPanelDto;
import work.onss.community.domain.shop.*;
import work.onss.community.domain.village.ProductPanel;
import work.onss.community.domain.village.ProductPanelRepository;
import work.onss.community.domain.village.QProductPanel;
import work.onss.community.service.exception.BusinessException;

import java.util.Collection;
import java.util.List;

@Log4j2
@RestController
public class ProductPanelController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductPanelRepository productPanelRepository;

    @GetMapping(value = {"productPanels/{id}"}, name = "橱窗详情")
    public Product productPanel(@PathVariable String id, @RequestHeader(name = "mid") String mid) {
        ProductPanel productPanel = productPanelRepository.findByIdAndMerchantId(id, mid).orElseThrow(() -> new BusinessException("橱窗数据不存在"));
        return productRepository.findById(productPanel.getProductId()).orElse(null);
    }

    @GetMapping(value = {"productPanels"}, name = "橱窗分页")
    public Page<ProductPanelDto> productPanels(@RequestHeader(name = "mid") String mid,
                                               @QuerydslPredicate(bindings = ProductRepository.class, root = Product.class) Predicate predicate,
                                               @PageableDefault(sort = {"orderLabel"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QProductPanel qProductPanel = QProductPanel.productPanel;
        QProduct qProduct = QProduct.product;
        QMerchant qMerchant = QMerchant.merchant;

        List<ProductPanelDto> products = jpaQueryFactory.select(Projections.fields(ProductPanelDto.class,
                        qMerchant,
                        qProduct,
                        qProductPanel
                )).from(qProductPanel)
                .leftJoin(qProduct).on(qProductPanel.productId.eq(qProduct.id))
                .leftJoin(qMerchant).on(qMerchant.id.eq(qProduct.merchantId))
                .where(qProductPanel.merchantId.eq(mid), predicate)
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetch();
        return new PageImpl<>(products);
    }

    @PostMapping(value = {"productPanels/save"}, name = "橱窗保存")
    public ProductPanel insert(@RequestHeader(name = "mid") String mid, @Validated @RequestBody ProductPanel productPanel) {
        productPanel.setMerchantId(mid);
        if (productPanel.getId() == null) {
            productPanelRepository.save(productPanel);
        } else {
            ProductPanel oldProductPanel = productPanelRepository.findById(productPanel.getId()).orElseThrow(() -> new BusinessException("橱窗不存在"));
            if (oldProductPanel.getMerchantId().equals(mid)) {
                productPanelRepository.save(productPanel);
            }
        }
        return productPanel;
    }

    @PostMapping(value = {"productPanels/delete"}, name = "橱窗删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        productPanelRepository.deleteByIdInAndMerchantId(ids, mid);
    }
}
