package work.onss.community.domain.vo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wangchanghao
 */
@Data
@Builder
public class Work<T> implements Serializable {

    private String code;
    private String message;
    private T data;

    public Work() {
    }

    public Work(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> Work<T> success(T t) {
        Code success = Code.SUCCESS;
        return new Work<>(success.name(), success.getValue(), t);
    }

    public static <T> Work<T> success() {
        Code success = Code.SUCCESS;
        return new Work<>(success.name(), success.getValue(), null);
    }


    public static <T> Work<T> fail() {
        Code fail = Code.FAIL;
        return new Work<>(fail.name(), fail.getValue(), null);
    }

    public static <T> Work<T> fail(String message) {
        Code fail = Code.FAIL;
        return new Work<>(fail.name(), message, null);
    }
}
