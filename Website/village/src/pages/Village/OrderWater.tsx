/**
 * 物业费列表
 */
import {houseTypeEnum, OrderStatusEnum} from '@/constants';
import {exportData} from '@/services/FileController';
import {OrderWater} from '@/services/OrderWaterController';
import services from '@/services/api';
import {ActionType, ProTable} from '@ant-design/pro-components';
import {Button, DatePicker, Descriptions, Dropdown, message, Modal, Popconfirm, Typography, Upload} from 'antd';
import dayjs, {Dayjs} from 'dayjs';
import {useRef, useState} from 'react';

const {page, del, sedSubscribeMessage} = services.OrderWaterController;

const OrderWaterList = () => {
    const [year, setYear] = useState<Dayjs | any>(dayjs());

    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '编号', align: 'center', dataIndex: 'code', copyable: true},
        {title: '房屋ID', align: 'center', dataIndex: 'houseId', copyable: true},
        {title: '楼号', align: 'center', dataIndex: 'floorNumber', copyable: true},
        {title: '单元', align: 'center', dataIndex: 'unit', copyable: true},
        {title: '室', align: 'center', dataIndex: 'roomNumber', copyable: true},
        {
            title: '类型',
            align: 'center',
            dataIndex: 'type',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: houseTypeEnum.columns,
        },
        {title: '水表初始值', dataIndex: 'waterInitial', copyable: true, search: false},
        {title: '水表当前值', dataIndex: 'waterCurrent', copyable: true, search: false},
        {title: '用水量', dataIndex: 'waterCount', copyable: true, search: false},
        {title: '水损系数', dataIndex: 'waterRatio', copyable: true, search: false},
        {title: '损耗', dataIndex: 'waterLoss', copyable: true, search: false},
        {title: '水费', dataIndex: 'waterTotal', copyable: true, sorter: {multiple: 1}, onSorter: false, search: false},
        {
            title: '开始日期',
            dataIndex: 'startDate',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DD'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker/>,
        },
        {
            title: '结束日期',
            dataIndex: 'endDate',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DD'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker/>,
        },
        {
            title: '状态',
            align: 'center',
            dataIndex: 'status',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: OrderStatusEnum.columns,
        },
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, orderWater: OrderWater) => (
                <Dropdown.Button
                    key={orderWater.id}
                    onClick={async (res) => {
                        setOrderWater(orderWater);
                        setModalVisit('DETAIL');
                    }}
                    menu={{
                        items: [
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${orderWater.roomNumber}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        del([orderWater.id]).then((res) => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(orderWater)}}>详情</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const [modalVisit, setModalVisit] = useState<string | boolean>(false);
    const ref = useRef<ActionType>();
    const [orderWater, setOrderWater] = useState<OrderWater>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        houseId: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                params={{year: year?.year()}}
                columns={columns}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/orderWaters/export', values, '水费信息.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        <Button
                          key="send"
                          onClick={() => {
                              const values = searchConfig?.form?.getFieldsValue();
                              const data= {...values, year: year?.year()}
                              sedSubscribeMessage(data).then((res) => {
                                  message.success('发送成功');
                              });
                          }}
                        >
                            通知
                        </Button>,
                        ...dom.values(),
                    ],
                }}
                toolBarRender={() => [
                    <Upload
                        action={`${process.env.baseURL}/village/orderWaters/import`}
                        onChange={(e) => {
                            switch (e.file.status) {
                                case 'uploading':
                                    message.loading({content: '上传中', key: e.file.uid});
                                    break;
                                case 'error':
                                    const response = e.file.response;
                                    if (response){
                                        message.error({content: response.message, key: e.file.uid});
                                    }else {
                                        message.error({content: '上传失败', key: e.file.uid});
                                    }
                                    break;
                                case 'done':
                                    message.success({content: '上传成功', key: e.file.uid});
                                    break;
                            }
                        }}
                        showUploadList={false}
                        headers={{
                            authorization: localStorage.getItem('authorization') as string,
                            cid: localStorage.getItem('cid') as string,
                            mid: localStorage.getItem('mid') as string,
                        }}
                    >
                        <Button>导入</Button>
                    </Upload>,
                ]}
                request={(params: any, sorter, filter) => page(params, sorter, filter)}
                toolbar={{
                    filter: <DatePicker picker={'year'} defaultValue={year} onChange={(date, value) => setYear(date)}/>,
                }}
            />

            <Modal
                width={'60%'}
                footer={false}
                open={modalVisit === 'DETAIL'}
                onCancel={() => {
                    setModalVisit(false);
                }}
            >
                <Descriptions size={'small'} title={`房屋信息：${[orderWater?.floorNumber, orderWater?.unit, orderWater?.roomNumber, orderWater?.type]}`} bordered>
                    <Descriptions.Item label="编号">{orderWater?.code}</Descriptions.Item>
                    <Descriptions.Item label="用水量">{orderWater?.waterCount}立方米（{orderWater?.waterInitial} ~ {orderWater?.waterCurrent}）</Descriptions.Item>
                    <Descriptions.Item label="损耗">{orderWater?.waterLoss}立方米（系数：{orderWater?.waterRatio}）</Descriptions.Item>
                    <Descriptions.Item label="合计">{orderWater?.waterTotal}元</Descriptions.Item>
                    <Descriptions.Item label="开始日期">{orderWater?.startDate} ~ {orderWater?.endDate}</Descriptions.Item>
                    <Descriptions.Item label="状态">{OrderStatusEnum.ProFormSelect[orderWater?.status]}</Descriptions.Item>
                    <Descriptions.Item label="支付时间">{orderWater?.payDatetime}</Descriptions.Item>
                </Descriptions>
            </Modal>
        </div>
    );
};

export default OrderWaterList;
