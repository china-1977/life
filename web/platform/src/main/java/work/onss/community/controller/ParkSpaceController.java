package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.village.ParkSpace;
import work.onss.community.domain.village.ParkSpaceRepository;
import work.onss.community.domain.village.QHouse;
import work.onss.community.domain.village.QParkSpace;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class ParkSpaceController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private ParkSpaceRepository parkSpaceRepository;

    @GetMapping(value = {"parkSpaces"}, name = "车位列表")
    public Page<ParkSpace> page(@QuerydslPredicate(bindings = ParkSpaceRepository.class) Predicate predicate, @PageableDefault Pageable pageable) {
        return parkSpaceRepository.findAll(predicate, pageable);
    }

    @PostMapping(value = {"parkSpaces/export"}, name = "车位导出")
    public void export(@QuerydslPredicate(bindings = ParkSpaceRepository.class, root = ParkSpace.class) Predicate predicate, HttpServletResponse httpServletResponse) throws Exception {
        QHouse qHouse = QHouse.house;
        QParkSpace qParkSpace = QParkSpace.parkSpace;
        List<ParkSpace> data = jpaQueryFactory.select(Projections.constructor(ParkSpace.class, qParkSpace, qHouse.floorNumber))
                .from(qParkSpace)
                .leftJoin(qHouse).on(qHouse.id.eq(qParkSpace.houseId))
                .where(predicate)
                .fetch();
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), ParkSpace.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<ParkSpace>> floorNumberData = data.stream().collect(Collectors.groupingBy(ParkSpace::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}
