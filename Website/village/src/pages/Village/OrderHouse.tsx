/**
 * 物业费列表
 */
import {houseTypeEnum, OrderStatusEnum} from '@/constants';
import {exportData} from '@/services/FileController';
import {OrderHouse} from '@/services/OrderHouseController';
import services from '@/services/api';
import {ActionType, ProTable} from '@ant-design/pro-components';
import {Button, DatePicker, Descriptions, Dropdown, message, Modal, Popconfirm, Typography, Upload} from 'antd';
import dayjs, {Dayjs} from 'dayjs';
import {useRef, useState} from 'react';

const {page, del, sedSubscribeMessage} = services.OrderHouseController;

const OrderHouseList = () => {
    const [year, setYear] = useState<Dayjs | any>(dayjs());

    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '编号', align: 'center', dataIndex: 'code', copyable: true},
        {title: '房屋ID', align: 'center', dataIndex: 'houseId', copyable: true},
        {title: '楼号', align: 'center', dataIndex: 'floorNumber', copyable: true},
        {title: '单元', align: 'center', dataIndex: 'unit', copyable: true},
        {title: '室', align: 'center', dataIndex: 'roomNumber', copyable: true},
        {
            title: '类型',
            align: 'center',
            dataIndex: 'type',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: houseTypeEnum.columns,
        },
        {title: '物业费', dataIndex: 'propertyTotal', copyable: true, search: false},
        {title: '垃圾费', dataIndex: 'rubbishPrice', copyable: true, search: false},
        {title: '车位数量', dataIndex: 'parkSpaceCount', copyable: true, search: false},
        {title: '车位费', dataIndex: 'parkSpaceTotal', copyable: true, search: false},
        {title: '合计', dataIndex: 'total', copyable: true, search: false},
        {
            title: '开始日期',
            dataIndex: 'startDate',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DD'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker/>,
        },
        {
            title: '结束日期',
            dataIndex: 'endDate',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DD'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker/>,
        },
        {
            title: '状态',
            align: 'center',
            dataIndex: 'status',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: OrderStatusEnum.columns,
        },
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, orderHouse: OrderHouse) => (
                <Dropdown.Button
                    key={orderHouse.id}
                    onClick={async (res) => {
                        setOrderHouse(orderHouse);
                        setModalVisit('DETAIL');
                    }}
                    menu={{
                        items: [
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${orderHouse.roomNumber}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        del([orderHouse.id]).then((res) => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(orderHouse)}}>详情</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const [modalVisit, setModalVisit] = useState<string | boolean>(false);
    const ref = useRef<ActionType>();
    const [orderHouse, setOrderHouse] = useState<OrderHouse>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        houseId: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                params={{year: year?.year()}}
                columns={columns}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/orderHouses/export', {...values, year: year?.year()}, '物业费.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        <Button
                          key="send"
                          onClick={() => {
                              const values = searchConfig?.form?.getFieldsValue();
                              const data= {...values, year: year?.year()}
                              sedSubscribeMessage(data).then((res) => {
                                  message.success('发送成功');
                              });
                          }}
                        >
                            通知
                        </Button>,
                        ...dom.values(),
                    ],
                }}
                toolBarRender={() => [
                    <Upload
                        action={`${process.env.baseURL}/village/orderHouses/import`}
                        onChange={(e) => {
                            switch (e.file.status) {
                                case 'uploading':
                                    message.loading({content: '上传中', key: e.file.uid});
                                    break;
                                case 'error':
                                    const response = e.file.response;
                                    if (response){
                                        message.error({content: response.message, key: e.file.uid});
                                    }else {
                                        message.error({content: '上传失败', key: e.file.uid});
                                    }
                                    break;
                                case 'done':
                                    message.success({content: '上传成功', key: e.file.uid});
                                    break;
                            }
                        }}
                        showUploadList={false}
                        headers={{
                            authorization: localStorage.getItem('authorization') as string,
                            cid: localStorage.getItem('cid') as string,
                            mid: localStorage.getItem('mid') as string,
                        }}
                    >
                        <Button>导入</Button>
                    </Upload>,
                ]}
                request={(params: any, sorter, filter) => page(params, sorter, filter)}
                toolbar={{
                    filter: <DatePicker picker={'year'} defaultValue={year} onChange={(date, value) => setYear(date)}/>,
                }}
            />

            <Modal
                width={'60%'}
                footer={false}
                open={modalVisit === 'DETAIL'}
                onCancel={() => {
                    setModalVisit(false);
                }}
            >
                <Descriptions size={'small'} title={`房屋信息：${[orderHouse?.floorNumber, orderHouse?.unit, orderHouse?.roomNumber, orderHouse?.type]}`} bordered>
                    <Descriptions.Item label="编号">{orderHouse?.code}</Descriptions.Item>
                    <Descriptions.Item label="物业费">{orderHouse?.propertyTotal}元/月</Descriptions.Item>
                    <Descriptions.Item label="垃圾费">{orderHouse?.rubbishPrice}元/月</Descriptions.Item>
                    <Descriptions.Item label="车位管理费">{`${orderHouse?.parkSpaceTotal}元/月（${orderHouse?.parkSpaceCount}个车位）`}</Descriptions.Item>
                    <Descriptions.Item label="开始日期">{orderHouse?.startDate} ~ {orderHouse?.endDate}（{orderHouse?.monthCount}个月）</Descriptions.Item>
                    <Descriptions.Item label="合计">{orderHouse?.total}元</Descriptions.Item>
                    <Descriptions.Item label="状态">{OrderStatusEnum.ProFormSelect[orderHouse?.status]}</Descriptions.Item>
                    <Descriptions.Item label="支付时间">{orderHouse?.payDatetime}</Descriptions.Item>
                </Descriptions>
            </Modal>
        </div>
    );
};

export default OrderHouseList;
