package work.onss.community.domain.converters;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import work.onss.community.domain.device.ParkGate;

public class ParkGateTypeConverter implements Converter<ParkGate.Type> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return ParkGate.Type.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public ParkGate.Type convertToJavaData(ReadConverterContext<?> context) {
        for (ParkGate.Type type : ParkGate.Type.values()) {
            if (type.getValue().equals(context.getReadCellData().getStringValue())) {
                return type;
            }
        }
        return null;
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<ParkGate.Type> context) {
        return new WriteCellData<>(context.getValue().getValue());
    }

}
