package work.onss.community.domain.village.relational;

import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.usertype.StringArrayType;

import java.io.Serializable;
import java.util.List;

@Log4j2
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@QueryEntity
@DynamicInsert
@DynamicUpdate
public class MessageCustomer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String merchantId;
    private String messageTemplateId;
    @Type(value = StringArrayType.class)
    private String[] openids;
    @Transient
    private List<Customer> customers;

    public MessageCustomer(String merchantId, String messageTemplateId) {
        this.merchantId = merchantId;
        this.messageTemplateId = messageTemplateId;
    }
}