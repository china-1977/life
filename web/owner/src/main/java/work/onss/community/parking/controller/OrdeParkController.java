package work.onss.community.parking.controller;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.MerchantRepository;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.park.OrderPark;
import work.onss.community.domain.park.OrderParkRepository;
import work.onss.community.domain.park.ParkRegion;
import work.onss.community.domain.park.ParkRegionRepository;
import work.onss.community.domain.village.PayOrder;
import work.onss.community.domain.village.PayOrderRepository;
import work.onss.community.domain.wechat.*;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.config.CollectionRunner;
import work.onss.community.service.exception.BusinessException;
import work.onss.community.service.utils.Utils;
import work.onss.community.service.wechat.MiniAppService;
import work.onss.community.service.wechat.WechatMpProperties;
import work.onss.community.service.wechat.WechatPayService;

import java.math.BigDecimal;
import java.net.URI;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Log4j2
@RestController
public class OrdeParkController {
    @Autowired
    private OrderParkRepository orderParkRepository;
    @Autowired
    private ParkRegionRepository parkRegionRepository;
    @Autowired
    private WechatMpProperties wechatMpProperties;
    @Autowired
    private WechatPayService wechatPayService;
    @Autowired
    private QuerydslService querydslService;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private PayOrderRepository payOrderRepository;
    @Autowired
    private MiniAppService miniAppService;

    @GetMapping(value = {"orderParks/detail"})
    public Map<String, Object> detail(@RequestParam(required = false) String id,
                                      @RequestParam(required = false) String carNumber) {
        OrderPark orderPark;
        if (StringUtils.hasLength(id)) {
            orderPark = orderParkRepository.findById(id).orElseThrow(() -> new BusinessException("无在场记录"));
        } else {
            List<OrderPark> orderParks = orderParkRepository.findAllByCarNumberAndParkStatus(carNumber, OrderPark.Status.I);
            if (orderParks.isEmpty()) {
                throw new BusinessException("无在场记录");
            } else if (orderParks.size() == 1) {
                orderPark = orderParks.getFirst();
            } else {
                return Map.of("orderParks", orderParks);
            }
        }
        // 查询主体信息
        Merchant merchant = merchantRepository.findById(orderPark.getMerchantId()).orElseThrow(() -> new BusinessException("缺少主体信息"));
        // 查询计费规则
        ParkRegion parkRegion = parkRegionRepository.findById(orderPark.getParkRegionId()).orElseThrow(() -> new BusinessException("缺少计费公式"));
        // 计算停车费用
        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(orderPark.getInDatetime(), now);
        long minutes = duration.toMinutes();
        // 计算应付金额
        BigDecimal moneyMeet = BigDecimal.ZERO;
        if (minutes > parkRegion.getFreeTime()) {
            // 停车时长 > 减免时长时，执行计费函数
            moneyMeet = (BigDecimal) CollectionRunner.invoke(parkRegion.getFormula(), "apply", duration, parkRegion.getPrice());
        }
        // 剩余应缴金额 = 应付金额 - 实付金额 - 减免金额
        BigDecimal moneyOwe = moneyMeet.subtract(orderPark.getMoneyPayment()).subtract(orderPark.getMoneyReduction());

        if (moneyOwe.compareTo(BigDecimal.ZERO) <= 0) {
            orderPark.setMoneyOwe(BigDecimal.ZERO);
        } else {
            orderPark.setMoneyOwe(moneyOwe);
        }
        orderPark.setMoneyMeet(moneyMeet);
        orderPark.setOutDatetime(now);
        orderParkRepository.save(orderPark);
        return Map.of("orderPark", orderPark, "merchant", merchant, "parkRegion", parkRegion);
    }

    /**
     * 车牌识别
     * 道闸通过返回的订单详情，判断是否放行
     *
     * @param merchantId   主体ID
     * @param parkRegionId 区域ID
     * @param deviceNumber 设备编号
     * @param carNumber    车牌号
     * @return 订单详情
     */
    @GetMapping(value = {"orderParks/cameraRecognition"})
    public OrderPark cameraRecognition(@RequestParam String merchantId,
                                       @RequestParam String parkRegionId,
                                       @RequestParam String deviceNumber,
                                       @RequestParam String carNumber) {
        // 查询入场记录
        Optional<OrderPark> orderParkOptional = orderParkRepository.findAllByMerchantIdAndParkRegionIdAndCarNumberAndParkStatus(merchantId, parkRegionId, carNumber, OrderPark.Status.I);
        if (orderParkOptional.isEmpty()) {
            log.error("无在场记录:{},{},{},{}", merchantId, parkRegionId, deviceNumber, carNumber);
            return null;
        }
        // 查询计费规则
        ParkRegion parkRegion = parkRegionRepository.findById(parkRegionId).orElseThrow(() -> new BusinessException("缺少计费公式"));
        // 计算停车费用
        OrderPark orderPark = orderParkOptional.get();
        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(orderPark.getInDatetime(), now);
        long minutes = duration.toMinutes();
        // 计算应付金额
        BigDecimal moneyMeet = BigDecimal.ZERO;
        if (minutes > parkRegion.getFreeTime()) {
            // 停车时长 > 减免时长时，执行计费函数
            moneyMeet = (BigDecimal) CollectionRunner.invoke(parkRegion.getFormula(), "apply", duration, parkRegion.getPrice());
        }
        // 剩余应缴金额 = 应付金额 - 实付金额 - 减免金额
        BigDecimal moneyOwe = moneyMeet.subtract(orderPark.getMoneyPayment()).subtract(orderPark.getMoneyReduction());

        if (moneyOwe.compareTo(BigDecimal.ZERO) <= 0) {
            orderPark.setMoneyOwe(BigDecimal.ZERO);
        } else {
            orderPark.setMoneyOwe(moneyOwe);
        }
        orderPark.setMoneyMeet(moneyMeet);
        orderPark.setOutDatetime(now);
        orderPark.setOutDeviceNumber(deviceNumber);
        orderParkRepository.save(orderPark);

        return orderPark;
    }

    @PostMapping(value = {"orderParks/{id}/pay"})
    public MiniPayToken transaction(@RequestHeader(name = "cid") String cid, @PathVariable String id, @RequestBody WXLogin wxLogin) {
        // TODO 加锁 订单ID：MiniPayToken

        OrderPark orderPark = orderParkRepository.findById(id).orElseThrow(() -> new BusinessException("停车订单不存在"));
        Merchant merchant = merchantRepository.findById(orderPark.getMerchantId()).orElseThrow(() -> new BusinessException("商户不存在"));

        ParkRegion parkRegion = parkRegionRepository.findById(orderPark.getParkRegionId()).orElseThrow(() -> new BusinessException("缺少计费公式"));
        PayOrder.Detail detail = new PayOrder.Detail();
        querydslService.getTotal(detail, orderPark, parkRegion);
        orderParkRepository.save(orderPark);

        if (detail.getTotal().compareTo(BigDecimal.ZERO) <= 0) {
            throw new BusinessException("金额等于零,无需支付");
        }

        WXMerchantScore.Amount amount = WXMerchantScore.Amount.builder()
                .currency("CNY")
                .total(detail.getTotal().movePointRight(2).intValue()).build();
        WechatMpProperties.AppConfig appConfig = wechatMpProperties.getAppConfigs().get(wxLogin.getAppid());
        String jscode2Session = miniAppService.jscode2session(wxLogin.getAppid(), appConfig.getSecret(), wxLogin.getCode(), "authorization_code");
        Jscode2SessionResult jscode2SessionResult = JacksonUtils.readValue(jscode2Session, Jscode2SessionResult.class);
        WXMerchantScore.Payer payer = WXMerchantScore.Payer.builder()
                .spOpenid(jscode2SessionResult.getOpenid()).build();

        LocalDateTime now = LocalDateTime.now();
        String nowStr = now.format(DateTimeFormatter.ofPattern("yyMMddHHmmssSS"));
        int i = RandomUtils.nextInt(9999);
        String outTradeNo = String.format("%s%04d", nowStr, i);

        WXMerchantScore wxMerchantScore = WXMerchantScore.builder()
                .spAppid(wechatMpProperties.getAppid())
                .spMchid(wechatMpProperties.getMchId())
                .subMchid(merchant.getSubMchId())
                .description(detail.getDescription())
                .outTradeNo(outTradeNo)
                .timeExpire(now.plusMinutes(110).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss+08:00")))
                .notifyUrl("https://1977.work/owner/order/notify")
                .amount(amount)
                .payer(payer).build();

        String json = JacksonUtils.writeValueAsString(wxMerchantScore);
        URI uri = URI.create("https://api.mch.weixin.qq.com/v3/pay/partner/transactions/jsapi");
        String nonceStr = UUID.randomUUID().toString().replace("-", "");
        String timestamp = String.valueOf(now.toEpochSecond(ZoneOffset.ofHours(8)));
        String signature = Utils.sign(wechatMpProperties.getPrivateKey(), "POST", uri.getPath(), timestamp, nonceStr, json);
        WxPayToken wxPayToken = WxPayToken.builder()
                .serialNo(wechatMpProperties.getCertSerialNo())
                .mchid(wechatMpProperties.getMchId())
                .signature(signature)
                .nonceStr(nonceStr)
                .timestamp(timestamp)
                .build();

        String authorization = Utils.getWxPayToken(wxPayToken);

        WXMerchantScore.Result result = wechatPayService.jsapi(json, authorization);
        if (result.getPrepayId() == null) {
            throw new BusinessException("微信支付异常");
        }

        String paySign = Utils.sign(wechatMpProperties.getPrivateKey(), timestamp, nonceStr, result.getPrepayId());
        MiniPayToken miniPayToken = new MiniPayToken(timestamp, nonceStr, result.getPrepayId(), "RSA", paySign);

        PayOrder payOrder = new PayOrder(
                merchant.getId(),
                merchant.getSubMchId(),
                merchant.getShortname(),
                cid,
                detail.getDescription(),
                new String[]{orderPark.getId()},
                Collections.singletonList(detail),
                detail.getTotal(),
                PayOrder.PayStatus.NOTPAY,
                result.getPrepayId(),
                outTradeNo
        );
        payOrderRepository.save(payOrder);

        return miniPayToken;
    }
}