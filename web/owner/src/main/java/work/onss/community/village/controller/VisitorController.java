package work.onss.community.village.controller;

import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.MerchantRepository;
import work.onss.community.domain.village.*;
import work.onss.community.service.exception.BusinessException;

import java.time.LocalDateTime;

@Log4j2
@RestController
public class VisitorController {
    @Autowired
    private VisitorRepository visitorRepository;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private HouseRepository houseRepository;

    @GetMapping(value = {"visitors/{id}"})
    public Visitor detail(@PathVariable String id, @RequestHeader("cid") String cid) {
        QVisitor qVisitor = QVisitor.visitor;
        return visitorRepository.findOne(qVisitor.id.eq(id).and(qVisitor.customerId.eq(cid))).orElse(null);
    }

    @GetMapping(value = {"visitors"})
    public Page<Visitor> page(@RequestHeader("cid") String cid, @PageableDefault(sort = {"visitDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QVisitor qVisitor = QVisitor.visitor;
        return visitorRepository.findAll(qVisitor.customerId.eq(cid).and(qVisitor.leaveDatetime.gt(LocalDateTime.now())), pageable);
    }

    @Transactional
    @PostMapping(value = {"visitors"})
    public void save(@RequestHeader("cid") String cid, @RequestBody Visitor visitor) {
        visitor.setCustomerId(cid);
        LocalDateTime now = LocalDateTime.now();
        if (visitor.getId() == null) {
            Merchant merchant = merchantRepository.findById(visitor.getMerchantId()).orElseThrow(() -> new BusinessException("主体信息不存在"));
            House house = houseRepository.findById(visitor.getHouseId()).orElseThrow(() -> new BusinessException("房屋信息不存在"));
            visitor.setShortname(merchant.getShortname());
            visitor.setHouseId(house.getId());
            visitor.setFloorNumber(house.getFloorNumber());
            visitor.setUnit(house.getUnit());
            visitor.setRoomNumber(house.getRoomNumber());
            visitor.setApplicationTime(now);
            visitorRepository.save(visitor);
        } else {
            QVisitor qVisitor = QVisitor.visitor;
            Visitor oldVisitor = visitorRepository.findOne(qVisitor.id.eq(visitor.getId()).and(qVisitor.customerId.eq(cid))).orElseThrow(() -> new BusinessException("访客记录不存在"));
            oldVisitor.setName(visitor.getName());
            oldVisitor.setIdCard(visitor.getIdCard());
            oldVisitor.setPhone(visitor.getPhone());
            oldVisitor.setCarCode(visitor.getCarCode());
            oldVisitor.setVisitDatetime(visitor.getVisitDatetime());
            oldVisitor.setLeaveDatetime(visitor.getLeaveDatetime());
            oldVisitor.setRemarks(visitor.getRemarks());
            visitorRepository.save(oldVisitor);
        }
    }

    @DeleteMapping(value = {"visitors/{id}/delete"})
    public void delete(@RequestHeader(name = "cid") String cid, @PathVariable String id) {
        visitorRepository.deleteByIdAndCustomerId(id, cid);
    }
}