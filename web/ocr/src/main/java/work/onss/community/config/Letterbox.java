package work.onss.community.config;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class Letterbox {

    public static Mat letterbox(Mat im, Size newShape, Boolean scaleUp, Boolean auto, Integer stride, Scalar color) { // 调整图像大小和填充图像，使满足步长约束，并记录参数
        Mat resizeDst = new Mat();
        int[] shape = {im.rows(), im.cols()}; // 当前形状 [height, width]
        // Scale ratio (new / old)
        double r = Math.min(newShape.height / shape[0], newShape.width / shape[1]);
        if (!scaleUp) { // 仅缩小，不扩大（一起为了mAP）
            r = Math.min(r, 1.0);
        }
        // Compute padding
        Size newUnpad = new Size(Math.round(shape[1] * r), Math.round(shape[0] * r));
        double dw = newShape.width - newUnpad.width;
        double dh = newShape.height - newUnpad.height; // wh 填充
        if (auto) { // 最小矩形
            dw = dw % stride;
            dh = dh % stride;
        }
        dw /= 2; // 填充的时候两边都填充一半，使图像居于中心
        dh /= 2;
        if (shape[1] != newUnpad.width || shape[0] != newUnpad.height) { // resize
            Imgproc.resize(im, resizeDst, newUnpad, 0, 0, Imgproc.INTER_LINEAR);
        }
        // 将图像填充为正方形
        Core.copyMakeBorder(im, resizeDst, (int) dh, (int) dh, (int) dw, (int) dw, Core.BORDER_CONSTANT, color);
        return resizeDst;
    }
}

