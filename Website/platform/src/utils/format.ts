import { history } from '@@/exports';
import { message, notification } from 'antd';
import Qs from 'qs';

export const sort = (sorter: object) => {
  let sort: string[] = [];
  if (sorter) {
    const sorters = Object.entries(sorter);
    for (let sorter of sorters) {
      if (sorter[1] == 'ascend') {
        sorter[1] = 'asc';
      } else {
        sorter[1] = 'desc';
      }
      sort.push(sorter.toString());
    }
  }
  return sort;
};

export const replacer = (key: string, value: any) => {
  if (value) {
    return value;
  }
};

export const paramsSerializer = (params: any) => {
  if (params?.current) {
    params.page = params.current;
  }
  if (params?.pageSize) {
    params.size = params.pageSize;
  }
  return Qs.stringify(params, {
    arrayFormat: 'repeat',
  });
};

export const requestInterceptor1 = (url: string, options: any) => {
  const infoStr: any = localStorage.getItem('info');
  const authorization = localStorage.getItem('authorization');
  const info = JSON.parse(infoStr);
  if (info?.cid) {
    options.headers.cid = info.cid;
  }
  if (info?.mid) {
    options.headers.mid = info.mid;
  }
  if (authorization) {
    options.headers.authorization = authorization;
  }
  return { url, options };
};

export const requestInterceptor2 = (error: Error) => {
  console.log(error);
  message.error('全局异常处理');
  return Promise.reject(error);
};

export const errorHandler = (error: any, opts: any) => {
  console.log(error);
  if (opts?.skipErrorHandler) throw error;
  if (error.name === 'BizError') {
    const errorInfo: ResponseStructure | undefined = error.info;
    if (errorInfo) {
      const { errorMessage, errorCode } = errorInfo;
      switch (errorInfo.showType) {
        case ErrorShowType.SILENT:
          // do nothing
          break;
        case ErrorShowType.WARN_MESSAGE:
          message.warning(errorMessage);
          break;
        case ErrorShowType.ERROR_MESSAGE:
          message.error(errorMessage);
          break;
        case ErrorShowType.NOTIFICATION:
          notification.open({
            description: errorMessage,
            message: errorCode,
          });
          break;
        case ErrorShowType.REDIRECT:
          // TODO: redirect
          break;
        default:
          message.error(errorMessage);
      }
    }
  } else if (error.response) {
    if (error.response.data?.code) {
      switch (error.response.data.code) {
        case 'SESSION_EXPIRE':
          localStorage.removeItem('info');
          localStorage.removeItem('authorization');
          history.push('/login');
          break;
        default:
          message.error(error.response.data.message);
          break;
      }
    }
  } else if (error.request) {
    message.error('None response! Please retry.');
  } else {
    message.error('Request error, please retry.');
  }
};

export const logout = () => {
  localStorage.clear();
  history.push('/login');
};

enum ErrorShowType {
  SILENT = 0,
  WARN_MESSAGE = 1,
  ERROR_MESSAGE = 2,
  NOTIFICATION = 3,
  REDIRECT = 9,
}

interface ResponseStructure {
  success: boolean;
  data: any;
  errorCode?: number;
  errorMessage?: string;
  showType?: ErrorShowType;
}
