Page({
  mixins: [require('../../mixin/common')],
  data: {
    houses: [],
    currentID: -1,
    buttons: [
      {
        type: "warn",
        text: '解除',
      }
    ],
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/houses`,
        header: { authorization },
      }).then((houses) => {
        console.log(houses);
        this.setData({
          houses
        })
      });
    });
  },

  bindButtonTap: function (e) {
    const index = e.currentTarget.id;
    const house = this.data.houses[index];
    if (e.detail.index === 0) {
      wx.showModal({
        title: '警示',
        content: '是否真的解除？',
        success: (res) => {
          if (res.confirm) {
            this.wxLogin().then(({ authorization }) => {
              this.wxRequest({
                url: `${this.data.domain}/owner/houses/unbinding`,
                method: 'POST',
                header: { authorization },
                data: [house.id]
              }).then((data) => {
                this.data.houses.splice(index, 1)
                this.setData({
                  houses: this.data.houses
                })
              })
            })
          }
        }
      })
    }
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/houses`,
        header: { authorization },
      }).then((houses) => {
        wx.stopPullDownRefresh();
        this.setData({
          houses
        });
      });
    });
  },
})