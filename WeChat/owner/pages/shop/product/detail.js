Page({
  mixins: [require('../../../mixin/common')],
  data: {
   count: 1
  },
  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      const merchantId = wx.getStorageSync('merchantId');
      this.wxRequest({
        url: `${this.data.domain}/owner/products/${options.id}`,
        header: { authorization },
        data: { merchantId }
      }).then((product) => {
        this.setData({
          product
        });
      })
    })
  },
})
