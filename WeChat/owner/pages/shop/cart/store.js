Page({
  mixins: [require('../../../mixin/common')],
  data: {
  },
  onShow: function (options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/carts/getStores`,
        header: { authorization }
      }).then((stores) => {
        this.setData({
          stores
        })
      })
    })
  },
})