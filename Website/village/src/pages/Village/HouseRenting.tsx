/**
 * 房屋租售列表
 */
import services from '@/services/api';
import {
    ActionType,

    ProTable
} from '@ant-design/pro-components';
import {
    Avatar,
    Button, DatePicker, Image,

} from 'antd';
import {useRef} from 'react';

const {page} = services.HouseRentingController;
const {exportData} = services.FileController;

const HouseRentingList = () => {
    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {
            title: '楼号',
            align: 'center',
            dataIndex: 'floorNumber',
            copyable: true,
            onSorter: false,
            sorter: {multiple: 1},
        },
        {title: '单元', align: 'center', dataIndex: 'unit', copyable: true, sorter: {multiple: 1}, onSorter: false},
        {
            title: '室',
            align: 'center',
            dataIndex: 'roomNumber',
            copyable: true,
            onSorter: false,
            sorter: {multiple: 1},
        },
        {
            title: '方式',
            align: 'center',
            dataIndex: 'mode',
            copyable: true,
        },
        {
            title: '户型',
            align: 'center',
            dataIndex: 'type',
            copyable: true,
        },
        {
            title: '价格',
            align: 'center',
            dataIndex: 'price',
            copyable: true,
        },
        {
            title: '姓名',
            align: 'center',
            dataIndex: 'name',
            copyable: true,
        },
        {
            title: '电话',
            align: 'center',
            dataIndex: 'phone',
            copyable: true,
        },
        {
            title: '标题',
            align: 'center',
            dataIndex: 'title',
            copyable: true,
        },
        {
            title: '图片', align: 'center', search: false,
            render: (text: object, record: { descriptionPictures:[string] }) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={record?.descriptionPictures?.map((value, index) => (
                                <Image src={value}/>
                            ))}
                        />
                    }/>
            )
        },
        {
            title: '发布时间', align: 'center', dataIndex: 'insertDatetime', copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
    ];

    const ref = useRef<ActionType>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params, sorter, filter) => page(params, sorter, filter)}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/houseRentings/export', values, '房屋租赁.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
            />
        </div>
    );
};

export default HouseRentingList;
