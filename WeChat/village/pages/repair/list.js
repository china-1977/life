import { wxLogin, domain, wxRequest, RepairStatus, size } from '../../utils/util.js';
Page({

  data: {
    RepairStatus, status: 'WAIT_ALLOT'
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      const status = this.data.status;
      wxRequest({
        url: `${domain}/village/repairs`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { status, size }
      }).then((data) => {
        this.setData({
          repairs: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      const status = this.data.status;
      wxRequest({
        url: `${domain}/village/repairs`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { status, size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          repairs: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      const status = this.data.status;
      wxRequest({
        url: `${domain}/village/repairs`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { page: this.data.number + 2, status, size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let repairs = this.data.repairs;
          repairs.push(...data.content)
          this.setData({
            repairs, number: data.number
          });
        }
      });
    });
  },

  itemTap: function (e) {
    this.setData({
      status: e.currentTarget.id
    })

    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/repairs`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { status: e.currentTarget.id, size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          repairs: data.content, number: data.number
        });
      });
    });
  }
})