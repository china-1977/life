package work.onss.community.domain.village;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.util.Collection;

public interface HouseRepository extends JpaRepository<House, String>, JpaSpecificationExecutor<House>, QuerydslPredicateExecutor<House>, QuerydslBinderCustomizer<QHouse> {

    default void customize(QuerydslBindings bindings, QHouse qHouse) {
        bindings.bind(qHouse.id).withDefaultBinding();
    }

    @Modifying
    @Transactional
    void deleteByMerchantIdAndIdIn(String vid, Collection<String> id);
}
