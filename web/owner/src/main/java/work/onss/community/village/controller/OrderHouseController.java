package work.onss.community.village.controller;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.MerchantRepository;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.village.*;
import work.onss.community.domain.village.relational.QHouseCustomer;
import work.onss.community.domain.wechat.*;
import work.onss.community.service.exception.BusinessException;
import work.onss.community.service.utils.Utils;
import work.onss.community.service.wechat.MiniAppService;
import work.onss.community.service.wechat.WechatMpProperties;
import work.onss.community.service.wechat.WechatPayService;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
public class OrderHouseController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private OrderHouseRepository orderHouseRepository;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private PayOrderRepository payOrderRepository;
    @Autowired
    private WechatMpProperties wechatMpProperties;
    @Autowired
    private WechatPayService wechatPayService;
    @Autowired
    private MiniAppService miniAppService;

    @GetMapping(value = {"orderHouses/{id}"})
    public OrderHouse detail(@RequestHeader(name = "cid") String cid,
                             @PathVariable String id) {
        OrderHouse orderHouse = orderHouseRepository.findById(id).orElse(null);
        if (orderHouse == null) {
            return null;
        }
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        List<String> houseIds = jpaQueryFactory.select(qHouseCustomer.houseId).from(qHouseCustomer).where(qHouseCustomer.customerId.eq(cid)).fetch();
        if (houseIds.contains(orderHouse.getHouseId())) {
            return orderHouse;
        } else {
            throw new BusinessException("权限不足");
        }
    }

    @GetMapping(value = {"orderHouses"})
    public Page<OrderHouse> page(@RequestHeader(name = "cid") String cid,
                                 @RequestParam Integer year,
                                 @QuerydslPredicate(bindings = OrderHouseRepository.class) Predicate predicate,
                                 @PageableDefault(sort = {"startDate"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        List<String> houseIds = jpaQueryFactory.select(qHouseCustomer.houseId).from(qHouseCustomer).where(qHouseCustomer.customerId.eq(cid)).fetch();
        QOrderHouse qOrderHouse = QOrderHouse.orderHouse;
        return orderHouseRepository.findAll(qOrderHouse.houseId.in(houseIds).and(qOrderHouse.status.ne(PayOrder.Status.WAIT_CONFIRM)).and(qOrderHouse.startDate.year().eq(year)).and(predicate), pageable);
    }


    @PostMapping(value = {"orderHouses/{id}/pay"})
    public MiniPayToken transaction(@RequestHeader(name = "cid") String cid, @PathVariable String id, @RequestBody WXLogin wxLogin) {
        // TODO 加锁 订单ID：MiniPayToken

        OrderHouse orderHouse = orderHouseRepository.findById(id).orElseThrow(() -> new BusinessException("物业费订单不存在"));
        Merchant merchant = merchantRepository.findById(orderHouse.getMerchantId()).orElseThrow(() -> new BusinessException("商户不存在"));

        int total = orderHouse.getTotal().movePointRight(2).intValue();
        WXMerchantScore.Amount amount = WXMerchantScore.Amount.builder()
                .currency("CNY")
                .total(total).build();
        WechatMpProperties.AppConfig appConfig = wechatMpProperties.getAppConfigs().get(wxLogin.getAppid());
        String jscode2Session = miniAppService.jscode2session(wxLogin.getAppid(), appConfig.getSecret(), wxLogin.getCode(), "authorization_code");
        Jscode2SessionResult jscode2SessionResult = JacksonUtils.readValue(jscode2Session, Jscode2SessionResult.class);
        WXMerchantScore.Payer payer = WXMerchantScore.Payer.builder()
                .spOpenid(jscode2SessionResult.getOpenid()).build();

        String description = String.join(",", "物业费", orderHouse.getFloorNumber(), orderHouse.getUnit(), orderHouse.getRoomNumber());

        LocalDateTime now = LocalDateTime.now();
        String nowStr = now.format(DateTimeFormatter.ofPattern("yyMMddHHmmssSS"));
        int i = RandomUtils.nextInt(9999);
        String outTradeNo = String.format("%s%04d", nowStr, i);

        WXMerchantScore wxMerchantScore = WXMerchantScore.builder()
                .spAppid(wechatMpProperties.getAppid())
                .spMchid(wechatMpProperties.getMchId())
                .subMchid(merchant.getSubMchId())
                .description(description)
                .outTradeNo(outTradeNo)
                .timeExpire(now.plusMinutes(110).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss+08:00")))
                .notifyUrl("https://1977.work/owner/order/notify")
                .amount(amount)
                .payer(payer).build();

        String json = JacksonUtils.writeValueAsString(wxMerchantScore);

        URI uri = URI.create("https://api.mch.weixin.qq.com/v3/pay/partner/transactions/jsapi");
        String nonceStr = UUID.randomUUID().toString().replace("-", "");
        String timestamp = String.valueOf(now.toEpochSecond(ZoneOffset.ofHours(8)));
        String signature = Utils.sign(wechatMpProperties.getPrivateKey(), "POST", uri.getPath(), timestamp, nonceStr, json);
        WxPayToken wxPayToken = WxPayToken.builder()
                .serialNo(wechatMpProperties.getCertSerialNo())
                .mchid(wechatMpProperties.getMchId())
                .signature(signature)
                .nonceStr(nonceStr)
                .timestamp(timestamp)
                .build();

        String authorization = Utils.getWxPayToken(wxPayToken);

        WXMerchantScore.Result result = wechatPayService.jsapi(json, authorization);
        if (result.getPrepayId() == null) {
            throw new BusinessException("微信支付异常");
        }
        String paySign = Utils.sign(wechatMpProperties.getPrivateKey(), timestamp, nonceStr, result.getPrepayId());
        MiniPayToken miniPayToken = new MiniPayToken(timestamp, nonceStr, result.getPrepayId(), "RSA", paySign);

        List<PayOrder.Detail> details = Collections.singletonList(new PayOrder.Detail(
                orderHouse.getId(),
                PayOrder.OrderType.ORDER_HOUSE,
                description,
                orderHouse.getTotal(),
                orderHouse.getStartDate().atStartOfDay(),
                orderHouse.getEndDate().atStartOfDay()
        ));
        PayOrder payOrder = new PayOrder(
                merchant.getId(),
                merchant.getSubMchId(),
                merchant.getShortname(),
                cid,
                description,
                new String[]{orderHouse.getId()},
                details,
                orderHouse.getTotal(),
                PayOrder.PayStatus.NOTPAY,
                result.getPrepayId(),
                outTradeNo
        );
        payOrderRepository.save(payOrder);

        return miniPayToken;
    }
}