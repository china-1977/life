import {sort} from '@/utils/format';
import {request} from '@@/exports';

export default {
    page: (params: any, sorter: any, filter: any) =>
        request<any>('/store/merchants', {
            params: {
                ...params,
                sort: sort(sorter),
                ...filter,
            },
        }).then((res: any) => {
            return {
                data: res.content,
                total: res.totalElements,
            };
        }),
    detail: (id: any) =>
        request<any>(`/store/merchants/${id}`),

    products: (id: string, params: any, sorter: any, filter: any) =>
        request<any>(`/store/merchants/${id}/products`, {
            params: {
                ...params,
                sort: sort(sorter),
                ...filter,
            },
        }).then((res: any) => {
            return {
                data: res.content,
                total: res.totalElements,
            };
        }),
}