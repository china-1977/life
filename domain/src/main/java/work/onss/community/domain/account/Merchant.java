package work.onss.community.domain.account;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.config.PointType;
import work.onss.community.domain.converters.MerchantCategoryConverter;
import work.onss.community.domain.usertype.StringArrayType;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@EntityListeners(AuditingEntityListener.class)
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class Merchant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    @Enumerated(EnumType.STRING)
    @ExcelProperty(value = "分类", converter = MerchantCategoryConverter.class)
    private Category category;
    @ExcelProperty(value = "简称")
    private String shortname;
    @ExcelProperty(value = "密码")
    private String password;
    @ExcelProperty(value = "LOGO")
    private String trademark;
    @ExcelProperty(value = "上班时间")
    private LocalTime openTime;
    @ExcelProperty(value = "下班时间")
    private LocalTime closeTime;
    @ExcelProperty(value = "描述")
    private String description;
    @Type(value = StringArrayType.class)
    private String[] pictures;
    @Type(value = StringArrayType.class)
    private String[] videos;
    private Boolean status = false;
    @ExcelProperty(value = "创建者ID")
    private String customerId;
    @ExcelProperty(value = "姓名")
    private String username;
    @ExcelProperty(value = "手机号")
    private String phone;
    @ExcelProperty(value = "坐标")
    @Type(value = PointType.class)
    @NotNull(message = "请重新定位收货地址")
    private Point2D.Double location;
    @ExcelProperty(value = "邮编")
    private String postcode;
    @ExcelProperty(value = "地区")
    @Type(value = StringArrayType.class)
    private String[] addressValue;
    @Type(value = StringArrayType.class)
    private String[] addressCode;
    @ExcelProperty(value = "地址详情")
    private String addressDetail;
    @ExcelProperty(value = "地址名称")
    private String addressName;
    @ExcelProperty(value = "微信商户ID")
    private String subMchId;
    @ExcelProperty(value = "支付宝商户ID")
    private String sellerId;
    @ExcelProperty(value = "注册时间")
    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;
    @LastModifiedDate
    @ExcelProperty(value = "更新时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateDatetime;

    public Merchant(Merchant merchant) {
        this.id = merchant.id;
        this.category = merchant.category;
        this.shortname = merchant.shortname;
        this.password = merchant.password;
        this.trademark = merchant.trademark;
        this.openTime = merchant.openTime;
        this.closeTime = merchant.closeTime;
        this.description = merchant.description;
        this.pictures = merchant.pictures;
        this.videos = merchant.videos;
        this.status = merchant.status;
        this.customerId = merchant.customerId;
        this.username = merchant.username;
        this.phone = merchant.phone;
        this.location = merchant.location;
        this.postcode = merchant.postcode;
        this.addressValue = merchant.addressValue;
        this.addressCode = merchant.addressCode;
        this.addressDetail = merchant.addressDetail;
        this.addressName = merchant.addressName;
        this.subMchId = merchant.subMchId;
        this.sellerId = merchant.sellerId;
        this.insertDatetime = merchant.insertDatetime;
        this.updateDatetime = merchant.updateDatetime;
    }

    @Getter
    @AllArgsConstructor
    public enum Category implements Serializable {
        VILLAGE("物业"),
        STORE("零售商"),
        PARKING("停车场"),
        ;
        private final String message;
    }
}
