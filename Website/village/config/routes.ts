export default [
  {
    path: '/',
    name: '首页',
    icon: 'dashboard',
    component: '@/pages/Index',
  },
  {
    path: '/shop',
    name: '商城管理',
    icon: 'dashboard',
    routes: [
      {
        path: '/shop/merchants',
        name: '商户筛选',
        component: '@/pages/Shop/Merchant',
        resources: [
          {
            name: '商户详情',
            path: '/village/商户详情',
          },
          {
            name: '商品分页',
            path: '/village/商户分页',
          },
          {
            name: '商户商品',
            path: '/village/商户代理',
          },
        ]
      },
      {
        path: '/shop/:id/products',
        name: '商品筛选',
        component: '@/pages/Shop/$merchantId/Product',
        hideInMenu: true,
      },
      {
        path: '/shop/panel',
        name: '橱窗管理',
        component: '@/pages/Shop/ProductPanel',
        resources: [
          {
            name: '橱窗分页',
            path: '/village/橱窗分页',
          },
          {
            name: '橱窗保存',
            path: '/village/橱窗保存',
          },
          {
            name: '橱窗删除',
            path: '/village/橱窗删除',
          },
          {
            name: '橱窗详情',
            path: '/village/橱窗详情',
          },
        ],
      },
    ]
    ,
  },
  {
    path: '/task',
    name: '日常任务',
    icon: 'dashboard',
    routes: [
      {
        path: '/task/visitors',
        name: '访客记录',
        icon: 'dashboard',
        component: '@/pages/Village/Visitor',
        resources: [
          {
            name: '访客列表',
            path: '/village/访客列表',
          },
          {
            name: '访客详情',
            path: '/village/访客详情',
          },
          {
            name: '访客批量审核',
            path: '/village/访客批量审核',
          },
          {
            name: '访客删除',
            path: '/village/访客删除',
          },
          {
            name: '访客导出',
            path: '/village/访客导出',
          },
        ],
      },
      {
        path: '/task/repair',
        name: '报事报修',
        icon: 'dashboard',
        component: '@/pages/Village/Repair',
        resources: [
          {
            name: '报事报修-完成',
            path: '/village/报事报修-完成',
          },
          {
            name: '报事报修导出',
            path: '/village/报事报修导出',
          },
          {
            name: '报事报修-分配',
            path: '/village/报事报修-分配',
          },
          {
            name: '报事报修详情',
            path: '/village/报事报修详情',
          },
          {
            name: '报事报修列表',
            path: '/village/报事报修列表',
          },
          {
            name: '报事报修批量删除',
            path: '/village/报事报修批量删除',
          },
        ],
      },
      {
        path: '/task/advice',
        name: '意见反馈',
        icon: 'dashboard',
        component: '@/pages/Village/Advice',
        resources: [
          {
            name: '意见导出',
            path: '/village/意见导出',
          },
          {
            name: '意见审核',
            path: '/village/意见审核',
          },
          {
            name: '意见编辑',
            path: '/village/意见编辑',
          },
          {
            name: '意见详情',
            path: '/village/意见详情',
          },
          {
            name: '意见列表',
            path: '/village/意见列表',
          },
        ],
      },
      {
        path: '/task/vote',
        name: '投票统计',
        icon: 'dashboard',
        component: '@/pages/Village/VoteList',
        resources: [
          {
            name: '投票结果同步',
            path: '/village/投票结果同步',
          },
          {
            name: '投票导出',
            path: '/village/投票导出',
          },
          {
            name: '投票导出详情',
            path: '/village/投票导出详情',
          },
          {
            name: '投票[创建、编辑]',
            path: '/village/投票[创建、编辑]',
          },
          {
            name: '投票详情',
            path: '/village/投票详情',
          },
          {
            name: '投票列表',
            path: '/village/投票列表',
          },
          {
            name: '投票批量删除',
            path: '/village/投票批量删除',
          },
        ],
      },
      {
        path: '/task/notice',
        name: '公告通知',
        icon: 'dashboard',
        component: '@/pages/Village/Notice',
        resources: [
          {
            name: '通知导出',
            path: '/village/通知导出',
          },
          {
            name: '通知[创建、编辑]',
            path: '/village/通知[创建、编辑]',
          },
          {
            name: '通知详情',
            path: '/village/通知详情',
          },
          {
            name: '通知列表',
            path: '/village/通知列表',
          },
          {
            name: '通知批量删除',
            path: '/village/通知批量删除',
          },
        ],
      },
    ],
  },
  {
    path: '/house',
    name: '房屋管理',
    icon: 'dashboard',
    routes: [
      {
        path: '/house/identity',
        name: '入住申请',
        icon: 'dashboard',
        component: '@/pages/Village/HouseIdentity',
        resources: [
          {
            name: '入住申请列表',
            path: '/village/入住申请列表',
          },
          {
            name: '入住申请详情',
            path: '/village/入住申请详情',
          },
          {
            name: '入住申请审核',
            path: '/village/入住申请审核',
          },
          {
            name: '入住申请删除',
            path: '/village/入住申请删除',
          },
          {
            name: '入住申请导出',
            path: '/village/入住申请导出',
          },
        ],

      },
      {
        path: '/house/list',
        name: '房屋管理',
        component: '@/pages/Village/HouseList',
        resources: [
          {
            name: '房屋绑定车位',
            path: '/village/房屋绑定车位',
          },
          {
            name: '房屋编辑',
            path: '/village/房屋编辑',
          },
          {
            name: '房屋车位',
            path: '/village/房屋车位',
          },
          {
            name: '房屋成员',
            path: '/village/房屋成员',
          },
          {
            name: '房屋创建',
            path: '/village/房屋创建',
          },
          {
            name: '房屋待授权列表',
            path: '/village/房屋待授权列表',
          },
          {
            name: '房屋导出',
            path: '/village/房屋导出',
          },
          {
            name: '房屋导入',
            path: '/village/房屋导入',
          },
          {
            name: '房屋快速创建',
            path: '/village/房屋快速创建',
          },
          {
            name: '房屋列表',
            path: '/village/房屋列表',
          },
          {
            name: '房屋批量删除',
            path: '/village/房屋批量删除',
          },
          {
            name: '房屋人数和车位同步',
            path: '/village/房屋人数和车位同步',
          },
          {
            name: '房屋详情',
            path: '/village/房屋详情',
          },
          {
            name: '房屋信息',
            path: '/village/房屋信息',
          },
        ],
      },
      {
        path: '/house/parkSpace',
        name: '车位管理',
        component: '@/pages/Village/ParkSpaceList',
        resources: [
          {
            name: '车位详情',
            path: '/village/车位详情',
          },
          {
            name: '车位列表',
            path: '/village/车位列表',
          },
          {
            name: '车位创建和编辑',
            path: '/village/车位创建和编辑',
          },
          {
            name: '车位删除',
            path: '/village/车位删除',
          },
          {
            name: '车位导出',
            path: '/village/车位导出',
          },
          {
            name: '车位导入',
            path: '/village/车位导入',
          },
          {
            name: '车位设置车牌号',
            path: '/village/车位设置车牌号',
          },

        ],
      },
      {
        path: '/house/renting',
        name: '租售管理',
        component: '@/pages/Village/HouseRenting',
        resources: [
          {
            name: '房屋租售导出',
            path: '/village/房屋租售导出',
          },
          {
            name: '房屋租售列表',
            path: '/village/房屋租售列表',
          },
        ],
      },
      {
        path: '/house/vacant',
        name: '空置记录',
        component: '@/pages/Village/HouseVacant',
        resources: [
          {
            name: '空置房列表',
            path: '/village/空置房列表',
          },
          {
            name: '空置房-水电初始值',
            path: '/village/空置房-水电初始值',
          },
          {
            name: '空置房-水电结束值',
            path: '/village/空置房-水电结束值',
          },
          {
            name: '空置房-结账',
            path: '/village/空置房-结账',
          },
          {
            name: '空置房批量删除',
            path: '/village/空置房批量删除',
          },
          {
            name: '空置房导出',
            path: '/village/空置房导出',
          },
        ],
      },
    ],
  },

  {
    path: '/money',
    name: '财务管理',
    icon: 'dashboard',
    routes: [
      {
        path: '/money/orderHouses',
        name: '物业费',
        icon: 'dashboard',
        component: '@/pages/Village/OrderHouse',
        resources: [
          {
            name: '物业费退款',
            path: '/village/物业费退款',
          },
          {
            name: '物业费导入',
            path: '/village/物业费导入',
          },
          {
            name: '物业费导出',
            path: '/village/物业费导出',
          },
          {
            name: '物业费确认',
            path: '/village/物业费确认',
          },
          {
            name: '物业费模板导出',
            path: '/village/物业费模板导出',
          },
          {
            name: '物业费详情',
            path: '/village/物业费详情',
          },
          {
            name: '物业费列表',
            path: '/village/物业费列表',
          },
          {
            name: '物业费批量删除',
            path: '/village/物业费批量删除',
          },
        ],
      },
      {
        path: '/money/orderWater',
        name: '水费',
        icon: 'dashboard',
        component: '@/pages/Village/OrderWater',
        resources: [
          {
            name: '水费退款',
            path: '/village/水费退款',
          },
          {
            name: '水费导入',
            path: '/village/水费导入',
          },
          {
            name: '水费导出',
            path: '/village/水费导出',
          },
          {
            name: '水费确认',
            path: '/village/水费确认',
          },
          {
            name: '水费模板导出',
            path: '/village/水费模板导出',
          },
          {
            name: '水费详情',
            path: '/village/水费详情',
          },
          {
            name: '水费列表',
            path: '/village/水费列表',
          },
          {
            name: '水费批量删除',
            path: '/village/水费批量删除',
          },
        ],
      },
      {
        path: '/money/orderHeat',
        name: '采暖费',
        icon: 'dashboard',
        component: '@/pages/Village/OrderHeat',
        resources: [
          {
            name: '采暖费导入',
            path: '/village/采暖费导入',
          },
          {
            name: '采暖费导出',
            path: '/village/采暖费导出',
          },
          {
            name: '采暖费确认',
            path: '/village/采暖费确认',
          },
          {
            name: '采暖费模板导出',
            path: '/village/采暖费模板导出',
          },
          {
            name: '采暖费详情',
            path: '/village/采暖费详情',
          },
          {
            name: '采暖费列表',
            path: '/village/采暖费列表',
          },
          {
            name: '采暖费批量删除',
            path: '/village/采暖费批量删除',
          },
        ],
      },
      {
        path: '/money/payOrder',
        name: '支付记录',
        icon: 'dashboard',
        component: '@/pages/Order/PayOrder',
        resources: [
          {
            name: '支付记录导出',
            path: '/village/支付记录导出',
          },
          {
            name: '支付记录详情',
            path: '/village/支付记录详情',
          },
          {
            name: '支付记录列表',
            path: '/village/支付记录列表',
          },
        ],
      },
    ],
  },
  {
    path: '/base',
    name: '账户设置',
    icon: 'dashboard',
    routes: [
      {
        path: '/base/merchants',
        name: '主体管理',
        component: '@/pages/System/Merchant',
        resources: [
          {
            name: '主体更新',
            path: '/account/主体更新',
          },
          {
            name: '主体图片',
            path: '/account/主体图片',
          },
          {
            name: '主体详情',
            path: '/account/主体详情',
          },
          {
            name: '主体状态',
            path: '/account/主体状态',
          },
        ],
      },
      {
        path: '/base/members',
        name: '员工列表',
        component: '@/pages/System/Member',
        resources: [
          {
            name: '营业成员权限设置',
            path: '/account/营业成员权限设置',
          },
          {
            name: '营业成员删除',
            path: '/account/营业成员删除',
          },
          {
            name: '营业成员身份证查询',
            path: '/account/营业成员身份证查询',
          },
          {
            name: '营业成员邀请',
            path: '/account/营业成员邀请',
          },
          {
            name: '营业成员资源列表',
            path: '/account/营业成员资源列表',
          },
          {
            name: '营业员导出',
            path: '/account/营业员导出',
          },
          {
            name: '营业员列表',
            path: '/account/营业员列表',
          },
        ],
      },
    ],
  },
  {
    path: '/login',
    name: '登录',
    component: '@/pages/Login',
    layout: false,
    hideInMenu: true,
  },
];