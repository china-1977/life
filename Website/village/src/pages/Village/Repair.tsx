/**
 * 报事报修列表
 */
import {RepairStatusEnum} from '@/constants';
import {Repair} from '@/services/RepairController';
import services from '@/services/api';
import {
    ActionType,
    ModalForm,
    ProFormDependency,
    ProFormDigit,
    ProFormSelect,
    ProFormText,
    ProFormTextArea,
    ProTable
} from '@ant-design/pro-components';
import {Avatar, Button, DatePicker, Dropdown, Form, Image, Popconfirm, Typography} from 'antd';
import {useRef, useState} from 'react';
import {geCustomerByKeywords} from "@/services/OpenController";

const {page, del, save} = services.RepairController;
const {exportData} = services.FileController;
const RepairList = () => {
    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {title: '姓名', align: 'center', dataIndex: 'name', copyable: true},
        {title: '电话', align: 'center', dataIndex: 'phone', copyable: true},
        {title: '地址', align: 'center', dataIndex: 'address', copyable: true, search: false},
        {title: '标题', align: 'center', dataIndex: 'title', copyable: true},
        {
            title: '图片', align: 'center', dataIndex: 'descriptionPictures', copyable: true,
            render: (text: object, record: { descriptionPictures: [string] }) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={record?.descriptionPictures?.map((value, index) => (
                                <Image src={value}/>
                            ))}
                        />
                    }/>
            )
        },
        {title: '标签', align: 'center', dataIndex: 'label', copyable: true},
        {title: '维修者-姓名', align: 'center', dataIndex: 'repairerName', copyable: true},
        {title: '维修者-电话', align: 'center', dataIndex: 'repairerPhone', copyable: true},
        {
            title: '状态',
            align: 'center',
            dataIndex: 'status',
            copyable: true,
            fieldProps: {mode: 'multiple'},
            valueEnum: RepairStatusEnum.columns,
        },
        {
            title: '创建时间',
            dataIndex: 'insertDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '更新时间',
            dataIndex: 'updateDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, repair: Repair) => (
                <Dropdown.Button
                    key={repair.id}
                    onClick={async () => {
                        form.resetFields();
                        form.setFieldsValue({...repair});
                        setModalVisit('EDIT');
                    }}
                    menu={{
                        items: [
                            {
                                key: '删除',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${repair.title}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        del([repair.id]).then(() => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(repair)}}>编辑</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const ref = useRef<ActionType>();
    const [form] = Form.useForm();
    const [modalVisit, setModalVisit] = useState<any>(false);

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        insertDatetime: {show: false},
                        updateDatetime: {show: false},
                    }
                }}
                rowSelection={{}}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params: any, sorter, filter) => page(params, sorter, filter)}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/repairs/export', values, '报事报修.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
            />

            <ModalForm
                title="报事报修"
                form={form}
                open={modalVisit === 'EDIT'}
                grid={true}
                colProps={{span: 8}}
                onOpenChange={(visible) => {
                    if (visible && modalVisit === 'EDIT') {
                        setModalVisit('EDIT');
                    } else {
                        form.resetFields();
                        setModalVisit(false);
                    }
                }}
                onFinish={async (values) => {
                    values.id = form.getFieldValue('id');
                    return save(values).then((res) => {
                        form.resetFields();
                        ref.current?.reload();
                        return true;
                    });
                }}
            >
                <ProFormText width="sm" name="name" label="姓名" disabled/>
                <ProFormDigit width="sm" name="phone" label="电话" disabled/>
                <ProFormSelect width="sm" name="title" label="标题" disabled/>
                <ProFormSelect width="sm" name="address" label="地址" disabled/>
                <ProFormTextArea colProps={{span: 24}} fieldProps={{autoSize: true}} name={'description'} label="描述"
                                 disabled/>
                <ProFormDependency style={{width: '100%'}} name={['descriptionPictures']}>
                    {(values, form) => (
                        <Image.PreviewGroup
                            children={values?.descriptionPictures?.map((value: string, index: number) => (
                                <Image width={100} height={100} src={value}/>
                            ))}
                        />
                    )}
                </ProFormDependency>
                <ProFormTextArea colProps={{span: 24}} fieldProps={{autoSize: true}} name={'reply'} label="回复"
                                 disabled/>
                <ProFormSelect width="sm" name="status" label="状态" valueEnum={RepairStatusEnum.ProFormSelect}/>
                <ProFormText width="sm" name="label" label="标签"/>
                <ProFormSelect
                    colProps={{span: 8}}
                    name={['repairerCustomerId']}
                    label={'维修者'}
                    showSearch={true}
                    debounceTime={1000}
                    width="sm"
                    fieldProps={{filterOption: false}}
                    params={{id: form.getFieldValue('repairerCustomerId')}}
                    request={async (params: { keyWords: string; id: string }) => {
                        if (params.id || params.keyWords) {
                            console.log(params)
                            return geCustomerByKeywords(params).then((values: [any]) => {
                                return values.map((customer) => {
                                    return {
                                        value: customer.id,
                                        label: `${customer.name}-${customer.phone}`,
                                    };
                                });
                            });
                        } else {
                            return [];
                        }
                    }}
                />

            </ModalForm>
        </div>
    );
};

export default RepairList;
