package work.onss.community.domain.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;


@RefreshScope
@Data
@ConfigurationProperties(prefix = "system")
@EnableConfigurationProperties(SystemConfig.class)
@Configuration
public class SystemConfig implements Serializable {
    private String filePath;
    private String secret;
    private String accessKey;
    private Integer expire;
}
