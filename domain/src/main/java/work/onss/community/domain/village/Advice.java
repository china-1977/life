package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.converters.AdviceStatusConverter;
import work.onss.community.domain.usertype.StringArrayType;

import java.io.Serializable;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
@EntityListeners(AuditingEntityListener.class)
public class Advice implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    private String merchantId;
    @ExcelProperty(value = "用户ID")
    private String customerId;
    @ExcelProperty(value = "姓名")
    private String name;
    @ExcelProperty(value = "电话")
    private String phone;
    @ExcelProperty(value = "标题")
    private String title;
    @ExcelProperty(value = "描述")
    private String description;
    @Type(value = StringArrayType.class)
    private String[] descriptionPictures;
    @ExcelProperty(value = "备注")
    private String remark;
    @Enumerated(EnumType.STRING)
    @ExcelProperty(value = "状态", converter = AdviceStatusConverter.class)
    private Status status;

    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ExcelProperty(value = "创建时间")
    private LocalDateTime insertDatetime;
    @LastModifiedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ExcelProperty(value = "更新时间")
    private LocalDateTime updateDatetime;

    @Getter
    public enum Status implements Serializable {
        WAIT("待处理"),
        CLOSED("已关闭"),
        ERROR("未解决"),
        SUCCESS("已解决");
        private final String value;

        Status(String value) {
            this.value = value;
        }
    }
}
