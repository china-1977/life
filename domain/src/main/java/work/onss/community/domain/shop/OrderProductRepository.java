package work.onss.community.domain.shop;

import jakarta.validation.constraints.NotBlank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;

public interface OrderProductRepository extends JpaRepository<OrderProduct, String>, QuerydslPredicateExecutor<OrderProduct>, QuerydslBinderCustomizer<QOrderProduct> {

    default void customize(QuerydslBindings bindings, QOrderProduct qOrderProduct) {
        bindings.bind(qOrderProduct.storeShortname).first((path, s) -> Objects.requireNonNull(path).contains(s));
        bindings.bind(qOrderProduct.insertDatetime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
    }

    Optional<OrderProduct> findByIdAndUserId(String id, String aid);

    Optional<OrderProduct> findByIdAndStoreId(String id, @NotBlank(message = "缺少商户参数") String sid);
    Optional<OrderProduct> findByIdAndVillageId(String id, @NotBlank(message = "缺少商户参数") String sid);
}
