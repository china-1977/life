package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import work.onss.community.domain.dto.village.HouseParkSpaceDto;
import work.onss.community.domain.village.ParkSpace;
import work.onss.community.domain.village.ParkSpaceRepository;
import work.onss.community.domain.village.QHouse;
import work.onss.community.domain.village.QParkSpace;
import work.onss.community.domain.vo.HouseVo;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class ParkSpaceController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private ParkSpaceRepository parkSpaceRepository;

    @GetMapping(value = {"parkSpaces/{id}"}, name = "车位详情")
    public HouseParkSpaceDto detail(@PathVariable String id) {
        QParkSpace qParkSpace = QParkSpace.parkSpace;
        QHouse qHouse = QHouse.house;
        return jpaQueryFactory.select(Projections.fields(HouseParkSpaceDto.class, qHouse, qParkSpace)).from(qParkSpace)
                .leftJoin(qHouse).on(qHouse.id.eq(qParkSpace.houseId))
                .where(qParkSpace.id.eq(id))
                .fetchOne();
    }

    @GetMapping(value = {"parkSpaces"}, name = "车位列表")
    public Page<ParkSpace> page(@RequestHeader(name = "mid") String mid,
                                @QuerydslPredicate(bindings = ParkSpaceRepository.class) Predicate predicate,
                                @PageableDefault Pageable pageable) {
        QParkSpace qParkSpace = QParkSpace.parkSpace;
        return parkSpaceRepository.findAll(qParkSpace.merchantId.eq(mid).and(predicate), pageable);
    }

    @Transactional
    @PostMapping(value = {"parkSpaces"}, name = "车位创建和编辑")
    public void saveOrInsert(@RequestHeader(name = "mid") String mid,
                             @RequestBody @Validated ParkSpace parkSpace) {
        if (parkSpace.getId() == null) {
            parkSpace.setMerchantId(mid);
            parkSpace.setState("无车");
            parkSpaceRepository.save(parkSpace);
        } else {
            QParkSpace qParkSpace = QParkSpace.parkSpace;
            jpaQueryFactory.update(qParkSpace)
                    .set(qParkSpace.region, parkSpace.getRegion())
                    .set(qParkSpace.code, parkSpace.getCode())
                    .set(qParkSpace.carCode, parkSpace.getCarCode())
                    .set(qParkSpace.status, parkSpace.getStatus())
                    .set(qParkSpace.remarks, parkSpace.getRemarks())
                    .where(qParkSpace.id.eq(parkSpace.getId()), qParkSpace.merchantId.eq(mid))
                    .execute();
        }
    }

    @DeleteMapping(value = {"parkSpaces"}, name = "车位删除")
    public void delete(@RequestHeader(name = "mid") String mid,
                       @RequestBody Collection<String> ids) {
        parkSpaceRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @PostMapping(value = {"parkSpaces/export"}, name = "车位导出")
    public void export(
            @RequestHeader(name = "mid") String mid,
            @QuerydslPredicate(bindings = ParkSpaceRepository.class, root = ParkSpace.class) Predicate predicate,
            HttpServletResponse httpServletResponse) throws Exception {
        QHouse qHouse = QHouse.house;
        QParkSpace qParkSpace = QParkSpace.parkSpace;
        List<ParkSpace> data = jpaQueryFactory.select(Projections.constructor(ParkSpace.class, qParkSpace, qHouse.floorNumber))
                .from(qParkSpace)
                .leftJoin(qHouse).on(qHouse.id.eq(qParkSpace.houseId))
                .where(qParkSpace.merchantId.eq(mid), predicate)
                .fetch();
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream()).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").head(ParkSpace.class).build());
        Map<String, List<ParkSpace>> floorNumberData = data.stream().collect(Collectors.groupingBy(ParkSpace::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).head(ParkSpace.class).build()));
        List<HouseVo> houseVos = jpaQueryFactory.select(Projections.constructor(HouseVo.class, qHouse))
                .from(qHouse)
                .where(qHouse.merchantId.eq(mid))
                .fetch();
        excelWriter.write(houseVos, EasyExcel.writerSheet("房屋列表").head(HouseVo.class).build());
        excelWriter.finish();
    }

    @PostMapping(value = "parkSpaces/imports", name = "车位导入")
    public void imports(@RequestHeader(name = "mid") String mid, @RequestParam(value = "file") MultipartFile file) throws Exception {
        ExcelReader excelReader = EasyExcel.read(file.getInputStream()).head(ParkSpace.class).build();
        List<ReadSheet> readSheets = excelReader.excelExecutor().sheetList();
        for (ReadSheet readSheet : readSheets) {
            if (!readSheet.getSheetName().equals("房屋列表")) {
                List<ParkSpace> parkSpaces = EasyExcel.read(file.getInputStream()).head(ParkSpace.class).sheet(readSheet.getSheetNo()).doReadSync();
                for (ParkSpace parkSpace : parkSpaces) {
                    parkSpace.setMerchantId(mid);
                }
                parkSpaceRepository.saveAll(parkSpaces);
            }
        }
    }

    @Transactional
    @PostMapping(value = {"parkSpaces/{id}/setVehicleCode"}, name = "车位设置车牌号")
    public void binding(@RequestHeader(name = "mid") String mid,
                        @PathVariable String id,
                        @RequestBody ParkSpace parkSpace) {
        QParkSpace qParkSpace = QParkSpace.parkSpace;
        jpaQueryFactory.update(qParkSpace)
                .set(qParkSpace.carCode, parkSpace.getCarCode())
                .set(qParkSpace.status, parkSpace.getStatus())
                .where(qParkSpace.id.eq(id), qParkSpace.merchantId.eq(mid))
                .execute();

    }
}
