package work.onss.community.service.wechat;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Log4j2
@Component
public class BeanInterface {
    @Bean
    public RestClient restClient() {
        return RestClient.create();
    }

    @Bean
    public MiniAppService miniAppService(RestClient restClient) {
        return HttpServiceProxyFactory
                .builderFor(RestClientAdapter.create(restClient))
                .build()
                .createClient(MiniAppService.class);
    }

    @Bean
    public WechatPayService wechatPayService(RestClient restClient) {
        return HttpServiceProxyFactory
                .builderFor(RestClientAdapter.create(restClient))
                .build()
                .createClient(WechatPayService.class);
    }

    @Bean
    public LePosPayService lePosPayService(RestClient restClient) {
        return HttpServiceProxyFactory
                .builderFor(RestClientAdapter.create(restClient))
                .build()
                .createClient(LePosPayService.class);
    }
}
