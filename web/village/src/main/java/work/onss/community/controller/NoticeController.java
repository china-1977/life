package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.*;

import java.time.LocalDateTime;
import java.util.Collection;

@Log4j2
@RestController
public class NoticeController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private NoticeRepository noticeRepository;

    @GetMapping(value = {"notices/{id}"}, name = "通知详情")
    public Notice detail(@PathVariable String id) {
        return noticeRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"notices"}, name = "通知列表")
    public Page<Notice> page(
            @RequestHeader(name = "mid") String mid,
            @QuerydslPredicate(bindings = NoticeRepository.class) Predicate predicate, @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QNotice qNotice = QNotice.notice;
        return noticeRepository.findAll(qNotice.merchantId.eq(mid).and(predicate), pageable);
    }

    @Transactional
    @PostMapping(value = {"notices"}, name = "通知[创建、编辑]")
    public void saveOrInsert(@RequestHeader(name = "mid") String mid, @RequestBody @Validated Notice notice) {
        if (notice.getId() == null) {
            notice.setMerchantId(mid);
            noticeRepository.save(notice);
        } else {
            QNotice qNotice = QNotice.notice;
            jpaQueryFactory.update(qNotice)
                    .set(qNotice.title, notice.getTitle())
                    .set(qNotice.description, notice.getDescription())
                    .set(qNotice.pictures, notice.getPictures())
                    .set(qNotice.videos, notice.getVideos())
                    .set(qNotice.updateDatetime, LocalDateTime.now())
                    .where(qNotice.id.eq(notice.getId()), qNotice.merchantId.eq(mid))
                    .execute();
        }
    }

    @DeleteMapping(value = {"notices"}, name = "通知批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        noticeRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }


    @PostMapping(value = "notices/export", name = "通知导出")
    public void export(
            HttpServletResponse httpServletResponse,
            @RequestHeader(name = "mid") String mid,
            @QuerydslPredicate(bindings = NoticeRepository.class, root = Notice.class) Predicate predicate) throws Exception {
        QNotice qNotice = QNotice.notice;
        Iterable<Notice> notices = noticeRepository.findAll(qNotice.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        EasyExcel.write(httpServletResponse.getOutputStream(), Notice.class).sheet().doWrite((Collection<?>) notices);
    }
}