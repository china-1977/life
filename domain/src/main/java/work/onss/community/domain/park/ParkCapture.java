package work.onss.community.domain.park;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
@EntityListeners(AuditingEntityListener.class)
public class ParkCapture implements Serializable {
    @Id
    private String id;
    private String code; // 车牌号
    private String codeImage; // 车牌号图片地址
    private double codeReliability; // 车牌可信度
    private String color; // 车牌颜色
    private double colorReliability; // 车牌颜色可信度
    private double shootingAngle; // 拍摄角度
    private String merchantId; // 主体ID
    private String parkRegionId; // 区域ID
    private String deviceNumber; // 设备编号
    private DataType dataType; // 出入类型
    private String captureImage; // 拍摄图片路径【/主体ID/车牌号+出入类型+拍摄时间.jpg】
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime captureTime; // 拍摄时间
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime uploadTime; // 上传时间
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime receiveTime; // 接收时间
    @LastModifiedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime saveTime; // 保存时间

    @Getter
    @AllArgsConstructor
    public enum DataType implements Serializable {
        I("入场"),
        O("离场");
        private final String value;
    }
}