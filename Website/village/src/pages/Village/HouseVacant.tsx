/**
 * 空置记录
 */
import services from '@/services/api';
import {
    ActionType,
    ModalForm,
    ProFormDatePicker,
    ProFormSelect,
    ProFormText,
    ProFormTextArea,
    ProTable
} from '@ant-design/pro-components';
import {Button, DatePicker, Dropdown, Popconfirm, Space} from 'antd';
import {useRef} from 'react';
import {HouseVacantStatusEnum} from '@/constants';

const {page, del} = services.HouseVacantController;
const {exportData} = services.FileController;

const HouseVacantList = () => {
    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {
            title: '房屋ID',
            align: 'center',
            dataIndex: 'houseId',
            copyable: true,
            onSorter: false,
        },
        {
            title: '楼号',
            align: 'center',
            dataIndex: 'floorNumber',
            copyable: true,
            onSorter: false,
            sorter: {multiple: 1},
        },
        {title: '单元', align: 'center', dataIndex: 'unit', copyable: true, sorter: {multiple: 1}, onSorter: false},
        {
            title: '室',
            align: 'center',
            dataIndex: 'roomNumber',
            copyable: true,
            onSorter: false,
            sorter: {multiple: 1},
        },
        {
            title: '初始水码',
            align: 'center',
            dataIndex: 'initialWater',
            copyable: true,
        },
        {
            title: '结束水码',
            align: 'center',
            dataIndex: 'finalWater',
            copyable: true,
        },
        {
            title: '初始电码',
            align: 'center',
            dataIndex: 'initialElectricity',
            copyable: true,
        },
        {
            title: '结束电码',
            align: 'center',
            dataIndex: 'finalElectricity',
            copyable: true,
        },
        {
            title: '开始日期', align: 'center', dataIndex: 'startDate', copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '结束日期', align: 'center', dataIndex: 'endDate', copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, record: any) => (
                <Dropdown.Button
                    key={record.id}
                    menu={{
                        items: [
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${record.floorNumber},${record.unit},${record.roomNumber}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        del([record.id]).then((res) => {
                                          ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    操作
                </Dropdown.Button>
            ),
        },
    ];
    const ref = useRef<ActionType>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                        houseId: {show: false},
                    }
                }}
                tableAlertOptionRender={(data) => {
                    return (
                        <Space size={16}>
                            <Popconfirm
                                title="删除"
                                description={`是否批量删除？`}
                                okText="确认"
                                cancelText="取消"
                                onConfirm={() => {
                                    del(data?.selectedRowKeys).then(() => {
                                        if (ref.current && typeof ref.current.clearSelected === 'function') {
                                            ref.current.reload();
                                            ref.current.clearSelected();
                                        }
                                    });
                                }}
                            >
                                <Button type="primary" danger>删除</Button>
                            </Popconfirm>
                        </Space>
                    );
                }}
                rowSelection={{}}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params, sorter, filter) => page(params, sorter, filter)}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/houseVacants/export', values, '空置记录.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        ...dom.values(),
                    ],
                }}
            />
        </div>
    );
};

export default HouseVacantList;
