package work.onss.community.domain.park;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.converters.ParkStatusConverter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class OrderPark implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    @ExcelProperty(value = "车牌号")
    private String carNumber;
    @ExcelProperty(value = "车牌图片")
    private String carNumberPicture;
    @ExcelProperty(value = "驶入时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime inDatetime;
    @ExcelProperty(value = "全景图片")
    private String inPicture;
    @ExcelProperty(value = "驶离时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime outDatetime;
    @ExcelProperty(value = "驶离图片")
    private String outPicture;
    @ExcelProperty(value = "应付金额")
    private BigDecimal moneyMeet;
    @ExcelProperty(value = "减免金额")
    private BigDecimal moneyReduction;
    @ExcelProperty(value = "实付金额")
    private BigDecimal moneyPayment;
    @Transient
    private BigDecimal moneyOwe;
    @Enumerated(EnumType.STRING)
    @ExcelProperty(value = "停车状态", converter = ParkStatusConverter.class)
    private Status parkStatus;
    @ExcelProperty(value = "驶入编号")
    private String inDeviceNumber;
    @ExcelProperty(value = "驶离编号")
    private String outDeviceNumber;
    @ExcelProperty(value = "主体ID")
    private String merchantId;
    @ExcelProperty(value = "区域ID")
    private String parkRegionId;
    @ExcelProperty(value = "备注")
    private String remarks;
    @ExcelProperty(value = "创建时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;

    @Getter
    @AllArgsConstructor
    public enum Status implements Serializable {
        I("在场"),
        O("离场");
        private final String value;
    }
}