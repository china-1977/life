import * as ApplicationController from './ApplicationController';
import * as FileController from './FileController';
import * as HouseController from './HouseController';
import * as HouseIdentityController from './HouseIdentityController';
import * as LoginController from './LoginController';
import * as MemberController from './MemberController';
import * as OrderHeatController from './OrderHeatController';
import * as OrderHouseController from './OrderHouseController';
import * as OrderWaterController from './OrderWaterController';
import * as ParkSpaceController from './ParkSpaceController';
import * as MerchantController from './MerchantController';
import * as VoteController from './VoteController';
import * as OpenController from './OpenController';
import * as PayOrderController from './PayOrderController';
import * as AdviceController from './AdviceController';
import * as ParkSpaceIdentityController from './ParkSpaceIdentityController';
import * as NoticeController from './NoticeController';
import * as RepairController from './RepairController';
import * as HouseRentingController from './HouseRentingController';
import * as VisitorController from './VisitorController';
import * as HouseVacantController from './HouseVacantController';

export default {
  ApplicationController,
  MerchantController,
  LoginController,
  MemberController,
  FileController,
  HouseController,
  ParkSpaceController,
  OrderHouseController,
  HouseIdentityController,
  VoteController,
  OrderWaterController,
  OrderHeatController,
  OpenController,
  PayOrderController,
  AdviceController,
  ParkSpaceIdentityController,
  NoticeController,
  RepairController,
  HouseRentingController,
  VisitorController,
  HouseVacantController,
};
