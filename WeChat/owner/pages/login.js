Page({
  mixins: [require('../mixin/common')],
  data: {},

  login: function (e) {
    this.wxRequest({
      url: `${this.data.domain}/account/login`,
      method: 'POST',
      data: e.detail.value
    }).then((content) => {
      wx.setStorageSync('authorization', content.authorization);
      wx.setStorageSync('info', content.info);
      wx.reLaunch({
        url: '/pages/main/index',
      });
    });
  },

  wxLogin: function (e) {
    wx.login({
      success: ({ code }) => {
        this.wxRequest({
          url: `${this.data.domain}/account/wxLogin`,
          method: 'POST',
          data: { code, appid: this.data.appid }
        }).then((content) => {
          wx.setStorageSync('authorization', content.authorization);
          wx.setStorageSync('info', content.info);
          if (content.info.cid) {
            wx.reLaunch({
              url: '/pages/main/index',
            });
          } else {
            wx.navigateTo({
              url: `/pages/user/list?openid=${content.info.openid}&authorization=${content.authorization}`
            })
          }
        });
      },
      fail: () => {
        wx.showModal({
          title: '警告',
          content: '获取微信code失败',
          confirmColor: '#e64340',
          showCancel: false,
        })
      },
    })
  },
})