package work.onss.community.domain.usertype;

import org.hibernate.boot.model.FunctionContributions;
import org.hibernate.dialect.PostgreSQLDialect;
import org.hibernate.type.StandardBasicTypes;

public class CustomPostgreSQLDialect extends PostgreSQLDialect {
    public CustomPostgreSQLDialect() {
        super();
    }

    public void initializeFunctionRegistry(FunctionContributions functionContributions) {
        super.initializeFunctionRegistry(functionContributions);
        // 注册 array_to_string 函数
        functionContributions.getFunctionRegistry().register(
                "array_to_string", // 函数名
                new PostgresqlArrayToStringFunction("array_to_string", StandardBasicTypes.STRING) // 函数实现
        );
    }
}
