import * as AccessController from './AccessController';
import * as ApplicationController from './ApplicationController';
import * as CustomerController from './CustomerController';
import * as DoorController from './DoorController';
import * as FileController from './FileController';
import * as HouseController from './HouseController';
import * as IdentityController from './IdentityController';
import * as LoginController from './LoginController';
import * as OrderHouseController from './OrderHouseController';
import * as MemberController from './MemberController';
import * as ParkSpaceController from './ParkSpaceController';
import * as VillageController from './VillageController';
import * as VoteController from './VoteController';
import * as OrderWaterController from './OrderWaterController';
import * as OrderHeatController from './OrderHeatController';

export default {
  ApplicationController,
  VillageController,
  LoginController,
  MemberController,
  CustomerController,
  FileController,
  HouseController,
  ParkSpaceController,
  DoorController,
  AccessController,
  OrderHouseController,
  IdentityController,
  VoteController,
  OrderWaterController,
  OrderHeatController,
};
