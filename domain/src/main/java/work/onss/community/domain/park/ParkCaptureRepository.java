package work.onss.community.domain.park;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

public interface ParkCaptureRepository extends JpaRepository<ParkCapture, String>, JpaSpecificationExecutor<ParkCapture>, QuerydslPredicateExecutor<ParkCapture>, QuerydslBinderCustomizer<QParkCapture> {

    default void customize(QuerydslBindings bindings, QParkCapture qParkCapture) {
        bindings.bind(qParkCapture.id).withDefaultBinding();
        bindings.bind(qParkCapture.captureTime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
        bindings.bind(qParkCapture.uploadTime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
    }

    @Modifying
    @Transactional
    void deleteByMerchantIdAndIdIn(String vid, Collection<String> id);
}