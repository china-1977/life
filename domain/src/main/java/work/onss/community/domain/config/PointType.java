package work.onss.community.domain.config;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.geometric.PGpoint;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class PointType implements UserType<Point2D.Double> {


    @Override
    public int getSqlType() {
        return Types.OTHER;
    }

    @Override
    public Class<Point2D.Double> returnedClass() {
        return Point2D.Double.class;
    }

    @Override
    public boolean equals(Point2D.Double point, Point2D.Double j1) {
        if (point == null && j1 == null)
            return true;
        else if (point == null || j1 == null)
            return false;
        return point.equals(j1);
    }

    @Override
    public int hashCode(Point2D.Double point) {
        return point.hashCode();
    }

    @Override
    public Point2D.Double nullSafeGet(ResultSet resultSet, int i, SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws SQLException {
        Object object = resultSet.getObject(i);
        if (object instanceof PGpoint pGpoint) {
            return new Point2D.Double(pGpoint.x, pGpoint.y);
        }
        return null;
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Point2D.Double point, int i, SharedSessionContractImplementor sharedSessionContractImplementor) throws SQLException {
        PGpoint pGpoint = new PGpoint(point.getX(), point.getY());
        preparedStatement.setObject(i, pGpoint);
    }

    @Override
    public Point2D.Double deepCopy(Point2D.Double point) {
        return point;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Point2D.Double point) {
        return point;
    }

    @Override
    public Point2D.Double assemble(Serializable serializable, Object o) {
        return (Point2D.Double) serializable;
    }

    @Override
    public Point2D.Double replace(Point2D.Double point, Point2D.Double j1, Object o) {
        return point;
    }
}
