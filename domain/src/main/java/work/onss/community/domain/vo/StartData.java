package work.onss.community.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StartData {

    private Integer id;
    private String roomNum;
    private String floor;
    private String name;
    private String phone;
    private String parkNum;
    private String remarks;

}
