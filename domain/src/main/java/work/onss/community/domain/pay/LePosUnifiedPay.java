package work.onss.community.domain.pay;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@NoArgsConstructor
@Data
public class LePosUnifiedPay {
    private String service = "get_tdcode"; // 接口名
    private String t0;
    private String sign_type; // 接口签名类型::MD5/SM3。支持MD5和国密SM3算法；(不上送默认为使用MD5算法)
    private String pay_way; // 支付类型
    private String merchant_id; // 商户号
    private String user_name; // 收银员名称
    private String third_order_id; // 商户订单号
    private String amount; // 订单金额
    private String jspay_flag; // 支付类型 0-支付宝扫码支付；1-微信公众号、支付宝服务窗支付<原生支付>；2-微信公众号、支付宝服务窗支付<简易支付>；3-微信小程序支付、支付宝小程序支付
    private String royalty; // 交易分账标识0：普通交易1：分账（用于分账的交易需传该参数）2：交易主体分账（延迟结算）3：预授权 注：如果未传该字段默认为0，按不分账处理
    private String appid; // 公众号appid 微信公众号ID;微信公众号支付的公众号id。选填，如果传了会使用此appid 进行下单；没传使用商户进件时最新配置的 appid
    private String sub_openid; // 用户子标识 微信公众号、小程序、支付宝服务窗、支付宝小程序、银联JS支付必填
    private String extend_business_params; // 业务拓展参数 当前可透传支付宝扫码点餐的参数，business_params、goods_detail、extend_params，具体格式详见文档下方：业务拓展参数示例
    private String jump_url; // 前台跳转地址 简易支付时必填：支付完成后，乐刷将跳转到该页面，需做UrlEncode 处理
    private String notify_url; // 通知地址 接收乐刷通知的URL，需做UrlEncode 处理，需要绝对路径，确保乐刷能正确访问，若不需要回调请忽略
    private String client_ip; // 用户IP地址 商户发起交易的IP地址
    private String body; // 商品描述 商品描述，不能包含回车换行等特殊字符
    private String shop_no; // 商户门店编号 只能是汉字、英文字母、数字
    private String pos_no; // 商户终端编号 只能是汉字、英文字母、数字
    private String attach; // 附加数据 原样返回
    private String limit_pay; // 指定支付方式 1：禁止使用信用卡；0或者不填：不限制
    private String goods_tag; // 订单优惠标记 订单优惠标记，透传给微信
    private String goods_detail; // 商品详情 按微信单品优惠券格式传递，透传给微信，请做UrlEncode
    private String order_expiration; // 订单有效时间 单位：秒（支付宝的超时时间最小粒度为分钟，建议上送的为60的整数倍）
    private String hb_fq_num; // 花呗分期数 支付宝花呗分期数，支持3、6、12期
    private String front_url; // 前端跳转地址 银联JS支付时选填，支付成功时跳转 。 front_url、front_fail_url 需要同时出现
    private String front_fail_url; // 支付失败前端跳转地址 银联JS支付时选填，支付失败时跳转。 front_url、front_fail_url 需要同时出现
    private String nonce_str; // 随机字符串
    private String sign; // 签名 MD5签名结果
    private String device_info; // 针对微信、支付宝有效，透传至银联。    微信：数字、字母、下划线格式    支付宝：目前支持非0开头的数字
    private String store_id; // 针对微信、支付宝有效，透传至银联。 微信：数字、字母、下划线格式 支付宝：目前支持非0开头的数字
    private String ass_merchant_id; // 通道商户号
    private String pnrins_id_cd; // 银联服务商标识
    private String gps_info; // gps信息
    private String bs_info; // 基站信息
    private String scene_info; // 场景信息
    private String need_receipt; // 电子发票功能

    @JacksonXmlRootElement(localName = "leshua")
    @Data
    public static class Result {
        private String respCode; // 0 - 成功，非0 - 失败。注：此字段是通信标识，是否获取到二维码要看result_code
        private String respMsg; // 错误描述：resp_code非0时返回
        private String resultCode; // 0 - 成功，非0 - 失败
        private String errorCode; // 参考错误码
        private String errorMsg; // 错误信息描述
        private String merchantId; // 由乐刷分配
        private String subMerchantId; // 微信、支付宝、QRC商户号
        private String thirdOrderId; // 商户内部订单号
        private String nonceStr; // 随机字符串
        private String sign; // MD5签名结果
        private String tdCode; // 非公众号支付返回，可直接通过该链接生成二维码扫码支付
        private String jspayInfo; // 原生公众号、服务窗、小程序、h5支付时返回，json格式字符串
        private String jspayUrl; // 简易支付时返回。重定向到该url可以完成后续支付流程。
        private String leshuaOrderId; // 乐刷订单号
        private String payWay; // 详见下方支付类型
    }
}
