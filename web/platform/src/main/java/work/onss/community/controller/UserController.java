package work.onss.community.controller;

import com.querydsl.core.types.Predicate;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.account.CustomerRepository;

import java.util.Map;

/**
 * 用户管理-列表、详情（用户、充值统计、消费统计、余额统计）、会员卡、充值记录、消费记录
 *
 * @author wangchanghao
 */
@Log4j2
@RestController
public class UserController {
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping(value = {"customers"}, name = "用户列表")
    public Page<Customer> customers(@QuerydslPredicate(bindings = CustomerRepository.class) Predicate predicate, @PageableDefault(sort = {"id", "updateDate"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return customerRepository.findAll(predicate, pageable);
    }

    @GetMapping(value = {"customers/{id}/statistics"}, name = "用户统计")
    public Map<String, Object> statistics(@PathVariable String id) {
        Customer customer = customerRepository.findById(id).orElse(new Customer());
        return Map.of("customer", customer);
    }

}

