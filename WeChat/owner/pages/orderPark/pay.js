Page({
  mixins: [require('../../mixin/common')],
  data: {
    carNumbers: [],
    orderPark:{
      carNumber:"鲁P0JR99"
    }
  },

  onLoad(options) {
    
  },

  bindCarNumberChange: function (e) {
    const carNumber = this.data.carNumbers[e.detail.value];
    this.setData({
      'orderPark.carNumber': carNumber
    })
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderParks/detail`,
        header: { authorization },
        data: { carNumber }
      }).then((data) => {
        if (data.orderPark) {
          this.setData({
            ...data
          });
        } else {
          console.log("跳转到列表页面")
          console.log(data)
        }
      });
    });
  },

  search(e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderParks/detail`,
        header: { authorization },
        data: e.detail.value
      }).then((data) => {
        if (data.orderPark) {
          this.setData({
            ...data
          });
        } else {
          console.log("跳转到列表页面")
          console.log(data)
        }
      });
    });
  },

  toPay(e) {
    const { id } = e.detail.value;
    if (id) {
      this.wxLogin().then(({ authorization }) => {
        wx.login({
          success: ({ code }) => {
            this.wxRequest({
              url: `${this.data.domain}/owner/orderParks/${id}/pay`,
              header: { authorization },
              method: 'POST',
              data: { appid, code }
            }).then((miniPayToken) => {
              wx.requestPayment({
                ...miniPayToken,
                success: (res) => {
                  wx.showModal({
                    title: '提示',
                    content: '支付成功',
                    showCancel: false,
                    success: (res) => {
                      wx.navigateBack({
                        delta: 1
                      });
                    }
                  })
                },
                fail: (res) => {
                  console.log(res);
                }
              })
            })
          },
          fail: () => {
            wx.reLaunch({
              url: '/pages/login'
            })
          },
        })
      });
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.wxLogin().then(({ authorization }) => {
      const id = this.data.orderPark?.id;
      if (id) {
        this.wxRequest({
          url: `${this.data.domain}/owner/orderParks/detail`,
          header: { authorization },
          data: { id }
        }).then((data) => {
          this.setData({
            ...data
          });
        });
      }
    });
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    this.wxLogin().then(({ authorization }) => {
      const id = this.data.orderPark?.id;
      if (id) {
        this.wxRequest({
          url: `${this.data.domain}/owner/orderParks/detail`,
          header: { authorization },
          data: { id }
        }).then((data) => {
          this.setData({
            ...data
          });
        });
      }
    });
  },
})