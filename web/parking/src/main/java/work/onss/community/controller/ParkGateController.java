package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.device.ParkGate;
import work.onss.community.domain.device.ParkGateRepository;
import work.onss.community.domain.device.QParkGate;

import java.util.Collection;

@Log4j2
@RestController
public class ParkGateController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private ParkGateRepository parkGateRepository;

    @GetMapping(value = {"parkGates/{id}"}, name = "道闸详情")
    public ParkGate detail(@PathVariable String id) {
        return parkGateRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"parkGates"}, name = "道闸列表")
    public Page<ParkGate> page(
            @RequestHeader(name = "mid") String mid,
            @QuerydslPredicate(bindings = ParkGateRepository.class) Predicate predicate, @PageableDefault Pageable pageable) {
        QParkGate qParkGate = QParkGate.parkGate;
        return parkGateRepository.findAll(qParkGate.merchantId.eq(mid).and(predicate), pageable);
    }

    @Transactional
    @PostMapping(value = {"parkGates"}, name = "道闸编辑")
    public void saveOrInsert(@RequestHeader(name = "mid") String mid, @RequestBody @Validated ParkGate parkGate) {
        QParkGate qParkGate = QParkGate.parkGate;
        jpaQueryFactory.update(qParkGate)
                .set(qParkGate.title, parkGate.getTitle())
                .set(qParkGate.type, parkGate.getType())
                .set(qParkGate.remarks, parkGate.getRemarks())
                .set(qParkGate.parkRegionId, parkGate.getParkRegionId())
                .where(qParkGate.id.eq(parkGate.getId()), qParkGate.merchantId.eq(mid))
                .execute();
    }

    @DeleteMapping(value = {"parkGates"}, name = "道闸批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        parkGateRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @PostMapping(value = "parkGates/export", name = "道闸导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = ParkGateRepository.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QParkGate qParkGate = QParkGate.parkGate;
        Iterable<ParkGate> parkGates = parkGateRepository.findAll(qParkGate.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        EasyExcel.write(httpServletResponse.getOutputStream(), ParkGate.class).sheet().doWrite((Collection<?>) parkGates);
    }

}