package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.village.HouseIdentity;
import work.onss.community.domain.village.HouseIdentityRepository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class IdentityController {
    @Autowired
    private HouseIdentityRepository houseIdentityRepository;

    @GetMapping(value = {"identitys"}, name = "入住申请列表")
    public Page<HouseIdentity> page(@QuerydslPredicate(bindings = HouseIdentityRepository.class) Predicate predicate, @PageableDefault Pageable pageable) {
        return houseIdentityRepository.findAll(predicate, pageable);
    }

    @PostMapping(value = {"identitys/export"}, name = "入住申请导出")
    public void export(@QuerydslPredicate(bindings = HouseIdentityRepository.class, root = HouseIdentity.class) Predicate predicate, HttpServletResponse httpServletResponse) throws Exception {
        Collection<HouseIdentity> data = (Collection<HouseIdentity>) houseIdentityRepository.findAll(predicate);
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), HouseIdentity.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        Map<String, List<HouseIdentity>> floorNumberData = data.stream().collect(Collectors.groupingBy(HouseIdentity::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}