package work.onss.community.domain.village;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

public interface HouseVacantRepository extends JpaRepository<HouseVacant, String>, JpaSpecificationExecutor<HouseVacant>, QuerydslPredicateExecutor<HouseVacant>, QuerydslBinderCustomizer<QHouseVacant> {

    @Modifying
    @Transactional
    void deleteByMerchantIdAndIdIn(String vid, Collection<String> id);

    default void customize(QuerydslBindings bindings, QHouseVacant qHouseVacant) {
        bindings.bind(qHouseVacant.id).withDefaultBinding();
        bindings.bind(qHouseVacant.startDate).all(((path, value) -> {
            Iterator<? extends LocalDate> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
        bindings.bind(qHouseVacant.endDate).all(((path, value) -> {
            Iterator<? extends LocalDate> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
    }

}
