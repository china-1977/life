package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.village.HouseRenting;
import work.onss.community.domain.village.HouseRentingRepository;
import work.onss.community.domain.village.QHouseRenting;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class HouseRentingController {
    @Autowired
    private HouseRentingRepository houseRentingRepository;

    @GetMapping(value = {"houseRentings"}, name = "房屋租售列表")
    public Page<HouseRenting> page(@RequestHeader(name = "mid") String mid, @QuerydslPredicate(bindings = HouseRentingRepository.class) Predicate predicate, @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QHouseRenting qHouseRenting = QHouseRenting.houseRenting;
        return houseRentingRepository.findAll(qHouseRenting.merchantId.eq(mid).and(predicate), pageable);
    }

    @PostMapping(value = {"houseRentings/export"}, name = "房屋租售导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = HouseRentingRepository.class, root = HouseRenting.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QHouseRenting qHouseRenting = QHouseRenting.houseRenting;
        Collection<HouseRenting> data = (Collection<HouseRenting>) houseRentingRepository.findAll(qHouseRenting.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), HouseRenting.class).build();
        Map<String, List<HouseRenting>> floorNumberData = data.stream().collect(Collectors.groupingBy(HouseRenting::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}