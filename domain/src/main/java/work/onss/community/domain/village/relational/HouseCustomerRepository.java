package work.onss.community.domain.village.relational;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.util.Collection;
import java.util.Optional;

public interface HouseCustomerRepository extends JpaRepository<HouseCustomer, String>, JpaSpecificationExecutor<HouseCustomer>, QuerydslPredicateExecutor<HouseCustomer>, QuerydslBinderCustomizer<QHouseCustomer> {

    default void customize(QuerydslBindings bindings, QHouseCustomer qHouseCustomer) {
        bindings.bind(qHouseCustomer.id).withDefaultBinding();
    }

    @Modifying
    @Transactional
    void deleteByMerchantIdAndIdIn(String vid, Collection<String> id);

    @Modifying
    @Transactional
    void deleteByCustomerIdAndHouseIdIn(String customerId, Collection<String> houseIds);

    @Modifying
    @Transactional
    void deleteByMerchantIdAndHouseIdAndCustomerId(String merchantId, String houseId, String customerId);

    Optional<HouseCustomer> findByMerchantIdAndHouseIdAndCustomerId(String merchantId, String houseId, String customerId);
}
