package work.onss.community.domain.vo;

import lombok.Getter;

import java.io.Serializable;

@Getter
public enum Code implements Serializable {
    SUCCESS("操作成功"),
    FAIL("操作失败"),
    SESSION_EXPIRE("请重新登录"),
    MERCHANT_NOT_BIND("请绑定主体"),
    DO_NOT_REAL_NAME("请完善个人信息");
    private final String value;

    Code(String value) {
        this.value = value;
    }

}
