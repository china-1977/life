const categories = ['VILLAGE']
Page({
  mixins: [require('../../mixin/common')],
  data: {
    relation: undefined,
    index: -1,
    villages: [],
    point: {},
    housesPicker: [],
  },

  chooseLocation: function () {
    wx.authorize({
      scope: 'scope.userLocation',
      success: (res) => {
        wx.chooseLocation({
          latitude: this.data.point.latitude,
          longitude: this.data.point.longitude,
          success: (point) => {
            this.setData({ point });
            const url = `${this.data.domain}/account/merchants/${point.longitude}-${point.latitude}/near?categories=${categories}`;
            this.wxRequest({ url }).then((villages) => {
              this.setData({
                villages
              })
            })
          },
          fail: (res) => {
            wx.getLocation({
              type: 'gcj02',
              success: (res) => {
                const point = {
                  address: '默认地址',
                  latitude: res.latitude,
                  longitude: res.longitude,
                  name: '默认地址',
                };
                this.setData({ point });
                const url = `${this.data.domain}/account/merchants/${point.longitude}-${point.latitude}/near?categories=${categories}`;
                this.wxRequest({ url }).then((villages) => {
                  this.setData({
                    villages
                  })
                })
              }
            })
          }
        })
      },
      fail: (res) => {
        wx.showModal({
          title: '提示',
          content: '请允许小程序使用位置消息',
          confirmColor: '#e64340',
          showCancel: false,
          success: () => {
            wx.openSetting();
          }
        })
      }
    })
  },

  villageChange: function (e) {
    this.setData({
      index: e.detail.value
    });
    const village = this.data.villages[e.detail.value];
    if (village) {
      this.wxLogin().then(({ authorization }) => {
        this.wxRequest({
          url: `${this.data.domain}/account/merchants/${village.id}/allHouse`,
          header: { authorization },
        }).then((c0) => {
          if (c0) {
            const c1 = c0[0].children;
            this.setData({
              dataTree: c0,
              housesPicker: [c0, c1, []]
            });
          }
        });
      });
    }
  },

  hoseColumnChange: function (e) {
    let c0;
    let c1;
    let c2
    switch (e.detail.column) {
      case 0:
        c0 = this.data?.housesPicker[0];
        c1 = c0[e.detail.value]?.children;
        if (c1) {
          this.setData({
            ['housesPicker[1]']: c1,
            ['housesPicker[2]']: [],
          })
        }
        break;
      case 1:
        c1 = this.data?.housesPicker[1];
        c2 = c1[e.detail.value]?.children;
        if (c2) {
          this.setData({
            [`housesPicker[2]`]: c2
          })
        }
        break;
      default:
        break;
    }
  },

  hoseChange: function (e) {
    const housesPicker = this.data.housesPicker;
    if (housesPicker.length > 0) {
      const housesIndex = e.detail.value;
      let houseKeyword;
      if (housesPicker[2]) {
        houseKeyword = housesPicker[2][housesIndex[2]]?.value
      } else if (housesPicker[1]) {
        houseKeyword = housesPicker[1][housesIndex[1]]?.value
      } else {
        houseKeyword = housesPicker[0][housesIndex[0]]?.value
      }
      this.setData({
        houseKeyword
      });
    }
  },

  bindingHouse: function (e) {
    this.wxLogin().then(({ authorization }) => {
      const { houseKeyword, relation, index, villages } = this.data;
      const keyword = houseKeyword.split(",");
      if (keyword.length == 3 && relation && index >= 0) {
        wx.showModal({
          title: '提示',
          content: '是否申请入住？',
          success: (res) => {
            if (res.confirm) {
              this.wxRequest({
                url: `${this.data.domain}/owner/houses/binding`,
                method: 'POST',
                header: { authorization },
                data: { merchantId: villages[index].id, floorNumber: keyword[0], unit: keyword[1], roomNumber: keyword[2], relation }
              }).then((houseIdentity) => {
                wx.showModal({
                  title: '提示',
                  content: '申请成功',
                  showCancel: false,
                  success: (res) => {
                    wx.navigateBack({
                      delta: 2
                    });
                  }
                })
              });
            }
          }
        })
      } else {
        wx.showModal({
          title: '提示',
          content: '请完善申请信息',
          showCancel: false,
        })
      }
    });
  },

  bindRelationChange: function (e) {
    this.setData({
      'relation': this.data.relations[e.detail.value]
    })
  },
})