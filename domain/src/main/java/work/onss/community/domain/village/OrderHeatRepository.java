package work.onss.community.domain.village;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

public interface OrderHeatRepository extends JpaRepository<OrderHeat, String>, JpaSpecificationExecutor<OrderHeat>, QuerydslPredicateExecutor<OrderHeat>, QuerydslBinderCustomizer<QOrderHeat> {

    default void customize(QuerydslBindings bindings, QOrderHeat qOrderHeat) {
        bindings.bind(qOrderHeat.id).withDefaultBinding();
        bindings.bind(qOrderHeat.payDatetime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
    }

    @Modifying
    @Transactional
    void deleteByMerchantIdAndIdIn(String vid, Collection<String> id);
}
