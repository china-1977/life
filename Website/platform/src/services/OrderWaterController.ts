import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/platform/orderWaters', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export interface OrderWater {
  id: string;
  code: string;
  merchantId: string;
  floorNumber: string;
  unit: string;
  roomNumber: string;
  waterPrice: number;
  waterInitial: number;
  waterCurrent: number;
  waterCount: number;
  waterRatio: number;
  waterLoss: number;
  waterTotal: number;
  type: string;
  remarks: string;
  startDate: any;
  endDate: any;
  status: string;
  payDatetime: any;
  updateDatetime: any;
}
