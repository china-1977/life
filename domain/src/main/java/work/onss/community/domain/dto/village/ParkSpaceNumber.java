package work.onss.community.domain.dto.village;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParkSpaceNumber implements Serializable {
    private String houseId;
    private Integer size;
}
