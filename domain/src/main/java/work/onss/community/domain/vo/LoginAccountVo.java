package work.onss.community.domain.vo;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.Set;

@Data
public class LoginAccountVo {
    @NotBlank(message = "请输入登录账户")
    private String username;
    private Set<String> password;
}
