package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.Advice;
import work.onss.community.domain.village.AdviceRepository;
import work.onss.community.domain.village.QAdvice;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Log4j2
@RestController
public class AdviceController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private AdviceRepository adviceRepository;

    @GetMapping(value = {"advices/{id}"}, name = "意见详情")
    public Advice detail(@RequestHeader("cid") String cid, @PathVariable String id) {
        QAdvice qAdvice = QAdvice.advice;
        return adviceRepository.findOne(qAdvice.id.eq(id).and(qAdvice.customerId.eq(cid))).orElse(null);
    }

    @GetMapping(value = {"advices"}, name = "意见列表")
    public Page<Advice> page(@RequestHeader("mid") String mid,
                             @QuerydslPredicate(bindings = AdviceRepository.class) Predicate predicate,
                             @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QAdvice qAdvice = QAdvice.advice;
        return adviceRepository.findAll(qAdvice.merchantId.eq(mid).and(predicate), pageable);
    }

    @Transactional
    @PostMapping(value = {"advices"}, name = "意见编辑")
    public void save(@RequestHeader("mid") String mid, @RequestBody Advice advice) {
        QAdvice qAdvice = QAdvice.advice;
        jpaQueryFactory.update(qAdvice)
                .set(qAdvice.remark, advice.getRemark())
                .set(qAdvice.status, advice.getStatus())
                .set(qAdvice.updateDatetime, LocalDateTime.now())
                .where(qAdvice.merchantId.eq(mid).and(qAdvice.id.eq(advice.getId())))
                .execute();
    }

    @Transactional
    @PostMapping(value = {"advices/{status}/setStatus"}, name = "意见审核")
    public void setStatus(@RequestHeader("mid") String mid, @PathVariable Advice.Status status, @RequestBody List<String> ids) {
        QAdvice qAdvice = QAdvice.advice;
        jpaQueryFactory.update(qAdvice)
                .set(qAdvice.status, status)
                .set(qAdvice.updateDatetime, LocalDateTime.now())
                .where(qAdvice.merchantId.eq(mid).and(qAdvice.id.in(ids)))
                .execute();
    }

    @PostMapping(value = {"advices/export"}, name = "意见导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = AdviceRepository.class, root = Advice.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QAdvice qAdvice = QAdvice.advice;
        Collection<Advice> data = (Collection<Advice>) adviceRepository.findAll(qAdvice.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), Advice.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        excelWriter.finish();
    }
}