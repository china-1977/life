Page({
  mixins: [require('../../mixin/common')],
  data: {
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/houseIdentitys/${options.id}`,
        header: { authorization },
      }).then((houseIdentity) => {
        this.setData({
          houseIdentity
        });
      });
    });
  },

  updateStatus: function (e) {
    this.wxLogin().then(({ authorization }) => {
      const status = e.currentTarget.id;
      let content = '是否同意入住？'
      if (status == 'NO') {
        content: '是否拒绝入住？'
      }
      wx.showModal({
        title: '提示',
        content,
        success: (res) => {
          if (res.confirm) {
            this.wxRequest({
              url: `${this.data.domain}/owner/houseIdentitys/${status}/setStatus`,
              method: 'POST',
              header: { authorization },
              data: [this.data.houseIdentity.id]
            }).then(() => {
              wx.showModal({
                title: '提示',
                content: '操作成功',
                showCancel: false,
                success: () => {
                  wx.navigateBack({
                    delta: 1
                  });
                }
              })
            });
          }
        }
      })
    });
  },
})