const fs = wx.getFileSystemManager()
module.exports = {
  data: {
    domain: 'http://127.0.0.1:7000',
    appid: "wxe78290c2a5313de3",
    size: 30,
    number: 1,
    page: 1,
    year: new Date().getFullYear(),
    MerchantCategory: {
      range: [
        { key: 'VILLAGE', value: '物业' },
        { key: 'STORE', value: '零售商' },
        { key: 'PARKING', value: '停车场' }
      ],
      data: {
        VILLAGE: "物业",
        STORE: "零售商",
        PARKING: "停车场",
      }
    },
    orderProductWay: {
      DDZX: "到店自选",
      MDBG: "门店帮购",
      PSSM: "配送上门",
    },
    orderProductStatus: {
      WAIT_PAY: "待支付",
      FINISH_PAY: "已完成",
      REFUND_SUCCESS: "退款成功",
      REFUND_CLOSED: "退款关闭",
      REFUND_PROCESSING: "退款处理中",
      REFUND_ABNORMAL: "退款异常"
    },
    rentingMode: ['置换', '整租', '出售', '合租', '公寓'],
    relations: ['房东', '租户', '员工'],
    AdviceStatus: {
      range: [
        { key: 'WAIT', value: '待处理' },
        { key: 'CLOSED', value: '已关闭' },
        { key: 'ERROR', value: '未解决' },
        { key: 'SUCCESS', value: '已解决' }
      ],
      data: {
        WAIT: '待处理',
        CLOSED: '已关闭',
        ERROR: '未解决',
        SUCCESS: '已解决'
      },
    },
    ParkSpaceIdentityStatus: {
      range: [
        { key: 'WAIT', value: '待处理' },
        { key: 'YES', value: '已通过' },
        { key: 'NO', value: '已驳回' },
        { key: 'FINISHED', value: '已结束' }
      ],
      data: {
        WAIT: '待处理',
        YES: '已通过',
        NO: '已驳回',
        FINISHED: '已结束'
      },
    },
    PayOrderstatus: {
      range: [
        { key: 'WAIT_CONFIRM', value: '待确认' },
        { key: 'WAIT_PAY', value: '待支付' },
        { key: 'FINISH', value: '已支付' }
      ],
      data: {
        WAIT_CONFIRM: '待确认',
        WAIT_PAY: '待支付',
        FINISH: '已支付'
      },
    },
    HouseIdentityStatus: {
      range: [
        { key: 'WAIT', value: '待处理' },
        { key: 'YES', value: '已通过' },
        { key: 'NO', value: '已驳回' },
      ],
      data: {
        WAIT: '待处理',
        YES: '已通过',
        NO: '已驳回',
      },
    },
  },

  wxLogin() {
    return new Promise((resolve) => {
      const authorization = wx.getStorageSync('authorization');
      if (authorization) {
        const info = wx.getStorageSync('info');
        if (info.cid) {
          const seonds = new Date(info.lastTime).getTime();
          const now = Date.now();
          const seond = (now - seonds) / 1000;
          if (seond > 18000) {
            wx.reLaunch({
              url: '/pages/login'
            })
          } else {
            resolve({ authorization, info });
          }
        } else if (info.openid) {
          wx.navigateTo({
            url: `/pages/user/list?openid=${info.openid}&authorization=${authorization}`
          })
        } else {
          wx.reLaunch({
            url: '/pages/login'
          })
        }
      } else {
        wx.reLaunch({
          url: '/pages/login'
        })
      }
    })
  },

  wxRequest({ url, data = {}, dataType = 'json', header, method = 'GET', responseType = 'text', timeout = 0 }) {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    return new Promise((resolve) => {
      wx.request({
        url,
        data,
        dataType,
        method,
        responseType,
        timeout,
        header: { 'Content-Type': 'application/json;charset=UTF-8', ...header },
        success: ({ data, statusCode }) => {
          if (statusCode === 200) {
            resolve(data);
          } else {
            const { code, message, content } = data;
            switch (code) {
              case 'SESSION_EXPIRE':
                wx.removeStorageSync('authorization');
                wx.removeStorageSync('info');
                wx.reLaunch({
                  url: '/pages/login'
                })
                break;
              case 'NOT_REGISTER_CUSTOMER':
                wx.reLaunch({
                  url: '/pages/login'
                })
                break;
              case 'DO_NOT_REAL_NAME':
                wx.showModal({
                  title: '警告',
                  content: message,
                  confirmColor: '#e64340',
                  showCancel: false,
                  success: () => {
                    wx.redirectTo({
                      url: '/pages/setting/idCard'
                    })
                  }
                })
                break;
              default:
                wx.showModal({
                  title: '警告',
                  content: message,
                  confirmColor: '#e64340',
                  showCancel: false,
                })
                break;
            }
          }
        },
        fail: (data) => {
          console.log(data);
          wx.hideLoading()
          const { code, message, content } = data;
          switch (code) {
            case 'SESSION_EXPIRE':
              wx.removeStorageSync('authorization');
              wx.removeStorageSync('info');
              wx.reLaunch({
                url: '/pages/login'
              })
              break;
            default:
              wx.showModal({
                title: '警告',
                content: message,
                confirmColor: '#e64340',
                showCancel: false,
              })
              break;
          }
        },
        complete: (res) => {
          wx.hideLoading()
        },
      })
    })
  },

  chooseImageToBase64(camera) {
    return new Promise((resolve, reject) => {
      wx.chooseMedia({
        count: 1,
        mediaType: ['image'],
        sizeType: ['compressed'],
        sourceType: ['album', 'camera'],
        camera,
        success: res => {
          fs.readFile({
            filePath: res.tempFiles[0].tempFilePath,
            encoding: 'base64',
            success: (res) => {
              resolve(`data:image/jpeg;base64,${res.data}`)
            },
            fail: (res) => {
              wx.showModal({
                title: '警告',
                content: '上传失败',
                confirmColor: '#e64340',
                showCancel: false,
              })
            }
          })
        }
      })
    });
  },

  chooseImagesToBase64(camera, count) {
    return new Promise((resolve, reject) => {
      wx.chooseMedia({
        count: count,
        mediaType: ['image'],
        sizeType: ['compressed'],
        sourceType: ['album', 'camera'],
        camera,
        success: res => {
          const data = res.tempFiles.map((value) => {
            const file = fs.readFileSync(value.tempFilePath, 'base64');
            return `data:image/jpeg;base64,${file}`;
          })
          resolve(data);
        }
      })
    })
  }
};
