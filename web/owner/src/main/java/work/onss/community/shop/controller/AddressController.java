package work.onss.community.shop.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.shop.Address;
import work.onss.community.domain.shop.AddressRepository;
import work.onss.community.service.exception.BusinessException;

import java.util.List;

@Log4j2
@RestController
public class AddressController {

    @Autowired
    protected AddressRepository addressRepository;

    /**
     * @param cid     用户ID
     * @param address 编辑内容
     * @return 最新收货地址内容
     */
    @PostMapping(value = {"addresses"})
    public Address saveOrInsert(@RequestHeader(name = "cid") String cid, @RequestBody @Validated Address address) {
        address.setCustomerId(cid);
        if (address.getId() == null) {
            addressRepository.save(address);
        } else {
            Address oldAddress = addressRepository.findByIdAndCustomerId(address.getId(), cid).orElseThrow(() -> new BusinessException("FAIL", "该数据不存在,请联系客服", address));
            address.setInsertDate(oldAddress.getInsertDate());
            addressRepository.save(address);
        }
        return address;
    }

    /**
     * @param cid 用户ID
     * @param id  主键
     */
    @DeleteMapping(value = {"addresses/{id}"})
    public void delete(@RequestHeader(name = "cid") String cid, @PathVariable String id) {
        addressRepository.deleteByIdAndCustomerId(id, cid);
    }

    /**
     * @param cid 用户ID
     * @param id  主键
     * @return 收货地址
     */
    @GetMapping(value = {"addresses/{id}"})
    public Address findOne(@RequestHeader(name = "cid") String cid, @PathVariable String id) {
        return addressRepository.findByIdAndCustomerId(id, cid).orElse(null);
    }

    /**
     * @param cid 用户ID
     * @return 所有收货地址
     */
    @GetMapping(value = {"addresses"})
    public List<Address> findAll(@RequestHeader(name = "cid") String cid) {
        return addressRepository.findByCustomerIdOrderByUpdateDate(cid);
    }
}
