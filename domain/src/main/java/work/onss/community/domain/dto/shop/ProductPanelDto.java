package work.onss.community.domain.dto.shop;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.shop.Product;
import work.onss.community.domain.village.ProductPanel;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductPanelDto {
    private Merchant merchant;
    private Product product;
    private ProductPanel productPanel;
}
