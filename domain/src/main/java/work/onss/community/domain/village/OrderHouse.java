package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.converters.OrderStatusConverter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class OrderHouse implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    @ExcelProperty(value = "编号")
    private String code;
    private String merchantId;
    @ExcelProperty(value = "房屋ID")
    private String houseId;
    @ExcelProperty(value = "楼号")
    private String floorNumber;
    @ExcelProperty(value = "单元")
    private String unit;
    @ExcelProperty(value = "室")
    private String roomNumber;
    @ExcelProperty(value = "类型")
    private String type;

    @ExcelProperty(value = "面积")
    private BigDecimal area;
    @ExcelProperty(value = "物业费")
    private BigDecimal propertyTotal;
    @ExcelProperty(value = "垃圾费")
    private BigDecimal rubbishPrice;
    @ExcelProperty(value = "车位数量")
    private Integer parkSpaceCount;
    @ExcelProperty(value = "车位管理费")
    private BigDecimal parkSpaceTotal;

    @ExcelProperty(value = "合计")
    private BigDecimal total;
    @ExcelProperty(value = "备注")
    private String remarks;

    @ExcelProperty(value = "开始日期")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;
    @ExcelProperty(value = "结束日期")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;
    @ExcelProperty(value = "月数")
    private Long monthCount;
    @Enumerated(EnumType.STRING)
    @ExcelProperty(value = "支付状态", converter = OrderStatusConverter.class)
    private PayOrder.Status status;
    @ExcelProperty(value = "支付时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime payDatetime;
    @ExcelProperty(value = "通知时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime noticeDatetime;
    @ExcelProperty(value = "创建时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;


    public OrderHouse(House house) {
        this.houseId = house.getId();
        this.floorNumber = house.getFloorNumber();
        this.unit = house.getUnit();
        this.roomNumber = house.getRoomNumber();
        this.type = house.getType();
        this.area = house.getArea();
        this.parkSpaceCount = house.getParkSpaceCount();
        this.status = PayOrder.Status.WAIT_CONFIRM;
    }
}
