import { wxLogin, domain, wxRequest, chooseImagesToBase64 } from '../../utils/util.js';
Page({
  data: {
    domain, pictures: []
  },

  chooseImages: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count
    const pictures = this.data[id] == null ? [] : this.data[id];
    count = count - pictures.length;
    chooseImagesToBase64('front', count).then((files) => {
      this.setData({
        [`${id}`]: [...pictures, ...files]
      })
    })
  },

  deletePictures: function (e) {
    const id = e.currentTarget.id;
    const index = e.currentTarget.dataset.index;
    const files = this.data[id];
    files.splice(index, 1);
    this.setData({
      [id]: files
    })
  },
  clearPictues: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: []
    })
  },

  textareaInput: function (e) {
    const id = e.currentTarget.id;
    const value = e.detail.value;
    this.setData({
      [id]: value
    })
  },

  bindInput: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count;
    const value = e.detail.value;
    if (value.length === count) {
      this.setData({
        [id]: value
      })
    }
  },

  createProduct: function (e) {
    const { description, pictures, id } = this.data;
    const data = { ...e.detail.value, description, pictures, id }
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/store/products`,
        data,
        method: "POST",
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((data) => {
        this.setData({
          ...data
        });
        let pages = getCurrentPages();
        let prevPage = pages[pages.length - 2];
        let { products = [] } = prevPage.data;
        products = [data, ...products];
        prevPage.setData({
          products
        });
        wx.navigateBack({
          delta: 1
        });
      })
    })
  },

  resetForm: function (e) {
    this.setData({
      pictures: [],
      vid: null,
      description: ''
    })
  },
})
