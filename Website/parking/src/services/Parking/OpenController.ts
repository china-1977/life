import { request } from '@@/exports';

export const getParkRegion = (params: { keyWords: any; id: any }) => request<any>('/parking/parkRegions/getParkRegion', { params });
export const geCustomerByKeywords = (params: { keyWords: any; id: any }) => request<any>('/parking/customers/geCustomerByKeywords', { params });