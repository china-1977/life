package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.converters.RepairStatusConverter;
import work.onss.community.domain.usertype.StringArrayType;

import java.io.Serializable;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
@EntityListeners(AuditingEntityListener.class)
public class Repair implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    @ExcelProperty(value = "用户ID")
    private String customerId;
    @ExcelProperty(value = "姓名")
    private String name;
    @ExcelProperty(value = "电话")
    private String phone;
    @ExcelProperty(value = "地址")
    private String address;
    @ExcelProperty(value = "标题")
    private String title;
    @ExcelProperty(value = "描述")
    private String description;
    @Type(value = StringArrayType.class)
    private String[] descriptionPictures;

    @Enumerated(EnumType.STRING)
    @ExcelProperty(value = "状态", converter = RepairStatusConverter.class)
    private Repair.Status status;
    @ExcelProperty(value = "标签")
    private String label;
    @ExcelProperty(value = "维修者ID")
    private String repairerCustomerId;
    @ExcelProperty(value = "维修者-姓名")
    private String repairerName;
    @ExcelProperty(value = "维修者-电话")
    private String repairerPhone;

    @ExcelProperty(value = "主体ID")
    private String merchantId;
    @ExcelProperty(value = "主体简称")
    private String merchantShortname;
    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ExcelProperty(value = "创建时间")
    private LocalDateTime insertDatetime;
    @LastModifiedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ExcelProperty(value = "更新时间")
    private LocalDateTime updateDatetime;

    @Getter
    public enum Status implements Serializable {
        WAIT_ALLOT("待分配"),
        WAIT_REPAIR("待维修"),
        FINISHED("已完成");
        private final String value;

        Status(String value) {
            this.value = value;
        }
    }
}
