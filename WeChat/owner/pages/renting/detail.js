

Page({
  mixins: [require('../../mixin/common')],
  data: {
  },

  onLoad(options) {
    const info = wx.getStorageSync('info');
    this.wxRequest({
      url: `${this.data.domain}/owner/houseRentings/${options.id}`,
    }).then((houseRenting) => {
      this.setData({
        houseRenting, info
      });
    });
  },

  call: function (e) {
    wx.makePhoneCall({
      phoneNumber: this.data.houseRenting.phone
    })
  },

  delete: function (e) {
    this.wxLogin().then(({ authorization }) => {
      wx.showModal({
        title: '提示',
        content: '是否删除？',
        success: (res) => {
          if (res.confirm) {
            this.wxRequest({
              url: `${this.data.domain}/owner/houses/deleteRentings`,
              header: { authorization },
              data: [this.data.houseRenting.id],
              method: "POST",
            }).then(() => {
              wx.showModal({
                title: '提示',
                content: '删除成功',
                showCancel: false,
                success: (res) => {
                  wx.navigateBack({
                    delta: 2
                  });
                }
              })
            })
          }
        }
      })
    });
  }
})