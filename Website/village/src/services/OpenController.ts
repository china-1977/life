import { request } from '@@/exports';

export const getParkRegion = (params: { keyWords: any; id: any }) => request<any>('/village/parkRegions/getParkRegion', { params });
export const geCustomerByKeywords = (params: { keyWords: any; id: any }) => request<any>('/village/customers/geCustomerByKeywords', { params });