Page({
  mixins: [require('../../../mixin/common')],
  data: {
  },

  onLoad: function (options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/account/merchants/${options.id}`,
        header: { authorization },
      }).then((data) => {
        this.setData({
          ...options,
          store: data
        });
      });
    })
  },

  makePhoneCall: function (e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone
    })
  },

  openLocation: function (e) {
    const { location, addressName } = this.data.store;
    wx.openLocation({
      latitude: parseFloat(location.y),
      longitude: parseFloat(location.x),
      name: addressName
    })
  },
})