package work.onss.community.domain.shop;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMin;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

/**
 * 购物车
 *
 * @author wangchanghao
 */
@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@EntityListeners(AuditingEntityListener.class)
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class Cart implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String customerId;
    private String merchantId;
    private String productId;
    @DecimalMin(value = "0", message = "购买数量不能小于{value}")
    private BigInteger num;
    @NumberFormat(pattern = "#.00", style = NumberFormat.Style.CURRENCY)
    private BigDecimal total = BigDecimal.ZERO;
    private Boolean checked = true;
    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate insertDate;
    @LastModifiedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate updateDate;
}
