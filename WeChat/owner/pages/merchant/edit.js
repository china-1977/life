Page({
  mixins: [require('../../mixin/common')],
  data: {
    videos: [], pictures: [], openTime: '07:30', closeTime: '22:30'
  },
  bindPickerChange: function (e) {
    const id = e.currentTarget.id;
    const index = e.currentTarget.dataset.index;
    const range = e.currentTarget.dataset.range;
    const ranges = this.data[range];
    const value = e.detail.value;
    this.setData({
      [id]: ranges[value].id,
      [index]: value
    })
  },
  bindRegionChange: function (e) {
    this.setData({
      ['addressCode']: e.detail.code,
      ['postcode']: e.detail.postcode,
      ['addressValue']: e.detail.value
    });
  },
  chooseLocation: function (e) {
    const { location, addressName } = this.data;
    if (location && location.x && location.y) {
      wx.chooseLocation({
        longitude: location.x,
        latitude: location.y,
        name: addressName,
        success: (res) => {
          this.setData({
            ['location']: { x: res.longitude, y: res.latitude },
            ['addressDetail']: res.address,
            ['addressName']: res.name
          });
        }
      })
    } else {
      wx.getLocation({
        type: 'gcj02',
        success: (res) => {
          wx.chooseLocation({
            longitude: parseFloat(res.longitude),
            latitude: parseFloat(res.latitude),
            success: (res) => {
              this.setData({
                ['location']: { x: res.longitude, y: res.latitude },
                ['addressDetail']: res.address,
                ['addressName']: res.name
              });
            }
          })
        }
      });
    }
  },

  chooseImages: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count
    const pictures = this.data[id] == null ? [] : this.data[id];
    count = count - pictures.length;
    this.chooseImagesToBase64('front', count).then((files) => {
      this.setData({
        [`${id}`]: [...pictures, ...files]
      })
    })
  },

  deletePictures: function (e) {
    const id = e.currentTarget.id;
    const index = e.currentTarget.dataset.index;
    const files = this.data[id];
    files.splice(index, 1);
    this.setData({
      [id]: files
    })
  },

  chooseImage: function (e) {
    const id = e.currentTarget.id;
    this.chooseImageToBase64('front').then((file) => {
      this.setData({
        [`${id}`]: file
      })
    })
  },

  deletePicture: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: null
    })
  },

  clearPictues: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: []
    })
  },
  videosBindInput: function (e) {
    this.setData({
      videos: e.detail.value.split(",")
    })
  },

  updateMerchant: function (e) {
    const { addressValue, addressCode, location, postcode, pictures, videos, merchant, trademark } = this.data;
    const data = { ...e.detail.value, addressValue, addressCode, location, postcode, pictures, videos, trademark }
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/account/merchants/${merchant.id}`,
        data,
        method: "PUT",
        header: { authorization },
      }).then(() => {
        wx.showModal({
          title: '提示',
          content: '编辑成功',
          showCancel: false,
          success: (res) => {
            wx.navigateBack({
              delta: 2
            });
          }
        })
      })
    })
  },

  onLoad: function (params) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/account/merchants/${params.id}`,
        header: { authorization },
      }).then((merchant) => {
        this.setData({
          ...merchant, merchant
        })
      })
    })
  },

  resetForm: function (e) {
    const merchant = this.data.merchant;
    this.setData({
      ...merchant
    })
  },

  timeChange: function (e) {
    const key = e.currentTarget.id;
    const value = e.detail.value;
    this.setData({
      [key]: value
    });
  }
})