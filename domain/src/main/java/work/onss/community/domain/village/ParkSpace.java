package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class ParkSpace implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    private String merchantId;
    @ExcelProperty(value = "区域")
    private String region;
    @ExcelProperty(value = "编号")
    private String code;
    @ExcelProperty(value = "设备状态")
    private String state;
    @ExcelProperty(value = "使用状态")
    private String status;
    @ExcelProperty(value = "房屋ID")
    private String houseId;
    @ExcelProperty(value = "车牌号")
    private String carCode;
    @ExcelProperty(value = "备注")
    private String remarks;

    @Transient
    @ExcelProperty(value = "楼号")
    private String floorNumber;

    public ParkSpace(ParkSpace parkSpace, String floorNumber) {
        this.id = parkSpace.id;
        this.merchantId = parkSpace.merchantId;
        this.region = parkSpace.region;
        this.code = parkSpace.code;
        this.state = parkSpace.state;
        this.status = parkSpace.status;
        this.houseId = parkSpace.houseId;
        this.carCode = parkSpace.carCode;
        this.remarks = parkSpace.remarks;
        this.floorNumber = floorNumber == null ? "无" : floorNumber;
    }
}
