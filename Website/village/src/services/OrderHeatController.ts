import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/village/orderHeats', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const save = async (body?: any) =>
  request<any>('/village/orderHeats', {
    method: 'POST',
    data: body,
  });

export const del = async (body?: any) =>
  request<any>('/village/orderHeats', {
    method: 'DELETE',
    data: body,
  });

export interface OrderHeat {
  id: string;
  code: string;
  merchantId: string;
  floorNumber: string;
  unit: string;
  roomNumber: string;
  area: number;

  heatPrice: number;
  heatTotal: number;

  waterRatio: number;
  waterLoss: number;
  waterTotal: number;

  type: string;
  remarks: string;
  startDate: any;
  endDate: any;
  status: string;
  payDatetime: any;
  updateDatetime: any;

  prepayId: string;
  outTradeNo: string;
}
