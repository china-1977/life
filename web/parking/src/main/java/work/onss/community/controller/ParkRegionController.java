package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.park.ParkRegion;
import work.onss.community.domain.park.ParkRegionRepository;
import work.onss.community.domain.park.QParkRegion;

import java.time.LocalDateTime;
import java.util.Collection;

@Log4j2
@RestController
public class ParkRegionController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private ParkRegionRepository parkRegionRepository;

    @GetMapping(value = {"parkRegions/{id}"}, name = "停车区域详情")
    public ParkRegion detail(@PathVariable String id) {
        return parkRegionRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"parkRegions"}, name = "停车区域列表")
    public Page<ParkRegion> page(
            @RequestHeader(name = "mid") String mid,
            @QuerydslPredicate(bindings = ParkRegionRepository.class) Predicate predicate, @PageableDefault Pageable pageable) {
        QParkRegion qParkRegion = QParkRegion.parkRegion;
        return parkRegionRepository.findAll(qParkRegion.merchantId.eq(mid).and(predicate), pageable);
    }

    @Transactional
    @PostMapping(value = {"parkRegions"}, name = "停车区域编辑")
    public void saveOrInsert(@RequestHeader(name = "mid") String mid, @RequestBody ParkRegion parkRegion) {
        parkRegion.setMerchantId(mid);
        if (parkRegion.getId() == null) {
            parkRegionRepository.save(parkRegion);
        } else {
            QParkRegion qParkRegion = QParkRegion.parkRegion;
            jpaQueryFactory.update(qParkRegion)
                    .set(qParkRegion.name, parkRegion.getName())
                    .set(qParkRegion.formula, parkRegion.getFormula())
                    .set(qParkRegion.price, parkRegion.getPrice())
                    .set(qParkRegion.freeTime, parkRegion.getFreeTime())
                    .set(qParkRegion.updateDatetime, LocalDateTime.now())
                    .where(qParkRegion.merchantId.eq(mid), qParkRegion.id.eq(parkRegion.getId()))
                    .execute();
        }
    }

    @DeleteMapping(value = {"parkRegions"}, name = "停车区域批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        parkRegionRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @PostMapping(value = "parkRegions/export", name = "停车区域导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = ParkRegionRepository.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {

        QParkRegion qParkRegion = QParkRegion.parkRegion;
        Iterable<ParkRegion> parkRegions = parkRegionRepository.findAll(qParkRegion.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        EasyExcel.write(httpServletResponse.getOutputStream(), ParkRegion.class).sheet().doWrite((Collection<?>) parkRegions);
    }

}