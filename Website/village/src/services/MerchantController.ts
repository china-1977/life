import { request } from '@@/exports';

export const getMerchants = (params: any) =>
  request<any>('/account/bindings/getMerchants', {
    params,
  }).then((res: any) => {
    return { data: res };
  });

export const bind = (id: any) =>
  request<any>(`/account/bindings/${id}`, {
    method: 'POST',
  });
