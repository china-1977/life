package work.onss.community.config;

import lombok.Data;

import java.io.Serializable;

/**
 * 车牌信息
 * 实现这个接口可以实现对比
 */
@Data
public class LicensePlate implements Comparable<LicensePlate>, Serializable {

    /**
     * 车牌得分（相似度得分）
     */
    private float score;
    /**
     * 车牌旋转角度
     */
    private float angle;
    /**
     * 车牌框坐标信息
     */
    private LicensePlatePosition licensePlatePosition;
    /**
     * 是否为单行车牌
     * true 单排 false 双排
     */
    private Boolean single;
    /**
     * 当前图片的base64编码值
     * 识别到的车牌图片
     */
    private String baseImage;
    /**
     * 车牌的文本信息
     */
    private String plateNo;
    /**
     * 车牌的颜色信息
     */
    private String plateColor;
    /**
     * 车牌颜色的分数
     */
    private float colorScore;


    /**
     * 构造函数
     *
     * @param score                车牌分数
     * @param licensePlatePosition 车牌框
     * @param angle                车牌旋转角度
     * @param single               是否为单行车牌
     */
    private LicensePlate(float score, LicensePlatePosition licensePlatePosition, float angle, boolean single) {
        this.score = score;
        this.angle = angle;
        this.licensePlatePosition = licensePlatePosition;
        this.single = single;
    }

    /**
     * 构造一个车牌信息
     *
     * @param score 车牌分数
     * @param box   车牌框
     */
    public static LicensePlate build(float score, LicensePlatePosition box) {
        return new LicensePlate(score, box, 0, true);
    }

    /**
     * 构造一个车牌信息
     *
     * @param score 车牌分数
     * @param box   车牌框
     */
    public static LicensePlate build(float score, LicensePlatePosition box, boolean single) {
        return new LicensePlate(score, box, 0, single);
    }

    /**
     * 构造一个车牌信息
     *
     * @param score 车牌分数
     * @param box   车牌框
     * @param angle 车牌旋转角度
     */
    public static LicensePlate build(float score, LicensePlatePosition box, float angle) {
        return new LicensePlate(score, box, angle, true);
    }

    /**
     * 对车牌框进行旋转对应的角度
     *
     * @return
     */
    public LicensePlatePosition rotatePlateBox() {
        return this.licensePlatePosition.rotate(this.angle);
    }

    @Override
    public int compareTo(LicensePlate that) {
        return Float.compare(that.score, this.score);
    }
}
