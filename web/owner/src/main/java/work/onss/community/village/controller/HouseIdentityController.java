package work.onss.community.village.controller;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.HouseIdentity;
import work.onss.community.domain.village.HouseIdentityRepository;
import work.onss.community.domain.village.QHouseIdentity;
import work.onss.community.domain.village.relational.HouseCustomer;
import work.onss.community.domain.village.relational.HouseCustomerRepository;
import work.onss.community.domain.village.relational.QHouseCustomer;
import work.onss.community.service.exception.BusinessException;

import java.util.List;

@Log4j2
@RestController
public class HouseIdentityController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private HouseIdentityRepository houseIdentityRepository;
    @Autowired
    private HouseCustomerRepository houseCustomerRepository;

    @GetMapping(value = {"houseIdentitys/{id}"})
    public HouseIdentity detail(@RequestHeader("cid") String cid, @PathVariable String id) {
        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        return houseIdentityRepository.findOne(qHouseIdentity.id.eq(id).and(qHouseIdentity.customerId.eq(cid))).orElse(null);
    }

    @GetMapping(value = {"houseIdentitys"})
    public Page<HouseIdentity> page(@RequestHeader("cid") String cid, @PageableDefault Pageable pageable) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        List<String> houseIds = jpaQueryFactory
                .select(qHouseCustomer.houseId)
                .from(qHouseCustomer)
                .where(qHouseCustomer.customerId.eq(cid))
                .fetch();
        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        return houseIdentityRepository.findAll(qHouseIdentity.houseId.in(houseIds), pageable);
    }


    @Transactional
    @PostMapping(value = {"houseIdentitys/setStatus"}, name = "入住申请审核")
    public void binding(@RequestHeader(name = "cid") String cid, @RequestBody HouseIdentity houseIdentity) {

        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        String houseId = jpaQueryFactory
                .select(qHouseCustomer.houseId)
                .from(qHouseCustomer)
                .where(qHouseCustomer.customerId.eq(cid), qHouseCustomer.houseId.eq(houseIdentity.getHouseId()))
                .fetchOne();
        if (houseId == null) {
            throw new BusinessException("权限不足");
        }

        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        BooleanExpression booleanExpression = qHouseIdentity.id.in(houseIdentity.getId()).and(qHouseIdentity.houseId.eq(houseId));
        HouseIdentity oldHouseIdentity = houseIdentityRepository.findOne(booleanExpression).orElseThrow(() -> new BusinessException("入住申请不存在"));

        HouseIdentity.Status status = houseIdentity.getStatus();
        if (status.equals(HouseIdentity.Status.YES)) {
            // TODO 门禁-注册用户、关联房屋信息
            HouseCustomer oldHouseCustomer = houseCustomerRepository.findByMerchantIdAndHouseIdAndCustomerId(oldHouseIdentity.getMerchantId(), oldHouseIdentity.getHouseId(), oldHouseIdentity.getCustomerId()).orElse(null);
            if (null == oldHouseCustomer) {
                HouseCustomer houseCustomer = new HouseCustomer(oldHouseIdentity.getRelation(), oldHouseIdentity.getHouseId(), oldHouseIdentity.getCustomerId(), oldHouseIdentity.getMerchantId());
                houseCustomerRepository.save(houseCustomer);
            } else {
                if (!oldHouseCustomer.getRelation().equals(oldHouseIdentity.getRelation())) {
                    oldHouseCustomer.setRelation(oldHouseIdentity.getRelation());
                    houseCustomerRepository.save(oldHouseCustomer);
                }
            }
            oldHouseIdentity.setStatus(status);
            houseIdentityRepository.save(oldHouseIdentity);
        } else if (status.equals(HouseIdentity.Status.NO)) {
            // TODO 门禁-删除用户、解除房屋信息
            houseCustomerRepository.deleteByMerchantIdAndHouseIdAndCustomerId(oldHouseIdentity.getMerchantId(), oldHouseIdentity.getHouseId(), oldHouseIdentity.getCustomerId());
            oldHouseIdentity.setStatus(status);
            houseIdentityRepository.save(oldHouseIdentity);
        }
    }

}