import { wxLogin, domain, wxRequest,status } from '../../utils/util.js';

Page({

  data: {
    domain,status,
    orderHeats: [],
    year: new Date().getFullYear()
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/owner/orderHeats`,
        header: { authorization, cid: info.cid },
        data: { year: this.data.year }
      }).then((data) => {
        this.setData({
          orderHeats: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/owner/orderHeats`,
        header: { authorization, cid: info.cid },
        data: { year: this.data.year }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          orderHeats: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/owner/orderHeats`,
        header: { authorization, cid: info.cid },
        data: { page: this.data.number + 1, year: this.data.year }
      }).then((data) => {
        this.setData({
          orderHeats: data.content, number: data.number
        });
      });
    });
  },

  yearChange: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/owner/orderHeats`,
        header: { authorization, cid: info.cid },
        data: { year: e.detail.value }
      }).then((data) => {
        this.setData({
          orderHeats: data.content, number: data.number, year: e.detail.value
        });
      });
    });
  }
})