import { checkToken, domain, wxRequest, chooseImageToBase64, chooseImagesToBase64 } from '../../../utils/util.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pictures: []
  },

  create: function (e) {
    checkToken().then(({ authorization, info }) => {
      const { pictures, startDate, startTime, endDate, endTime } = this.data;
      wxRequest({
        url: `${domain}/village/products/create`,
        header: { authorization, cid: info.cid, mid: info.mid },
        method: 'POST',
        data: { pictures, startDatetime: `${startDate}T${startTime}`, endDatetime: `${endDate}T${endTime}`, ...e.detail.value }
      }).then((content) => {
        wx.showModal({
          title: '提示',
          content: '创建成功',
          showCancel: false,
          success: (res) => {
            wx.navigateBack({
              delta: 2
            });
          }
        })
      })
    })
  },

  dateTimeChange: function (e) {
    this.setData({
      [e.currentTarget.id]: e.detail.value
    })
  },

  chooseImages: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count
    const pictures = this.data[id] == null ? [] : this.data[id];
    count = count - pictures.length;
    chooseImagesToBase64('front', count).then((files) => {
      this.setData({
        [`${id}`]: [...pictures, ...files]
      })
    })
  },

  deletePictures: function (e) {
    const id = e.currentTarget.id;
    const index = e.currentTarget.dataset.index;
    const files = this.data[id];
    files.splice(index, 1);
    this.setData({
      [id]: files
    })
  },

  chooseImage: function (e) {
    const id = e.currentTarget.id;
    chooseImageToBase64('front').then((file) => {
      this.setData({
        [`${id}`]: file
      })
    })
  },

  deletePicture: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: null
    })
  },

  clearPictues: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: []
    })
  },
})