/**
 * 投票列表
 */
import services from '@/services/api';
import {
    ActionType,
    ModalForm,
    ProFormDependency,
    ProFormFieldSet,
    ProFormList,
    ProFormText,
    ProFormTextArea, ProFormUploadButton,
    ProTable
} from '@ant-design/pro-components';
import { Avatar, Button, DatePicker, Dropdown, Form, Image, message, Popconfirm, Typography, Upload } from 'antd';
import {useRef, useState} from 'react';

const {page, save, del, syncResult, sedSubscribeMessage} = services.VoteController;
const {exportData} = services.FileController;

const VoteList = () => {
    const columns: any = [
        {
            title: '序号',
            dataIndex: 'index',
            valueType: 'indexBorder',
            align: 'center',
        },
        {title: '主键', align: 'center', dataIndex: 'id', copyable: true},
        {title: '标题', align: 'center', dataIndex: 'title', copyable: true},
        {
            title: '描述',
            align: 'center',
            dataIndex: 'description',
            copyable: true,
            search: false,
        },
        {
            title: '图片', align: 'center', dataIndex: 'pictures', copyable: true,
            render: (text: object, record: { pictures: [string] }) => (
                <Avatar
                    shape="square"
                    src={
                        <Image.PreviewGroup
                            children={record?.pictures?.map((value, index) => (
                                <Image src={value}/>
                            ))}
                        />
                    }/>
            )
        },
        {title: '视频', align: 'center', dataIndex: 'videos', copyable: true},
        {title: '选项', align: 'center', dataIndex: 'options', copyable: true, search: false,},
        {title: '结果', align: 'center', dataIndex: 'result', copyable: true, search: false,},
        {
            title: '创建日期',
            dataIndex: 'insertDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '更新日期',
            dataIndex: 'updateDatetime',
            align: 'center',
            copyable: true,
            fieldProps: {format: 'YYYY-MM-DDTHH:mm:ss'},
            sorter: {multiple: 1},
            onSorter: false,
            renderFormItem: () => <DatePicker.RangePicker showTime/>,
        },
        {
            title: '操作',
            align: 'center',
            valueType: 'option',
            render: (text: any, record: any) => (
                <Dropdown.Button
                    key={record.id}
                    onClick={async () => {
                        syncResult([record.id]).then((res) => {
                            ref.current?.reload();
                        });
                    }}
                    menu={{
                        items: [
                            {
                                key: 'EDIT',
                                label: <Button type="primary">编辑</Button>,
                                onClick: () => {
                                    form.resetFields();
                                    const pictures = record.pictures?.map((value: any) => {
                                        return {thumbUrl: value};
                                    });
                                    const videos = record.videos?.map((value: any) => {
                                        return {value};
                                    });
                                    const options = record.options?.map((value: any) => {
                                        return {value};
                                    });
                                    form.setFieldsValue({...record, pictures, videos, options});
                                    setModalVisit('EDIT');
                                },
                            },
                            {
                                key: '导出',
                                label: <Button>导出</Button>,
                                onClick: () => {
                                    exportData(`/village/votes/${record.id}/detailExport`, {}, '投票结果详情.xlsx');
                                },
                            },
                            {
                                key: 'DEL',
                                label: <Popconfirm
                                    title="删除确认"
                                    description={`是否删除【${record.title}】？`}
                                    okText="确认"
                                    cancelText="取消"
                                    onConfirm={() => {
                                        del([record.id]).then((res) => {
                                            ref.current?.reload();
                                        });
                                    }}
                                >
                                    <Button type="primary" danger>删除</Button>
                                </Popconfirm>,
                            },
                        ],
                    }}
                >
                    <Typography.Text copyable={{text: JSON.stringify(record)}}>同步</Typography.Text>
                </Dropdown.Button>
            ),
        },
    ];

    const [form] = Form.useForm();
    const [modalVisit, setModalVisit] = useState<any>(false);
    const [preview, setPreview] = useState<any>(false);
    const ref = useRef<ActionType>();

    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey="id"
                headerTitle="查询表格"
                columns={columns}
                request={(params, sorter, filter) => page(params, sorter, filter)}
                search={{
                    optionRender: (searchConfig, formProps, dom) => [
                        <Button
                            key="out"
                            onClick={() => {
                                const values = searchConfig?.form?.getFieldsValue();
                                exportData('/village/votes/export', values, '投票结果.xlsx');
                            }}
                        >
                            导出
                        </Button>,
                        <Button
                          key="send"
                          onClick={() => {
                              const values = searchConfig?.form?.getFieldsValue();
                              sedSubscribeMessage(values).then((res) => {
                                  message.success('发送成功');
                              });
                          }}
                        >
                            通知
                        </Button>,
                        ...dom.values(),
                    ],
                }}
                toolBarRender={() => [
                    <ModalForm
                        width={500}
                        title="投票信息"
                        form={form}
                        open={modalVisit === 'EDIT'}
                        grid={true}
                        onOpenChange={(visible) => {
                            if (visible && modalVisit === 'EDIT') {
                                setModalVisit('EDIT');
                            } else {
                                form.resetFields();
                                setModalVisit(false);
                            }
                        }}
                        onFinish={async (values) => {
                            values.id = form.getFieldValue('id');
                            values.pictures = values.pictures?.map((res: any) => res.thumbUrl);
                            values.videos = values.videos?.map((res: any) => res.value);
                            values.options = values.options?.map((res: any) => res.value);
                            return save(values).then((res) => {
                                form.resetFields();
                                ref.current?.reload();
                                return true;
                            });
                        }}
                        trigger={<Button type="primary"> 新建 </Button>}
                    >
                        <ProFormText width="sm" name="title" label="标题" placeholder="请输入标题"/>
                        <ProFormTextArea width="lg" colProps={{span: 24}} fieldProps={{autoSize: true}}
                                         name="description" label="描述"/>
                        <ProFormUploadButton
                            colProps={{span: 24}}
                            name="pictures"
                            label="图片"
                            fieldProps={{
                                onPreview: (file) => {
                                    setPreview(
                                        {
                                            visible: true,
                                            src: file.thumbUrl,
                                            onVisibleChange: (value: any) => {
                                                setPreview(value);
                                            }
                                        }
                                    )
                                },
                                name: 'file',
                                listType: 'picture-card',
                            }}
                        />
                        <ProFormList colProps={{span: 24}} name="videos" label="视频ID">
                            {(meta, index, action, count) => <ProFormText name={'value'} colProps={{span: 24}}/>}
                        </ProFormList>

                        <ProFormList colProps={{span: 24}} name="options" label="选项">
                            {(meta, index, action, count) => <ProFormText name={'value'} colProps={{span: 24}}/>}
                        </ProFormList>
                    </ModalForm>,
                ]}
            />
            <Image preview={preview}/>
        </div>
    );
};

export default VoteList;
