package work.onss.community.controller;

import lombok.extern.log4j.Log4j2;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.ai.ollama.OllamaChatModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;
import work.onss.community.config.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

@Log4j2
@RestController
public class OcrController {

    @Autowired
    private OllamaChatModel ollamaChatModel;

    public static void main(String[] args) throws IOException {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        ClassPathResource classPathResource = new ClassPathResource("car");
        Stream<Path> pathStream = Files.walk(classPathResource.getFile().toPath());
        pathStream.filter(Files::isRegularFile).forEach(path -> {
            // 原始图片
            Mat mat = Imgcodecs.imread(path.toString());
            // 检测车牌
            ExtractParam extractParam = new ExtractParam();
            LicensePlateDetectionImpl licensePlateDetection = new LicensePlateDetectionImpl("web/ocr/src/main/resources/model/plate_detect.onnx", 1);
            List<LicensePlate> licensePlates = licensePlateDetection.inference(mat, extractParam.getScoreThreshold(), extractParam.getIouThreshold(), new HashMap<>(0));

            log.info("检测到{}个车牌", licensePlates.size());
            // 识别车牌号
            LicensePlateInfoInterfaceImpl licensePlateInfoInterface = new LicensePlateInfoInterfaceImpl("web/ocr/src/main/resources/model/plate_rec_color.onnx", 1);
            for (LicensePlate licensePlate : licensePlates) {
                Mat crop = MatTool.crop(mat, licensePlate.getLicensePlatePosition());
                LicensePlate inference = licensePlateInfoInterface.inference(crop, licensePlate.getSingle(), new HashMap<>(), licensePlate);
                log.info("可信度：{}，车牌号：{}，车牌颜色：{}，颜色得分：{}，角度：{}，是否为单行车牌：{}",
                        inference.getScore(), inference.getPlateNo(), inference.getPlateColor(), inference.getColorScore(), inference.getAngle(), inference.getSingle());
            }
        });
    }

    @PostMapping(value = {"licensePlates"})
    public List<LicensePlate> licensePlates(@RequestParam(value = "file") MultipartFile file) throws IOException {
        // 原始图片
        Mat mat = MatTool.convertMultipartFileToMat(file);
        // 检测车牌
        ExtractParam extractParam = new ExtractParam();
        LicensePlateDetectionImpl licensePlateDetection = new LicensePlateDetectionImpl("web/ocr/src/main/resources/model/plate_detect.onnx", 1);
        List<LicensePlate> licensePlates = licensePlateDetection.inference(mat, extractParam.getScoreThreshold(), extractParam.getIouThreshold(), new HashMap<>(0));

        log.info("检测到{}个车牌", licensePlates.size());
        // 识别车牌号
        LicensePlateInfoInterfaceImpl licensePlateInfoInterface = new LicensePlateInfoInterfaceImpl("web/ocr/src/main/resources/model/plate_rec_color.onnx", 1);
        for (LicensePlate licensePlate : licensePlates) {
            Mat crop = MatTool.crop(mat, licensePlate.getLicensePlatePosition());

            LicensePlate inference = licensePlateInfoInterface.inference(crop, licensePlate.getSingle(), new HashMap<>(), licensePlate);
            log.info("可信度：{}，车牌号：{}，车牌颜色：{}，颜色得分：{}，角度：{}，是否为单行车牌：{}",
                    inference.getScore(), inference.getPlateNo(), inference.getPlateColor(), inference.getColorScore(), inference.getAngle(), inference.getSingle());
        }
        return licensePlates;
    }

    @GetMapping(value = {"chat"}, name = "对话")
    public Flux<String> chat(@RequestParam String msg) {
        return ollamaChatModel.stream(msg);
    }
}