import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/village/parkSpaces', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const save = async (body?: any) =>
  request<any>('/village/parkSpaces', {
    method: 'POST',
    data: body,
  });
export const del = async (id: string[]) =>
  request<any>('/village/parkSpaces', {
    method: 'DELETE',
    data: id,
  });

export const getParkSpaceByKeywords = (params: { keyWords: string; id: string }) => request<any>('/village/parkSpaces/getParkSpaceByKeywords', { params });

export interface ParkSpace {
  id: string;
  region: string;
  code: string;
  money: number;
  carCode: string;
  state: string;
  status: string;
  remarks: string;
  houseId: string;
  merchantId: string;
}
