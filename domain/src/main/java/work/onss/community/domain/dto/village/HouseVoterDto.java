package work.onss.community.domain.dto.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.village.House;
import work.onss.community.domain.village.Voter;

import java.io.Serializable;

@Log4j2
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ExcelIgnoreUnannotated
public class HouseVoterDto implements Serializable {

    @ExcelProperty(value = "楼号")
    private String floorNumber;
    @ExcelProperty(value = "单元")
    private String unit;
    @ExcelProperty(value = "室")
    private String roomNumber;
    @ExcelProperty(value = "类型")
    private String type;
    @ExcelProperty(value = "人数")
    private Integer peopleNumber;

    @ExcelProperty(value = "真实姓名")
    private String name;
    @ExcelProperty(value = "身份证")
    private String idCard;
    @ExcelProperty(value = "手机号")
    private String phone;

    @ExcelProperty(value = "选项")
    private String option;
    @ExcelProperty(value = "备注")
    private String remark;

    public HouseVoterDto(House house, Customer customer, Voter voter) {
        this.floorNumber = house.getFloorNumber();
        this.unit = house.getUnit();
        this.roomNumber = house.getRoomNumber();
        this.type = house.getType();
        this.peopleNumber = house.getPeopleNumber();
        this.name = customer.getName();
        this.idCard = customer.getIdCard();
        this.phone = customer.getPhone();
        this.option = voter.getOption();
        this.remark = voter.getRemark();
    }
}