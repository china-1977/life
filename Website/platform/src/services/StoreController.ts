import { request } from '@@/exports';

export async function update(body?: any) {
  return request<any>(`/platform/stores/${body.id}`, { method: 'PUT', data: body });
}

export async function getStores(params: any) {
  return request<any>('/platform/stores', {
    params,
  }).then((res: any) => {
    return { data: res.content, total: res.totalElements };
  });
}

export async function detail(id: any) {
  return request<any>(`/platform/stores/${id}`);
}

export async function updateStore(data: any) {
  return request<any>(`/platform/stores/${data.id}`, { method: 'PUT', data });
}
