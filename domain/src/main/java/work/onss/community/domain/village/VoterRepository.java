package work.onss.community.domain.village;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Optional;

public interface VoterRepository extends JpaRepository<Voter, String>, JpaSpecificationExecutor<Voter>, QuerydslPredicateExecutor<Voter>, QuerydslBinderCustomizer<QVoter> {

    default void customize(QuerydslBindings bindings, QVoter qVoter) {
        bindings.bind(qVoter.id).withDefaultBinding();
        bindings.bind(qVoter.insertDatetime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
    }
}