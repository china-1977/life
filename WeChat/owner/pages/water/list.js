Page({
  mixins: [require('../../mixin/common')],
  data: {
    orderWaters: [],
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderWaters`,
        header: { authorization },
        data: { year: this.data.year, size: this.data.size }
      }).then((data) => {
        this.setData({
          orderWaters: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderWaters`,
        header: { authorization },
        data: { year: this.data.year, size: this.data.size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          orderWaters: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderWaters`,
        header: { authorization },
        data: { page: this.data.number + 2, year: this.data.year, size: this.data.size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let orderWaters = this.data.orderWaters;
          orderWaters.push(...data.content)
          this.setData({
            orderWaters, number: data.number
          });
        }
      });
    });
  },

  yearChange: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/orderWaters`,
        header: { authorization },
        data: { year: e.detail.value }
      }).then((data) => {
        this.setData({
          orderWaters: data.content, number: data.number, year: e.detail.value
        });
      });
    });
  }
})