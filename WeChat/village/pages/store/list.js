import {domain, getStores} from '../../utils/util.js';

Page({
    data: {
        domain,
        stores: [],
        name: '请选择地理位置',
        keyword: ''
    },

    onLoad: function (options) {
        this.init();
    },

    onPullDownRefresh: function () {
        this.init();
        wx.stopPullDownRefresh();
    },

    init: function () {
        const point = wx.getStorageSync('point');
        if (point) {
            getStores(point.longitude, point.latitude, 0, this.data.keyword).then((stores) => {
                this.setData({
                    point, number: 0, last: false, stores
                })
            });
        } else {
            wx.authorize({
                scope: 'scope.userLocation',
                success: (res) => {
                    wx.getLocation({
                        type: 'gcj02',
                        success: (res) => {
                            wx.chooseLocation({
                                latitude: res.latitude,
                                longitude: res.longitude,
                                success: ({address, latitude, longitude, name}) => {
                                    const point = {
                                        address: address === '' ? '默认地址' : address,
                                        latitude,
                                        longitude,
                                        name: name === '' ? '默认地址' : name
                                    };
                                    wx.setStorageSync('point', point);
                                    this.setData({point});
                                    getStores(longitude, latitude, 0, this.data.keyword).then((stores) => {
                                        this.setData({number: 0, last: false, stores})
                                    });
                                },
                                fail: () => {
                                    const point = {
                                        address: '默认地址',
                                        latitude: res.latitude,
                                        longitude: res.longitude,
                                        name: '默认地址',
                                    };
                                    wx.setStorageSync('point', point);
                                    this.setData({point});
                                    getStores(res.longitude, res.latitude, 0, this.data.keyword).then((stores) => {
                                        this.setData({number: 0, last: false, stores})
                                    });
                                }
                            })
                        }
                    })
                },
                fail: (res) => {
                    wx.showModal({
                        title: '提示',
                        content: '请允许小程序使用位置消息',
                        confirmColor: '#e64340',
                        showCancel: false,
                        success: () => {
                            wx.openSetting();
                        }
                    })
                }
            })
        }
    },

    onReachBottom: function () {
        if (this.data.last) {
        } else {
            let {point, number, keyword} = this.data;
            number = number + 1;
            getStores(point.longitude, point.latitude, number, keyword).then((stores) => {
                this.setData({
                    number, last: stores.length == 0,
                    stores: [...this.data.stores, ...stores],
                });
            });
        }
    },

    chooseLocation: function () {
        wx.authorize({
            scope: 'scope.userLocation',
            success: (res) => {
                wx.chooseLocation({
                    latitude: this.data.point.latitude,
                    longitude: this.data.point.longitude,
                    success: ({address, latitude, longitude, name}) => {
                        const point = {address, latitude, longitude, name};
                        wx.setStorageSync('point', point);
                        this.setData({point});
                    },
                    fail: (res) => {
                        wx.getLocation({
                            type: 'gcj02',
                            success: (res) => {
                                const point = {
                                    address: '默认地址',
                                    latitude: res.latitude,
                                    longitude: res.longitude,
                                    name: '默认地址',
                                };
                                wx.setStorageSync('point', point);
                                this.setData({point});
                                getStores(res.longitude, res.latitude, 0, this.data.keyword).then((stores) => {
                                    this.setData({number: 0, last: false, stores});
                                });
                            }
                        })
                    }
                })
            },
            fail: (res) => {
                wx.showModal({
                    title: '提示',
                    content: '请允许小程序使用位置消息',
                    confirmColor: '#e64340',
                    showCancel: false,
                    success: () => {
                        wx.openSetting();
                    }
                })
            }
        })
    },

    search: function ({detail}) {
        const point = this.data.point;
        if (point) {
            getStores(point.longitude, point.latitude, 0, detail.value.keyword).then((stores) => {
                this.setData({number: 0, last: false, stores})
            });
        } else {
            wx.getLocation({
                type: 'gcj02',
                success: (res) => {
                    const point = {
                        address: '默认地址',
                        latitude: res.latitude,
                        longitude: res.longitude,
                        name: '默认地址',
                    };
                    wx.setStorageSync('point', point);
                    this.setData({point});
                    getStores(res.longitude, res.latitude, 0, detail.value.keyword).then((stores) => {
                        this.setData({number: 0, last: false, stores});
                    });
                }
            })
        }
    },
    inputChange: function ({detail}) {
        this.setData({
            keyword: detail.value
        })
    }
})
