package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.querydsl.core.types.Predicate;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.device.ParkGate;
import work.onss.community.domain.device.ParkGateRepository;

import java.util.Collection;

@Log4j2
@RestController
public class ParkGateController {
    @Autowired
    private ParkGateRepository parkGateRepository;

    @GetMapping(value = {"parkGates"}, name = "道闸列表")
    public Page<ParkGate> page(@QuerydslPredicate(bindings = ParkGateRepository.class) Predicate predicate, @PageableDefault Pageable pageable) {
        return parkGateRepository.findAll(predicate, pageable);
    }

    @PostMapping(value = "parkGates/export", name = "道闸导出")
    public void export(@QuerydslPredicate(bindings = ParkGateRepository.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        Iterable<ParkGate> parkGates = parkGateRepository.findAll(predicate);
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        EasyExcel.write(httpServletResponse.getOutputStream(), ParkGate.class)
                .sheet().doWrite((Collection<?>) parkGates);
    }

}