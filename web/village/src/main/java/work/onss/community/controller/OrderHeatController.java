package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import work.onss.community.domain.village.OrderHeat;
import work.onss.community.domain.village.OrderHeatRepository;
import work.onss.community.domain.village.PayOrder;
import work.onss.community.domain.village.QOrderHeat;
import work.onss.community.service.exception.BusinessException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class OrderHeatController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private OrderHeatRepository orderHeatRepository;

    @GetMapping(value = {"orderHeats/{id}"}, name = "采暖费详情")
    public OrderHeat detail(@PathVariable String id) {
        return orderHeatRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"orderHeats"}, name = "采暖费列表")
    public Page<OrderHeat> page(
            @RequestHeader(name = "mid") String mid,
            @RequestParam Integer year,
            @QuerydslPredicate(bindings = OrderHeatRepository.class) Predicate predicate,
            @PageableDefault(sort = {"startDate"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QOrderHeat qOrderHeat = QOrderHeat.orderHeat;
        return orderHeatRepository.findAll(qOrderHeat.merchantId.eq(mid).and(qOrderHeat.startDate.year().eq(year)).and(predicate), pageable);
    }

    @DeleteMapping(value = {"orderHeats"}, name = "采暖费批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        List<OrderHeat> orderHeats = orderHeatRepository.findAllById(ids);
        for (OrderHeat orderHeat : orderHeats) {
            if (!orderHeat.getStatus().equals(PayOrder.Status.WAIT_CONFIRM)) {
                StringJoiner message = new StringJoiner(",");
                message.add(orderHeat.getFloorNumber());
                message.add(orderHeat.getUnit());
                message.add(orderHeat.getRoomNumber());
                message.add(orderHeat.getStatus().getMessage());
                message.add("无法删除");
                throw new BusinessException(message.toString());
            }
        }
        orderHeatRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @Transactional
    @PostMapping(value = {"orderHeats/import"}, name = "采暖费导入")
    public void importWater(@RequestHeader(name = "mid") String mid, @RequestParam(value = "file") MultipartFile file) throws Exception {
        ExcelReader excelReader = EasyExcel.read(file.getInputStream()).build();
        List<ReadSheet> readSheets = excelReader.excelExecutor().sheetList();
        for (ReadSheet readSheet : readSheets) {
            List<OrderHeat> data = EasyExcel.read(file.getInputStream()).head(OrderHeat.class).sheet(readSheet.getSheetNo()).doReadSync();
            for (OrderHeat next : data) {
                next.setMerchantId(mid);
                if (next.getId() != null) {
                    orderHeatRepository.findById(next.getId()).ifPresentOrElse(item -> {
                        if (!item.getMerchantId().equals(mid)) {
                            throw new BusinessException("采暖费主键错误:".concat(item.getId()));
                        }
                    }, () -> next.setId(null));
                }
                orderHeatRepository.save(next);
            }
        }
    }

    @Transactional
    @PostMapping(value = {"orderHeats/confirm"}, name = "采暖费确认")
    public void confirm(@RequestHeader(name = "mid") String mid, @RequestBody OrderHeat orderHeat) {
        QOrderHeat qOrderHeat = QOrderHeat.orderHeat;
        jpaQueryFactory.update(qOrderHeat)
                .set(qOrderHeat.status, PayOrder.Status.WAIT_PAY)
                .where(
                        qOrderHeat.merchantId.eq(mid),
                        qOrderHeat.startDate.eq(orderHeat.getStartDate().withDayOfMonth(1)),
                        qOrderHeat.endDate.eq(orderHeat.getEndDate().withDayOfMonth(1)),
                        qOrderHeat.status.eq(PayOrder.Status.WAIT_CONFIRM))
                .execute();
    }

    @PostMapping(value = {"orderHeats/export"}, name = "采暖费导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = OrderHeatRepository.class, root = OrderHeat.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QOrderHeat qOrderHeat = QOrderHeat.orderHeat;
        Collection<OrderHeat> data = (Collection<OrderHeat>) orderHeatRepository.findAll(qOrderHeat.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), OrderHeat.class).autoCloseStream(true).build();
        Map<String, List<OrderHeat>> floorNumberData = data.stream().collect(Collectors.groupingBy(OrderHeat::getFloorNumber));
        floorNumberData.forEach((k, v) -> excelWriter.write(v, EasyExcel.writerSheet(k).build()));
        excelWriter.finish();
    }
}