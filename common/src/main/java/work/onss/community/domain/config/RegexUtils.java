package work.onss.community.domain.config;

public class RegexUtils {
    private static final String PLATE_NUMBER_REGEX = "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z][A-Z0-9]{4}[A-Z0-9挂学警港澳]$";
    private static final String NAME_REGEX = "^[\\u4e00-\\u9fa5]{2,4}$";
    private static final String ID_CARD_REGEX = "^(\\d{15}$|^\\d{18}$|^\\d{17}(\\d|X|x))$";
    private static final String PHONE_NUMBER_REGEX = "^1[3-9]\\d{9}$";

    public static boolean isPlateNumber(String plateNumber) {
        return plateNumber.matches(PLATE_NUMBER_REGEX);
    }

    public static boolean isName(String name) {
        return name.matches(NAME_REGEX);
    }

    public static boolean isIdCard(String idCard) {
        return idCard.matches(ID_CARD_REGEX);
    }

    public static boolean isPhoneNumber(String phoneNumber) {
        return phoneNumber.matches(PHONE_NUMBER_REGEX);
    }
}
