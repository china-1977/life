import { wxLogin, domain, wxRequest,OrderParkStatus } from '../../utils/util.js';

Page({
  data: {
    domain,OrderParkStatus
  },
  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/orderParks/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((orderPark) => {
        this.setData({
          orderPark
        });
      });
    });
  },
})