import { wxLogin, domain, wxRequest, size } from '../../utils/util.js';

Page({

  data: {
    domain, votes: [],
  },
  
  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/votes`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data:{size}
      }).then((data) => {
        this.setData({
          votes: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/votes`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data:{size}
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          votes: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/votes`,
        header: { authorization, cid: info.cid, mid: info.mid },
        data: { page: this.data.number + 2, size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let votes = this.data.votes;
          votes.push(...data.content)
          this.setData({
            votes, number: data.number
          });
        }
      });
    });
  }
})