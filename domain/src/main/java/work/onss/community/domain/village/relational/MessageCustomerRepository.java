package work.onss.community.domain.village.relational;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

public interface MessageCustomerRepository extends JpaRepository<MessageCustomer, String>, JpaSpecificationExecutor<MessageCustomer>, QuerydslPredicateExecutor<MessageCustomer>, QuerydslBinderCustomizer<QMessageCustomer> {

    default void customize(QuerydslBindings bindings, QMessageCustomer qMessageCustomer) {
        bindings.bind(qMessageCustomer.id).withDefaultBinding();
    }
}