package work.onss.community.domain.usertype;

import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.query.ReturnableType;
import org.hibernate.sql.ast.SqlAstTranslator;
import org.hibernate.sql.ast.spi.SqlAppender;
import org.hibernate.sql.ast.tree.SqlAstNode;
import org.hibernate.type.BasicTypeReference;

import java.util.List;

public class PostgresqlArrayToStringFunction extends StandardSQLFunction {


    public PostgresqlArrayToStringFunction(String name, BasicTypeReference<?> type) {
        super(name, type);
    }

    @Override
    public void render(
            SqlAppender sqlAppender,
            List<? extends SqlAstNode> arguments,
            ReturnableType<?> returnType,
            SqlAstTranslator<?> walker) {
        if (arguments == null || arguments.size() != 2) {
            throw new IllegalArgumentException("array_to_string function requires exactly 2 arguments");
        }

        sqlAppender.appendSql("array_to_string(");
        arguments.get(0).accept(walker);
        sqlAppender.appendSql(", ");
        arguments.get(1).accept(walker);
        sqlAppender.appendSql(")");
    }
}
