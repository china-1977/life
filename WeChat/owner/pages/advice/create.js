Page({
  mixins: [require('../../mixin/common')],
  data: {
    index: -1,
    villages: [],
    point: {},
    descriptionPictures: []
  },
  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      const categories = ['VILLAGE']
      this.wxRequest({
        url: `${this.data.domain}/account/bindings/getMerchants?categories=${categories}`,
        header: { authorization }
      }).then((villages) => {
        this.setData({ villages });
      });
    });
  },
  create: function (e) {
    const villagesIndex = e.detail.value.villagesIndex;
    if (villagesIndex < 0) {
      wx.showModal({
        title: '提示',
        content: '请选择社区',
        confirmColor: '#e64340',
        showCancel: false,
      })
    } else {
      const merchantId = this.data.villages[villagesIndex].id;
      this.wxLogin().then(({ authorization }) => {
        wx.showModal({
          title: '提示',
          content: '是否提交？',
          success: (res) => {
            if (res.confirm) {
              const descriptionPictures = this.data.descriptionPictures;
              this.wxRequest({
                url: `${this.data.domain}/owner/advices`,
                data: { ...e.detail.value, merchantId, descriptionPictures },
                method: "POST",
                header: { authorization },
              }).then(() => {
                wx.showModal({
                  title: '提示',
                  content: '提交成功，请耐心等待',
                  showCancel: false,
                  success: (res) => {
                    wx.navigateBack({
                      delta: 2
                    });
                  }
                })
              })
            }
          }
        })
      })
    }
  },

  villageChange: function (e) {
    this.setData({
      index: e.detail.value
    });
  },

  chooseImages: function (e) {
    const id = e.currentTarget.id;
    let count = e.currentTarget.dataset.count
    const pictures = this.data[id] == null ? [] : this.data[id];
    count = count - pictures.length;
    this.chooseImagesToBase64('front', count).then((files) => {
      this.setData({
        [`${id}`]: [...pictures, ...files]
      })
    })
  },

  clearPictues: function (e) {
    const id = e.currentTarget.id;
    this.setData({
      [id]: []
    })
  },
})