package work.onss.community.controller;

import com.querydsl.core.types.Predicate;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.MerchantRepository;
import work.onss.community.domain.account.QMerchant;
import work.onss.community.domain.shop.Product;
import work.onss.community.domain.shop.ProductRepository;
import work.onss.community.domain.shop.QProduct;

@Log4j2
@RestController
public class MerchantController {
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private ProductRepository productRepository;

    @GetMapping(value = {"merchants/{id}"}, name = "商户详情")
    public Merchant product(@PathVariable String id) {
        return merchantRepository.findById(id).orElse(null);
    }

    @GetMapping(value = {"merchants"}, name = "商户分页")
    public Page<Merchant> merchants(@RequestHeader(name = "mid") String mid,
                                    @QuerydslPredicate(bindings = MerchantRepository.class) Predicate predicate,
                                    @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QMerchant qMerchant = QMerchant.merchant;
        return merchantRepository.findAll(qMerchant.id.ne(mid).and(predicate), pageable);
    }

    @GetMapping(value = {"merchants/{id}/products"}, name = "商户商品")
    public Page<Product> products(@PathVariable String id,
                                  @QuerydslPredicate(bindings = ProductRepository.class) Predicate predicate,
                                  @PageableDefault(sort = {"orderLabel"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QProduct qProduct = QProduct.product;
        return productRepository.findAll(qProduct.merchantId.eq(id).and(predicate), pageable);
    }
}
