Page({
  mixins: [require('../../../mixin/common')],
  data: {
    store: {}, products: [], sum: '0.00', checkAll: false, intoIndex: 0
  },

  onLoad: function (options) {
    this.wxLogin().then(({ authorization }) => {
      const merchantId = wx.getStorageSync('merchantId');
      if (merchantId) {
        this.wxRequest({
          url: `${this.data.domain}/owner/products`,
          header: { authorization },
          data: { merchantId }
        }).then((data) => {
          this.setData({ ...data });
        })
      } else {
        wx.navigateTo({
          url: '/pages/shop/store/list?back=/pages/shop/product/list',
        })
      }
    })
  },

  scrollInto: function (e) {
    this.setData({
      intoIndex: e.currentTarget.id
    })
  },

  addCount: function (e) {
    const product = e.currentTarget.dataset.product
    this.updateCart(product, 1);
  },

  subtractCount: function (e) {
    const product = e.currentTarget.dataset.product
    this.updateCart(product, -1);
  },

  updateCart: function (product, count) {
    this.wxLogin().then(({ authorization }) => {
      const { store } = this.data;
      if (product.cartId) {
        this.wxRequest({
          url: `${this.data.domain}/owner/carts`,
          method: 'POST',
          header: { authorization },
          data: {
            id: product.cartId,
            productId: product.id,
            num: product.num + count,
            merchantId: store.id
          },
        }).then((data) => {
          this.wxRequest({
            url: `${this.data.domain}/owner/products`,
            header: { authorization },
            data: { merchantId: store.id }
          }).then((data) => {
            this.setData({ ...data });
          })
        });
      } else {
        this.wxRequest({
          url: `${this.data.domain}/owner/carts`,
          method: 'POST',
          header: { authorization },
          data: { productId: product.id, num: 1, merchantId: store.id },
        }).then((data) => {
          const { store } = this.data;
          this.wxRequest({
            url: `${this.data.domain}/owner/products`,
            header: { authorization },
            data: { merchantId: store.id }
          }).then((data) => {
            this.setData({ ...data });
          })
        });
      }
    })
  },

  checkedChange: function (e) {
    const product = e.currentTarget.dataset.product;
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/carts/${product.cartId}/setChecked?checked=${product.checked}`,
        method: 'POST',
        header: { authorization },
      }).then((data) => {
        const { store } = this.data;
        this.wxRequest({
          url: `${this.data.domain}/owner/products`,
          header: { authorization },
          data: { merchantId: store.id }
        }).then((data) => {
          this.setData({ ...data });
        })
      })
    })
  },
  clearCart: function (e) {
    const { store } = this.data;
    if (store) {
      this.wxLogin().then(({ authorization }) => {
        wx.showModal({
          title: '温馨提示',
          content: '是否清空清单？',
          complete: (res) => {
            if (res.confirm) {
              const { store } = this.data;
              this.wxRequest({
                url: `${this.data.domain}/owner/carts`,
                method: 'DELETE',
                header: { authorization },
                data: { merchantId: store.id }
              }).then(() => {
                this.wxRequest({
                  url: `${this.data.domain}/owner/products`,
                  header: { authorization },
                  data: { merchantId: store.id }
                }).then((data) => {
                  this.setData({ ...data });
                })
              })
            }
          }
        })
      })
    }
  },
  syncCart: function (e) {
    this.wxLogin().then(({ authorization }) => {
      const { store } = this.data;
      this.wxRequest({
        url: `${this.data.domain}/owner/products`,
        header: { authorization },
        data: { merchantId: store.id }
      }).then((data) => {
        this.setData({ ...data });
      })
    })
  }
})
