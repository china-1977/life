export default [
  {
    path: '/',
    name: '首页',
    icon: 'dashboard',
    component: '@/pages/Index',
    hideInMenu: true,
  },
  {
    path: '/task',
    name: '日常任务',
    icon: 'dashboard',
    routes: [
      {
        path: '/task/identity',
        name: '入住申请',
        icon: 'dashboard',
        component: '@/pages/Village/Identity',
      },
      {
        path: '/task/vote',
        name: '投票统计',
        icon: 'dashboard',
        component: '@/pages/Village/VoteList',
      },
    ],
  },
  {
    path: '/money',
    name: '财务统计',
    icon: 'dashboard',
    routes: [
      {
        path: '/money/orderHouses',
        name: '物业费管理',
        icon: 'dashboard',
        component: '@/pages/Village/OrderHouse',
      },
      {
        path: '/money/orderWater',
        name: '水费管理',
        icon: 'dashboard',
        component: '@/pages/Village/OrderWater',
      },
      {
        path: '/money/orderHeat',
        name: '采暖费管理',
        icon: 'dashboard',
        component: '@/pages/Village/OrderHeat',
      },
    ],
  },
  {
    path: '/setting',
    name: '系统设置',
    icon: 'dashboard',
    routes: [
      {
        path: '/setting/accesses',
        name: '道闸管理',
        component: '@/pages/Village/ParkGateList',
      },
      {
        path: '/setting/doors',
        name: '门禁管理',
        component: '@/pages/Village/DoorList',
      },
      {
        path: '/setting/villages',
        name: '主体管理',
        component: '@/pages/Village/List',
      },
      {
        path: '/setting/members',
        name: '员工列表',
        component: '@/pages/Village/Member',
      },
    ],
  },
  {
    path: '/base',
    name: '基础数据',
    icon: 'dashboard',
    routes: [
      {
        path: '/base/houses',
        name: '房屋管理',
        component: '@/pages/Village/HouseList',
      },
      {
        path: '/base/parkSpace',
        name: '车位管理',
        component: '@/pages/Village/ParkSpaceList',
      },
    ],
  },
  {
    path: '/login',
    name: '登录',
    component: '@/pages/Login',
    layout: false,
    hideInMenu: true,
  },
];
