import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: object, filter: any) => {
  return request<any>('/platform/votes', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });
};

export const syncResult = async (id: string[]) =>
  request<any>('/platform/votes/syncResult', {
    method: 'POST',
    data: id,
  });
