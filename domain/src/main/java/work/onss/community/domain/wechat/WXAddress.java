package work.onss.community.domain.wechat;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WXAddress {
    @NotEmpty(message = "缺少银行地址")
    @Size(min = 3, max = 3, message = "银行地址错误")
    private String[] value;
    @NotBlank(message = "缺少邮编")
    private String postcode;
    @NotEmpty(message = "缺少银行地址编号")
    @Size(min = 3, max = 3, message = "银行地址编号错误")
    private String[] code;
}
