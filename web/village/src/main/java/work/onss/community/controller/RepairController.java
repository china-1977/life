package work.onss.community.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.account.CustomerRepository;
import work.onss.community.domain.village.QRepair;
import work.onss.community.domain.village.Repair;
import work.onss.community.domain.village.RepairRepository;

import java.time.LocalDateTime;
import java.util.Collection;

@Log4j2
@RestController
public class RepairController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private RepairRepository repairRepository;
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping(value = {"repairs/{id}"}, name = "报事报修详情")
    public Repair detail(@RequestHeader("cid") String cid, @PathVariable String id) {
        QRepair qRepair = QRepair.repair;
        return repairRepository.findOne(qRepair.id.eq(id).and(qRepair.customerId.eq(cid))).orElse(null);
    }

    @GetMapping(value = {"repairs"}, name = "报事报修列表")
    public Page<Repair> page(@RequestHeader("mid") String mid,
                             @QuerydslPredicate(bindings = RepairRepository.class) Predicate predicate,
                             @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QRepair qRepair = QRepair.repair;
        return repairRepository.findAll(qRepair.merchantId.eq(mid).and(predicate), pageable);
    }

    @Transactional
    @PostMapping(value = {"repairs"}, name = "报事报修-分配")
    public void save(@RequestHeader("mid") String mid, @RequestBody Repair repair) {
        if (StringUtils.hasLength(repair.getRepairerCustomerId())) {
            Customer customer = customerRepository.findById(repair.getRepairerCustomerId()).orElse(null);
            if (customer != null) {
                repair.setRepairerName(customer.getName());
                repair.setRepairerPhone(customer.getPhone());
            }
        }
        QRepair qRepair = QRepair.repair;
        jpaQueryFactory.update(qRepair)
                .set(qRepair.status, repair.getStatus())
                .set(qRepair.label, repair.getLabel())
                .set(qRepair.repairerCustomerId, repair.getRepairerCustomerId())
                .set(qRepair.repairerName, repair.getRepairerName())
                .set(qRepair.repairerPhone, repair.getRepairerPhone())
                .set(qRepair.updateDatetime, LocalDateTime.now())
                .where(qRepair.merchantId.eq(mid).and(qRepair.id.eq(repair.getId())))
                .execute();
    }

    @Transactional
    @PostMapping(value = {"repairs/reply"}, name = "报事报修-完成")
    public void reply(@RequestHeader("mid") String mid, @RequestHeader("cid") String cid, @RequestBody Repair repair) {
        QRepair qRepair = QRepair.repair;
        jpaQueryFactory.update(qRepair)
                .set(qRepair.status, repair.getStatus())
                .set(qRepair.updateDatetime, LocalDateTime.now())
                .where(qRepair.merchantId.eq(mid).and(qRepair.repairerCustomerId.eq(cid)).and(qRepair.id.eq(repair.getId())))
                .execute();

    }

    @DeleteMapping(value = {"repairs"}, name = "报事报修批量删除")
    public void delete(@RequestHeader(name = "mid") String mid, @RequestBody Collection<String> ids) {
        repairRepository.deleteByMerchantIdAndIdIn(mid, ids);
    }

    @PostMapping(value = {"repairs/export"}, name = "报事报修导出")
    public void export(@RequestHeader(name = "mid") String mid,
                       @QuerydslPredicate(bindings = RepairRepository.class, root = Repair.class) Predicate predicate,
                       HttpServletResponse httpServletResponse) throws Exception {
        QRepair qRepair = QRepair.repair;
        Collection<Repair> data = (Collection<Repair>) repairRepository.findAll(qRepair.merchantId.eq(mid).and(predicate));
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = EasyExcel.write(httpServletResponse.getOutputStream(), Repair.class).build();
        excelWriter.write(data, EasyExcel.writerSheet("全部").build());
        excelWriter.finish();
    }
}