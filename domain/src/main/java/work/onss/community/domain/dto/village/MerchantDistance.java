package work.onss.community.domain.dto.village;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import work.onss.community.domain.account.Merchant;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MerchantDistance extends Merchant {
    private double distance;

    public MerchantDistance(Merchant merchant, double distance) {
        super(merchant);
        this.distance = distance;
    }
}
