import { ParkSpace } from '@/services/ParkSpaceController';
import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/village/houses', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const customers = (id: string) => request<any>(`/village/houses/${id}/customers`);

export const getParkSpaces = (id: string) => request<any>(`/village/houses/${id}/parkSpaces`);

export const save = async (body?: any) =>
  request<any>('/village/houses', {
    method: 'POST',
    data: body,
  });

export const houseFastCreate = async (body?: any) =>
    request<any>('/village/houses/houseFastCreate', {
        method: 'POST',
        data: body,
    });

export const updateHouse = async (body?: any) =>
  request<any>('/village/houses/updateHouse', {
    method: 'POST',
    data: body,
  });

export const setWater = async (body?: any) =>
  request<any>('/village/houses/setWater', {
    method: 'POST',
    data: body,
  });

export const bindingParkSpace = async (id: string, body: string[]) =>
  request<any>(`/village/houses/${id}/bindingParkSpace`, {
    method: 'POST',
    data: body,
  });

export const del = async (body?: any) =>
  request<any>('/village/houses', {
    method: 'DELETE',
    data: body,
  });

export const syncPeopleNumber = async () => request<any>('/village/houses/syncPeopleNumber', { method: 'POST' });

export const getFloor = () => request<any>(`/village/houses/getFloor`);

export interface HouseCustomer {
  id: string;
  relation: string;
  houseId: string;
  customerId: string;
  merchantId: string;
  customer: Customer;
}

export interface House {
  id: string;
  merchantId: string;
  floorNumber: string;
  unit: string;
  roomNumber: string;
  area: number;
  type: string;
  remarks: string;
  houseCustomers: [HouseCustomer];
    parkSpaces: [ParkSpace];
}

export interface Customer {
  id: string;
  name: string;
  idCard: string;
  idCardFace: string;
  idCardNational: string;
  face: string;
  phone: string;
  username: string;
  password: string;
  insertDate: string;
  updateDate: string;
  sessionKey: string;
  appid: string;
  openid: string;
  status: boolean;
}
