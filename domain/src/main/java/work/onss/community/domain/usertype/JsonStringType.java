package work.onss.community.domain.usertype;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.SqlTypes;
import org.hibernate.usertype.UserType;
import org.postgresql.util.PGobject;
import work.onss.community.domain.config.JacksonUtils;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class JsonStringType implements UserType<Object> {

    @Override
    public int getSqlType() {
        return SqlTypes.JSON;
    }

    @Override
    public Class<Object> returnedClass() {
        return Object.class;
    }

    @Override
    public boolean equals(Object x, Object y) {
        if (x == y) {
            return true;
        }
        if (x == null || y == null) {
            return false;
        }
        try {
            String jsonX = JacksonUtils.writeValueAsString(x);
            String jsonY = JacksonUtils.writeValueAsString(y);
            return jsonX.equals(jsonY);
        } catch (Exception e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public int hashCode(Object object) throws HibernateException {
        try {
            return JacksonUtils.writeValueAsString(object).hashCode();
        } catch (Exception e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public Object nullSafeGet(ResultSet resultSet, int i, SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws SQLException {
        String jsonString = resultSet.getString(i);
        return JacksonUtils.readValue(jsonString, Object.class);

    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        String jsonString = JacksonUtils.writeValueAsString(value);
        PGobject pgobject = new PGobject();
        pgobject.setType("json");
        pgobject.setValue(jsonString);
        st.setObject(index, pgobject);
    }

    @Override
    public Object deepCopy(Object o) {
        return null;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object o) {
        return null;
    }

    @Override
    public Object assemble(Serializable serializable, Object o) {
        return null;
    }
}
