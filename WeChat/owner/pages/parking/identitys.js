Page({
  mixins: [require('../../mixin/common')],
  data: {
  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/parkSpaceIdentitys`,
        header: { authorization },
        data: { size: this.data.size }
      }).then((data) => {
        this.setData({
          parkSpaceIdentitys: data.content, number: data.number
        });
      });
    });
  },

  onPullDownRefresh: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/parkSpaceIdentitys`,
        header: { authorization },
        data: { size: this.data.size }
      }).then((data) => {
        wx.stopPullDownRefresh();
        this.setData({
          parkSpaceIdentitys: data.content, number: data.number
        });
      });
    });
  },

  onReachBottom: function (e) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/parkSpaceIdentitys`,
        header: { authorization },
        data: { page: this.data.number + 2, size: this.data.size }
      }).then((data) => {
        if (data.empty) {
          console.log('空');
        } else {
          let parkSpaceIdentitys = this.data.parkSpaceIdentitys;
          parkSpaceIdentitys.push(...data.content)
          this.setData({
            parkSpaceIdentitys, number: data.number
          });
        }
      });
    });
  },
})