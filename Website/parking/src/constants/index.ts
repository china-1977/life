export const DEFAULT_NAME = 'Umi Max';

export const merchantStatusEnum = {
  true: { text: '是' },
  false: { text: '否' },
};

export const DeviceStatusEnum = {
  ProFormSelect: {
    online: '上线',
    offline: '下线',
    abnormal: '异常',
  },
  columns: {
    online: { text: '上线' },
    offline: { text: '下线' },
    abnormal: { text: '异常' },
  },
};

export const AccessTypeEnum = {
  ProFormSelect: {
    I: '入口',
    O: '出口',
  },
  columns: {
    I: { text: '入口' },
    O: { text: '出口' },
  },
};

export const ParkStatusEnum = {
  ProFormSelect: {
    I: '在场',
    O: '离场',
  },
  columns: {
    I: { text: '在场' },
    O: { text: '离场' },
  },
};

export const OrderStatusEnum = {
  ProFormSelect: {
    WAIT_CONFIRM: '待确认',
    WAIT_PAY: '待支付',
    FINISH: '已支付',
  },
  columns: {
    WAIT_CONFIRM: { text: '待确认' },
    WAIT_PAY: { text: '待支付' },
    FINISH: { text: '已支付' },
  },
};

export const PayStatusEnum = {
  ProFormSelect: {
    SUCCESS: '支付成功',
    REFUND: '转入退款',
    NOTPAY: '未支付',
    CLOSED: '已关闭',
    REVOKED: '已撤销',
    USERPAYING: '用户支付中',
    PAYERROR: '支付失败',
  },
  columns: {
    SUCCESS: { text: '支付成功' },
    REFUND: { text: '转入退款' },
    NOTPAY: { text: '未支付' },
    CLOSED: { text: '已关闭' },
    REVOKED: { text: '已撤销' },
    USERPAYING: { text: '用户支付中' },
    PAYERROR: { text: '支付失败' },
  },
};
