package work.onss.community.domain.park;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@EntityListeners(AuditingEntityListener.class)
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class ParkRegion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String name;
    private String formula;
    private BigDecimal price;
    private Integer freeTime;
    private String merchantId;
    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;
    @LastModifiedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateDatetime;
}
