
Page({
  mixins: [require('../../mixin/common')],
  data: {

  },

  onLoad(options) {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/houses/rentings`,
        header: { authorization },
      }).then((houseRentings) => {
        this.setData({
          houseRentings
        });
      });
    });
  },

  onPullDownRefresh() {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/houses/rentings`,
        header: { authorization },
      }).then((houseRentings) => {
        this.setData({
          houseRentings
        });
      });
    });
  },

})