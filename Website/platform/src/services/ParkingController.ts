import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/platform/parkSpaces', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export interface ParkSpace {
  id: string;
  region: string;
  code: string;
  money: number;
  carCode: string;
  state: string;
  status: string;
  remarks: string;
  houseId: string;
  merchantId: string;
}
