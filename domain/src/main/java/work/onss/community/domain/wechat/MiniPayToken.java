package work.onss.community.domain.wechat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MiniPayToken implements Serializable {
    private String timeStamp;
    private String nonceStr;
    @JsonProperty("package")
    private String prepayId;
    private String signType;
    private String paySign;
}