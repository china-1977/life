import OrderProductAPI from '@/services/Store/OrderProductAPI';
import {Avatar, Button, DatePicker, Image} from 'antd';
import {ActionType, ProTable,} from '@ant-design/pro-components';
import {useRef, useState} from 'react';
import {OrderProductWayEnum, OrderStatusEnum} from "@/constants";
import dayjs, {Dayjs} from 'dayjs';

const OrderProductList = () => {
    const columns: any = [
        {title: '序号', dataIndex: 'index', valueType: 'indexBorder', align: 'center',},
        {
            title: '配送方式', dataIndex: 'way', align: 'center', fieldProps: {mode: 'multiple'},
            valueEnum: OrderProductWayEnum.columns,
        },
        {
            title: '状态', align: 'center', dataIndex: 'status', copyable: true, fieldProps: {mode: 'multiple'},
            valueEnum: OrderStatusEnum.columns,
        },
        {title: '金额', dataIndex: 'total', copyable: true},

        {title: '姓名', dataIndex: 'username', copyable: true},
        {title: '电话', dataIndex: 'phone', copyable: true},
        {title: '地址', dataIndex: 'userAddressDetail', copyable: true, search: false},
        {title: '创建时间', dataIndex: 'insertDatetime', copyable: true, align: 'center',},
        {title: '主键', dataIndex: 'id', copyable: true, align: 'center',},
    ];

    const expandColumns: any = [
        {
            title: '图片', dataIndex: 'picture', align: 'center',
            render: (text: string) => (
                <Avatar
                    shape="square"
                    src={
                        <Image src={text}/>
                    }/>
            )
        },
        {
            title: '合作伙伴', dataIndex: 'shortname', align: 'center',
            render: (text: string, record: any) =>
                <Button color="primary" variant="link" href={`#/merchant/${record.merchantId}/products`}>
                    {record.shortname}
                </Button>,
        },
        {title: '名称', dataIndex: 'name',},
        {title: '数量', dataIndex: 'num',},
        {title: '价格', dataIndex: 'price',},
        {title: '单位', dataIndex: 'priceUnit',},
        {title: '合计', dataIndex: 'total',},
        {title: '商品ID', dataIndex: 'productId', copyable: true, align: 'center',},
    ];

    const ref = useRef<ActionType>();
    const [year, setYear] = useState<Dayjs | any>(dayjs());
    return (
        <div>
            <ProTable
                columnsState={{
                    defaultValue: {
                        id: {show: false},
                    }
                }}
                actionRef={ref}
                rowKey={(record) => record.id}
                headerTitle="查询表格"
                columns={columns}
                params={{year: year?.year()}}
                toolbar={{
                    filter: <DatePicker picker={'year'} defaultValue={year} onChange={(date, value) => setYear(date)}/>,
                }}
                request={(params, sorter, filter) => OrderProductAPI.page(params, sorter, filter)}
                rowSelection={{}}
                expandable={{
                    expandedRowRender: (expandDataSource: any) => {
                        return <ProTable<any>
                            columns={expandColumns}
                            dataSource={expandDataSource.details}
                            pagination={false}
                            search={false}
                            options={false}
                        />
                    },
                }}
            />
        </div>
    );
};

export default OrderProductList;
