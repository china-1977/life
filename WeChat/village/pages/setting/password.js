import { domain, wxLogin, wxRequest } from '../../utils/util.js';

Page({
  data: {
  },

  changeLoginAccount(e) {
    wxLogin().then(({ authorization, info }) => {
      wx.showModal({
        title: '提示',
        content: '是否设置账号？',
        success: (res) => {
          if (res.confirm) {
            wxRequest({
              url: `${domain}/account/customers/loginAccount`,
              header: { authorization, cid: info.cid },
              data: { username: e.detail.value.username, password: [e.detail.value.password, e.detail.value.confirmPassword] },
              method: "POST",
            }).then((res) => {
              wx.navigateBack({
                delta: 1
              });
              wx.showModal({
                title: '提示',
                content: '设置成功',
                showCancel: false,
              })
            });
          }
        }
      })
    })
  },
})