package work.onss.community.shop.controller;


import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.MerchantRepository;
import work.onss.community.domain.shop.*;
import work.onss.community.domain.vo.ConfirmScore;
import work.onss.community.service.exception.BusinessException;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Log4j2
@RestController
public class OrderProductController {

    @Autowired
    private OrderProductRepository orderProductRepository;
    @Autowired
    private MerchantRepository storeRepository;
    @Autowired
    private ProductRepository productRepository;

    /**
     * @param id  主键
     * @param cid 用户ID
     * @return 订单信息
     */
    @GetMapping(value = {"orderProducts/{id}"})
    public OrderProduct orderProduct(@PathVariable String id, @RequestHeader(name = "cid") String cid) throws BusinessException {
        return orderProductRepository.findByIdAndUserId(id, cid).orElseThrow(() -> new BusinessException("FAIL", "该订单不存,请联系平台服务商", null));
    }

    /**
     * @param cid      用户ID
     * @param year     订单创建年份
     * @param pageable 默认创建时间排序并分页
     * @return 订单分页
     */
    @GetMapping(value = {"orderProducts"})
    public Page<OrderProduct> all(@RequestHeader(name = "cid") String cid,
                                  @RequestParam(name = "year") Integer year,
                                  @PageableDefault(sort = {"insertDatetime",}, direction = Sort.Direction.DESC) Pageable pageable) {
        QOrderProduct qOrderProduct = QOrderProduct.orderProduct;
        return orderProductRepository.findAll(qOrderProduct.userId.eq(cid).and(qOrderProduct.insertDatetime.year().eq(year)), pageable);
    }

    /**
     * @param cid          用户ID
     * @param confirmScore 确认订单信息
     * @return 订单信息
     */
    @PostMapping(value = {"orderProducts"})
    public Map<String, Object> orderProduct(@RequestHeader(name = "cid") String cid, @Valid @RequestBody ConfirmScore confirmScore) {
        if (confirmScore.getWay().equals(OrderProduct.Way.PSSM)) {
            if (confirmScore.getUserAddressName() == null ||
                    confirmScore.getUserAddress() == null ||
                    confirmScore.getUserAddressPoint() == null ||
                    Strings.isBlank(confirmScore.getUserAddressDetail())) {
                throw new BusinessException("请完善收货信息!");
            }
        } else {
            confirmScore.setUserAddressName(null);
            confirmScore.setUserAddress(null);
            confirmScore.setUserAddressDetail(null);
            confirmScore.setUserAddressPoint(null);
        }

        Merchant village = storeRepository.findById(confirmScore.getVillageId()).orElseThrow(() -> new BusinessException("FAIL", "该店铺不存,请联系客服", confirmScore));
        if (!village.getStatus()) {
            throw new BusinessException("FAIL", "正在准备中,请稍后重试!", null);
        }

        LocalDateTime nowDateTime = LocalDateTime.now();
        LocalTime nowTime = nowDateTime.toLocalTime();
        if (nowTime.isAfter(village.getCloseTime()) & nowTime.isBefore(village.getOpenTime())) {
            throw new BusinessException(MessageFormat.format("营业时间:{0}-{1}", village.getOpenTime(), village.getCloseTime()));
        }


        Map<String, BigDecimal> cart = new TreeMap<>();
        for (String pid_numStr : confirmScore.getCart()) {
            String[] pid_num = pid_numStr.split(":");
            cart.put(pid_num[0], new BigDecimal(pid_num[1]));
        }
        List<Product> products = productRepository.findAllById(cart.keySet());

        Map<String, Merchant> storeMap = new HashMap<>();
        List<OrderProduct> orderProducts = new ArrayList<>(products.size());
        for (Product product : products) {
            Merchant store = storeMap.getOrDefault(product.getMerchantId(), null);
            if (store == null) {
                store = storeRepository.findById(product.getMerchantId()).orElseThrow(() -> new BusinessException("FAIL", "该店铺不存,请联系客服", confirmScore));
                storeMap.put(product.getMerchantId(), store);
            }
            OrderProduct orderProduct = new OrderProduct(cid, confirmScore, village, store, product, cart.get(product.getId()), nowDateTime);
            orderProducts.add(orderProduct);
        }

        orderProductRepository.saveAll(orderProducts);
        Map<String, Object> data = new HashMap<>();
        data.put("orderProduct", orderProducts);
        return data;
    }
}
