package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.time.LocalDate;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class HouseVacant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    private String merchantId;
    @ExcelProperty(value = "房屋ID")
    private String houseId;
    @ExcelProperty(value = "楼号")
    private String floorNumber;
    @ExcelProperty(value = "单元")
    private String unit;
    @ExcelProperty(value = "室")
    private String roomNumber;
    @ExcelProperty(value = "初始水码")
    private Integer initialWater;
    @ExcelProperty(value = "初始电码")
    private Integer initialElectricity;
    @ExcelProperty(value = "结束电码")
    private Integer finalElectricity;
    @ExcelProperty(value = "结束水码")
    private Integer finalWater;

    private String initialWaterImage;
    private String initialElectricityImage;
    private String finalWaterImage;
    private String finalElectricityImage;

    @ExcelProperty(value = "开始日期")
    private LocalDate startDate;
    @ExcelProperty(value = "结束日期")
    private LocalDate endDate;

    @ExcelProperty(value = "备注")
    private String remarks;

}
