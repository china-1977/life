import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/platform/identitys', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export interface Identity {
  id: string;
  merchantId: string;
  floorNumber: string;
  unit: string;
  roomNumber: string;
  area: number;
  areaPrice: number;
  rubbishPrice: number;
  total: number;
  type: string;
  relation: string;
  remarks: string;
}
