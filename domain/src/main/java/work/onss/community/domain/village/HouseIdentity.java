package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.converters.IdentityStatusConverter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@EntityListeners(AuditingEntityListener.class)
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class HouseIdentity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @ExcelProperty(value = "用户ID")
    private String customerId;
    @ExcelProperty(value = "真实姓名")
    @NotBlank(message = "请输入真实姓名")
    private String name;
    @ExcelProperty(value = "身份证")
    @Size(min = 18, max = 18, message = "请输入18位身份证")
    private String idCard;
    @ExcelProperty(value = "手机号")
    @NotBlank(message = "请输入11位手机号")
    private String phone;
    private String idCardFace;
    private String idCardNational;
    private String face;

    @ExcelProperty(value = "房屋ID")
    private String houseId;
    @ExcelProperty(value = "楼号")
    private String floorNumber;
    @ExcelProperty(value = "单元")
    private String unit;
    @ExcelProperty(value = "室")
    private String roomNumber;
    @ExcelProperty(value = "类型")
    private String type;

    @ExcelProperty(value = "关系")
    private String relation;
    @Enumerated(EnumType.STRING)
    @ExcelProperty(value = "状态", converter = IdentityStatusConverter.class)
    private Status status;

    @ExcelProperty(value = "申请时间")
    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;
    @ExcelProperty(value = "更新时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @LastModifiedDate
    private LocalDateTime updateDatetime;


    private String merchantId;

    public void setIdentity(Customer customer, House house, Status status, LocalDateTime insertDatetime) {
        this.customerId = customer.getId();
        this.name = customer.getName();
        this.idCard = customer.getIdCard();
        this.phone = customer.getPhone();
        this.idCardFace = customer.getIdCardFace();
        this.idCardNational = customer.getIdCardNational();
        this.face = customer.getFace();
        this.status = status;
        this.houseId = house.getId();
        this.floorNumber = house.getFloorNumber();
        this.unit = house.getUnit();
        this.roomNumber = house.getRoomNumber();
        this.type = house.getType();
        this.merchantId = house.getMerchantId();
        this.insertDatetime = insertDatetime;
    }

    public void setCustomer(Customer customer) {
        this.name = customer.getName();
        this.idCard = customer.getIdCard();
        this.phone = customer.getPhone();
    }

    @Getter
    public enum Status implements Serializable {
        WAIT("待处理"),
        YES("已通过"),
        NO("已驳回");
        private final String value;

        Status(String value) {
            this.value = value;
        }
    }
}
