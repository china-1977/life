import { checkToken, domain, wxRequest, MerchantCategory } from '../../utils/util.js';

Page({
  data: {
    domain, openTime: '07:30', closeTime: '22:30', addressCode: [], addressValue: [], MerchantCategory
  },

  bindRegionChange: function (e) {
    this.setData({
      ['addressCode']: e.detail.code,
      ['postcode']: e.detail.postcode,
      ['addressValue']: e.detail.value
    });
  },

  chooseLocation: function (e) {
    const { location, addressDetail } = this.data;
    if (location && location.x && location.y) {
      wx.chooseLocation({
        longitude: location.x,
        latitude: location.y,
        name: addressDetail,
        success: (res) => {
          this.setData({
            ['location']: { x: res.longitude, y: res.latitude },
            ['addressDetail']: res.address,
            ['addressName']: res.name
          });
        }
      })
    } else {
      wx.getLocation({
        type: 'gcj02',
        success: (res) => {
          wx.chooseLocation({
            longitude: parseFloat(res.longitude),
            latitude: parseFloat(res.latitude),
            success: (res) => {
              this.setData({
                ['location']: { x: res.longitude, y: res.latitude },
                ['addressDetail']: res.address,
                ['addressName']: res.name
              });
            }
          })
        }
      });
    }
  },

  createMerchant: function (e) {
    const { addressValue, addressCode, location, postcode, category } = this.data;
    const merchant = { ...e.detail.value, addressValue, addressCode, location, postcode, category }
    checkToken().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/account/bindings/insertMerchant`,
        data: merchant,
        method: "POST",
        header: { authorization, cid: info.cid },
      }).then(() => {
        wx.showModal({
          title: '提示',
          content: '注册成功',
          showCancel: false,
          success: (res) => {
            wx.navigateBack({
              delta: 2
            });
          }
        })
      })
    })
  },

  resetForm: function (e) {
    const merchant = this.data.merchant;
    this.setData({
      ...merchant
    })
  },

  timeChange: function (e) {
    const key = e.currentTarget.id;
    const value = e.detail.value;
    this.setData({
      [key]: value
    });
  },

  bindCategoryChange: function (e) {
    this.setData({
      'category': this.data.MerchantCategory.range[e.detail.value].key
    })
  },

  textareaInput: function (e) {

  }
})
