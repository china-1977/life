package work.onss.community.service.exception;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import work.onss.community.domain.account.MerchantRepository;
import work.onss.community.domain.account.QApplication;
import work.onss.community.domain.account.QMerchant;
import work.onss.community.domain.account.QMerchantCustomerApplication;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.dto.account.ApplicationDto;
import work.onss.community.domain.vo.Code;
import work.onss.community.domain.vo.Work;

import java.io.IOException;
import java.util.List;

@Log4j2
@Component
@Order(1)
public class GlobalRequestWebFilter extends HttpFilter {

    @Autowired
    private JPAQueryFactory jpaueryFactory;
    @Autowired
    private MerchantRepository merchantRepository;

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        String path = request.getHeader("path");
        String cid = request.getHeader("cid");
        String mid = request.getHeader("mid");
        log.error(path);
        log.error(cid);
        log.error(mid);

        if (path != null && cid != null && mid != null) {
            log.info("请求地址:{}", path);
            QMerchant qMerchant = QMerchant.merchant;
            BooleanExpression booleanExpression = qMerchant.id.eq(mid).and(qMerchant.customerId.eq(cid));
            boolean exists = merchantRepository.exists(booleanExpression);
            if (!exists) {
                QApplication qApplication = QApplication.application;
                QMerchantCustomerApplication merchantCustomerApplication = QMerchantCustomerApplication.merchantCustomerApplication;
                List<ApplicationDto> applicationCustomerDto = jpaueryFactory.select(Projections.bean(ApplicationDto.class, qApplication, merchantCustomerApplication))
                        .from(qApplication)
                        .leftJoin(merchantCustomerApplication).on(
                                merchantCustomerApplication.applicationId.eq(qApplication.id),
                                merchantCustomerApplication.customerId.eq(cid),
                                merchantCustomerApplication.merchantId.eq(mid)
                        )
                        .where(qApplication.path.eq(path))
                        .fetch();
                for (ApplicationDto customerDto : applicationCustomerDto) {
                    if (customerDto.getMerchantCustomerApplication() == null) {
                        Code fail = Code.FAIL;
                        Work<Object> work = Work.builder().code(fail.name()).message(customerDto.getApplication().getName().concat("权限不足")).build();
                        response.setStatus(HttpStatus.UNAUTHORIZED.value());
                        String s = JacksonUtils.writeValueAsString(work);
                        response.getWriter().print(s);
                        return;
                    }
                }
            }
        }
        chain.doFilter(request, response); // 继续执行后续过滤器或目标资源
    }
}
