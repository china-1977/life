package work.onss.community.domain.account;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanTemplate;
import com.querydsl.core.types.dsl.Expressions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;

public interface MerchantRepository extends JpaRepository<Merchant, String>, JpaSpecificationExecutor<Merchant>, QuerydslPredicateExecutor<Merchant>, QuerydslBinderCustomizer<QMerchant> {

    default void customize(QuerydslBindings bindings, QMerchant qMerchant) {
        bindings.bind(qMerchant.id).withDefaultBinding();
        bindings.bind(qMerchant.shortname).first((path, s) -> Objects.requireNonNull(path).contains(s));
        bindings.bind(qMerchant.description).first((path, s) -> Objects.requireNonNull(path).contains(s));
        bindings.bind(qMerchant.addressName).first((path, s) -> Objects.requireNonNull(path).contains(s));
        bindings.bind(qMerchant.addressDetail).first((path, s) -> Objects.requireNonNull(path).contains(s));
        bindings.bind(qMerchant.insertDatetime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
        bindings.bind(qMerchant.updateDatetime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));

        bindings.bind(qMerchant.addressCode).all(((path, value) -> {
            if (value.isEmpty()) {
                return Optional.empty();
            }
            BooleanBuilder booleanBuilder = new BooleanBuilder();
            value.forEach(v -> {
                BooleanTemplate booleanTemplate = Expressions.booleanTemplate("array_to_string({0},',') like {1}", path, String.join(",", v).concat("%"));
                booleanBuilder.or(booleanTemplate);
            });
            return Optional.of(booleanBuilder);
        }));
    }

}
