package work.onss.community.domain.shop;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.config.PointType;
import work.onss.community.domain.usertype.StringArrayType;
import work.onss.community.domain.vo.ConfirmScore;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@EntityListeners(AuditingEntityListener.class)
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class OrderProduct implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String userId;
    private String userName;
    private String userPhone;
    private String userAddressName;
    private String userAddress;
    private String userAddressDetail;
    @Type(value = PointType.class)
    private Point2D.Double userAddressPoint;

    private String productId;
    private String productName;
    private String productPriceUnit;
    private String productPicture;
    private BigDecimal productPrice;
    private BigDecimal productCount;
    @NumberFormat(pattern = "#.00", style = NumberFormat.Style.CURRENCY)
    private BigDecimal productTotal;

    private String storeId;
    private String storeAddressDetail;
    private String storeAddressName;
    @Type(value = StringArrayType.class)
    private String[] storeAddressValue;
    @Type(value = StringArrayType.class)
    private String[] storeAddressCode;
    private String storePhone;
    @Type(value = PointType.class)
    private Point2D.Double storeLocation;
    private String storePostcode;
    private String storeShortname;
    private String storeUsername;

    private String villageId;
    private String villageAddressDetail;
    private String villageAddressName;
    @Type(value = StringArrayType.class)
    private String[] villageAddressValue;
    @Type(value = StringArrayType.class)
    private String[] villageAddressCode;
    private String villagePhone;
    @Type(value = PointType.class)
    private Point2D.Double villageLocation;
    private String villagePostcode;
    private String villageShortname;
    private String villageUsername;

    @Enumerated(value = EnumType.STRING)
    private Way way;
    @Enumerated(value = EnumType.STRING)
    private Status status;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;

    public OrderProduct(String customerId, ConfirmScore confirmScore, Merchant village, Merchant store, Product product, BigDecimal productCount, LocalDateTime insertDatetime) {
        this.userId = customerId;
        this.userName = confirmScore.getUserName();
        this.userPhone = confirmScore.getUserPhone();
        this.userAddressName = confirmScore.getUserAddressName();
        this.userAddress = confirmScore.getUserAddress();
        this.userAddressDetail = confirmScore.getUserAddressDetail();
        this.userAddressPoint = confirmScore.getUserAddressPoint();

        this.productId = product.getId();
        this.productName = product.getName();
        this.productPriceUnit = product.getPriceUnit();
        this.productPrice = product.getPrice();
        this.productCount = productCount;
        this.productTotal = productCount.multiply(product.getPrice());
        this.productPicture = product.getPictures()[0];

        this.storeId = store.getId();
        this.storeShortname = store.getShortname();
        this.storeUsername = store.getUsername();
        this.storePhone = store.getPhone();
        this.storeAddressName = store.getAddressName();
        this.storeAddressDetail = store.getAddressDetail();
        this.storeLocation = store.getLocation();
        this.storePostcode = store.getPostcode();
        this.storeAddressCode = store.getAddressCode();
        this.storeAddressValue = store.getAddressValue();

        this.villageId = village.getId();
        this.villageShortname = village.getShortname();
        this.villageUsername = village.getUsername();
        this.villagePhone = village.getPhone();
        this.villageAddressName = village.getAddressName();
        this.villageAddressDetail = village.getAddressDetail();
        this.villageLocation = village.getLocation();
        this.villagePostcode = village.getPostcode();
        this.villageAddressCode = village.getAddressCode();
        this.villageAddressValue = village.getAddressValue();

        this.way = confirmScore.getWay();
        this.insertDatetime = insertDatetime;
    }

    /**
     * 待支付
     * 已支付
     */
    @Getter
    @AllArgsConstructor
    public enum Status implements Serializable {
        WAIT_PAY("待支付"),
        FINISH_PAY("已支付");
        private final String message;
    }

    @Getter
    @AllArgsConstructor
    public enum Way implements Serializable {
        DDZX("到店自选"),
        MDBG("门店帮购"),
        PSSM("配送上门");
        private final String message;
    }
}
