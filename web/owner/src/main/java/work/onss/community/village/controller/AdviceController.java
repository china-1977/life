package work.onss.community.village.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.village.Advice;
import work.onss.community.domain.village.AdviceRepository;
import work.onss.community.domain.village.QAdvice;

@Log4j2
@RestController
public class AdviceController {
    @Autowired
    private AdviceRepository adviceRepository;

    @GetMapping(value = {"advices/{id}"})
    public Advice detail(@RequestHeader("cid") String cid, @PathVariable String id) {
        QAdvice qAdvice = QAdvice.advice;
        return adviceRepository.findOne(qAdvice.id.eq(id).and(qAdvice.customerId.eq(cid))).orElse(null);
    }

    @GetMapping(value = {"advices"})
    public Page<Advice> page(@RequestHeader("cid") String cid, @PageableDefault(sort = {"insertDatetime"}, direction = Sort.Direction.DESC) Pageable pageable) {
        QAdvice qAdvice = QAdvice.advice;
        return adviceRepository.findAll(qAdvice.customerId.eq(cid), pageable);
    }

    @PostMapping(value = {"advices"})
    public void save(@RequestHeader("cid") String cid, @RequestBody Advice advice) {
        advice.setCustomerId(cid);
        advice.setId(null);
        advice.setStatus(Advice.Status.WAIT);
        adviceRepository.save(advice);
    }
}