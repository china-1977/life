Page({
  mixins: [require('../../mixin/common')],
  data: {
    index: -1
  },

  onLoad(options) {
    this.init();
  },

  indexChange: function (e) {
    this.wxLogin().then(({ authorization }) => {
      const index = e.detail.value;
      const myHouseRenting = this.data.myHouseRentings[index];
      if (myHouseRenting) {
        this.wxRequest({
          url: `${this.data.domain}/owner/houseRentings/${myHouseRenting.id}/supersede`,
          header: { authorization },
          data: { size: this.data.size }
        }).then((data) => {
          this.setData({
            houseRentings: data.content, number: data.number, index
          });
        });
      }
    });
  },
  init: function () {
    this.wxLogin().then(({ authorization }) => {
      this.wxRequest({
        url: `${this.data.domain}/owner/houses/rentings`,
        header: { authorization },
      }).then((data) => {
        if (data.length > 0) {
          const myHouseRentings = data.map((value) => {
            return { id: value.id, title: `${value.floorNumber},${value.unit},${value.roomNumber}` }
          })
          const index = 0;
          this.setData({
            myHouseRentings,
            index
          });
          this.wxRequest({
            url: `${this.data.domain}/owner/houseRentings/${myHouseRentings[index].id}/supersede`,
            header: { authorization },
            data: { size: this.data.size },
          }).then((data) => {
            this.setData({
              houseRentings: data.content, number: data.number
            });
          });
        } else {
          this.setData({
            myHouseRentings: [],
            index: -1
          });
        }
      });
    });
  },

  onPullDownRefresh() {
    this.init();
    wx.stopPullDownRefresh();
  },

  onReachBottom() {
    this.wxLogin().then(({ authorization }) => {
      const index = this.data.index;
      const myHouseRentings = this.data.myHouseRentings;
      if (index && myHouseRentings) {
        this.wxRequest({
          url: `${this.data.domain}/owner/houseRentings/${myHouseRentings[index].id}/supersede`,
          header: { authorization },
          data: { page: this.data.number + 2, size: this.data.size }
        }).then((data) => {
          if (data.empty) {
            console.log('空');
          } else {
            let houseRentings = this.data.houseRentings;
            houseRentings.push(...data.content)
            this.setData({
              houseRentings, number: data.number
            });
          }
        });
      }
    });
  },
})