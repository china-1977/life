package work.onss.community.config;

import com.aliyun.sdk.service.alidns20150109.AsyncClient;
import com.aliyun.sdk.service.alidns20150109.models.DescribeDomainRecordsRequest;
import com.aliyun.sdk.service.alidns20150109.models.DescribeDomainRecordsResponse;
import com.aliyun.sdk.service.alidns20150109.models.DescribeDomainRecordsResponseBody;
import com.aliyun.sdk.service.alidns20150109.models.UpdateDomainRecordRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ExecutionException;

@Log4j2
@Service
public class Task {

    @Autowired
    private AsyncClient asyncClient;
    @Autowired
    private RestTemplate restTemplate;

    //@Scheduled(cron = "*/1 * * * * ?")
    public void dns() throws ExecutionException, InterruptedException {
        DescribeDomainRecordsRequest describeDomainRecordsRequest = DescribeDomainRecordsRequest.builder().domainName("1977.work").build();
        DescribeDomainRecordsResponse describeDomainRecordsResponse = asyncClient.describeDomainRecords(describeDomainRecordsRequest).get();
        for (DescribeDomainRecordsResponseBody.Record record : describeDomainRecordsResponse.getBody().getDomainRecords().getRecord()) {
            if (record.getRr().equals("@") || record.getRr().equals("*")) {
                String hostAddress = restTemplate.getForObject("http://checkip.amazonaws.com", String.class).trim();
                if (!record.getValue().equals(hostAddress)) {
                    UpdateDomainRecordRequest updateDomainRecordRequest = UpdateDomainRecordRequest.builder()
                            .recordId(record.getRecordId())
                            .rr(record.getRr())
                            .type(record.getType())
                            .value(hostAddress)
                            .build();
                    asyncClient.updateDomainRecord(updateDomainRecordRequest);
                }
            }
        }
    }
}
