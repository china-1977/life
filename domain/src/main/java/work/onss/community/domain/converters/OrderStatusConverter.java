package work.onss.community.domain.converters;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import work.onss.community.domain.village.PayOrder;

public class OrderStatusConverter implements Converter<PayOrder.Status> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return PayOrder.Status.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public PayOrder.Status convertToJavaData(ReadConverterContext<?> context) {
        for (PayOrder.Status status : PayOrder.Status.values()) {
            if (status.getMessage().equals(context.getReadCellData().getStringValue())) {
                return status;
            }
        }
        return null;
    }

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<PayOrder.Status> context) {
        return new WriteCellData<>(context.getValue().getMessage());
    }

}
