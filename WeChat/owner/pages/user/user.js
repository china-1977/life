Page({
  mixins: [require('../../mixin/common')],
  data: {},

  setOpenid: function (e) {
    wx.showModal({
      title: '提示',
      content: '是否绑定微信？',
      success: (res) => {
        if (res.confirm) {
          wx.login({
            success: ({ code }) => {
              this.wxLogin().then(({ authorization }) => {
                this.wxRequest({
                  url: `${this.data.domain}/account/customers/setOpenid`,
                  header: { authorization },
                  method: "POST",
                  data: { appid: this.data.appid, code }
                }).then((content) => {
                  wx.setStorageSync('authorization', content.authorization);
                  wx.setStorageSync('info', content.info);
                  wx.showModal({
                    title: '提示',
                    content: '绑定成功',
                    showCancel: false,
                  })
                });
              });
            },
          })
        }
      }
    })
  }
})