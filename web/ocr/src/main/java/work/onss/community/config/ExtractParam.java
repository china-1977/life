package work.onss.community.config;

import lombok.Data;

/**
 * 识别用到的参数
 */
@Data
public class ExtractParam {

    /**
     * 车牌预测分值阈值，置信度
     */
    private float scoreThreshold = 0.3f;
    /**
     * 车牌重叠iou阈值
     */
    private float iouThreshold = 0.5f;
    /**
     *
     */
    private Integer topK = 5;

}
