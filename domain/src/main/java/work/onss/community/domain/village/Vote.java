package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.usertype.StringArrayType;

import java.io.Serializable;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class Vote implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String merchantId;
    @ExcelProperty(value = "名称")
    private String title;
    @ExcelProperty(value = "描述")
    private String description;
    @Type(value = StringArrayType.class)
    private String[] pictures;
    @Type(value = StringArrayType.class)
    private String[] videos;
    @Type(value = StringArrayType.class)
    private String[] options;
    @ExcelProperty(value = "结果")
    private String result;
    @ExcelProperty(value = "通知时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime noticeDatetime;
    @ExcelProperty(value = "创建时间")
    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;
    @ExcelProperty(value = "更新时间")
    @LastModifiedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateDatetime;
}
