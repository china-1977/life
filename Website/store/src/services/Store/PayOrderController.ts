import { sort } from '@/utils/format';
import { request } from '@@/exports';

export const page = (params: any, sorter: any, filter: any) =>
  request<any>('/store/payOrders', {
    params: {
      ...params,
      sort: sort(sorter),
      ...filter,
    },
  }).then((res: any) => {
    return {
      data: res.content,
      total: res.totalElements,
    };
  });

export const save = async (body?: any) =>
  request<any>('/store/payOrders', {
    method: 'POST',
    data: body,
  });

export const detail = (id: string) => request<any>(`/store/payOrders/${id}`);