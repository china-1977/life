package work.onss.community.domain.village;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.DateTimeFormat;
import work.onss.community.domain.converters.OrderStatusConverter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Log4j2
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@QueryEntity
@ExcelIgnoreUnannotated
@DynamicInsert
@DynamicUpdate
public class OrderWater implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @ExcelProperty(value = "主键")
    private String id;
    @ExcelProperty(value = "编号")
    private String code;
    private String merchantId;
    @ExcelProperty(value = "房屋ID")
    private String houseId;
    @ExcelProperty(value = "楼号")
    private String floorNumber;
    @ExcelProperty(value = "单元")
    private String unit;
    @ExcelProperty(value = "室")
    private String roomNumber;
    @ExcelProperty(value = "类型")
    private String type;

    @ExcelProperty(value = "水表初始值")
    private BigDecimal waterInitial;
    @ExcelProperty(value = "水表当前值")
    private BigDecimal waterCurrent;
    @ExcelProperty(value = "用水量")
    private BigDecimal waterCount;
    @ExcelProperty(value = "水损系数")
    private BigDecimal waterRatio;
    @ExcelProperty(value = "损耗")
    private BigDecimal waterLoss;
    @ExcelProperty(value = "水费")
    private BigDecimal waterTotal;

    @ExcelProperty(value = "备注")
    private String remarks;

    @ExcelProperty(value = "开始日期")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;
    @ExcelProperty(value = "结束日期")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @ExcelProperty(value = "支付状态", converter = OrderStatusConverter.class)
    private PayOrder.Status status;
    @ExcelProperty(value = "支付时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime payDatetime;
    @ExcelProperty(value = "通知时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime noticeDatetime;
    @ExcelProperty(value = "创建时间")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime insertDatetime;

    public OrderWater(House house) {
        this.houseId = house.getId();
        this.floorNumber = house.getFloorNumber();
        this.unit = house.getUnit();
        this.roomNumber = house.getRoomNumber();
        this.type = house.getType();
        this.status = PayOrder.Status.WAIT_CONFIRM;
    }
}
