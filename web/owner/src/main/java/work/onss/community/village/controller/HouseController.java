package work.onss.community.village.controller;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.*;
import work.onss.community.domain.dto.village.HouseCustomerDto;
import work.onss.community.domain.village.*;
import work.onss.community.domain.village.relational.HouseCustomer;
import work.onss.community.domain.village.relational.HouseCustomerRepository;
import work.onss.community.domain.village.relational.QHouseCustomer;
import work.onss.community.domain.vo.Code;
import work.onss.community.service.exception.BusinessException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Log4j2
@RestController
public class HouseController {
    @Autowired
    private ParkSpaceRepository parkSpaceRepository;
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private HouseRepository houseRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private HouseIdentityRepository houseIdentityRepository;
    @Autowired
    private HouseCustomerRepository houseCustomerRepository;
    @Autowired
    private HouseRentingRepository houseRentingRepository;
    @Autowired
    private MerchantRepository merchantRepository;

    @GetMapping(value = {"houses/{id}/detail"})
    public HouseCustomerDto customers(@RequestHeader(name = "cid") String cid, @PathVariable String id) {
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        QCustomer qCustomer = QCustomer.customer;
        House house = houseRepository.findById(id).orElse(null);
        List<HouseCustomer> houseCustomers = jpaQueryFactory.select(
                        Projections.fields(HouseCustomer.class,
                                qCustomer,
                                qHouseCustomer.id,
                                qHouseCustomer.houseId,
                                qHouseCustomer.customerId,
                                qHouseCustomer.merchantId,
                                qHouseCustomer.relation
                        )
                )
                .from(qHouseCustomer)
                .leftJoin(qCustomer).on(qCustomer.id.eq(qHouseCustomer.customerId))
                .where(qHouseCustomer.houseId.eq(id))
                .fetch();
        Optional<HouseCustomer> houseCustomer = houseCustomers.stream().filter(item -> Objects.equals(item.getCustomerId(), cid)).findFirst();
        if (houseCustomer.isPresent()) {
            List<ParkSpace> parkSpaces = parkSpaceRepository.findAllByHouseId(id);
            return new HouseCustomerDto(house, houseCustomers, parkSpaces);
        } else {
            throw new BusinessException("权限不足");
        }
    }

    @GetMapping(value = {"houses"})
    public List<House> list(@RequestHeader(name = "cid") String cid, @QuerydslPredicate(bindings = HouseCustomerRepository.class, root = HouseCustomer.class) Predicate predicate) {
        QHouse qHouse = QHouse.house;
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        return jpaQueryFactory.select(qHouse).from(qHouseCustomer)
                .innerJoin(qHouse).on(qHouse.id.eq(qHouseCustomer.houseId))
                .where(qHouseCustomer.customerId.eq(cid), predicate)
                .fetch();
    }

    @Transactional
    @PostMapping(value = {"houses/binding"})
    public HouseIdentity binding(@RequestHeader(name = "cid") String cid, @RequestBody HouseIdentity houseIdentity) {

        QHouse qHouse = QHouse.house;
        House house = houseRepository.findOne(
                qHouse.merchantId.eq(houseIdentity.getMerchantId())
                        .and(qHouse.floorNumber.eq(houseIdentity.getFloorNumber()))
                        .and(qHouse.unit.eq(houseIdentity.getUnit()))
                        .and(qHouse.roomNumber.eq(houseIdentity.getRoomNumber()))
        ).orElseThrow(() -> new BusinessException("房屋信息不存在"));

        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        HouseIdentity oldHouseIdentity = jpaQueryFactory.select(qHouseIdentity).from(qHouseIdentity)
                .where(qHouseIdentity.houseId.eq(house.getId()), qHouseIdentity.customerId.eq(cid))
                .fetchOne();
        Customer customer = customerRepository.findById(cid).orElseThrow(() -> new BusinessException("个人信息不存在"));
        if (customer.getIdCard() == null || customer.getName() == null || customer.getPhone() == null) {
            Code doNotRealName = Code.DO_NOT_REAL_NAME;
            throw new BusinessException(doNotRealName.name(), doNotRealName.getValue(), null);
        }

        if (oldHouseIdentity == null) {
            houseIdentity.setIdentity(customer, house, HouseIdentity.Status.WAIT, LocalDateTime.now());
            oldHouseIdentity = houseIdentity;
        } else {
            oldHouseIdentity.setIdentity(customer, house, HouseIdentity.Status.WAIT, LocalDateTime.now());
        }

        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        List<HouseCustomer> houseCustomers = jpaQueryFactory.select(qHouseCustomer).from(qHouseCustomer)
                .where(qHouseCustomer.houseId.eq(house.getId()))
                .fetch();
        if (houseCustomers.isEmpty()) {
            HouseCustomer houseCustomer = new HouseCustomer(oldHouseIdentity.getRelation(), oldHouseIdentity.getHouseId(), oldHouseIdentity.getCustomerId(), oldHouseIdentity.getMerchantId());
            houseCustomerRepository.save(houseCustomer);
            oldHouseIdentity.setStatus(HouseIdentity.Status.YES);
        } else {
            for (HouseCustomer houseCustomer : houseCustomers) {
                if (houseCustomer.getCustomerId().equals(cid)) {
                    oldHouseIdentity.setStatus(HouseIdentity.Status.YES);
                    break;
                }
            }
        }

        houseIdentityRepository.save(oldHouseIdentity);
        return oldHouseIdentity;
    }

    @PostMapping(value = {"houses/unbinding"})
    public void unbinding(@RequestHeader(name = "cid") String cid, @RequestBody List<String> ids) {
        houseCustomerRepository.deleteByCustomerIdAndHouseIdIn(cid, ids);
    }

    @GetMapping(value = {"houses/houseIdentity"})
    public List<HouseIdentity> houseIdentity(@RequestHeader(name = "cid") String cid) {
        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        return jpaQueryFactory.select(qHouseIdentity).from(qHouseIdentity)
                .innerJoin(qHouseCustomer).on(qHouseCustomer.houseId.eq(qHouseIdentity.houseId), qHouseCustomer.customerId.eq(cid))
                .where(qHouseIdentity.status.eq(HouseIdentity.Status.WAIT))
                .fetch();
    }

    @Transactional
    @PostMapping(value = {"houseIdentitys/{status}/setStatus"})
    public void binding(@RequestHeader(name = "cid") String cid, @PathVariable HouseIdentity.Status status, @RequestBody List<String> ids) {
        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        BooleanExpression booleanExpression = qHouseIdentity.id.in(ids);
        Iterable<HouseIdentity> identities = houseIdentityRepository.findAll(booleanExpression);

        QHouseCustomer qHouseCustomer = QHouseCustomer.houseCustomer;
        List<String> houseIds = jpaQueryFactory.select(qHouseCustomer.houseId).from(qHouseCustomer)
                .where(qHouseCustomer.customerId.eq(cid))
                .fetch();

        if (status.equals(HouseIdentity.Status.YES)) {
            for (HouseIdentity houseIdentity : identities) {
                if (houseIds.contains(houseIdentity.getHouseId())) {
                    HouseCustomer oldHouseCustomer = houseCustomerRepository.findByMerchantIdAndHouseIdAndCustomerId(houseIdentity.getMerchantId(), houseIdentity.getHouseId(), houseIdentity.getCustomerId()).orElse(null);
                    if (null == oldHouseCustomer) {
                        HouseCustomer houseCustomer = new HouseCustomer(houseIdentity.getRelation(), houseIdentity.getHouseId(), houseIdentity.getCustomerId(), houseIdentity.getMerchantId());
                        houseCustomerRepository.save(houseCustomer);
                    } else {
                        if (!oldHouseCustomer.getRelation().equals(houseIdentity.getRelation())) {
                            oldHouseCustomer.setRelation(houseIdentity.getRelation());
                            houseCustomerRepository.save(oldHouseCustomer);
                        }
                    }
                    houseIdentity.setStatus(status);
                    houseIdentityRepository.save(houseIdentity);
                }
            }
        } else if (status.equals(HouseIdentity.Status.NO)) {
            for (HouseIdentity houseIdentity : identities) {
                if (houseIds.contains(houseIdentity.getHouseId())) {
                    houseCustomerRepository.deleteByMerchantIdAndHouseIdAndCustomerId(houseIdentity.getMerchantId(), houseIdentity.getHouseId(), houseIdentity.getCustomerId());
                    houseIdentity.setStatus(status);
                    houseIdentityRepository.save(houseIdentity);
                }
            }
        }
    }


    @GetMapping(value = {"houses/rentings"})
    public Iterable<HouseRenting> rentings(@RequestHeader("cid") String cid) {
        QHouseRenting qHouseRenting = QHouseRenting.houseRenting;
        return houseRentingRepository.findAll(qHouseRenting.customerId.eq(cid));
    }

    @Transactional
    @PostMapping(value = {"houses/deleteRentings"})
    public void deleteRentings(@RequestHeader("cid") String cid, @RequestBody List<String> ids) {
        QHouseRenting qHouseRenting = QHouseRenting.houseRenting;
        List<String> id = jpaQueryFactory.select(qHouseRenting.id)
                .from(qHouseRenting)
                .where(qHouseRenting.customerId.eq(cid), qHouseRenting.id.in(ids))
                .fetch();
        jpaQueryFactory.update(qHouseRenting)
                .set(qHouseRenting.customerId, Expressions.nullExpression())
                .where(qHouseRenting.id.in(id))
                .execute();
    }

    @PostMapping(value = {"houses/renting"})
    public void create(@RequestHeader("cid") String cid, @RequestBody HouseRenting houseRenting) {
        if (houseRenting.getMode().equals("置换")) {
            if (!StringUtils.hasLength(houseRenting.getDesiredName()) || !StringUtils.hasLength(houseRenting.getDesiredAddress()) || houseRenting.getDesiredPosition() == null) {
                throw new BusinessException("期望位置不能为空");
            }
        }
        House house = houseRepository.findById(houseRenting.getId()).orElseThrow(() -> new BusinessException("房屋信息不存在"));
        houseCustomerRepository.findByMerchantIdAndHouseIdAndCustomerId(house.getMerchantId(), house.getId(), cid).orElseThrow(() -> new BusinessException("权限不足"));
        Merchant merchant = merchantRepository.findById(house.getMerchantId()).orElseThrow(() -> new BusinessException("商户信息不存在"));
        houseRenting.setId(house.getId());
        houseRenting.setLocation(merchant.getLocation());
        houseRenting.setMerchantId(house.getMerchantId());
        houseRenting.setFloorNumber(house.getFloorNumber());
        houseRenting.setUnit(house.getUnit());
        houseRenting.setRoomNumber(house.getRoomNumber());
        houseRenting.setInsertDatetime(LocalDateTime.now());
        houseRenting.setCustomerId(cid);
        houseRentingRepository.save(houseRenting);
    }
}