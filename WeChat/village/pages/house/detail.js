import { wxLogin, domain, wxRequest } from '../../utils/util.js';

Page({

  data: {
    domain
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/houses/${options.id}/detail`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((data) => {
        this.setData({
          ...data
        });
      });
    });
  },
})