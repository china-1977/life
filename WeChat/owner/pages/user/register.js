Page({
  mixins: [require('../../mixin/common')],
  data: {
  },


  register: function (e) {
    wx.showModal({
      title: '提示',
      content: '是否注册？',
      success: (res) => {
        if (res.confirm) {
          this.wxRequest({
            url: `${this.data.domain}/account/register`,
            data: { ...e.detail.value },
            method: "POST",
          }).then((res) => {
            wx.showModal({
              title: '提示',
              content: '注册成功',
              showCancel: false,
              success: (res) => {
                wx.navigateBack({
                  delta: 1
                });
              }
            })
          });
        }
      }
    })
  }
})