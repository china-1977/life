package work.onss.community.domain.dto.village;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import work.onss.community.domain.village.House;
import work.onss.community.domain.village.ParkSpace;
import work.onss.community.domain.village.relational.HouseCustomer;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HouseCustomerDto {

    private House house;
    private List<HouseCustomer> houseCustomers;
    private List<ParkSpace> parkSpaces;
}
