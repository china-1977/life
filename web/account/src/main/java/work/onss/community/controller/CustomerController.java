package work.onss.community.controller;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.hibernate.query.sqm.mutation.internal.UpdateHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.Customer;
import work.onss.community.domain.account.CustomerRepository;
import work.onss.community.domain.account.QCustomer;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.config.SystemConfig;
import work.onss.community.domain.village.HouseIdentity;
import work.onss.community.domain.village.HouseIdentityRepository;
import work.onss.community.domain.village.QHouseIdentity;
import work.onss.community.domain.vo.Code;
import work.onss.community.domain.vo.Info;
import work.onss.community.domain.vo.LoginAccountVo;
import work.onss.community.domain.wechat.Jscode2SessionResult;
import work.onss.community.domain.wechat.WXLogin;
import work.onss.community.service.exception.BusinessException;
import work.onss.community.service.utils.Utils;
import work.onss.community.service.wechat.MiniAppService;
import work.onss.community.service.wechat.WechatMpProperties;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 用户管理
 *
 * @author wangchanghao
 */
@Log4j2
@RestController
public class CustomerController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private WechatMpProperties wechatMpProperties;
    @Autowired
    private MiniAppService miniAppService;
    @Autowired
    private SystemConfig systemConfig;
    @Autowired
    private HouseIdentityRepository houseIdentityRepository;

    /**
     * 用户详情
     *
     * @param cid 用户ID
     * @return 用户信息
     */
    @GetMapping(value = {"customers/detail"})
    public Customer detail(@RequestHeader("cid") String cid) {
        return customerRepository.findById(cid).orElse(null);
    }

    /**
     * 更新用户信息
     *
     * @param id       用户ID
     * @param customer 用户信息
     */
    @Transactional
    @PostMapping(value = {"customers/update"})
    public void update(@RequestHeader("cid") String id, @Validated(value = UpdateHandler.class) @RequestBody Customer customer) {
        QCustomer qCustomer = QCustomer.customer;
        jpaQueryFactory.update(qCustomer)
                .set(qCustomer.name, customer.getName())
                .set(qCustomer.phone, customer.getPhone())
                .set(qCustomer.idCard, customer.getIdCard())
                .set(qCustomer.idCardFace, customer.getIdCardFace())
                .set(qCustomer.idCardNational, customer.getIdCardNational())
                .set(qCustomer.face, customer.getFace())
                .where(qCustomer.id.eq(id)).execute();
        QHouseIdentity qHouseIdentity = QHouseIdentity.houseIdentity;
        BooleanExpression booleanExpression = qHouseIdentity.customerId.eq(id);
        Iterable<HouseIdentity> identities = houseIdentityRepository.findAll(booleanExpression);
        for (HouseIdentity houseIdentity : identities) {
            houseIdentity.setCustomer(customer);
            houseIdentity.setStatus(HouseIdentity.Status.WAIT);
            houseIdentityRepository.save(houseIdentity);
        }
    }

    /**
     * 更新手机号
     *
     * @param id       用户ID
     * @param customer 用户信息
     */
    @Transactional
    @PostMapping(value = {"customers/setPhone"})
    public void setPhone(@RequestHeader("cid") String id, @Validated(value = UpdateHandler.class) @RequestBody Customer customer) {
        QCustomer qCustomer = QCustomer.customer;
        jpaQueryFactory.update(qCustomer)
                .set(qCustomer.phone, customer.getPhone())
                .where(qCustomer.id.eq(id)).execute();
    }

    /**
     * 更新密码
     *
     * @param id             用户ID
     * @param loginAccountVo 登录账号
     */
    @Transactional
    @PostMapping(value = {"customers/loginAccount"})
    public void setAccount(@RequestHeader("cid") String id, @Validated @RequestBody LoginAccountVo loginAccountVo) {
        Set<String> password = loginAccountVo.getPassword();
        if (password.size() != 1) {
            throw new BusinessException("输入的密码不一致");
        }
        QCustomer qCustomer = QCustomer.customer;
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String next = password.iterator().next();
        int length = next.length();
        if (6 > length || 12 < length) {
            throw new BusinessException("请输入6~12位密码");
        }
        String newPassword = bCryptPasswordEncoder.encode(next);
        jpaQueryFactory.update(qCustomer)
                .set(qCustomer.password, newPassword)
                .set(qCustomer.username, loginAccountVo.getUsername())
                .where(qCustomer.id.eq(id))
                .execute();
    }

    /**
     * 更新密码
     *
     * @param id       用户ID
     * @param password 密码
     */
    @Transactional
    @PostMapping(value = {"customers/changePassword"})
    public void changePassword(@RequestHeader("cid") String id, @RequestBody Set<String> password) {
        if (password.size() != 1) {
            throw new BusinessException("输入的密码不一致");
        }
        QCustomer qCustomer = QCustomer.customer;
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String next = password.iterator().next();
        int length = next.length();
        if (6 > length || 12 < length) {
            throw new BusinessException("请输入6~12位密码");
        }
        String newPassword = bCryptPasswordEncoder.encode(next);
        jpaQueryFactory.update(qCustomer)
                .set(qCustomer.password, newPassword)
                .where(qCustomer.id.eq(id))
                .execute();
    }

    /**
     * 根据【微信用户标识】查询用户
     *
     * @param openid 微信用户标识
     * @return 用户集合
     */
    @GetMapping(value = {"customers/getAllByOpenid"})
    public Iterable<Customer> getAllByOpenid(@RequestHeader("openid") String openid) {
        QCustomer qCustomer = QCustomer.customer;
        return customerRepository.findAll(qCustomer.openid.eq(openid));
    }

    /**
     * @param id     主体ID
     * @param openid 微信标识
     * @return 密钥
     */
    @PostMapping(value = {"customers/{id}/binding"})
    public Map<String, Object> bind(@PathVariable String id, @RequestHeader(name = "openid") String openid) {
        Customer customer = customerRepository.findById(id).orElseThrow(() -> new BusinessException(Code.FAIL.name(), "账号不存在，请联系客服", MessageFormat.format("营业员ID:{0},OPENID:{1}", id, openid)));
        if (id.equals(openid) || openid.equals(customer.getOpenid())) {
            Map<String, Object> result = new HashMap<>();
            LocalDateTime now = LocalDateTime.now();
            Info info = new Info(customer.getId(), customer.getName(), now);
            String subject = JacksonUtils.writeValueAsString(info);
            String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.toInstant(ZoneOffset.ofHours(8)), subject, customer.getId(), "");
            result.put("authorization", authorization);
            result.put("info", info);
            return result;
        } else {
            throw new BusinessException("微信用户已发生变更,无法登录");
        }
    }

    /**
     * 绑定微信登录
     *
     * @param wxLogin 微信登陆信息
     */
    @Transactional
    @PostMapping(value = {"customers/setOpenid"})
    public Map<String, Object> setOpenid(@RequestHeader("cid") String cid, @RequestBody WXLogin wxLogin) {
        WechatMpProperties.AppConfig appConfig = wechatMpProperties.getAppConfigs().get(wxLogin.getAppid());
        String jscode2Session = miniAppService.jscode2session(wxLogin.getAppid(), appConfig.getSecret(), wxLogin.getCode(), "authorization_code");
        Jscode2SessionResult jscode2SessionResult = JacksonUtils.readValue(jscode2Session, Jscode2SessionResult.class);

        Customer customer = customerRepository.findById(cid).orElseThrow(() -> new BusinessException("个人信息不存在"));
        customer.setOpenid(jscode2SessionResult.getOpenid());

        customerRepository.save(customer);

        LocalDateTime now = LocalDateTime.now();
        Info info = new Info(customer.getId(), customer.getName(), now);
        String subject = JacksonUtils.writeValueAsString(info);
        String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.toInstant(ZoneOffset.ofHours(8)), subject, customer.getId(), "");
        Map<String, Object> result = new HashMap<>();
        result.put("authorization", authorization);
        result.put("info", info);
        return result;
    }

    /**
     * 解除微信登录
     *
     * @param id 用户ID
     */
    @Transactional
    @PostMapping(value = {"customers/removeOpenid"})
    public void removeOpenid(@RequestHeader("cid") String id) {
        QCustomer qCustomer = QCustomer.customer;
        jpaQueryFactory.update(qCustomer)
                .setNull(qCustomer.openid)
                .where(qCustomer.id.eq(id)).execute();
    }
}

