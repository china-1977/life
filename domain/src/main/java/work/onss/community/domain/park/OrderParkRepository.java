package work.onss.community.domain.park;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public interface OrderParkRepository extends JpaRepository<OrderPark, String>, JpaSpecificationExecutor<OrderPark>, QuerydslPredicateExecutor<OrderPark>, QuerydslBinderCustomizer<QOrderPark> {

    default void customize(QuerydslBindings bindings, QOrderPark qOrderPark) {
        bindings.bind(qOrderPark.id).withDefaultBinding();
        bindings.bind(qOrderPark.inDatetime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));

        bindings.bind(qOrderPark.outDatetime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));

        bindings.bind(qOrderPark.insertDatetime).all(((path, value) -> {
            Iterator<? extends LocalDateTime> iterator = value.iterator();
            if (value.size() == 1) {
                return Optional.ofNullable(path.eq(iterator.next()));
            }
            return Optional.ofNullable(path.between(iterator.next(), iterator.next()));
        }));
    }

    List<OrderPark> findAllByCarNumberAndParkStatus(String carNumber, OrderPark.Status status);

    Optional<OrderPark> findAllByMerchantIdAndParkRegionIdAndCarNumberAndParkStatus(String merchantId, String parkRegionId, String carNumber, OrderPark.Status status);

    Optional<OrderPark> findFirstByMerchantIdAndParkRegionIdAndCarNumberOrderByInDatetimeAsc(String merchantId, String parkRegionId, String carNumber);
}