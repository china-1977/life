import { wxLogin, domain, wxRequest } from '../../utils/util.js';

Page({
  data: {
    domain,
    currentID: -1,
    checked: false,
    productStatus: { 'false': '上架', 'true': '下架' },
    icons: {
      'false': 'clear',
      'true': 'success'
    }
  },

  onShow: function () {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/store/products/all`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((content) => {
        this.setData({ ...content }
        )
      })
    })
  },


  scrollInto: function (e) {
    this.setData({
      intoIndex: e.currentTarget.id
    })
  },

  bindShow: function (e) {
    this.setData({
      currentID: e.target.id
    });
    setTimeout(() => {
      this.setData({
        currentID: -1
      })
    }, 3000)
  },

  bindButtonTap: function (e) {
    const product = e.currentTarget.dataset.product;
    this.updateStatus([product.id], !product.status)
  },

  checkboxChange: function (e) {
    const key = e.currentTarget.id;
    this.setData({
      [key]: e.detail.value
    });
  },

  actions: function (e) {
    const status = e.detail.target.id;
    const ids = e.detail.value.ids;
    if (ids.length > 0) {
      switch (status) {
        case '上架':
          this.updateStatus(ids, true)
          break;
        case '下架':
          this.updateStatus(ids, false)
          break;
        case '删除':
          this.deleteProduct(ids)
          break;
        default:
          break;
      }
      this.setData({ checked: false })
    }

  },

  updateStatus: function (ids, status) {
    wx.showModal({
      title: '警示',
      content: '请仔细审核商品信息！',
      success: (res) => {
        if (res.confirm) {
          wxLogin().then(({ authorization, info }) => {
            wxRequest({
              url: `${domain}/store/products/updateStatus?status=${status}`,
              method: 'POST',
              header: { authorization, cid: info.cid, mid: info.mid },
              data: ids
            }).then(() => {
              wxRequest({
                url: `${domain}/store/products/all`,
                header: { authorization, cid: info.cid, mid: info.mid },
              }).then((content) => {
                this.setData({ ...content }
                )
              })
            })
          })
        }
      }
    })
  },

  deleteProduct: function (ids) {
    wx.showModal({
      title: '警示',
      content: '是否删除商品?',
      success: (res) => {
        if (res.confirm) {
          wxLogin().then(({ authorization, info }) => {
            wxRequest({
              url: `${domain}/store/products`,
              method: 'DELETE',
              header: { authorization, cid: info.cid, mid: info.mid },
              data: ids
            }).then(() => {
              wxRequest({
                url: `${domain}/store/products/all`,
                header: { authorization, cid: info.cid, mid: info.mid },
              }).then((content) => {
                this.setData({ ...content }
                )
              })
            })
          })
        }
      }
    })
  },

  add: function () {
    wx.navigateTo({
      url: '/pages/product/create',
    })
  }
})