package work.onss.community.domain.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.transaction.annotation.Transactional;

public interface MerchantCustomerRepository extends JpaRepository<MerchantCustomer, String>, JpaSpecificationExecutor<MerchantCustomer>, QuerydslPredicateExecutor<MerchantCustomer>, QuerydslBinderCustomizer<QMerchantCustomer> {

    default void customize(QuerydslBindings bindings, QMerchantCustomer qVillageCustomer) {
        bindings.bind(qVillageCustomer.id).withDefaultBinding();

    }

    @Modifying
    @Transactional
    void deleteByMerchantIdAndCustomerId(String vid, String cid);
}
