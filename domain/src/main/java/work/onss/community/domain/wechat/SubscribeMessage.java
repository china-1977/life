package work.onss.community.domain.wechat;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubscribeMessage implements Serializable {
    private String touser;
    @JsonProperty(value = "template_id")
    private String templateId;
    private String page;
    @JsonProperty(value = "miniprogram_state")
    private String miniprogramState;
    private String lang;
    private Map<String, Value> data;

    public static SubscribeMessage getSubscribeMessage1(
            String id, String cid, String authorization,
            String openid, String thing1, String thing2, String time19, String amount4, String phrase15) {
        String page = String.format("/pages/oderHouse/detail?id=%s&cid=%s&authorization&=%s", id, cid, authorization);
        Map<String, Value> data = Map.of(
                "thing1", Value.builder().value(thing1).build(),
                "thing2", Value.builder().value(thing2).build(),
                "time19", Value.builder().value(time19).build(),
                "amount4", Value.builder().value(amount4).build(),
                "phrase15", Value.builder().value(phrase15).build()
        );

        return SubscribeMessage.builder()
                .touser(openid)
                .templateId("r9KTb1oSteXOf6gVbdM5yWFM9mI1akuKsbytu7hCdwk")
                .page(page)
                .miniprogramState("developer")
                .lang("zh_CN")
                .data(data).build();
    }

    /**
     * 访客到达通知
     */
    public static SubscribeMessage getSubscribeMessage2(String id, String cid, String authorization, String openid,
                                                        String thing6, String phone_number7, String thing8, String car_number5, String date2) {
        String page = String.format("/pages/oderHouse/detail?id=%s&cid=%s&authorization&=%s", id, cid, authorization);
        Map<String, Value> data = Map.of(
                "thing6", Value.builder().value(thing6).build(),
                "phone_number7", Value.builder().value(phone_number7).build(),
                "thing8", Value.builder().value(thing8).build(),
                "car_number5", Value.builder().value(car_number5).build(),
                "date2", Value.builder().value(date2).build()
        );

        return SubscribeMessage.builder()
                .touser(openid)
                .templateId("wicpfM3EPq-l2WBQxdf0M4VNjlHPlhqa-3DYm77W_as")
                .page(page)
                .miniprogramState("developer")
                .lang("zh_CN")
                .data(data).build();
    }

    /**
     * 新报修通知
     */
    public static SubscribeMessage getSubscribeMessage3(
            String id, String cid, String authorization, String openid,
            String thing4, String time5, String thing2, String thing3) {
        String page = String.format("/pages/oderHouse/detail?id=%s&cid=%s&authorization&=%s", id, cid, authorization);
        Map<String, Value> data = Map.of(
                "thing4", Value.builder().value(thing4).build(),
                "time5", Value.builder().value(time5).build(),
                "thing2", Value.builder().value(thing2).build(),
                "thing3", Value.builder().value(thing3).build()
        );

        return SubscribeMessage.builder()
                .touser(openid)
                .templateId("Nb4aoJSxjUZwGJ0Rovr0e8pivfVhESpGu0IE5HdukX4")
                .page(page)
                .miniprogramState("developer")
                .lang("zh_CN")
                .data(data).build();
    }

    /**
     * 收到新意见提醒
     */
    public static SubscribeMessage getSubscribeMessage4(String id, String cid, String authorization, String openid,
                                                        Integer number2, String thing1) {
        String page = String.format("/pages/oderHouse/detail?id=%s&cid=%s&authorization&=%s", id, cid, authorization);
        Map<String, Value> data = Map.of(
                "number2", Value.builder().value(number2).build(),
                "thing1", Value.builder().value(thing1).build()
        );

        return SubscribeMessage.builder()
                .touser(openid)
                .templateId("ziU9pw1LWMj7CnzkMbX-9kfiCo51SARvZK7jwoFonKM")
                .page(page)
                .miniprogramState("developer")
                .lang("zh_CN")
                .data(data).build();
    }

    /**
     * 投票通知
     */
    public static SubscribeMessage getSubscribeMessage5(String id, String cid, String authorization, String openid,
                                                        String thing2, String thing6) {
        String page = String.format("/pages/vote/detail?id=%s&cid=%s&authorization&=%s", id, cid, authorization);
        Map<String, Value> data = Map.of(
                "thing2", Value.builder().value(thing2).build(),
                "thing6", Value.builder().value(thing6).build()
        );

        return SubscribeMessage.builder()
                .touser(openid)
                .templateId("WzWSRNwCGipnuVLNli9RlOu0l19GGvmGGNXAL14sQ5M")
                .page(page)
                .miniprogramState("developer")
                .lang("zh_CN")
                .data(data).build();
    }

    /**
     * 水费通知
     */
    public static SubscribeMessage getSubscribeMessage6(
            String id, String cid, String authorization, String openid,

            String thing1, String thing2, String thing3, String amount1, String thing4) {
        String page = String.format("/pages/oderWater/detail?id=%s&cid=%s&authorization&=%s", id, cid, authorization);

        Map<String, Value> data = Map.of(
                "thing1", Value.builder().value(thing1).build(),
                "thing2", Value.builder().value(thing2).build(),
                "thing3", Value.builder().value(thing3).build(),
                "amount1", Value.builder().value(amount1).build(),
                "thing4", Value.builder().value(thing4).build()
        );

        return SubscribeMessage.builder()
                .touser(openid)
                .templateId("yXR2wtLQji0qgEyHtM7HcFjFpjWDwvh2nwjp0EEFfdg")
                .page(page)
                .miniprogramState("developer")
                .lang("zh_CN")
                .data(data).build();
    }

    /**
     * 小区公告通知
     */
    public static SubscribeMessage getSubscribeMessage7(
            String id, String cid, String authorization, String openid,

            String thing1, String thing4, String time6, String thing2) {
        String page = String.format("/pages/notice/detail?id=%s&cid=%s&authorization&=%s", id, cid, authorization);
        Map<String, Value> data = Map.of(
                "thing1", Value.builder().value(thing1).build(),
                "thing4", Value.builder().value(thing4).build(),
                "time6", Value.builder().value(time6).build(),
                "thing2", Value.builder().value(thing2).build()
        );

        return SubscribeMessage.builder()
                .touser(openid)
                .templateId("Q6iyOhMQvBqaLW1kGVFz1eJbp37Xx3Ud40UYP03TIBw")
                .page(page)
                .miniprogramState("developer")
                .lang("zh_CN")
                .data(data).build();
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Data
    public static class Value implements Serializable {
        private Object value;
    }
}
