package work.onss.community.domain.pay;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@NoArgsConstructor
@Data
public class LePosBarcodePay {
    private String service = "upload_authcode"; // 接口名 upload_authcode（此为固定值）
    private String t0; // t0交易标志 默认为0，0：d1交易 1：d0交易
    private String sign_type; // sign_type 接口签名类型::MD5/SM3。支持MD5和国密SM3算法；(不上送默认为使用MD5算法)
    private String auth_code; // 付款码
    private String merchant_id; // 商户号 由乐刷分配
    private String third_order_id; // 商户订单号 商户内部订单号，可以包含字母：确保同一个商户下唯一
    private String amount; // 订单金额
    private String royalty; // 分账标识 交易分账标识0：普通交易1：分账（用于分账的交易需传该参数）2：交易主体分账（延迟结算）3：预授权 注：如果未传该字段默认为0，按不分账处理
    private String appid; // 商户公众账号ID 商户或者商户所属渠道号主体的公众号appid,由微信统一分配，仅对微信交易生效，传送前请确定商户和该appid已经建立绑定关系
    private String notify_url; // 通知地址 接收乐刷通知的URL，需做UrlEncode 处理，需要绝对路径，确保乐刷能正确访问，若不需要回调请忽略
    private String client_ip; // 用户IP地址 商户发起交易的IP地址
    private String body; // 商品描述 商品描述,不能包含回车换行等特殊字符
    private String shop_no; // 商户门店编号 只能是汉字、英文字母、数字
    private String pos_no; // 商户终端编号 只能是汉字、英文字母、数字 注意：银联类型交易时，只能 8位：数字字母  不支持 特殊字符
    private String attach; // 附加数据 支付成功原样返回；注意：只能是汉字、英文字母、数字
    private String limit_pay; // 指定支付方式 1：禁止使用信用卡；0或者不填：不限制
    private String goods_tag; // 订单优惠标记 订单优惠标记，透传给微信
    private String goods_detail; // 商品详情 按微信单品优惠券格式传递，透传给微信;银联扫码交易时，透传给银联的“收款方附加数据”acqAddnData参数，当前银联的单品营销及扫码点餐均使用到该参数，具体参数格式及说明见：银联收款方附加数据。
    private String hb_fq_num; // 花呗分期数 支付宝花呗分期数，支持3、6、12期
    private String nonce_str; // 随机字符串
    private String sign; // 签名 MD5签名结果
    private String extend_business_params; // 业务拓展参数 该字段 为JSON格式的字符串，当前可透传支付宝的参数见下表
    private String device_info; // 设备编号 针对微信、支付宝有效，透传至银联。微信：数字、字母、下划线格式;支付宝：目前支持非0开头的数字
    private String store_id; // 门店编号	针对微信、支付宝有效，透传至银联。    微信：数字、字母、下划线格式    支付宝：目前支持非0开头的数字
    private String ass_merchant_id; // 通道商户号 非必填。可使用指定的微信、支付宝子商户号进行交易，一个乐刷商户号有多个通道商户号情况下可用。注：对应乐刷商户号大于200条时不能传该参数
    private String pnrins_id_cd; // 银联服务商标识 银联扫码交易时，透传给银联，对应银联的 pnrInsIdCd参数
    private String gps_info; // gps信息 校验GPS信息格式， 如：-128.12,23.1
    private String bs_info; // 基站信息 校验基站信息格式, 格式说明： -- 1. 格式为：xxx,xxx,xxx,xxx  如：460,1,12,23454 -- 2. xxx均为整数，长度不超过11位
    private String scene_info; // 场景信息 该字段用于上报场景信息，仅针对微信交易有效，目前支持上报实际门店信息。该字段 为JSON格式的字符串，对象格式为{"store_info":{"id": "门店 ID","name ": "名称","area_code": "编码","address": "地址"}} ，字段详细说明参考下文
    private String need_receipt; // true false 电子发票功能 电子发票功能。仅针对微信交易有效，需要和微信公众平台的发票功能联合使用
    private String terminal_info; // 交易终端信息  {"longitude":"-121.48352","latitude":"+31.221345","network_license":"P3100","device_type":"01","device_id":"54MPDz39","serial_num":"5","app_version":"v1.1.1","device_ip":"252.1.1.25"}


    @JacksonXmlRootElement(localName = "leshua")
    @Data
    public static class Result {
        private String sign; // 签名
        private String status; // 订单状态
        private String leshua_order_id; // 乐刷订单号
        private String pay_way; // 支付类型
        private String pay_time; // 支付完成时间
        private String bank_type; // 银行类型
        private String openid; // 用户openid
        private String out_transaction_id; // 微信、支付宝等订单号
        private String sub_openid; // 用户子标识
        private String attach; // 附加数据
        private String trade_type; // 交易类型
        private String channel_order_id; // 通道订单号
        private String channel_datetime; // 通道订单时间
        private String coupon; // 支付宝红包金额
        private String settlement_amount; // 应结算金额
        private String discount_amount; // 折扣优惠金额
        private String promotion_detail; // 优惠详情
        private String active_flag; // 活动标志
        private String buyer_pay_amount; // 买家实付金额
        private String cost_time; // 请求耗时
        private String channel_flag; // 通道标记
        private String channel_code; // 银联/网联状态码
    }
}