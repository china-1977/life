package work.onss.community.controller;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import work.onss.community.domain.account.*;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.config.SystemConfig;
import work.onss.community.domain.vo.Info;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.exception.BusinessException;
import work.onss.community.service.utils.Utils;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 主体授权
 *
 * @author wangchanghao
 */
@Log4j2
@RestController
public class BindingController {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;
    @Autowired
    private MerchantRepository merchantRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private MerchantCustomerRepository merchantCustomerRepository;
    @Autowired
    private SystemConfig systemConfig;
    @Autowired
    private QuerydslService querydslService;

    /**
     * @param cid 营业员ID
     * @return 主体分页
     */
    @GetMapping(value = {"bindings/getMerchants"})
    public List<Merchant> merchants(@RequestHeader(name = "cid") String cid, @RequestParam List<Merchant.Category> categories) {
        QMerchant qMerchant = QMerchant.merchant;
        return querydslService.get(cid, categories, qMerchant,
                qMerchant.id,
                qMerchant.shortname,
                qMerchant.description,
                qMerchant.subMchId,
                qMerchant.addressName,
                qMerchant.addressDetail,
                qMerchant.trademark,
                qMerchant.pictures,
                qMerchant.username,
                qMerchant.phone,
                qMerchant.openTime,
                qMerchant.closeTime,
                qMerchant.status,
                qMerchant.category);
    }

    /**
     * @param id  主体ID
     * @param cid 营业员ID
     * @return 密钥及主体信息
     */
    @PostMapping(value = {"bindings/{id}"})
    public Map<String, Object> bind(@PathVariable String id, @RequestHeader(name = "cid") String cid) {
        Customer customer = customerRepository.findById(cid).orElseThrow(() -> new BusinessException("FAIL", "账号不存在，请联系客服", MessageFormat.format("营业员ID:{0}", cid)));
        QMerchantCustomer qMerchantCustomer = QMerchantCustomer.merchantCustomer;
        QMerchant qMerchant = QMerchant.merchant;
        Merchant merchant = jpaQueryFactory.select(qMerchant).from(qMerchant)
                .innerJoin(qMerchantCustomer).on(qMerchant.id.eq(qMerchantCustomer.merchantId))
                .where(qMerchant.id.eq(id), qMerchantCustomer.customerId.eq(cid))
                .fetchOne();
        if (merchant == null) {
            throw new BusinessException("FAIL", "主体不存在，请联系客服!", MessageFormat.format("主体ID:{0}", id));
        }

        Map<String, Object> result = new HashMap<>();
        LocalDateTime now = LocalDateTime.now();
        Info info = new Info(customer.getId(), customer.getName(), merchant.getId(), merchant.getShortname(), now);
        String subject = JacksonUtils.writeValueAsString(info);
        String authorization = Utils.authorization(systemConfig.getSecret(), "1977", now.toInstant(ZoneOffset.ofHours(8)), subject, customer.getId(), merchant.getCategory().name());

        result.put("authorization", authorization);
        result.put("info", info);
        return result;
    }

    /**
     * @param cid      营业员ID
     * @param merchant 新增主体详情
     * @return 新主体详情
     */
    @Transactional
    @PostMapping(value = {"bindings/insertMerchant"})
    public Merchant insert(@RequestHeader(name = "cid") String cid, @Validated @RequestBody Merchant merchant) {
        merchant.setCustomerId(cid);
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode(merchant.getPassword());
        merchant.setPassword(encode);
        merchantRepository.save(merchant);
        MerchantCustomer merchantCustomer = new MerchantCustomer(cid, merchant.getId());
        merchantCustomerRepository.save(merchantCustomer);
//        String text = "https://1977.work/merchant/#/pages/index?storeId=".concat(merchant.getId().toString());
//        Utils.qrCode(text, 600, 600, systemConfig.getFilePath(), "store", String.valueOf(merchant.getId()), "other", "qrCode.png");
        return merchant;
    }

    /**
     * @param id  主体ID
     * @param cid 营业员ID
     */
    @Transactional
    @DeleteMapping(value = {"bindings/{id}/deleteMerchant"})
    public void deleteMerchant(@PathVariable String id, @RequestHeader(name = "cid") String cid) {
        Merchant merchant = merchantRepository.findById(id).orElseThrow(() -> new BusinessException("主体不存在，请联系平台客服"));
        if (merchant.getStatus()) {
            throw new BusinessException("请先停止营业");
        }
        if (!merchant.getCustomerId().equals(cid)) {
            throw new BusinessException("权限不足");
        }
        merchantRepository.deleteById(id);
    }
}

