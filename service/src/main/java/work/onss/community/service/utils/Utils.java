package work.onss.community.service.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitArray;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;
import work.onss.community.domain.config.JacksonUtils;
import work.onss.community.domain.wechat.WxPayToken;
import work.onss.community.service.exception.BusinessException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.Instant;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;

@Log4j2
public class Utils {

    public static String getWxPayToken(WxPayToken wxPayToken) {
        String token = null;
        try {
            token = JacksonUtils.writeValueAsString(wxPayToken);
            token = token.substring(1, token.length() - 1);
            token = "WECHATPAY2-SHA256-RSA2048".concat(" ").concat(token.replaceAll("\":", "=").replaceAll(",\"", ",").replaceFirst("\"", ""));
        } catch (Exception e) {
            log.error(e);
        }
        return token;
    }

    public static String sign(String privateKeyStr, String... value) {
        String sign = null;
        try {
            PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKeyStr));
            KeyFactory keyf = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = keyf.generatePrivate(priPKCS8);
            String message = String.join("\n", value).concat("\n");
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initSign(privateKey);
            signature.update(message.getBytes());
            sign = Base64.encodeBase64String(signature.sign());
        } catch (Exception e) {
            log.error(e);
        }
        return sign;
    }

    public static void uploadFile(MultipartFile file, String filePath) throws BusinessException, IOException {
        Path path = Paths.get(filePath);
        Path parent = path.getParent();
        if (!Files.exists(parent) && !parent.toFile().mkdirs()) {
            throw new BusinessException("上传失败!");
        }
        if (!Files.exists(path)) {
            file.transferTo(path);
        }
    }

    public static Path uploadFile(MultipartFile file, String dir, String... more) throws BusinessException, IOException {
        Path path = Paths.get(dir, more);
        Path parent = path.getParent();
        if (!Files.exists(parent) && !parent.toFile().mkdirs()) {
            throw new BusinessException("上传失败!");
        }
        if (!Files.exists(path)) {
            file.transferTo(path);
        }
        return path;
    }

    public static String authorization(String secret, String issuer, Instant instant, String subject, String jwtId, String... audience) {
        Algorithm algorithm = Algorithm.HMAC256(secret);
        JWTCreator.Builder jwt = JWT.create()
                .withIssuer(issuer)
                .withAudience(audience)
                .withNotBefore(Date.from(instant))
                .withIssuedAt(Date.from(instant))
                .withExpiresAt(Date.from(instant.plusSeconds(18000)));

        return jwt
                .withSubject(subject)
                .withJWTId(jwtId)
                .sign(algorithm);
    }

    public static Map<String, String> OPENTM207940503(String first, String remark, Integer count, String... keyword) {
        Map<String, String> data = new HashMap<>(count);
        data.put("first", first);
        for (int i = 0; i < keyword.length; i++) {
            data.put("keyword".concat(String.valueOf(i + 1)), keyword[i]);
        }
        data.put("remark", remark);
        return data;
    }

    /**
     * @param text 内容
     * @param w    宽
     * @param h    高
     * @param dir  文件父级路径
     * @param more 子路径
     * @throws IOException     IO异常
     * @throws WriterException Writer异常
     */
    public static void qrCode(String text, Integer w, Integer h, String dir, String... more) throws IOException, WriterException {
        Path path = Paths.get(dir, more);
        Path parent = path.getParent();
        if (!Files.exists(parent) && !parent.toFile().mkdirs()) {
            throw new BusinessException("上传失败!");
        }
        if (!Files.exists(path)) {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix matrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, w, h);
            int width = matrix.getWidth();
            int height = matrix.getHeight();
            BufferedImage image = new BufferedImage(width, height, 12);
            int[] rowPixels = new int[width];
            BitArray row = new BitArray(width);
            for (int y = 0; y < height; ++y) {
                row = matrix.getRow(y, row);
                for (int x = 0; x < width; ++x) {
                    rowPixels[x] = row.get(x) ? -16777216 : -1;
                }
                image.setRGB(0, y, width, 1, rowPixels, 0, width);
            }
            ImageIO.write(image, "PNG", Files.newOutputStream(path));
        }
    }

    public static String decrypt(String sessionKey, String encryptedData, String ivStr) {
        try {
            AlgorithmParameters params = AlgorithmParameters.getInstance("AES");
            params.init(new IvParameterSpec(Base64.decodeBase64(ivStr)));

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(Base64.decodeBase64(sessionKey), "AES"), params);

            return new String(decode(cipher.doFinal(Base64.decodeBase64(encryptedData))), UTF_8);
        } catch (Exception e) {
            throw new BusinessException("AES解密失败！");
        }
    }

    public static String decryptToString(String associatedData, String nonce, String ciphertext, String apiV3Key) throws GeneralSecurityException {
        try {
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            SecretKeySpec key = new SecretKeySpec(apiV3Key.getBytes(), "AES");
            GCMParameterSpec spec = new GCMParameterSpec(128, nonce.getBytes());
            cipher.init(2, key, spec);
            cipher.updateAAD(associatedData.getBytes());
            return new String(cipher.doFinal(java.util.Base64.getDecoder().decode(ciphertext)), StandardCharsets.UTF_8);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException var7) {
            throw new IllegalStateException(var7);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException var8) {
            throw new IllegalArgumentException(var8);
        }
    }

    public static byte[] decode(byte[] decrypted) {
        int pad = decrypted[decrypted.length - 1];
        if (pad < 1 || pad > 32) {
            pad = 0;
        }
        return Arrays.copyOfRange(decrypted, 0, decrypted.length - pad);
    }

    public static String generateSign(Map<String, String> params, String apiKey) {
        // 1. 排序参数
        Map<String, String> sortedParams = new TreeMap<>(params);
        StringBuilder signStr = new StringBuilder();
        for (Map.Entry<String, String> entry : sortedParams.entrySet()) {
            signStr.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        // 2. 拼接API密钥
        signStr.append("key=").append(apiKey);
        // 3. 生成签名
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digest = md.digest(signStr.toString().getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b));
            }
            return sb.toString().toUpperCase();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return null;
    }
}
