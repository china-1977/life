package work.onss.community.domain.usertype;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.jdbc.PgArray;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;

public class StringArrayType implements UserType<String[]> {

    @Override
    public int getSqlType() {
        return Types.ARRAY;
    }

    @Override
    public Class<String[]> returnedClass() {
        return String[].class;
    }

    @Override
    public boolean equals(String[] x, String[] y) {
        if (x == y) {
            return true;
        }
        if (x == null || y == null) {
            return false;
        }
        return Arrays.equals(x, y);
    }

    @Override
    public int hashCode(String[] object) {
        return Arrays.hashCode(object);
    }

    @Override
    public String[] nullSafeGet(ResultSet resultSet, int i, SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws SQLException {
        Object object = resultSet.getObject(i);
        if (object instanceof PgArray pgArray) {
            object = pgArray.getArray();
        }
        return object == null ? null : (String[]) object;
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, String[] strings, int i, SharedSessionContractImplementor sharedSessionContractImplementor) throws SQLException {
        preparedStatement.setObject(i, strings);
    }

    @Override
    public String[] deepCopy(String[] strings) {
        return strings;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(String[] strings) {
        return strings;
    }

    @Override
    public String[] assemble(Serializable serializable, Object o) {
        return (String[]) serializable;
    }
}
