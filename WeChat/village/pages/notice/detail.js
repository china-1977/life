import { wxLogin, domain, wxRequest } from '../../utils/util.js';

Page({
  data: {
    domain,
    windowWidth: wx.getSystemInfoSync().windowWidth,
  },

  onLoad(options) {
    wxLogin().then(({ authorization, info }) => {
      wxRequest({
        url: `${domain}/village/notices/${options.id}`,
        header: { authorization, cid: info.cid, mid: info.mid },
      }).then((notice) => {
        this.setData({
          notice
        });
      });
    });
  },
})