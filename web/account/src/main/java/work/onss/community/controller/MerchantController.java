package work.onss.community.controller;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import work.onss.community.domain.account.Merchant;
import work.onss.community.domain.account.QMerchant;
import work.onss.community.domain.config.SystemConfig;
import work.onss.community.domain.dto.village.MerchantDistance;
import work.onss.community.domain.village.House;
import work.onss.community.domain.village.QHouse;
import work.onss.community.domain.vo.DataTree;
import work.onss.community.service.QuerydslService;
import work.onss.community.service.exception.BusinessException;
import work.onss.community.service.utils.Utils;

import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 主体管理
 *
 * @author wangchanghao
 */
@Log4j2
@RestController
public class MerchantController {
    @Autowired
    private QuerydslService querydslService;
    @Autowired
    private SystemConfig systemConfig;
    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    /**
     * @param id  主体ID
     * @param cid 营业员ID
     * @return 主体详情
     */
    @GetMapping(value = {"merchants/{id}"}, name = "主体详情")
    public Merchant detail(@PathVariable String id, @RequestHeader(name = "cid") String cid) {
        return querydslService.get(id, cid);
    }

    /**
     * @param id 主体ID
     * @return 树形房屋信息
     */
    @GetMapping(value = {"merchants/{id}/allHouse"})
    public List<DataTree> allHouse(@PathVariable String id) {
        QHouse qHouse = QHouse.house;
        List<House> houses = jpaQueryFactory.select(qHouse)
                .from(qHouse)
                .where(qHouse.merchantId.eq(id))
                .orderBy(
                        qHouse.floorNumber.asc(),
                        qHouse.unit.asc(),
                        qHouse.roomNumber.asc()
                ).fetch();
        if (!houses.isEmpty()) {
            Map<String, List<House>> floorNumberHose = houses.stream().collect(Collectors.groupingBy(House::getFloorNumber));

            List<DataTree> fs = new ArrayList<>(floorNumberHose.size() + 1);
            DataTree fs0 = new DataTree();
            fs0.setTitle("全部");
            fs0.setValue("");
            fs0.setChildren(new ArrayList<>(0));
            fs.add(fs0);
            floorNumberHose.forEach((floorNumber, value) -> {
                // 当前楼
                DataTree dataTree = new DataTree();
                dataTree.setValue(floorNumber);
                dataTree.setTitle(floorNumber);
                Map<String, List<House>> unitHouses = value.stream().collect(Collectors.groupingBy(House::getUnit));
                List<DataTree> us = new ArrayList<>(unitHouses.size() + 1);
                DataTree us0 = new DataTree();
                us0.setTitle("全部");
                us0.setValue(floorNumber);
                us0.setChildren(new ArrayList<>(0));
                us.add(us0);
                unitHouses.forEach((unit, rooms) -> {
                    // 当前单元
                    DataTree dataTree1 = new DataTree();
                    String value1 = String.join(",", floorNumber, unit);
                    dataTree1.setValue(value1);
                    dataTree1.setTitle(unit);
                    List<DataTree> rs = new ArrayList<>(rooms.size() + 1);
                    DataTree rs0 = new DataTree();
                    rs0.setTitle("全部");
                    rs0.setValue(value1);
                    rs0.setChildren(new ArrayList<>(0));
                    rs.add(rs0);
                    rooms.forEach((room -> {
                        // 当前室
                        DataTree dataTree2 = new DataTree();
                        String value2 = String.join(",", value1, room.getRoomNumber());
                        dataTree2.setValue(value2);
                        dataTree2.setTitle(room.getRoomNumber());
                        rs.add(dataTree2);
                    }));
                    dataTree1.setChildren(rs);
                    us.add(dataTree1);
                });
                dataTree.setChildren(us);
                fs.add(dataTree);
            });
            return fs;
        } else {
            return null;
        }
    }

    @GetMapping(value = {"merchants/{x}-{y}/near"})
    public List<MerchantDistance> near(@PathVariable Double x,
                                       @PathVariable Double y,
                                       @RequestParam(defaultValue = "100000") Double r,
                                       @RequestParam(required = false) String keyword,
                                       @RequestParam(required = false) List<Merchant.Category> categories,
                                       @PageableDefault Pageable pageable) {
        QMerchant qMerchant = QMerchant.merchant;
        SimpleExpression<Double> distance = Expressions.template(Double.class, " st_distance(geometry({0}), geometry(ST_MakePoint({1}, {2}))) ", qMerchant.location, x, y);
        Predicate withinDistance = Expressions.predicate(Ops.LOE, distance, Expressions.constant(r));
        BooleanBuilder keywordCondition = new BooleanBuilder();
        if (keyword != null && !keyword.trim().isEmpty()) {
            keywordCondition.or(qMerchant.shortname.containsIgnoreCase(keyword));
            keywordCondition.or(qMerchant.description.containsIgnoreCase(keyword));
        }

        return jpaQueryFactory.select(Projections.constructor(MerchantDistance.class, qMerchant, distance.as("distance"))).from(qMerchant)
                .where(withinDistance, keywordCondition, qMerchant.category.in(categories))
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetch();
    }

    /**
     * @param id     主体ID
     * @param cid    营业员ID
     * @param status 更新主体状态
     */
    @PutMapping(value = {"merchants/{id}/updateStatus"}, name = "主体状态")
    public void updateStatus(@PathVariable(name = "id") String id, @RequestHeader(name = "cid") String cid, @RequestHeader(name = "status") Boolean status) throws BusinessException {
        Merchant merchant = querydslService.get(id, cid);
        if (merchant == null) {
            throw new BusinessException("FAIL", "主体不存在，请联系客服", MessageFormat.format("主体ID:{0},状态:{1}", id, status));
        }
        if (merchant.getSubMchId() == null) {
            throw new BusinessException("请联系客服，免费加入特约主体");
        } else {
            querydslService.setMerchant(id, status);
        }
    }

    /**
     * @param id       主体ID
     * @param cid      营业员ID
     * @param merchant 更新主体详情
     */
    @PutMapping(value = {"merchants/{id}"}, name = "主体更新")
    public void update(@PathVariable(name = "id") String id, @RequestHeader(name = "cid") String cid, @Validated @RequestBody Merchant merchant) {
        Merchant oldMerchant = querydslService.get(id, cid);
        if (oldMerchant == null) {
            throw new BusinessException("FAIL", "主体不存在，请联系客服", merchant);
        }
        querydslService.setMerchant(id, merchant);
    }

    /**
     * @param file 文件
     * @param mid  主体ID
     * @return 文件存储路径
     * @throws Exception 文件上传失败异常
     */
    @PostMapping(value = "merchants/uploadPicture", name = "主体图片")
    public String uploadPicture(@RequestHeader(name = "mid") String mid, @RequestParam(value = "file") MultipartFile file) throws Exception {
        String subtype = MediaType.valueOf(Objects.requireNonNull(file.getContentType())).getSubtype();
        String fileName = DigestUtils.sha256Hex(file.getBytes()).concat(".").concat(subtype);
        Path path = Utils.uploadFile(file, systemConfig.getFilePath(), "merchant", String.valueOf(mid), "other", fileName);
        return StringUtils.cleanPath(path.toString());
    }
}

