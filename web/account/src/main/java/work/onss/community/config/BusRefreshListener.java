package work.onss.community.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.bus.event.RefreshRemoteApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class BusRefreshListener {

    @EventListener(RefreshRemoteApplicationEvent.class)
    public void handleRefreshEvent(RefreshRemoteApplicationEvent event) {

    }
}
